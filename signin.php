<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"])."/includes/phpHeader.php";
$pagetextarray = $pagetextObject->GetPageText('signin.php',$_SESSION['language']);
////////////////////////////////////////////////////////////////////

$navbtn = isset($_GET['btn'])?$_GET['btn']:"res";
$msg = "";


if($_SERVER['REQUEST_METHOD'] === 'GET' && !empty($_GET['confirm_hash'])) {
    # TODO: confirm email
    $userDataObj = new UserData();
    $userData =  $userDataObj->GetUserDataByToken($_GET['confirm_hash']);
    if(!empty($userData)) {
        # no activation data found

        $now = date_create('now');
        if(date_create($userData['validate_token_expiration'])->format('U') > $now->format('U')) {
            # valid
            $saveData['id'] = $userData['id'];
            $saveData['emailVerified'] = 1;
            $saveData['validate_token'] = null;
            $saveData['validate_token_expiration'] = null;
            $userDataObj->Update($saveData);

            # todo: display message about email confirmed
        }
    }

    unset($userData);
}

if(isset($_POST['submit']))
{

	if(strlen($_POST['email']) > 0)
	{
		$userObj = new User();
		$user = $userObj->Validate($_POST['email'],$_POST['password']);

		/////////////////////////////////////////////////////////////////////
        $sql = "SELECT TIME_FORMAT(TIMEDIFF(NOW(), UTC_TIMESTAMP), '%H:%i') AS offset";
        $data = $userObj->runQuery($sql);
        $local_offest = $data[0]['offset'];
        if($local_offest[0] != '-'){
            $local_offest = "+".$local_offest;
        }
        $_SESSION['local_timezone_offset'] = $local_offest;
        /////////////////////////////////////////////////////////////////////

		$startDate = date('Y-m-d');
		if(isset($user['user_id']) && isValidUserId($user['user_id']))
		{

            $resultVars = openSession($user['user_id']);

            echo '<script>location.href="' . $GLOBALS['coreURL'] . '/quickstart_results.php?cycle_id='. $_SESSION['current_cycle_id'] .'"</script>';

		}
		else
		{
            closeSession();
			$msg = "UN/PW not found";
		}

	}
}
///////////////////////////////////////////////////////////////////

?>
<?php include_once $GLOBALS['corePath']."/includes/htmlHeader.php"; ?>

<script>
$(function(){
		$("input[name='email']").focus();

});
</script>


<section id="loginPage">
     <form action="" method="post" autocomplete="off">
        <div class="loginBox">
            <?php if(strlen($msg)>0){
                echo "<div align='center' style='color:darkred'>".$msg."</div>";
            } ?>
            <h1><?php echo $pagetextarray['greeting']; ?></h1>
            <fieldset>
                <input type="text" name="email" placeholder="<?php echo $pagetextarray['email']; ?>"/>
            </fieldset>
            <fieldset>
                <input type="password" name="password" placeholder="<?php echo $pagetextarray['password']; ?>"/>
            </fieldset>
            <fieldset>
                <button type="submit" name="submit" class="btn2 btnSubmit"><?php echo $buttontextarray['btnSignin']; ?></button>
            </fieldset>
            <div align="center">
                <a href="<?php echo $GLOBALS['coreURL']; ?>/forgotpassword.php"><?php echo $pagetextarray['forgotpassword']; ?></a>
            </div>
        </div>
     </form>
</section>

<?php include_once $GLOBALS['corePath']."/includes/htmlFooter.php"; ?>

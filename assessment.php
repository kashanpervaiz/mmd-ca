<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"])."/includes/phpHeader.php";

$assessmentQuestions = $pagetextObject->GetPageText('assessment_questions2',$_SESSION['language']);
$formfieldsarray = $pagetextObject->GetPageText('formfields',$_SESSION['language']);

$assessmentsObj = new UserAssessments();
$KitDataObj = new KitData();

$newAssessment = strpos($_SERVER['REQUEST_URI'],"/new") !== false ? true : false;

$kitsCount = $KitDataObj->getKitCountForUserId($_SESSION['userid']);
$assessmentCount = $assessmentsObj->getCount( $_SESSION['userid']);
if(($kitsCount > $assessmentCount) or $newAssessment){
    $assessments_array = $assessmentsObj->GetLatestUserData($_SESSION['current_cycle_id']);
    if(count($assessments_array) == 0){
        $current_assessment_id = $assessmentsObj->Add(array('cycle_id' => $_SESSION['current_cycle_id']));
        $_SESSION['current_assessment_id'] = $current_assessment_id;
    }
}

$assessments_array = $assessmentsObj->GetData($_SESSION['current_assessment_id']);
$assessmentData = count($assessments_array)>0? $assessments_array[0] : [];


$frequency = array('never','rarely','sometimes','often','always');
$symptoms = !empty($assessmentData['symptomjson'])  ? json_decode($assessmentData['symptomjson'],true): array("1"=>"","2"=>"","3"=>"") ;
?>

<?php include_once $GLOBALS['corePath']."/includes/htmlHeader.php" ?>
<script>

</script>
<section id="assessment">
	<div class="inner-container">
		
		<?php include_once $GLOBALS['corePath']."/includes/includes.registerKitStatus.php"; ?>

		<?php include_once $GLOBALS['corePath']."/includes/includes.assessment.php"; ?>
	</div>
</section>


<?php include_once $GLOBALS['corePath']."/includes/htmlFooter.php"; ?>

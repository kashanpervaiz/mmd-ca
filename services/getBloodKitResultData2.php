<?php
//error_reporting(E_ERROR | E_PARSE);
//error_reporting(E_ALL); //debugging only
//ini_set('display_errors', True);
include_once realpath($_SERVER["DOCUMENT_ROOT"])."/includes/phpHeader.php";
//$_POST['testkitid'] = 2080;
//$_POST['currDate'] = '2017-11-09';
//$_POST['lang'] = 'en';
//$_POST['userid'] = '6';
//$_POST['age'] = '43';
//$_POST['gender'] = 'M';

if(!isset($_SESSION['preferredUnits'])){$_SESSION['preferredUnits'] = 'us';}

$pageTextObj = new PageText();
$kitVendorValueMapObj = new KitVendorValueMap();
$kitDisplayDataObj = new KitDisplayData();
$dataRangesObj = new DataRanges();
$kitDataObj = new KitData();

$kitVendorValueMapObj->GetMap('mmd');

$pageText = $pageTextObj->GetPageText('VendorNameMap',$_SESSION['language']);
$unitsText = $pageTextObj->GetPageText('units',$_SESSION['language']);
$conversionFactors = $pageTextObj->GetPageText('conversionFactors',$_SESSION['language']);
$conversionFormulae = $pageTextObj->GetPageText('conversionFormulae',$_SESSION['language']);

$blockTitlesText = $pageTextObj->GetPageText('ResultBlockTitles',$_SESSION['language']);
$rangeText = $pageTextObj->GetPageText('rangeText',$_SESSION['language']);
$periodTitles = $pageTextObj->GetPageText('periodTitles',$_SESSION['language']); 
$kitData = $kitDataObj->GetData($_POST['testkitid']);

	if(isset($kitData[0])){
		$kitData = $kitData[0];
	} else {
		$kitData = ['barcode' => "", 'timestamp_registration' => "", 'timestamp_ready' => ""];
	}

$data = array();
$error = "";
$bloodData = "";
$kitId = "";
$kitResults = ""; //added for testing
$diurnals = [];

	if( isset($_POST['testkitid'])  )
	{
		$obj = new KitResults();
		$bloodData = $obj->GetTestKitResult($_POST['testkitid']);

		$kitId = $_POST['testkitid'];
	}
	else if(isset($_POST['userid']))
	{
		$hbkObj = new KitData();
		$testkitData = $hbkObj->GetUserTestKitsUpToDate($_POST['userid'],$_POST['currDate'])	;
		$hbkResultObj = new KitResults();
		$bloodData = $hbkResultObj->GetTestKitResult($testkitData[0]['id']);
		$kitId = $testkitData[0]['id'];
	}

		$kitResults = json_decode($bloodData['data'],true);
		if(isset($kitResults['result']['RAW']['Tests']))
		{
			foreach($kitResults['result']['RAW']['Tests'] as $test => $testValue){
				$pos = strripos($testValue['Code'], "diurnal");
				if($pos!==false){
					$parts = explode("-",$testValue['Code']);
					if (!in_array($parts[0],$diurnals)) {
						$diurnals[] = $parts[0] ;
					}
				}
			}

			foreach($kitResults['result']['RAW']['Tests'] as $test => $testValue)
			{
				if(!is_numeric($testValue['Result'])){continue;}

				$parts = explode("-",trim($testValue['Code']));
				$end = end($parts);
				$period = is_numeric($end) ? $end : "1";
        
				$pos = strripos($testValue['Code'], "diurnal");
				$periodTitle = ($pos === false)? $periodTitles['period-1'] : $periodTitles['diurnal-'.$period];

				$vendorkey = trim($testValue['Code']);
				if(count($parts) > 1 && is_numeric($end)){
            		$vendorkey = str_replace("-".$end,"",$vendorkey);
				}

				$wbKey = $kitVendorValueMapObj->LookupVendorKey(trim($vendorkey));
				$rangeData = $dataRangesObj->GetRangeData($wbKey,$period,$testValue['Result'],$_POST['age'],$_POST['gender'],$_POST['menses_state']);
				
				$topEnd = min($testValue['Result'],$rangeData['range_max']);
				$sliderVal = $rangeData['slider_min']+ (100*(($testValue['Result'] - $rangeData['range_min'])/($rangeData['range_max'] - $rangeData['range_min'])));
				$sliderVal = max($sliderVal,$rangeData['slider_min']);

				$rangeMin = setValue($wbKey, $rangeData['min'], 'us', $_SESSION['preferredUnits'],$conversionFormulae);
				$rangeMax = setValue($wbKey, $rangeData['max'], 'us', $_SESSION['preferredUnits'],$conversionFormulae);
				$result = setValue($wbKey, $testValue['Result'], 'us', $_SESSION['preferredUnits'],$conversionFormulae);
				$descriptionKey = $rangeData['descriptionKey'];	
				$desirable = $rangeData['desireable'];	
				
				$linedata = array();
				$linedata['Test'] = $pageText[$bloodData['vendor']."_".$vendorkey];
                $linedata['Period'] = $period;
				$linedata['Result'] = $result;
				$linedata['UnitOfMeasure'] = $unitsText[$wbKey."-".$_SESSION['preferredUnits']];

				$linedata['Range'] = genRangeText($rangeMin,$rangeMax,$descriptionKey,$desirable,$rangeText);
				$linedata['ResultLevel'] = intval($rangeData['indicatorKey']);
				$linedata['SliderClass'] = $rangeData['slider_class'];
				$linedata['SliderValue'] = $sliderVal;
        		$linedata['periodTitle'] = $periodTitle;
        		$linedata['diurnal'] = in_array($parts[0],$diurnals);
        

				$linedata['error'] = "";
				
				$displayData = $kitDisplayDataObj->GetDataForKey(trim($testValue['Code']));
				$linedata = array_merge($linedata,$displayData);
				
				$linedata = array_merge($linedata,$rangeData);

				
				$data[] = $linedata;			
			}	
		}		
	
	if(count($data)>0)
	{	
		// Obtain a list of columns
		foreach ($data as $key => $row) {
		    $sortOrder[$key]  = $row['sortOrder'];
		    $groupKey[$key] = $row['groupKey'];
		    $groupSort[$key] = $row['groupSortOrder'];
		}
		array_multisort($groupSort, SORT_ASC,$groupKey, SORT_ASC, $sortOrder, SORT_ASC, $data);

		//Create groupings AFTER the are sorted
		$group = "";
		$grouped_data = [];
		foreach($data as $data_row){
			if(!isset($grouped_data[$data_row['groupKey']])){
				$grouped_data[$data_row['groupKey']] = [];
			}
			$grouped_data[$data_row['groupKey']][] = $data_row;
		}
	}
	else
	{
		$error = "No Data";
		$grouped_data = [];
	}
	
	$blockTitles = array();
	$blockTitles['neurotransmitters-metabolites'] = $blockTitlesText['neurotransmitters_block_title'];
	$blockTitles['androgens-estrogens'] = $blockTitlesText['androgens_estrogens_block_title'];
	$blockTitles['adrenal-hormones'] = $blockTitlesText['adrenal_hormones_block_title'];
	$blockTitles['lipids'] = $blockTitlesText['lipids_block_title'];
	$blockTitles['bloodsugar'] = $blockTitlesText['bloodsugar_block_title'];
	$blockTitles['liverkidney'] = $blockTitlesText['liverkidney_block_title'];
	$blockTitles['liverkidneyhealth'] = $blockTitlesText['liverkidneyhealth_block_title'];
	$blockTitles['stress'] = $blockTitlesText['stress_block_title'];
	$blockTitles['prostate'] = $blockTitlesText['prostate_block_title'];
	$blockTitles['thyroid'] = $blockTitlesText['thyroid_block_title'];
	$blockTitles['hearthealth'] = $blockTitlesText['hearthealth_block_title'];
	$blockTitles['bonesheart'] = $blockTitlesText['bonesheart_block_title'];
	$blockTitles['hormones'] = $blockTitlesText['hormones_block_title'];
	$blockTitles['ungrouped'] = $blockTitlesText['ungrouped_block_title'];
	
	$imbalance_data = generateImbalances($data);
	
	$result = array();
	$result['resultData'] = $grouped_data;//$data;
	$result['kitData'] = ['barcode' => $kitData['barcode'], 'timestamp_registration' => $kitData['timestamp_registration'], 'timestamp_ready' => $kitData['timestamp_ready']];
	$result['diurnals'] = $diurnals;
	$result['imbalances'] = $imbalance_data['imbalances'];
	$result['imbalances_name_list'] = $imbalance_data['imbalances_name_list'];
	$result['titles'] = $blockTitles;
	$result['error'] = $error;
	$result['kitResults'] = $kitResults;
	echo json_encode($result);
	
	
//	usort($data, "cmp");
//	function cmp($a, $b)
//	{
//	    if ($a['sortOrder'] == $b['sortOrder']) {
//	        return 0;
//	    }
//	    return ($a['sortOrder'] < $b['sortOrder']) ? -1 : 1;
//	}
	
function generateImbalances($data){
    $imbalances = array();
    $imbalances_name_list = array();
    $imbalancesLevel = array();
	$imbalanceTypeList = array('-2'=>'low','-1'=>'low normal','0'=>'normal','1'=>'high normal','2'=>'high');
	foreach($data as $line){

		if( isset($imbalances[$line['moreInfoKey']] )){  // we have a diurnal
            if( $imbalances[$line['moreInfoKey']] != "mixed" )
			{
				if( ($imbalancesLevel[$line['moreInfoKey']] < 0 && $line['ResultLevel'] > 0)  || ($imbalancesLevel[$line['moreInfoKey']] > 0 && $line['ResultLevel'] < 0)){
                    $imbalances[$line['moreInfoKey']] = "mixed";

				} else if ($imbalancesLevel[$line['moreInfoKey']] < 0 && $line['ResultLevel'] < $imbalancesLevel[$line['moreInfoKey']]){
                    $imbalances[$line['moreInfoKey']] = $imbalanceTypeList[$line['ResultLevel']];
                    $imbalancesLevel[$line['moreInfoKey']] = $line['ResultLevel'];
				} else if($imbalancesLevel[$line['moreInfoKey']] > 0 && $line['ResultLevel'] > $imbalancesLevel[$line['moreInfoKey']]){
                    $imbalances[$line['moreInfoKey']] = $imbalanceTypeList[$line['ResultLevel']];
                    $imbalancesLevel[$line['moreInfoKey']] = $line['ResultLevel'];
				}
			}

		} else {
			if($line['ResultLevel'] != 0){
				$imbalances[$line['moreInfoKey']] = $imbalanceTypeList[$line['ResultLevel']];
				$imbalancesLevel[$line['moreInfoKey']] = $line['ResultLevel'];
				if(!in_array($line['Test'],$imbalances_name_list)){
					$imbalances_name_list[]  = $line['Test'];
				}
			}
		}

	}

	return array("imbalances"=>$imbalances,"imbalances_name_list"=>$imbalances_name_list);

}
	
function setValue($wbKey, $sourceValue, $sourceUnits, $desinationUnits,$conversionFormulae) //$dataUnits are the units from the data source i.e. ZRT                                                                                                                           y .
{
	$sourceValue = str_replace("<","",trim($sourceValue));
	$sourceValue = str_replace(">","",$sourceValue);
	$result = $sourceValue;
	
	if($sourceUnits != $desinationUnits)
	{
		$formula = "return ".$conversionFormulae[$wbKey."-".$sourceUnits."-to-".$desinationUnits].";";
		$result  = eval($formula);
		$result = round(10*$result)/10;			
		
//		$factor =   floatval($conversionFactors[$wbKey."-".$sourceUnits."-to-".$desinationUnits]);
//		if($wbKey == "a1c")
//		{
//			$constant =   floatval($conversionFactors[$wbKey."-".$sourceUnits."-to-".$desinationUnits."-constant"]);
//			$direction = $sourceUnits."-to-".$desinationUnits;
//			if($direction = "us-to-si")
//			{
//				//From % to mmol/mol
//				//($sourceValue-2.15)*10.929
//				$result = round(10*(($sourceValue-$constant)*$factor))/10;				
//			}
//			else
//			{
//				//From mmol/mol to %
//				//($sourceValue/10.929)+2.15
//				$result = round(10*(($sourceValue/$factor)-$constant))/10;				
//			}			
//		}
//		else
//		{
//			$result = round(10*$sourceValue*$factor)/10;
//		}
	}
	return $result;
}	


function genRangeText($min,$max,$descriptionKey,$desirable,$rangeText)
{
	$resultText = "Error";
	if(!isset($rangeText[$descriptionKey]))
	{
		$resultText = "Error: ".$descriptionKey." doesn't exist";
	}
	elseif(stripos($descriptionKey,"below") !== false)
	{
		$resultText = $rangeText[$descriptionKey]." ".$max;
	}
	elseif(stripos($descriptionKey,"above") !== false)
	{
		$resultText = $rangeText[$descriptionKey]." ".$min;
	}
	elseif(stripos($descriptionKey,"between") !== false)
	{
		$resultText = $rangeText[$descriptionKey]." ".$min." ".$rangeText['and']." ".$max;
	}
	else
	{
		$resultText = $rangeText[$descriptionKey];
	}
	
	
//	$switchKey = $descriptionKey."-".$desirable;
//	switch($switchKey)
//	{
//		case 'unhealthyAbove-down':
//		$resultText = $rangeText['unhealthyAbove']." ".$min ;
//		break;
//		
//		case 'moderateBetween-down':
//		$resultText = $rangeText['moderateBetween']." ".$min." ".$rangeText['and']." ".$max;
//		break;
//		
//		case 'healthyBetween-down':
//		$resultText = $rangeText['healthyBetween']." ".$min." ".$rangeText['and']." ".$max;
//		break;
//					
//		case 'idealBetween-down':
//		$resultText = $rangeText['idealBetween']." ".$min." ".$rangeText['and']." ".$max;
//		break;	
//			
//		case 'healthyBelow-down':
//		$resultText = $rangeText['healthyBelow']." ".$max;
//		break;
//			
//		case 'idealBelow-down':
//		$resultText = $rangeText['idealBelow']." ".$max;
//		break;
//		
//		case 'unhealthyBelow-down':
//		$resultText = $rangeText['unhealthyBelow']." ".$max;
//		break;
//		
//		//upside down like HDL
//		
//		
//		case 'consideredLowBelow-up':
//		$resultText = $rangeText['consideredLowBelow']." ".$max ;
//		break;
//		
//		case 'idealAbove-up':
//		$resultText = $rangeText['idealAbove']." ".$min ;
//		break;
//				
//		case 'healthyAbove-up':
//		$resultText = $rangeText['healthyAbove']." ".$min ;
//		break;
//		
//		case 'moderateBetween-up':
//		$resultText = $rangeText['moderateBetween']." ".$min." ".$rangeText['and']." ".$max;
//		break;
//		
//		case 'healthyBetween-up':
//		$resultText = $rangeText['healthyBetween']." ".$min." ".$rangeText['and']." ".$max;
//		break;	
//		
//		case 'idealBetween-up':
//		$resultText = $rangeText['idealBetween']." ".$min." ".$rangeText['and']." ".$max;
//		break;	
//			
//		case 'unhealthyBelow-up':
//		$resultText = $rangeText['unhealthyBelow']." ".$max;
//		break;		
//		
//	}
	return $resultText;
}

//function genRangeText($result,$min,$max,$descriptionKey,$rangeText)
//{
//	$resultText = "Error";
//	$inrange = inRange($result,$min,$max) ;
//	if($descriptionKey == "1")
//	{
//		switch($inrange)
//		{
//			case -1:
//			$resultText = $rangeText['idealBelow']." ".$min;
//			break;
//			
//			case 0:
//			$resultText = $rangeText['moderateBetween']." ".$min." ".$rangeText['and']." ".$max;
//			break;
//			
//			case 1:
//			$resultText = $rangeText['atRiskAbove']." ".$max ;
//			break;
//			
//		}
//	}
//	else
//	{
//		switch($inrange)
//		{
//			case -1:
//			$resultText = $rangeText['atRiskBelow']." ".$min;
//			break;
//			
//			case 0:
//			$resultText = $rangeText['moderateBetween']." ".$min." ".$rangeText['and']." ".$max;
//			break;
//			
//			case 1:
//			$resultText = $rangeText['idealAbove']." ".$max ;
//			break;
//			
//		}
//	}	
//	return $resultText;
//}




?>

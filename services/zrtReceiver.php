<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);


include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/class_ZRTReferenceCodes.php";

//Example notification call:
//http://myzrt.zrtlab.com/api/report?ID={REFERENCECODE}

//Example response
//JSON example for expected response:
//{"success": "true|false", "message": "string"}

$result = array("success"=>"false", "message"=>"No Reference Code");
if(isset($_GET['id']) and strlen($_GET['id']) > 0)
{
	$refCodes = new ZRTReferenceCodes();
	$data = array("referencecode"=> $_GET['id']);
	$newId = $refCodes->Add($data);
	
	if( intval($newId)>0 )
	{
		$result = array("success"=>"true", "message"=>"");
	}
	else
	{
		$result = array("success"=>"false", "message"=>"Insertion Error");
	}
	
}
	$jsonData = json_encode($result);
	echo $jsonData;

?>
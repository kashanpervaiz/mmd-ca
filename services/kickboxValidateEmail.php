<?php
//Documentation : https://kickbox.io/app/api

if(isset($_POST['email']) and strlen($_POST['email'])>0)
{
	$email = $_POST['email'];
	$apiKey = "f2e3c0055ddb20705aa69b478ea09bc5712c9a9f899abe67cfdce9a56e584a38";
	$emailAPIUrl = "https://api.kickbox.io/v2/verify?email=".$email."&apikey=".$apiKey;
	
	// Get cURL resource
	$curl = curl_init();
	// Set some options - we are passing in a useragent too here
	curl_setopt_array($curl, array(
	    CURLOPT_RETURNTRANSFER => 1,
	    CURLOPT_URL => $emailAPIUrl
	));
	// Send the request & save response to $resp
	$resp = curl_exec($curl);
	$data = json_decode($resp,true);
	// Close request to clear up some resources
	curl_close($curl);
	
	echo $data['result'] == 'deliverable' ? "1" : "0";
}
else
{
	echo "0";
}


//example useage:
//
//  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
//  <script>
//		$(document).ready(function() {
//			  
//		
//		var email = "bavanyo@outlook.com";
//		 
//		$.when( checkemail(email) ).done(function ( data) {
//		    $('.result').html('<p>'+data+'</p>');
//		});	
//		
//		
//			function checkemail(_email)
//			{
//				return $.post( "<?php echo $GLOBALS['coreURL']; ?>/services/kickboxValidateEmail.php", {email: _email} );
//			}
//		
//					
//		});	
//  </script>

?>
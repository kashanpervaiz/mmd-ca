<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

	require_once 'Image/Barcode2.php';
	$bc = new Image_Barcode2();
	$im = Image_Barcode2::draw($_REQUEST['barcode'], 'code128', 'gif',false,20,1,true,Image_Barcode2::ROTATE_NONE);
	
	header('Content-type: image/gif');
	imagegif($im);
	imagedestroy($im);
	
	
	
	
/*
resource draw( string $text, [string $type = Image_Barcode2::BARCODE_INT25], [string $imgtype = Image_Barcode2::IMAGE_PNG], [boolean $bSendToBrowser = true], [integer $height = 60], [integer $width = 1], [boolean $showText = true], [integer $rotation = Image_Barcode2::ROTATE_NONE])

Draws a image barcode

    Return: The corresponding gd image resource
    Author: Marcelo Subtil Marcal <msmarcal@php.net>
    Since: Image_Barcode2 0.3
    Throws: Image_Barcode2_Exception
    Access: public


Parameters:
string   	$text   	�  	A text that should be in the image barcode
string   	$type   	�  	The barcode type. Supported types: code39 - Code 3 of 9 int25 - 2 Interleaved 5 ean13 - EAN 13 upca - UPC-A upce - UPC-E code128 ean8 postnet
string   	$imgtype   	�  	The image type that will be generated (gif, jpg, png)
boolean   	$bSendToBrowser   	�  	if the image shall be outputted to the browser, or be returned.
integer   	$height   	�  	The image height
integer   	$width   	�  	The image width
boolean   	$showText   	�  	The text should be placed under barcode
integer   	$rotation   	�  	The rotation angle

*/	
	
?>
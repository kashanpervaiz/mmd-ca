<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"])."/includes/phpHeader.php";
$userSmartLog = new UserSmartLog();
$mode = $_REQUEST['mode'];


if($mode == 'create'){
    echo json_encode("create");

} elseif($mode == 'select'){
    $user_id = $_REQUEST['user_id'];
    $date = $_REQUEST['date'];
    $user_log = $userSmartLog->getSmartLogForUserByIaAndDate($user_id,$date);
    echo json_encode($user_log);

} elseif($mode == 'update'){
    $response = ['stateChanged'=>false,'hasFirstEntry'=>false,'firstLogDate'=>'0000-00-00'];
    if( isset($_REQUEST['user_id'])){

        $initial_log_first_day = $userSmartLog->GetFirstLogEntryDate($_REQUEST['user_id']);
        $userSmartLog->setCheck($_REQUEST);
        $final_log_first_day = $userSmartLog->GetFirstLogEntryDate($_REQUEST['user_id']);


        if(($initial_log_first_day == '0000-00-00' && $final_log_first_day != '0000-00-00') || ($initial_log_first_day != '0000-00-00' && $final_log_first_day == '0000-00-00')){
            $response['stateChanged'] = true;
        }

        $_SESSION['first_log_entry_date'] = $final_log_first_day;
        $userCyclesObj = new UserCycles();
        if($final_log_first_day != '0000-00-00'){
            $userCyclesObj->UpdateUserCycles($_REQUEST['user_id']);
            $response['hasFirstEntry'] = true;
            $response['firstLogDate'] = $final_log_first_day;
        }else{
            $userCyclesObj->ClearUserCycles($_REQUEST['user_id']);
        }
    }
    echo json_encode(["data"=>$response]);

} else {
    echo json_encode("default");
}
?>



<?php
error_reporting(E_ERROR | E_PARSE);
error_reporting(E_ALL); //debugging only
ini_set('display_errors', True);
include_once realpath($_SERVER["DOCUMENT_ROOT"])."/includes/phpHeader.php";
session_write_close();

$htUser = "gpcuser";
$htPass = "roses";

require $GLOBALS['corePath'] . '/lib/phpwkhtmltopdf/vendor/autoload.php';
use mikehaertl\wkhtmlto\Pdf;

//$url = $GLOBALS['coreURL'] ."/services/manifest.php?boxId=".$_REQUEST['boxId'];
//$urlData = get_data_file_get($url); //echo($urlData);die();
$pageData = array();
////////////////////////////////////////////////////////////////////////////////
//$html = "";
//$html .="<html><head>";	
//$html .="<link rel='stylesheet' type='text/css' href='".$GLOBALS['coreURL']."/css/global.min.css' />";
//$html .="</head><body>";
$html = "";

try {
	$kitDataObj = new KitData();
	$kits = $kitDataObj->GetBoxContent($_REQUEST['boxId']);
	$pages = floor(count($kits) / 2)  + 1;
	$pageNum = 1;
	if(count($kits) > 0)
	{
		foreach($kits as $index => $kit)
		{
	
			if($index%2 == 0)
			{
				$html = "";
				$html .="<html><head>";	
				$html .="<link rel='stylesheet' type='text/css' href='".$GLOBALS['coreURL']."/css/global.min.css' />";
				$html .="</head><body>";		
			}
			$userDataObj2 = new UserData();
			$userData = $userDataObj2->GetUserData($kit['phase_id']);
			
			$mensesStatus = "n/a";
			$mensesFirstDay = "n/a";
			if(isset($userData['mensesstatus_id']) and strlen($userData['mensesstatus_id'])>0 and intval($userData['mensesstatus_id']) > 0 and $userData['gender'] == "F")
			{
				$mensesObj = new MensesStatus($userData['mensesstatus_id']);
				$mensesStatus = $mensesObj->name;
				if(intval($mensesObj->bPeriod) == 1)
				{
					$mensesFirstDay = date("m/d/Y", strtotime($userData['mensesfirstday']));
				}
			}
			$html .= "<p> Ordering physician: Keving Rogers, MD  Address: 123 Main St Anytown, CA 11111 Ph#555 555 5555</p>";
			//$html .= "<p> Ordering physician: Andrei Issakov M.D., Ph.D. Address: 1081 Lausanne, Switzerland Ph#555 555 5555</p>";

			$html .= "<table border='1' width='90%'bgcolor='#eeeeee'>";
			$html .="<tr><td>Patient Count</td><td colspan='3' align='center'>".intval($index+1)." of ".count($kits)."</td><tr>";
			
			$html .="<tr><td>wellBeingz Id</td><td colspan='3' align='center'><img src='".$GLOBALS['coreURL']."/services/barcode-gif.php?barcode=".$kit['barcode']."'/></td><tr>";
		
			$html .="<tr><td>Patient Id</td><td colspan='3'>".$userData['screenname'].$kit['phase_id']."</td></tr>";
			$html .="<tr><td>Gender</td><td>".$userData['gender']."</td><td>Ordered Panel</td><td>".$kit['productRef']."</td></tr>";
			$html .="<tr><td>DOB</td><td>".date("m/d/Y", strtotime($userData['birthday']))."</td><td>Sample Type</td><td>Blood</td></tr>";
			$html .="<tr><td>Menses Status</td><td>".$mensesStatus."</td><td>Collection Date</td><td>".date("m/d/Y", strtotime($kit['collectionDate']))."</td></tr>";
			$html .="<tr><td>First Day of Last Menses</td><td>".$mensesFirstDay."</td><td>Collection Time</td><td>".date("H:i", strtotime($kit['collectionTime']))."</td></tr>";
			//Add medications
			$html .="<tr><td colspan='4' align='center'>Hormone/Medication Use</td></tr>";
			$html .="<tr><td colspan='4'><table border='1' width='100%'>";
			$html .="<tr><td>Type</td><td>Brand</td><td>Delivery</td><td>Dose</td><td>Date Last Used</td><td>Times Last Used</td><td>Frequency Used</td></tr>";
			$medicationObj = new UserMedications();
			$medications = $medicationObj->GetUserMedications($kit['phase_id']);

			foreach($medications as $medication)
			{
		
				$medType = "";
				if(strlen($medication['medication_type'])>0 and intval($medication['medication_type']) > 0)
				{
					$MedTypeObj = new MedType($medication['medication_type']);
					$medType = $MedTypeObj->name;
				}
				
				$medFrequency = "";
				if(strlen($medication['dose_frequency'])>0 and intval($medication['dose_frequency']) > 0)
				{
					$medFrequencyObj = new MedFrequency($medication['dose_frequency']);
					$medFrequency = $medFrequencyObj->name;
				}
				
				$medDelivery = "";
				if(strlen($medication['delivery'])>0 and intval($medication['delivery']) > 0)
				{
					$medDeliveryObj = new MedDelivery($medication['delivery']);
					$medDelivery = $medDeliveryObj->name;
				}
				
				$html .="<tr><td>".$medication['medication']."</td><td>".$medType."</td><td>".$medDelivery."</td><td>".$medication['dose_strength']."</td><td>".date("m/d/Y", strtotime($medication['date_last_used']))."</td><td>".date("H:i", strtotime($medication['time_last_used']))."</td><td>".$medFrequency."</td></tr>";		
		
			}
			$html .="</table></td></tr>";
			//end medications
			$html .= "</table><br/><br/>";

			if($index%2 == 1 or ($index+1 == count($kits)))
			{
				$html .="</body></html>";	
				$pageData[] = $html;
				$pageNum++;	
			}
			
		}
	}
	else
	{
			$html = "";
			$html .="<html><head>";	
			$html .="<link rel='stylesheet' type='text/css' href='".$GLOBALS['coreURL']."/css/global.min.css' />";
			$html .="</head><body>";	
			$html .= "<h1>There are no kits in this box</h1>"	;
			$html .="</body></html>";	
			$pageData[] = $html;
	}
} catch (Exception $e){
		$html = "";
		$html .="<html><head>";	
		$html .="<link rel='stylesheet' type='text/css' href='".$GLOBALS['coreURL']."/css/global.min.css' />";
		$html .="</head><body>";	
		$html .= "<h1>".$e."</h1>"	;
		$html .="</body></html>";	
		$pageData[] = $html;
}

//$html .="</body></html>";	
////////////////////////////////////////////////////////////////////////////////
// You can pass a filename, a HTML string or an URL to the constructor
//$pdf = new Pdf();
//$pdf->binary = "/usr/local/bin/wkhtmltopdf";

$pdf = new Pdf(array(
    'binary' => '/usr/local/bin/wkhtmltopdf',
    'ignoreWarnings' => true,
    'commandOptions' => array(
        'useExec' => true,      // Can help if generation fails without a useful error message
        'procEnv' => array(
            // Check the output of 'locale' on your system to find supported languages
            'LANG' => 'en_US.utf-8',
        ),
    ),
));

foreach($pageData as $page)
{
	$pdf->addPage($page,array('username'=>$htUser,'password'=>$htPass));
}
//$pdf->send('report.pdf'); //open in file download
//$pdf->send(); 						//display in pdf viewer
if (!$pdf->send()) {
    throw new Exception('Could not create PDF: '.$pdf->getError());
}

function get_data_curl($url) {
	$ch = curl_init();
	$timeout = 5;
	$htUsrPswd = $htUser.":".$htPass;
  curl_setopt($ch, CURLOPT_USERPWD, $htUsrPswd);
  curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($ch, CURLOPT_COOKIE, session_name() . '=' . session_id());
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_HEADER, 0);	
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	$data = curl_exec($ch);
	curl_close($ch);
	return $data;
}

function get_image_curl($url) {
	$ch = curl_init();
	$timeout = 20;
	$htUsrPswd = $htUser.":".$htPass;
  curl_setopt($ch, CURLOPT_USERPWD, $htUsrPswd);
  curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($ch, CURLOPT_COOKIE, session_name() . '=' . session_id());
  curl_setopt($ch, CURLOPT_HEADER, 0);	
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	$data = curl_exec($ch);
	curl_close($ch);
	return $data;
}

function get_data_file_get($url)
{
    $username = $htUser;
    $password = $htPass;
    $context = stream_context_create(array( 'http' => array( 'method'=>"POST",'header'  => "Authorization: Basic " . base64_encode("$username:$password")."\r\n"."Cookie: ".session_name()."=".session_id()."\r\n"  ) ));
    $html  = file_get_contents($url, false, $context);
    return $html;	
}



?>
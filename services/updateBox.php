<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"])."/includes/phpHeader.php";
$result = array();
if(isset($_POST['id']))
{
	$boxObj = new Box();
	if(isset($_POST['destination_id']))
	{
		$result = $boxObj->Update($_POST);
	}
	else
	{
		$_POST['timestamp_completed'] = date("Y-m-d H:m:s");
		$result = $boxObj->Update($_POST);
	}
}
echo json_encode($result);
?>
<?php

//$_REQUEST['imbalances'] = json_encode(array("serotonin"=>"low normal","5-hiaa"=>"low normal","gaba"=>"normal","glycine"=>"normal","glutamate"=>"normal","histamine"=>"normal","phenethylamine"=>"normal","dopamine"=>"normal","dopac"=>"normal","homovanillic-acid"=>"normal","vanillylmandelic-acid"=>"normal","normetanephrine"=>"normal","epinephrine-total"=>"normal","epinephrine-diurnal"=>"normal","norepinephrine-total"=>"normal","norepinephrine-diurnal"=>"normal","cortisol-diurnal"=>"normal","cortisone-diurnal"=>"normal"));
//$_REQUEST['lang'] = 'en';
//$_REQUEST['user_id'] = 6;


include_once realpath($_SERVER["DOCUMENT_ROOT"])."/includes/phpHeader.php";

$RecommendationStepsPerTestKitObj = new RecommendationStepsPerTestKit();
$RecommendationTopicsPerStepObj = new RecommendationTopicsPerStep();
$RecommendationActivitiesPerTopicObj = new RecommendationActivitiesPerTopic();
$RecommendationActivitiesPerNeurotransmitterObj = new RecommendationActivitiesPerNeurotransmitter();

$RecommendationStepsObj = new RecommendationSteps();
$RecommendationTopicsObj = new RecommendationTopics();
$RecommendationActivitiesObj = new RecommendationActivities();

$UserSmartLogObj = new UserSmartLog();
$userLifeStyleObj = new UserLifestyle();
$userCustomRecObj = new UserCustomRecommendations();

$loglength = $UserSmartLogObj->GetLogLength($_REQUEST['user_id']);
$this_cycle = intval($loglength / 14);

$custom_rec_data = [];

$pageText = new PageText();
$recommendationPageText = $pageText->GetRecommendationText($_SESSION['language']);

$RecommendationActivitiesPerNeurotransmitters = $RecommendationActivitiesPerNeurotransmitterObj->GetAllData();
$activitiesByTopic = $RecommendationActivitiesPerTopicObj->GetTopics();

$custom_record = $userCustomRecObj->GetCustomRec($_REQUEST['cycle_id']);
$custom_rec = isset($custom_record['recommendations_json'])? json_decode($custom_record['recommendations_json'],true) : [] ;

$result = array();
$lifestyleData = $userLifeStyleObj->getLifeStyleRecommendations($_REQUEST['user_id'],$_REQUEST['kit_id'],$_REQUEST['cycle_id']);

$previous_admin_id = 0;

if(count($custom_rec) > 0){
    $result = array();
    $smartlog = array();
    $smartlog_sort_order = 1;
    $result['activity_progress_titles'] = $lifestyleData['titles'];
    $result['activity_progress_messages'] = $lifestyleData['messages'];
    $result['activity_preactivity_messages'] = $lifestyleData['pre_activity_messages'];
    foreach($custom_rec as $step => $topics) {
        if (!isset($result[$step])) {$result[$step] = array();}

        $step_data = $RecommendationStepsObj->GetData($step);
        $result[$step]['step_info'] = isset($step_data[0]) ? $step_data[0] : array();
        $result[$step]['step_info']['description'] = getRecPageTextVal("recommendation_steps",$result[$step]['step_info']['text_key']);
        $topicsPerStep = $RecommendationTopicsPerStepObj->GetTopicsForStep($step);

        foreach($topics as $topic => $activities) {

            if (in_array($topic,$topicsPerStep)) {
                $topic_data = $RecommendationTopicsObj->GetData($topic);
                $result[$step][$topic]['topic_info'] = isset($topic_data[0]) ? $topic_data[0] : array();
                $result[$step][$topic]['topic_info']['description'] = getRecPageTextVal("recommendation_topics",$result[$step][$topic]['topic_info']['text_key']);
                //education
                $result[$step][$topic]['topic_info']['education_title'] = getRecPageTextVal("recommendation_topics_education", $result[$step][$topic]['topic_info']['text_key']."_title");
                $result[$step][$topic]['topic_info']['education_content'] = getRecPageTextVal("recommendation_topics_education", $result[$step][$topic]['topic_info']['text_key']."_content");
                switch($topic){
                    case 1:
                        $result[$step][$topic]['topic_info']['progress'] = $lifestyleData['exercise_progress'];
                        break;
                    case 2:
                        $result[$step][$topic]['topic_info']['progress'] = $lifestyleData['diet_progress'];
                        break;
                    case 3:
                        $result[$step][$topic]['topic_info']['progress'] = $lifestyleData['sleep_progress'];
                        break;
//                    case 4:
//                        $result[$step][$topic]['topic_info']['progress'] = $lifestyleData['mind_progress'];
//                        break;
                    default:
                        $result[$step][$topic]['topic_info']['progress'] = array();
                }
                $imbalance_list = [];
//                if ( in_array($topic, [1, 2, 3, 4])) { //for body , diet, sleep, mind use lifestyle/smartlog for recs
//                    if (!isset($result[$step][$topic]['activities'])) {
//                        $result[$step][$topic]['activities'] = array();
//                    }
//                    $key = 'exercise_1';
//                    switch($topic){
//                        case 1:
//                            $key = isset($lifestyleData['exercise_queue'][0]) ? $lifestyleData['exercise_queue'][0] : 'exercise_1';
//                            break;
//                        case 2:
//                            $key = isset($lifestyleData['diet_queue'][0]) ? $lifestyleData['diet_queue'][0] : "diet_1";
//                            break;
//                        case 3:
//                            $key = isset($lifestyleData['sleep_queue'][0]) ? $lifestyleData['sleep_queue'][0] : "sleep_1";
//                            break;
//                        case 4:
//                            $key = "mind_9"; //Set Maintenance
//                            foreach($lifestyleData['mind_progress'] as $mind_key=>$mind_val){
//                                while($mind_val == 1){
//                                    $key = $mind_key;
//                                    break; // find first zero val which should be the next one.
//                                }
//                            }
//                            break;
//
//                    }
//                    $activityData = $RecommendationActivitiesObj->getActivityByTextKey($key);
//                    $activityData['explanation'] = getRecPageTextVal("recommendation_activities_explanation", $activityData['text_key']);
//                    $activityData['imbalance'] = "";
//                    $activityData['title'] = getRecPageTextVal("recommendation_activities_title", $activityData['text_key']);
//                    $activityData['subtitle'] = getRecPageTextVal("recommendation_activities_subtitle", $activityData['text_key']);
//                    if($activityData['subtitle'] == 'No data found.'){$activityData['subtitle'] = "";}
//                    if(!isset($result[$step][$topic]['activities'])){$result[$step][$topic]['activities'] = [];}
//                    if (activityNotPresent($result[$step][$topic]['activities'],$activityData)) {
//                        $result[$step][$topic]['activities'][] = $activityData;
//                        $smartlog[] = array("topic_id" => $topic, "activity_id" => $activityData['id'], "sort_order" => $topic_data[0]['display_order'] + $smartlog_sort_order);
//                        $smartlog_sort_order++;
//                        $custom_rec_data[$step][$topic][]=$activityData['id'];
//                    }
//
//                } else {
                    if(true){
                    $activity_number = 1;

                    foreach ($activities as $activity ) {
                        $imbalance_list = array('Not Appicable'); //set up a dummy array until this is cleaned up.
                        if (gettype($imbalance_list) != 'array' || count($imbalance_list) == 0) {
                            continue;
                        }
                        if (!isset($result[$step][$topic]['activities'])) {
                            $result[$step][$topic]['activities'] = array();
                        }
                        if (gettype($imbalance_list) == 'array' && count($imbalance_list) > 0) {
                            $activityData = $RecommendationActivitiesObj->GetData($activity)[0];
                            $activityData['explanation'] = getRecPageTextVal("recommendation_activities_explanation", $activityData['text_key']);
                            $activityData['explanation_popup'] = getRecPageTextVal("recommendation_activities_explanation_popup", $activityData['text_key']);
                            $activityData['explanation_foodsource'] = getRecPageTextVal("recommendation_activities_explanation_foodsource", $activityData['text_key']);
                            $activityData['imbalance'] = "";
                            foreach ($imbalance_list as $imbalance_text) {
                                $activityData['imbalance'] .= "<p>" . $imbalance_text . "</p>";
                            }
                            $activityData['title'] = getRecPageTextVal("recommendation_activities_title", $activityData['text_key']);
                            $activityData['subtitle'] = getRecPageTextVal("recommendation_activities_subtitle", $activityData['text_key']);
                            if($activityData['subtitle'] == 'No data found.'){$activityData['subtitle'] = "";}
                            if(!isset($result[$step][$topic]['activities'])){$result[$step][$topic]['activities'] = [];}
                            if (activityNotPresent($result[$step][$topic]['activities'],$activityData)) {
                                $result[$step][$topic]['activities'][] = $activityData;
                                $smartlog[] = array("topic_id" => $topic, "activity_id" => $activityData['id'], "sort_order" => $topic_data[0]['display_order'] + $smartlog_sort_order);
                                $smartlog_sort_order++;
                                $custom_rec_data[$step][$topic][]=$activityData['id'];
                            }
                        }
                        //Limit body,diet, and sleep to just the first activity
                        if (in_array($topic, [1, 2, 3, 4, 9, 10])) {
                            break;
                        }
                        //Limit body,diet, and sleep to just the first activity
//
//                        //Limit everything else to 2 activities
//                        if (in_array($topic, [5, 6, 7]) && $activity_number == 2) {
//                            break;
//                        }
                        //Limit everything else to 2 activities
                        $activity_number++;
                    }
                }
            }
        }
    }
    $UserSmartLogObj->CreateLog($_REQUEST['user_id'],date("Y-m-d"),$smartlog);

}elseif(isset($_REQUEST['imbalances'])) {

    $previous_custom_rec_row = $userCustomRecObj->GetCustomRec($_REQUEST['cycle_id']);
    $previous_custom_rec = [];
    $previous_admin_id = 0;
    if (isset($previous_custom_rec_row['admin_id']) && $previous_custom_rec_row['admin_id'] != 0) {
        $previous_admin_id = $previous_custom_rec_row['admin_id'];
        $previous_custom_rec = isset($previous_custom_rec_row['recommendations_json']) ? json_decode($previous_custom_rec_row['recommendations_json'], true) : [];

        foreach($previous_custom_rec as $key => $val){
            $steps[]= $key;
        }
    }

    $processedTopics =  populateTopics($_REQUEST['imbalances']);
    //$steps = $RecommendationStepsPerTestKitObj->GetStepsForKit($_REQUEST['testkitid']);
    if(count($previous_custom_rec) == 0){
                $steps = array(1, 2, 3);
    }


    $result = array();
    $smartlog = array();
    $smartlog_sort_order = 1;
    $result['activity_progress_titles'] = $lifestyleData['titles'];
    $result['activity_progress_messages'] = $lifestyleData['messages'];
    $result['activity_preactivity_messages'] = $lifestyleData['pre_activity_messages'];
    foreach($steps as $step){
        if(!isset($result[$step])){$result[$step] = array();}
        $step_data = $RecommendationStepsObj->GetData($step);
        $result[$step]['step_info'] = isset($step_data[0])? $step_data[0] : array();
        $result[$step]['step_info']['description']  = getRecPageTextVal("recommendation_steps", $result[$step]['step_info']['text_key']);
        $topicsPerStep = $RecommendationTopicsPerStepObj->GetTopicsForStep($step);


        foreach($processedTopics as $topic => $activities) {

            if (in_array($topic,$topicsPerStep)) {
                $topic_data = $RecommendationTopicsObj->GetData($topic);
                $result[$step][$topic]['topic_info'] = isset($topic_data[0]) ? $topic_data[0] : array();
                $result[$step][$topic]['topic_info']['description'] = getRecPageTextVal("recommendation_topics", $result[$step][$topic]['topic_info']['text_key']);
                //education
                $result[$step][$topic]['topic_info']['education_title'] = getRecPageTextVal("recommendation_topics_education", $result[$step][$topic]['topic_info']['text_key']."_title");
                $result[$step][$topic]['topic_info']['education_content'] = getRecPageTextVal("recommendation_topics_education", $result[$step][$topic]['topic_info']['text_key']."_content");
                switch($topic){
                    case 1:
                        $result[$step][$topic]['topic_info']['progress'] = $lifestyleData['exercise_progress'];
                        break;
                    case 2:
                        $result[$step][$topic]['topic_info']['progress'] = $lifestyleData['diet_progress'];
                        break;
                    case 3:
                        $result[$step][$topic]['topic_info']['progress'] = $lifestyleData['sleep_progress'];
                        break;
//                    case 4:
//                        $result[$step][$topic]['topic_info']['progress'] = $lifestyleData['mind_progress'];
//                        break;
                    default:
                        $result[$step][$topic]['topic_info']['progress'] = array();
				}
				$imbalance_list = [];
                if (in_array($topic, [1, 2, 3, 4])) { //for body , diet, sleep, mind use lifestyle/smartlog for recs
                    if (!isset($result[$step][$topic]['activities'])) {
                        $result[$step][$topic]['activities'] = array();
                    }
                    $key = 'exercise_1';
                    switch($topic){
                        case 1:
                            $key = isset($lifestyleData['exercise_queue'][0]) ? $lifestyleData['exercise_queue'][0] : 'exercise_1';
                            break;
                        case 2:
                            $key = isset($lifestyleData['diet_queue'][0]) ? $lifestyleData['diet_queue'][0] : "diet_1";
                            break;
                        case 3:
                            $key = isset($lifestyleData['sleep_queue'][0]) ? $lifestyleData['sleep_queue'][0] : "sleep_1";
                            break;
                        case 4:
                            $key = "mind_9"; //Set Maintenance
                            foreach($lifestyleData['mind_progress'] as $mind_key=>$mind_val){
                                while($mind_val == 1){
                                    $key = $mind_key;
                                    break; // find first zero val which should be the next one.
                                }
                            }
                            break;

                    }
                    $activityData = $RecommendationActivitiesObj->getActivityByTextKey($key);
                    $activityData['explanation'] = getRecPageTextVal("recommendation_activities_explanation", $activityData['text_key']);
                    $activityData['explanation_popup'] = getRecPageTextVal("recommendation_activities_explanation_popup", $activityData['text_key']);
                    $activityData['explanation_foodsource'] = getRecPageTextVal("recommendation_activities_explanation_foodsource", $activityData['text_key']);
                    $activityData['imbalance'] = "";
                    $activityData['title'] = getRecPageTextVal("recommendation_activities_title", $activityData['text_key']);
                    $activityData['subtitle'] = getRecPageTextVal("recommendation_activities_subtitle", $activityData['text_key']);
                    if($activityData['subtitle'] == 'No data found.'){$activityData['subtitle'] = "";}
                    if(!isset($result[$step][$topic]['activities'])){$result[$step][$topic]['activities'] = [];}
                    if (activityNotPresent($result[$step][$topic]['activities'],$activityData)) {
                        $result[$step][$topic]['activities'][] = $activityData;
                        $smartlog[] = array("topic_id" => $topic, "activity_id" => $activityData['id'], "sort_order" => $topic_data[0]['display_order'] + $smartlog_sort_order);
                        $smartlog_sort_order++;
                        $custom_rec_data[$step][$topic][]=$activityData['id'];
                    }

                } else {

                    if (count($previous_custom_rec) == 0) { //no previous "CUSTOM" rec
                        $activity_number = 1;
                        if ($topic == 5 && $activity_number == 1) { //Add omega 3 to everyone
                            $activity = 66;
                            $imbalance_list[] = $activities['66'];
                            if (gettype($imbalance_list) == 'array' && count($imbalance_list) > 0) {
                                $activityData = $RecommendationActivitiesObj->GetData($activity)[0];
                                $activityData['explanation'] = getRecPageTextVal("recommendation_activities_explanation", $activityData['text_key']);
                                $activityData['explanation_popup'] = getRecPageTextVal("recommendation_activities_explanation_popup", $activityData['text_key']);
                                $activityData['explanation_foodsource'] = getRecPageTextVal("recommendation_activities_explanation_foodsource", $activityData['text_key']);
                                $activityData['imbalance'] = "";
                                foreach ($imbalance_list as $imbalance_text) {
                                    $activityData['imbalance'] .= "<p>Omega3 for everyone!</p>";
                                }
                                $activityData['title'] = getRecPageTextVal("recommendation_activities_title", $activityData['text_key']);
                                $activityData['subtitle'] = getRecPageTextVal("recommendation_activities_subtitle", $activityData['text_key']);
                                if ($activityData['subtitle'] == 'No data found.') {
                                    $activityData['subtitle'] = "";
                                }
                                if (!isset($result[$step][$topic]['activities'])) {
                                    $result[$step][$topic]['activities'] = [];
                                }
                                if (activityNotPresent($result[$step][$topic]['activities'], $activityData)) {
                                    $result[$step][$topic]['activities'][] = $activityData;
                                    $smartlog[] = array("topic_id" => $topic, "activity_id" => $activityData['id'], "sort_order" => $topic_data[0]['display_order'] + $smartlog_sort_order);
                                    $smartlog_sort_order++;
                                    $custom_rec_data[$step][$topic][] = $activityData['id'];
                                }
                            }

                            $activity_number++;
                        }
                        foreach ($activities as $activity => $imbalance_list) {
                            if (gettype($imbalance_list) != 'array' || count($imbalance_list) == 0) {
                                continue;
                            }
                            if (!isset($result[$step][$topic]['activities'])) {
                                $result[$step][$topic]['activities'] = array();
                            }
                            if (gettype($imbalance_list) == 'array' && count($imbalance_list) > 0) {
                                $activityData = $RecommendationActivitiesObj->GetData($activity)[0];
                                $activityData['explanation'] = getRecPageTextVal("recommendation_activities_explanation", $activityData['text_key']);
                                $activityData['explanation_popup'] = getRecPageTextVal("recommendation_activities_explanation_popup", $activityData['text_key']);
                                $activityData['explanation_foodsource'] = getRecPageTextVal("recommendation_activities_explanation_foodsource", $activityData['text_key']);
                                $activityData['imbalance'] = "";
                                foreach ($imbalance_list as $imbalance_text) {
                                    $activityData['imbalance'] .= "<p>" . $imbalance_text . "</p>";
                                }
                                $activityData['title'] = getRecPageTextVal("recommendation_activities_title", $activityData['text_key']);
                                $activityData['subtitle'] = getRecPageTextVal("recommendation_activities_subtitle", $activityData['text_key']);
                                if ($activityData['subtitle'] == 'No data found.') {
                                    $activityData['subtitle'] = "";
                                }
                                if (!isset($result[$step][$topic]['activities'])) {
                                    $result[$step][$topic]['activities'] = [];
                                }
                                if (activityNotPresent($result[$step][$topic]['activities'], $activityData)) {
                                    $result[$step][$topic]['activities'][] = $activityData;
                                    $smartlog[] = array("topic_id" => $topic, "activity_id" => $activityData['id'], "sort_order" => $topic_data[0]['display_order'] + $smartlog_sort_order);
                                    $smartlog_sort_order++;
                                    $custom_rec_data[$step][$topic][] = $activityData['id'];
                                }
                            }
                            //Limit body,diet, and sleep to just the first activity
                            if (in_array($topic, [1, 2, 3, 4])) {
                                break;
                            }
                            //Limit body,diet, and sleep to just the first activity

                            //Limit everything else to 2 activities
                            if (in_array($topic, [5, 6, 7]) && $activity_number == 2) {
                                break;
                            }
                            //Limit everything else to 2 activities
                            $activity_number++;
                        }
                    } else {  //Use the previous rec for adaptogens and supplements
                        $activity_number = 1;

                        $previous_activities = $previous_custom_rec[$step][$topic];

                        foreach ($previous_activities as $activity ) {
                            $imbalance_list = array('Not Appicable'); //set up a dummy array until this is cleaned up.
                            if (gettype($imbalance_list) != 'array' || count($imbalance_list) == 0) {
                                continue;
                            }
                            if (!isset($result[$step][$topic]['activities'])) {
                                $result[$step][$topic]['activities'] = array();
                            }
                            if (gettype($imbalance_list) == 'array' && count($imbalance_list) > 0) {
                                $activityData = $RecommendationActivitiesObj->GetData($activity)[0];
                                $activityData['explanation'] = getRecPageTextVal("recommendation_activities_explanation", $activityData['text_key']);
                                $activityData['explanation_popup'] = getRecPageTextVal("recommendation_activities_explanation_popup", $activityData['text_key']);
                                $activityData['explanation_foodsource'] = getRecPageTextVal("recommendation_activities_explanation_foodsource", $activityData['text_key']);
                                $activityData['imbalance'] = "";
                                foreach ($imbalance_list as $imbalance_text) {
                                    $activityData['imbalance'] .= "<p>" . $imbalance_text . "</p>";
                                }
                                $activityData['title'] = getRecPageTextVal("recommendation_activities_title", $activityData['text_key']);
                                $activityData['subtitle'] = getRecPageTextVal("recommendation_activities_subtitle", $activityData['text_key']);
                                if($activityData['subtitle'] == 'No data found.'){$activityData['subtitle'] = "";}
                                if(!isset($result[$step][$topic]['activities'])){$result[$step][$topic]['activities'] = [];}
                                if (activityNotPresent($result[$step][$topic]['activities'],$activityData)) {
                                    $result[$step][$topic]['activities'][] = $activityData;
                                    $smartlog[] = array("topic_id" => $topic, "activity_id" => $activityData['id'], "sort_order" => $topic_data[0]['display_order'] + $smartlog_sort_order);
                                    $smartlog_sort_order++;
                                    $custom_rec_data[$step][$topic][]=$activityData['id'];
                                }
                            }
                            //Limit body,diet, and sleep to just the first activity
                            if (in_array($topic, [1, 2, 3, 4, 9, 10])) {
                                break;
                            }
                            //Limit body,diet, and sleep to just the first activity
//
//                        //Limit everything else to 2 activities
//                        if (in_array($topic, [5, 6, 7]) && $activity_number == 2) {
//                            break;
//                        }
                            //Limit everything else to 2 activities
                            $activity_number++;
                        }

                    }
                }
            }

        }

//        $activityData = $RecommendationActivitiesObj->GetData(77)[0];
//        $activityData['explanation'] = getRecPageTextVal("recommendation_activities_explanation", $activityData['text_key']);
//        $activityData['imbalance'] = "";
//        $activityData['imbalance'] = "";
//        $result[3][8]['activities'][77] = $activityData;


        if ($step == 4) {
            if (count($previous_custom_rec) == 0) { //no previous "CUSTOM" rec
                $activityData = $RecommendationActivitiesObj->GetData(78)[0];
                $custom_rec_data[4][9][] = 78;

            } else {
                if(isset($previous_custom_rec[4][9]) ){ //if there was either before, show the continue message
                    $activityData = $RecommendationActivitiesObj->GetData(109)[0];
                    $custom_rec_data[4][9][] = 109;
                }

            }
            $activityData['title'] = getRecPageTextVal("recommendation_activities_title", $activityData['text_key']);
            $activityData['subtitle'] = getRecPageTextVal("recommendation_activities_subtitle", $activityData['text_key']);
            if($activityData['subtitle'] == 'No data found.'){$activityData['subtitle'] = "";}
            $activityData['explanation'] = getRecPageTextVal("recommendation_activities_explanation", $activityData['text_key']);
            $activityData['explanation_popup'] = getRecPageTextVal("recommendation_activities_explanation_popup", $activityData['text_key']);
            $activityData['explanation_foodsource'] = getRecPageTextVal("recommendation_activities_explanation_foodsource", $activityData['text_key']);
            $activityData['imbalance'] = "";
            $activityData['imbalance'] = "";
            $result[4][9]['activities'][78] = $activityData;
        }

        if ($step == 5) {
            if (count($previous_custom_rec) == 0) {
                $custom_rec_data[5][10][] = 79;
                $activityData = $RecommendationActivitiesObj->GetData(79)[0];
            } else {
                if(isset($previous_custom_rec[5][10]) ){ //if there was either before, show the continue message
                    $activityData = $RecommendationActivitiesObj->GetData(110)[0];
                    $custom_rec_data[5][10][] = 110;
                }
            }

            $activityData['title'] = getRecPageTextVal("recommendation_activities_title", $activityData['text_key']);
            $activityData['subtitle'] = getRecPageTextVal("recommendation_activities_subtitle", $activityData['text_key']);
            if($activityData['subtitle'] == 'No data found.'){$activityData['subtitle'] = "";}
            $activityData['explanation'] = getRecPageTextVal("recommendation_activities_explanation", $activityData['text_key']);
            $activityData['explanation_popup'] = getRecPageTextVal("recommendation_activities_explanation_popup", $activityData['text_key']);
            $activityData['explanation_foodsource'] = getRecPageTextVal("recommendation_activities_explanation_foodsource", $activityData['text_key']);
            $activityData['imbalance'] = "";
            $activityData['imbalance'] = "";
            $result[5][10]['activities'][79] = $activityData;
        }


    }
    $UserSmartLogObj->CreateLog($_REQUEST['user_id'],date("Y-m-d"),$smartlog);

    ///New Custom Rec
    $recommendation_json =  json_encode($custom_rec_data);
    $custom_record = array(
        "cycle_id" => $_REQUEST['cycle_id'],
        "recommendations_json" => $recommendation_json,
        "admin_id" => $previous_admin_id
    );

    if(isset($custom_record['id']) && $custom_record['id'] > 0){
        $userCustomRecObj->Update($custom_record); //Should never happen!
    } else {
        $userCustomRecObj->Add($custom_record);
    }

}





/////////////

// echo "<pre>";
$package = array();
$package['errors'] = '';
$package['recommendationData'] = $result;

echo json_encode($package);
//echo "</pre>";


function populateTopics($imbalancesJson){
    global $pageText;
    $imbalances = json_decode($imbalancesJson,true);
    $RecommendationActivitiesPerNeurotransmitterObj = new RecommendationActivitiesPerNeurotransmitter();
    $RecommendationActivitiesPerTopicObj = new RecommendationActivitiesPerTopic();
    $RecommendationActivitiesPerNeurotransmitters = $RecommendationActivitiesPerNeurotransmitterObj->GetAllData();
    $activitiesByTopic = $RecommendationActivitiesPerTopicObj->GetTopics();

    foreach($RecommendationActivitiesPerNeurotransmitters as $index => $activitiesPerNtRow){

        $activity_list = json_decode($activitiesPerNtRow['activity_list'],true);

        foreach($imbalances as $imbalance_nt => $imbalance_direction){

            if(strpos($imbalance_direction,'high') !== false ){$imbalance_direction = 'high';}
            if(strpos($imbalance_direction,'low') !== false ){$imbalance_direction = 'low';}

            if($activitiesPerNtRow['neurotransmitter'] == $imbalance_nt &&  $activitiesPerNtRow['imbalance'] == $imbalance_direction){

                foreach($activitiesByTopic as $topic_id => $activities){
                    foreach($activities as $activity => $count) {
                        if(in_array($activity,$activity_list)){
                            $prettyName = getRecPageTextVal("VendorNameMap", "mmd_".$imbalance_nt);
                            $activitiesByTopic[$topic_id][$activity][] = $prettyName.": ".strtoupper($imbalance_direction);
                        }

                    }

                }
                $test = $activitiesByTopic;
            }
        }
    }

    foreach($activitiesByTopic as $topic => &$activityList){
        if(isset($activityList['26']) && count($activityList['26']) > 1) {
            $activityList['27'] = array();  //Remove exercise 2 if 1 is set
        }

        if($topic == 8){$activityList['77'] = 1;}
        if($topic == 9){$activityList['78'] = 1;}
        if($topic == 10){$activityList['79'] = 1;}

        //arsort ($activityList);
    }

    // Set defaults when there are no activities
    foreach($activitiesByTopic as $topic => &$activityList){
            $is_anything_set = 0;
            foreach($activityList as $activities)
            {
                if (is_array($activities)) {
                    $is_anything_set += count($activities);
                }
            }
        if($is_anything_set == 0){
            switch($topic){
                case 1: //exercise
                    $activityList['26'] = array("Diurnal EPINEPHRINE: HIGH");
                    break;
                case 2:  //diet
                    $activityList['32'] = array("Diurnal EPINEPHRINE: HIGH");
                    break;
                case 3: //sleep
                    $activityList['47'] = array("Diurnal EPINEPHRINE: HIGH");
                    break;
                case 4: //mind
                    $activityList['48'] = array("Diurnal EPINEPHRINE: HIGH");
                    $activityList['49'] = array("Diurnal EPINEPHRINE: HIGH");
                    break;
                case 5: //sups (nurtrients)
                    $activityList['53'] = array("Diurnal EPINEPHRINE: HIGH");
                    $activityList['55'] = array("Diurnal EPINEPHRINE: HIGH");
                    break;
                case 6: //adaptogens
                    $activityList['66'] = array("Diurnal EPINEPHRINE: HIGH");
                    $activityList['67'] = array("Diurnal EPINEPHRINE: HIGH");
                    $activityList['69'] = array("Diurnal EPINEPHRINE: HIGH");
                    break;
                default:
            }

        }

        //arsort ($activityList);
    }

    return $activitiesByTopic;

}

function activityNotPresent($activitiesArray = [], $nextActivity = []){
    if(count($activitiesArray) == 0){return true;}

    foreach($activitiesArray as $activity){
        if($nextActivity['id'] == $activity['id']){
            return false;
        }
    }
    return true;
}

function getRecPageTextVal($pagename,$elementname,$replace = []){
    global $recommendationPageText;
    $value =  isset($recommendationPageText[$pagename][$elementname]) ? $recommendationPageText[$pagename][$elementname] : "";
    if(count($replace)){
        $key = key($replace);
        $val= $replace[$key];
        $find = "%%".$key."%%";
        $value = str_replace($find,$val,$value);
    }
    return $value;
}

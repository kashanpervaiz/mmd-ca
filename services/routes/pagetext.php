<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"]) . "/includes/phpHeader.php";
$pagetext_obj = new PageText();

if(!isset($route[3])) {
    echo json_encode("No method sent");
    exit;
}

$method = $route[3];
switch($method){
    case $method == "getpagetext":
        $body = $_POST;
        if(empty($_POST)){
            $body =  json_decode(file_get_contents("php://input"),true);
        }

        if(!isset($body)){
            http_response_code(400);
            echo json_encode(array("error"=>"Missing Data","body"=>$body));
            exit;
        }
        if(isset($body['page'])  && isset($body['vlanguage'])){
            $array = $pagetext_obj->GetPageText(trim($body['page']), $body['vlanguage']);
            echo json_encode($array);
        }else{
            http_response_code(400);
            echo json_encode(array("error"=>"Missing data"));
        }
        break;

    case $method == "getspecificpagetext":
        $body = $_POST;
        if(empty($_POST)){
            $body =  json_decode(file_get_contents("php://input"),true);
        }

        if(!isset($body)){
            http_response_code(400);
            echo json_encode(array("error"=>"Missing Data","body"=>$body));
            exit;
        }
        if(isset($body['page']) && isset($body['key']) && isset($body['vlanguage'])){
            $array = $pagetext_obj->GetSpecificPageText(trim($body['page']), trim($body['key']), $body['vlanguage']);
            $result = "No data found.";

            if(count($array)>0)
            {
                $result = $array[ trim($body['key']) ];
            }

            if(strlen($result) == 0) //Just in case the key is present but empty
            {
                $result = "No data found.";
            }
            echo json_encode($result);


        }else{
            http_response_code(400);
            echo json_encode(array("error"=>"Missing data"));
        }
        break;

    default:
        http_response_code(400);
        echo json_encode(array("error"=>"Method not found"));

}
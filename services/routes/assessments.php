<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"]) . "/includes/phpHeader.php";
$user_assessment_obj = new UserAssessments();
$pagetext_obj = new PageText();

if(!isset($route[3])) {
    echo json_encode("No method sent");
    exit;
}

$method = $route[3];
switch($method){
    case $method == "get":
        $body = $_POST;
        if(empty($_POST)){
            $body =  json_decode(file_get_contents("php://input"),true);
        }

        if(!isset($body)){
            http_response_code(400);
            echo json_encode(array("error"=>"Missing Data","body"=>$body));
            exit;
        }

        if(isset($body['id']) && isset($body['lang'])){
            $result = array('error'=>'no data found','results'=>array());

            $assessmentQuestions = $pagetext_obj->GetPageText('assessment_questions2',$body['lang']);
            $frequency = array('-1'=>'','0'=>'never','1'=>'rarely','2'=>'sometimes','3'=>'often','4'=>'always');

            if(isset($body['id']) && $body['id'] > 0 )
            {
                $UserAssessmentsObj = new UserAssessments();
                $assessmentDataCollection = array();

                $assessmentDataCollection = $UserAssessmentsObj->GetUserData($body['user_id']);
                $position = array_search($body['id'], array_keys($assessmentDataCollection));
                $count = count($assessmentDataCollection);
                $assessment = $assessmentDataCollection[$body['id']];
                if(isset($assessment)){
                    $_SESSION['current_assessment_id'] = $assessment['id'];
                    $questionsList = array();
                    $symptomsList = array();
                    $symptoms = array();
                    $assessments = array();
                    $current_assessment_position = "";
                    $symptomsList = !empty($assessment['symptomjson'])  ? json_decode($assessment['symptomjson'],true): array() ;
                    $assessmentForm = !empty($assessment['formjson'])?json_decode($assessment['formjson'],true) : array();
                    $date_submitted = formatToUSDate($assessment['date_submitted']);



                    foreach($assessmentQuestions as $questionKey => $question){
                        $frequencyVal = isset( $assessmentForm[$questionKey]) ?  $assessmentForm[$questionKey] : -1 ;
                        if (isset( $assessmentForm[$questionKey])) {
                            $questionsList[] = '<tr><td class="assessment_question">' . $question . '</td><td class="assessment_answer">' . $frequency[$frequencyVal] . '</td></tr>';
                        }
                    }
                    foreach($symptomsList as $symptom){
                        if(strlen($symptom) > 0){
                            $symptomsList[] = '<tr><td class="assessment_symptom">'.$symptom.'</td></tr>';
                            $symptoms[] = $symptom;
                        }
                    }
                    reset($assessmentDataCollection);
                    foreach($assessmentDataCollection as $assessmentData){
                        $assessments[] = array('id'=>$assessmentData['id'],'date'=>formatToUSDate($assessmentData['date_submitted']));
                    }


                    $result['error'] = "";
                    $result['results']['questionsList']  = $questionsList;
                    $result['results']['symptomsList']  = $symptomsList;
                    $result['results']['symptoms']  = $symptoms;
                    $result['results']['assessment']  = $assessment;
                    $result['results']['assessments']  = $assessments;
                    $result['results']['assessmentForm'] = $assessmentForm;
                    $result['results']['position']  = ++$position;
                    $result['results']['count']  = $count;
                    $result['results']['date_submitted']  = $date_submitted;
                }

            } else {
                $questionsList = array();
                $position = 0;

                foreach($assessmentQuestions as $questionKey => $question){
                    $frequencyVal =  -1 ;
                    $questionsList[] = '<tr><td class="assessment_question">'.$question.'</td><td class="assessment_answer">'.$frequency[$frequencyVal].'</td></tr>';
                }

                $result['error'] = "";
                $result['results']['questionsList']  = $questionsList;
                $result['results']['symptomsList']  = array();
                $result['results']['symptoms']  = array();
                $result['results']['assessment']  = array();
                $result['results']['assessments']  = array();
                $result['results']['assessmentForm'] = array();
                $result['results']['position']  = ++$position;
                $result['results']['count']  = 0;
                $result['results']['date_submitted']  = Date('Y-m-d H:i:s');

            }
            echo json_encode($result);
            
        }else{
            http_response_code(400);
            echo json_encode(array("error"=>"Missing user id"));
        }
        break;

    case $method == "set":
        $body = $_POST;
        if(empty($_POST)){
            $body =  json_decode(file_get_contents("php://input"),true);
        }

        if(!isset($body)){
            http_response_code(400);
            echo json_encode(array("error"=>"Missing Data","body"=>$body));
            exit;
        }

        if(isset($body['update_assessment_form'])){
            $stressScore = 0;
            $stressCount = 0;
            $anxietyScore = 0;
            $anxietyCount = 0;
            $depressionScore = 0;
            $depressionCount = 0;
            $symptomsArray = array();
            $repostArray = array();
            $i=1;
            foreach($body as $key => $val){

                if(strpos($key,'symptoms-details-') !== false){
                    if (strlen($val)>0 ) {
                        $symptomsArray[$i] = preg_replace('~[[:cntrl:]]~', ' ', trim($val));
                        $i++;
                    }


                } else if(strpos($key,'stress_') !== false){
                    if (strlen($val)>0 ) {
                        $stressScore += $val;
                    }
                    $stressCount++;

                } else if(strpos($key,'anxiety_') !== false){
                    if (strlen($val)>0 ) {
                        $anxietyScore += $val;
                    }
                    $anxietyCount++;

                } else if(strpos($key,'depression_') !== false){
                    if (strlen($val)>0 ) {
                        $depressionScore += $val;
                    }
                    $depressionCount++;

                } else {

                    $repostArray[$key] = $val;
                }

            }
            if(isset($body['id']) && $body['id'] > 0){
                $repostArray['id'] = $body['id'];
            }

            $repostArray['stress_score'] = makeScore(4,$stressScore,$stressCount);
            $repostArray['anxiety_score'] = makeScore(3,$anxietyScore,$anxietyCount);
            $repostArray['depression_score'] = makeScore(3,$depressionScore,$depressionCount);
            $repostArray['symptomjson'] = json_encode($symptomsArray);
            $repostArray['formjson'] = json_encode(preg_replace('~[[:cntrl:]]~', ' ', $body)); //json_encode($body);

//          $id = $user_assessment_obj->Add($repostArray,[],false);
//          if($id < 0){
//              $id = $user_assessment_obj->Update($repostArray,[],false);
//          }
//          echo $id;

            if (isset($repostArray['id']) && $repostArray['id'] > 0) {
                echo json_encode(["id" => $user_assessment_obj->Update($repostArray, [], false)]);
            } else {
                $new_assessment_id = $user_assessment_obj->Add($repostArray, [], false);
                $_SESSION['current_assessment_id'] = $new_assessment_id;
                echo json_encode(["id" => $new_assessment_id]);
            }

        }else{
            http_response_code(400);
            echo json_encode(array("error"=>"Missing user id"));
        }
        break;

    default:
        http_response_code(400);
        echo json_encode(array("error"=>"Method not found"));
}

function makeScore($max,$score,$count){
    $maxPossible = $count*$max;
    $result = intval(300 * ($score/$maxPossible)); //the gyr slider is 0 to 300

    return $result;
}
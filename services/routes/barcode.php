<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"]) . "/includes/phpHeader.php";
require_once realpath($_SERVER["DOCUMENT_ROOT"]).'/vendor/autoload.php';

if(!isset($route[3])) {
    echo json_encode("No method sent");
    exit;
}

$obj = new KitData();

$method = $route[3];
switch($method){

    case $method == "validate":
        $body = $_POST;
        if(empty($_POST)){
            $body =  json_decode(file_get_contents("php://input"),true);
        }
        if(!isset($body)){
            http_response_code(400);
            echo json_encode(array("error"=>"Missing Data","body"=>$body));
            exit;
        }
        if(isset($body['barcode'])){

            if( strlen( $body['barcode']) >= 7) //Used to be 13
            {

                $id = $obj->validateBarcode($_POST['barcode']);

                if(isset($id) and intval($id)>0)
                {
                    echo json_encode(array("data"=>$id));
                }
                else if(isset($id) and intval($id)== -1)
                {
                    echo json_encode(array("data"=>"-1"));//Already in use
                }
                else
                {
                    echo json_encode(array("data"=>"-2"));//Combo not found
                }
            }
            else
            {
                echo json_encode(array("data"=>"-4")); //Invalid Barcode
            }

        }else{
            echo json_encode(array("data"=>"-3")); //Missing Data
        }
        break;

    case $method == 'register':
        $body = $_POST;
        if(empty($_POST)){
            $body =  json_decode(file_get_contents("php://input"),true);
        }
        if(!isset($body)){
            http_response_code(400);
            echo json_encode(array("error"=>"Missing Data","body"=>$body));
            exit;
        }
        if( isset($body['barcode']) && isset($body['latest_cycle_id']) )
        {
            $kitDataObj = new KitData();
            $numRows = $kitDataObj->registerBarcode($body['barcode'],$body['latest_cycle_id'],formatToDBDate($body['date']),$body['time1'],$body['time2'],$body['time3'],$body['time4']);

            if($numRows > 0){
                $current_kit_id = $kitDataObj->GetIdForBarcode($body['barcode']);
                if($current_kit_id > 0){
                    $_SESSION['currentKit'] = $current_kit_id;
                    echo json_encode(array("data" => $current_kit_id));
                }else{
                    echo json_encode(array("data"=>"-1"));
                }
            } else {
                echo json_encode(array("data"=>"-1"));
            }
        }
        break;

    default:
        http_response_code(400);
        echo json_encode(array("error"=>"Method not found"));

}

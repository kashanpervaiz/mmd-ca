<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"]) . "/includes/phpHeader.php";
$user_data_obj = new UserData();

if(!isset($route[3])) {
    echo json_encode("No method sent");
    exit;
}

$method = $route[3];
switch($method){
    case $method == "getuserdata":
        if(isset($route[4])){
            echo json_encode(array("data"=>$user_data_obj->GetUserData(intval($route[4]))));
        } else {
            http_response_code(400);
            echo json_encode(array("error"=>"Missing user id"));
        }
    break;

    case $method == "update":
        $body = $_POST;
        if(empty($_POST)){
            $body =  json_decode(file_get_contents("php://input"),true);
        }

        if(!isset($body)){
            http_response_code(400);
            echo json_encode(array("error"=>"Missing Data","body"=>$body));
            exit;
        }
        if(isset($body['id'])){

            //update password
            if(isset($body['password']) && strlen($body['password'])>0){
                $user = new User();
                $new_data = ["user_id"=>$body['user_id'],"password"=>$body['password']];
                $response_u = $user->Update($new_data,[],false);
                if($response_u <= 0){
                    http_response_code(400);
                    echo json_encode(array("error"=>"MySQL Error Code: $response_u"));
                }
            }
            //update email
            if(isset($body['email']) && strlen($body['email'])>0){
                $user = new User();
                $new_data = ["user_id"=>$body['user_id'],"email"=>$body['email']];
                $response_u = $user->Update($new_data,[],false);
                if($response_u <= 0){
                    http_response_code(400);
                    echo json_encode(array("error"=>"MySQL Error Code: $response_u"));
                }
            }
            //update everything else
            $response = $user_data_obj->Update($body,[],false);
            if ($response > 0) {
                echo json_encode(array("data" => array("id" => $response)));
            } else {
                http_response_code(400);
                echo json_encode(array("error"=>"MySQL Error Code: $response"));
            }
        } else {
            http_response_code(400);
            echo json_encode(array("error"=>"Missing user id"));
        }
        break;

    case $method == "updatebyuserid":
        $body = $_POST;
        if(empty($_POST)){
            $body =  json_decode(file_get_contents("php://input"),true);
        }

        if(!isset($body)){
            http_response_code(400);
            echo json_encode(array("error"=>"Missing Data","body"=>$body));
            exit;
        }
        if(isset($body['user_id'])){

            //update password
            if(isset($body['password']) && strlen($body['password'])>0){
                $user = new User();
                $new_data = ["user_id"=>$body['user_id'],"password"=>$body['password']];
                $response_u = $user->Update($new_data,[],false);
                if($response_u <= 0){
                    http_response_code(400);
                    echo json_encode(array("error"=>"MySQL Error Code: $response_u"));
                }
            }
            //update email
            if(isset($body['email']) && strlen($body['email'])>0){
                $user = new User();
                $new_data = ["user_id"=>$body['user_id'],"email"=>$body['email']];
                $response_u = $user->Update($new_data,[],false);
                if($response_u <= 0){
                    http_response_code(400);
                    echo json_encode(array("error"=>"MySQL Error Code: $response_u"));
                }
            }
            //update everything else
            if (!isset($body['id'])) {
                $user_data = $user_data_obj->GetUserData($body['user_id']);
                $body['id'] = $user_data['id'];
            }

            if ($body['id'] > 0) {
                $response = $user_data_obj->Update($body, [], false);
                //if(isset($body['request_uri'])){
                //   $_SESSION['request_uri'] = $body['request_uri'];
                //}
            } else {
                $response = 0;
            }
            if ($response >= 0) {

                http_response_code(200);
                echo json_encode(array("data" => array("id" => $response)));
            } else {
                http_response_code(400);
                echo json_encode(array("error"=>"MySQL Error Code: $response"));
            }
        } else {
            http_response_code(400);
            echo json_encode(array("error"=>"Missing user id"));
        }
        break;

        case $method = "getallaffiliations":
            $allAffiliations = $user_data_obj->getAllAffiliations();
            echo json_encode(array("data"=>$allAffiliations));

        break;

    default:
        http_response_code(400);
        echo json_encode(array("error"=>"Method not found"));
    
}


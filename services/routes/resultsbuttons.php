<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"]) . "/includes/phpHeader.php";
$data_obj = new UserLifestyle();

if(!isset($route[3])) {
    echo json_encode("No method sent");
    exit;
}

$method = $route[3];
switch($method){
    case $method == "getbuttonstates":
        if( isset($route[5]) && isset($route[4]) ){ // cycle_id /  kit_id
            $states = array(
                'leftnav-recommendations' => '0',
                'leftnav-smartlog' => '0',
                'leftnav-progress'=>'0',
                'leftnav-moodassessment'=>'0',
                'leftnav-neurohormonal-retest'=>'0');
            $userCycleObj = new UserCycles();
            $userData = $userCycleObj->GetData($route[4]);
            if(isset($userData[0])){
                $user_id = $userData[0]['user_id'];
                $userLifeStyleObj = new UserLifestyle();
                $userLifeStyleData = $userLifeStyleObj->getUserLifestyleData($user_id,$route[5]);
                $lifeStyleData = isset($userLifeStyleData['form_json']) ? json_decode($userLifeStyleData['form_json'],true) : [];
                $userSmartLogObj = new UserSmartLog();
                $log_length = $userSmartLogObj->GetLogLength($user_id);
                $userAssessmentObj = new UserAssessments();
                $assessmentComplete = $userAssessmentObj->isAssessmentComplete($route[4]);
                $cycle_num = intval($log_length/14);
                $count = 0;
                foreach($lifeStyleData as $key=>$val){
                    if(strlen($val)>0){
                        $count++;
                    }
                }

                $days_in_cycle = intval($log_length % 14);

                if ($count > 16) {
                    $states['leftnav-recommendations'] = 1;
                    $states['leftnav-smartlog'] = 1;
                    if($days_in_cycle  > 11 || !$assessmentComplete){
                        $states['leftnav-moodassessment'] = 1;
                    }
                    if($log_length  > 13){
                        $states['leftnav-progress'] = 1;
                    }
                    if($log_length > 49){ //at the 7th week
                        $states['leftnav-neurohormonal-retest'] = 1;
                    }
                }
            }

            echo json_encode(array("data"=>$states));
        } else {
            http_response_code(400);
            echo json_encode(array("error"=>"Missing cycle_id or kit_id"));
        }
        break;


    default:
        http_response_code(400);
        echo json_encode(array("error"=>"Method not found"));

}


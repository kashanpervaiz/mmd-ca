<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"]) . "/includes/phpHeader.php";
$userSummaryObj = new UserSummary();

if(!isset($route[3])) {
    echo json_encode("No method sent");
    exit;
}

$method = $route[3];
switch($method){
    case $method == "getsummaries":
        if(isset($route[4])){
            echo json_encode(array("data"=>$userSummaryObj->GetUserSummaryData(intval($route[4]))));
        } else {
            http_response_code(400);
            echo json_encode(array("error"=>"Missing user id"));
        }
        break;

    default:
        http_response_code(400);
        echo json_encode(array("error"=>"Method not found"));

}


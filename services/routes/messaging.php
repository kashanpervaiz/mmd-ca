<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"]) . "/includes/phpHeader.php";
require_once realpath($_SERVER["DOCUMENT_ROOT"]).'/vendor/autoload.php';
use Twilio\Rest\Client;


if(!isset($route[3])) {
    echo json_encode("No method sent");
    exit;
}

$method = $route[3];
switch($method){

    case $method == "send_dashboard_sms":
        $body = $_POST;
        if(empty($_POST)){
            $body =  json_decode(file_get_contents("php://input"),true);
        }
        if(!isset($body)){
            http_response_code(400);
            echo json_encode(array("error"=>"Missing Data","body"=>$body));
            exit;
        }
        if(isset($body['sms_to']) && isset($body['sms_from']) && isset($body['user_id']) /*&& isset($body['sms_message'])*/){

            $account_sid = 'ACf9627284a5c108a941b22d965c79fa3c';
            $auth_token = 'e5a0ce0a97e6bde724a13f1b1b49e9e2';
            $twilio_number = "+17789495070";
            $userDataObj = new UserData();
            $user_data = $userDataObj->GetUserData($body['user_id']);

            $sms_to = $body['sms_to'] == 'coaching'? '+17788743367':'+17789849131'; // Ian : Kevin
            $sms_from = $body['sms_from'];

            $twilio = new Client($account_sid,  $auth_token);

            $sms_message1 = "MoodMD text request: ".$user_data['firstname']." ".$user_data['lastname']." is requesting you text them back at ". $sms_from;
            $message1 = $twilio->messages->create(
                $sms_to, // to
                [
                    "body" => $sms_message1,
                    "from" => $twilio_number
                ]
            );

            $sms_message2 = "This is MoodMD. Your request was received and our staff will contact you soon.";
            $message2 = $twilio->messages->create(
                $sms_from, // to
                [
                    "body" => $sms_message2,
                    "from" => $twilio_number
                ]
            );

            if(isset($message1->sid) && isset($message2->sid)){
                echo json_encode(array("data" => array("ids" => [$message1->sid,$message2->sid])));
            }else{
                http_response_code(400);
                $errors[] = $message1->error_message;
                $errors[] = $message2->error_message;
                echo json_encode(array("error" => "MySQL Errors: ".json_encode($errors)));
            }

        }else{
            http_response_code(400);
            echo json_encode(array("error"=>"Missing one or more of the fields: 'user_id', 'sms_to', 'sms_from'"));
        }
        break;

    default:
        http_response_code(400);
        echo json_encode(array("error"=>"Method not found"));

}


<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"]) . "/includes/phpHeader.php";
use \DrewM\MailChimp\MailChimp;

$user_obj = new User();

if(!isset($route[3])) {
    echo json_encode("No method sent");
    exit;
}

$method = $route[3];
switch($method){

    case $method == "confirm":
        $body = $_POST;
        if(empty($_POST)){
            $body =  json_decode(file_get_contents("php://input"),true);
        }

        if(!isset($body)){
            http_response_code(400);
            echo json_encode(array("error"=>"Missing Data","body"=>$body));
            exit;
        }

        //$body = urldecode(base64_decode($body['data']));

        //update password
        if(isset($body['password']) && strlen($body['password'])>0 && isset($body['email']) && strlen($body['email'])>0){

           // if(isset($body['invitation_code']) && $body['invitation_code'] == 'healthy' ){
                if(isset($body['recaptcha_response'])){

                    // Build POST request:
                    $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
                    $recaptcha_secret = $_SESSION['recaptcha_v3']['secret_key'];
                    $recaptcha_response = $body['recaptcha_response'];

                    // Take action based on the score returned:
                    if($user_obj->isUniqueEmail($body['email'])){

                        // Make and decode POST request:
                        $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $_SESSION['recaptcha_v3']['secret_key'] . '&response=' . $recaptcha_response);
                        $recaptcha = json_decode($recaptcha, true);

                        if(isset($recaptcha['success']) && $recaptcha['success'] == true && isset($recaptcha['score']) && $recaptcha['score'] >= 0.5){
                            $new_data = ["email" => $body['email'], "password" => $body['password']];
                            $response_u = $user_obj->Add($new_data, [], false);
                            if(!isValidUserId($response_u)){ //could not add so try update
                                http_response_code(400);
                                echo json_encode(array("error" => "$response_u"));
                            }else{
                                $user_data = [];
                                if(isset($body['firstname'])){
                                    $user_data['firstname'] = $body['firstname'];
                                }
                                if(isset($body['lastname'])){
                                    $user_data['lastname'] = $body['lastname'];
                                }
                                if(isset($body['postalcode'])){
                                    $user_data['postalcode'] = $body['postalcode'];
                                }
                                if(isset($body['countrycode'])){
                                    $user_data['countrycode'] = $body['countrycode'];
                                }
                                if(isset($body['lead_source'])){
                                    $user_data['lead_source'] = $body['lead_source'];
                                }
                                if(isset($body['emailVerified'])){
                                    $user_data['emailVerified'] = $body['emailVerified'];
                                }
                                if(isset($body['affiliation'])){
                                    $user_data['affiliation'] = $body['affiliation'];
                                }
                                if(count($user_data) > 0){
                                    $user_data['user_id'] = $response_u;
                                    $userDataObj = new UserData();
                                    $userDataObj->Add($user_data, [], false);
                                }
                                $userCycleObj = new UserCycles();
                                $cycle_id = $userCycleObj->Add(["user_id" => $response_u], ["id", "firstname", "lastname"], false);
                                if($cycle_id > 0){
                                    
                                    # Subscribe to newsletter
                                    # Init Mailchimp API
                                    $mailchimp = new MailChimp('645700ebf19003da165836c0acd45055-us4');
                                    $audience_list_id = '9d850eae3d'; # a157e50934 for testing only
                                    //live audience list: 9d850eae3d
                                    $result = $mailchimp->post("lists/$audience_list_id/members",
[
                                        'email_address' => strtolower($body['email']),
                                        'merge_fields' => [
                                            'FNAME' => $user_data['firstname'],
                                            'LNAME' => $user_data['lastname'],
                                        ],
                                        'status' => 'subscribed',
                                    ]);

                                    #send welcome email

                                    $firstName = $user_data['firstname'];
                                    $msgParams = [
                                    'first_name' => $firstName
                                    ];

                                    sendEMail($user_data['user_id'], "moodMD <info@moodmd.com>", "1a-welcome-to-moodmd", $msgParams);

                                    echo json_encode(array("data" => $cycle_id));
                                }else{
                                    http_response_code(400);
                                    echo json_encode(array("error" => "$cycle_id"));
                                }
                                openSession($response_u);
                            }
                        }else{
                            echo json_encode(array("error" => "Recaptcha Error. Refresh your browser and try again"));
                        }

                    }else{
                        echo json_encode(array("error" => "Email Already In Use"));
                    }


                }else{
                    http_response_code(400);
                    echo json_encode(array("error" => "Missing data"));
                }
            //}else{
            //    echo json_encode(array("error" => "Invalid Invitation Code"));
           // }


        } else {
            http_response_code(400);
            echo json_encode(array("error"=>"Missing data"));
        }
        break;

    case $method == "replace":
        $body = $_POST;
        if(empty($_POST)){
            $body =  json_decode(file_get_contents("php://input"),true);
        }

        if(!isset($body)){
            http_response_code(400);
            echo json_encode(array("error"=>"Missing Data","body"=>$body));
            exit;
        }

        $body = urldecode(base64_decode($body['data']));

        //update password
        if(isset($body['password']) && strlen($body['password'])>0 && isset($body['email']) && strlen($body['email'])>0){
            $new_data = ["email"=>$body['email'],"password"=>$body['password']];
            $response_u = $user_obj->Add($new_data,[],false);
            if(isValidUserId($response_u)){ //could not add so try update
                $user_id= $user_obj->GetUserIdFromEmail($body['email']);
                $new_data['user_id'] = $user_id;
                $response_u = $user_obj->Update($new_data,[],false);
                if(!isValidUserId($response_u)){
                    http_response_code(400);
                    echo json_encode(array("error"=>"MySQL Error Code: $response_u"));
                } else {
                    echo json_encode(array("data"=>"$user_id"));
                }
            } else {
                echo json_encode(array("data"=>"$response_u"));
            }
        } else {
            http_response_code(400);
            echo json_encode(array("error"=>"Missing data"));
        }
        break;

    default:
        http_response_code(400);
        echo json_encode(array("error"=>"Method not found"));

}



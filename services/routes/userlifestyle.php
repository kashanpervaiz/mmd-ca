<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"]) . "/includes/phpHeader.php";
$data_obj = new UserLifestyle();

if(!isset($route[3])) {
    echo json_encode("No method sent");
    exit;
}

$method = $route[3];
switch($method){
    case $method == "getuserdata":
        if(isset($route[4])){
            if(isset($route[5])){
                echo json_encode(array("data"=>$data_obj->getUserLifestyleData(intval($route[4]),intval($route[5]))));
            } else {
                http_response_code(400);
                echo json_encode(array("error"=>"Missing kit id"));
            }

        } else {
            http_response_code(400);
            echo json_encode(array("error"=>"Missing user id"));
        }
        break;

    case $method == "update":
    $body = $_POST;
    if(empty($_POST)){
        $body =  json_decode(file_get_contents("php://input"),true);
    }

    if(!isset($body)){
        http_response_code(400);
        echo json_encode(array("error"=>"Missing Data","body"=>$body));
        exit;
    }
    if(isset($body['id'])){
        $response = $data_obj->Update($body,[],false);
        if ($response > 0) {
            echo json_encode(array("data" => array("id" => $response)));
        } else {
            http_response_code(400);
            echo json_encode(array("error"=>"MySQL Error Code: $response"));
        }
    } else {
        http_response_code(400);
        echo json_encode(array("error"=>"Missing user id"));
    }
    break;

    case $method == "setuserdata":
        $body = $_POST;
        if(empty($_POST)){
            $body =  json_decode(file_get_contents("php://input"),true);
        }

        if(!isset($body)){
            http_response_code(400);
            echo json_encode(array("error"=>"Missing Data","body"=>$body));
            exit;
        }
        if(isset($body['user_id'])){
            $response = $data_obj->setUserLifestyleData($body);
            if ($response > 0) {
                echo json_encode(array("data" => array("id" => $response)));
            } else {
                http_response_code(400);
                echo json_encode(array("error"=>"MySQL Error Code: $response"));
            }
        } else {
            http_response_code(400);
            echo json_encode(array("error"=>"Missing user id"));
        }
        break;

    case $method == "getuserslifestylehabits":
        $ls_frequency = array("-1" => "","0" => "No","1"=> "Yes");
        $ls_frequency_multiple = array("-1" => "","0" => "Never","1"=> "Almost Never","2"=> "Sometimes","3"=> "Fairly Often","4"=> "Very Often");
        $exercise_frequency = array("-1" => "","0" => "0-50","1"=> "51-75","2" => "76-100","3" => "101-149","4"=> "150+");
       if(isset($route[4])){

            $results = array();
            $pagetestObj = new PageText();
            $language = isset($route[5]) ? $route[5] : 'en';
            $questions = $pagetestObj->GetPageText("lifestyle_questions",$language);
            $user_data = $data_obj->getAllUserData($route[4],'ASC');
            foreach($user_data as $index => $row){
               $result_row = array();
               $result_row['id'] = $row['id'];
               $result_row['position'] = count($user_data) - $index;
               $result_row['date'] = formatToUSDate($row['date_submitted']);
               $form_data = json_decode($row['form_json'],true);
               $question_list = array();
               foreach($form_data as $question_key => $val){
                   if(in_array($question_key,['user_id','kit_id'])){continue;}
                   //$answer = strpos($question_key,'exercise') === false ? $ls_frequency[$val] : $exercise_frequency[$val] ;

                    if(in_array($question_key,['exercise_q1'])){
                        $answer = $exercise_frequency[$val];
                    }elseif(in_array($question_key,['exercise_q2','diet_q9','diet_q10','sleep_q9','sleep_q10',])){
                        $answer = $ls_frequency_multiple[$val];
                    }else{ // yes /no
                        $answer = $ls_frequency[$val];
                    }

                   if (isset($questions[$question_key])){
                       $question_list[] ='<tr><td class="assessment_question>">' . $questions[$question_key] . '</td><td class="assessment_answer">' . $answer . '</td></tr>';
                   }
               }
                $result_row['questions'] = $question_list;
                $results[$index + 1]= $result_row;
            }
           echo json_encode(array("data" => $results));

       } else{
           http_response_code(400);
           echo json_encode(array("error"=>"Missing user id"));
       }
        break;

    default:
        http_response_code(400);
        echo json_encode(array("error"=>"Method not found"));

}
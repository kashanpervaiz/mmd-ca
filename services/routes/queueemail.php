<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"]) . "/includes/phpHeader.php";
$data_obj = new EmailQueue();

if(!isset($route[3])) {
    echo json_encode("No method sent");
    exit;
}

$method = $route[3];
switch($method){

    case $method == "queue":
        $body = $_POST;
        if(empty($_POST)){
            $body =  json_decode(file_get_contents("php://input"),true);
        }
        if(!isset($body)){
            http_response_code(400);
            echo json_encode(array("error"=>"Missing Data","body"=>$body));
            exit;
        }
        if(isset($body['send_to']) && isset($body['send_subject']) && isset($body['message_body']) && isset($body['send_from'])){
            $send_to = $body['send_to'];
            $send_subject = $body['send_subject'];
            $message_body = $body['message_body'];
            $send_from = $body['send_from'];
            $send_cc = isset($body['send_cc'])? $body['send_cc'] : '';
            $send_bcc = isset($body['send_bcc'])? $body['send_bcc'] : '';
            $other_headers = isset($body['other_headers'])? $body['other_headers'] : [];

            $response = $data_obj->QueueMessage($send_to, $send_subject, $message_body, $send_from, $send_cc, $send_bcc, $other_headers);
            if($response > 0){
                echo json_encode(array("data" => array("id" => $response)));
            }else{
                http_response_code(400);
                echo json_encode(array("error" => "MySQL Error Code: $response"));
            }
        }else{
            http_response_code(400);
            echo json_encode(array("error"=>"Missing one or more of the fields: 'send_to', 'send_from', 'send_subject' or 'message_body'"));
        }
        break;

    default:
        http_response_code(400);
        echo json_encode(array("error"=>"Method not found"));

}
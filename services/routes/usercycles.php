<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"]) . "/includes/phpHeader.php";
$userCyclesObj = new UserCycles();

if(!isset($route[3])) {
    echo json_encode("No method sent");
    exit;
}

$method = $route[3];
switch($method){
    case $method == "getcycles":
        if(isset($route[4])){
            echo json_encode(array("data"=>$userCyclesObj->GetUserCycles(intval($route[4]))));
        } else {
            http_response_code(400);
            echo json_encode(array("error"=>"Missing user id"));
        }
        break;

    case $method == "gettimelinedata":
        if(isset($route[4]) && isset($route[5])){ //user_id  cycle_id
            $cycles = $userCyclesObj->GetUserCycles(intval($route[4]));
            $userRecsObj = new UserCustomRecommendations();
            $kitDataObj = new KitData();
            $row_count = 0;
            $kit_date_ranges = $kitDataObj->getKitDateRanges($route[4]);

            $intervention_list = ['testkit'];
            $last_kit_id = "";
            $initial_start_defaulted = false;

            foreach($cycles as &$cycle){

                $kit_data = $kitDataObj->GetUserTestKitForCycleId($cycle['id']);
                if(isset($kit_data['id']) && $kit_data['id'] != $last_kit_id){
                    $last_kit_id = $kit_data['id'];
                }
                $cycle['kit_id'] = $last_kit_id;
                $cycle['cycle_id'] = $cycle['id'];

                if($cycle['start_date'] == 0 && !isset($cycle['start_date'])){
                    $today_obj = new DateTime();
                    $kit_ready_date_obj = new DateTime($kit_data['timestamp_ready']);
                    $diff = abs($today_obj->diff($kit_ready_date_obj)->format('%a'));
                    $kit_end_date_obj = new DateTime($kit_data['timestamp_ready']." +".max(27,$diff)."days");
                    $cycle['start_date'] = $kit_ready_date_obj->format('Y-m-d');
                    $cycle['end_date'] = $kit_end_date_obj->format('Y-m-d');
                    $initial_start_defaulted = true;
                }

                $start_date_obj = new DateTime($cycle['start_date']);
                $cycle['start_day'] = $start_date_obj->format('jS');
                $end_date_obj = new DateTime($cycle['end_date']);
                $cycle['end_day'] = $end_date_obj->format('jS');

                if(count($kit_data) > 0){

                    $ending_cycle_id = $kit_date_ranges[$kit_data['id']]['cycle_id'];
                    $ending_cycle_num = $kit_date_ranges[$kit_data['id']]['cycle_num'];
                    $end_date = isset($kit_date_ranges[$kit_data['id']]['end_date'])? $kit_date_ranges[$kit_data['id']]['end_date'] : $cycle['end_date'];
                    $interventions = array(
                       array('title'=>'testkit','kitid'=>$kit_data['id'],'barcode'=>$kit_data['barcode'],
                           'start_date'=>$cycle['start_date'],'end_date'=>$end_date,'ending_cycle_id'=>$ending_cycle_id,'ending_cycle_num'=>$ending_cycle_num),
                    );

                } else {
                    $interventions = array(
                        array('title'=>'testkit','kitid'=>'','barcode'=>'','start_date'=>'','end_date'=>'','ending_cycle_id'=>'','ending_cycle_num'=>''),
                    );
                }


                $user_rec = $userRecsObj->GetCustomRec(intval($cycle['id']));
                $recs = isset($user_rec['recommendations_json'])?json_decode($user_rec['recommendations_json'],true):[];

                if( (isset($recs[1]) || isset($recs[2]) || isset($recs[3])) ){
                    $interventions[]=array('title'=>'lifestyle','start_date'=>$cycle['start_date'],'end_date'=>$cycle['end_date'],'defaulted'=>$initial_start_defaulted);

                    if(!in_array('lifestyle',$intervention_list)){$intervention_list[] = 'lifestyle';}
                }

                if( isset($recs[4]) ){
                    $interventions[]=array('title'=>'therapy','start_date'=>$cycle['start_date'],'end_date'=>$cycle['end_date'],'defaulted'=>$initial_start_defaulted);

                    if(!in_array('therapy',$intervention_list)){$intervention_list[] = 'therapy';}
                }
                if( isset($recs[5]) ){
                    $interventions[]=array('title'=>'medication','start_date'=>$cycle['start_date'],'end_date'=>$cycle['end_date'],'defaulted'=>$initial_start_defaulted);
                    if(!in_array('medication',$intervention_list)){$intervention_list[] = 'medication';}

                }
                //tbd: allow for other interventions

                $cycle['interventions'] = $interventions;
                if(count($interventions) > $row_count){$row_count = count($interventions);}

                unset($cycle['id']);
            }

            echo json_encode(array("data"=>array('rows'=>$row_count,'interventions'=>$intervention_list,'cycles'=>$cycles)));


        } else {
            http_response_code(400);
            echo json_encode(array("error"=>"Missing user id"));
        }
        break;

    case $method == "gettimelinedatabyyear":
        if(isset($route[4]) && isset($route[5])){ //user_id  cycle_id
            $cycles = $userCyclesObj->GetUserCycles(intval($route[4]));
            $userRecsObj = new UserCustomRecommendations();
            $kitDataObj = new KitData();
            $row_count = 0;
            $kit_date_ranges = $kitDataObj->getKitDateRanges($route[4]);

            $intervention_list = ['testkit'];
            $yearly_cycles = array();
            $last_kit_id = "";
            $initial_start_defaulted = false;

            foreach($cycles as &$cycle){
                $kit_data = $kitDataObj->GetUserTestKitForCycleId($cycle['id']);
                if(isset($kit_data['id']) && $kit_data['id'] != $last_kit_id){
                    $last_kit_id = $kit_data['id'];
                }
                $cycle['kit_id'] = $last_kit_id;
                $cycle['cycle_id'] = $cycle['id'];

                if($cycle['start_date'] == 0 && !isset($cycle['start_date'])){
                    $today_obj = new DateTime();
                    $kit_ready_date_obj = new DateTime($kit_data['timestamp_ready']);
                    $diff = abs($today_obj->diff($kit_ready_date_obj)->format('%a'));
                    $kit_end_date_obj = new DateTime($kit_data['timestamp_ready']." +".max(27,$diff)."days");
                    $cycle['start_date'] = $kit_ready_date_obj->format('Y-m-d');
                    $cycle['end_date'] = $kit_end_date_obj->format('Y-m-d');
                    $initial_start_defaulted = true;
                }

                $start_date_obj = new DateTime($cycle['start_date']);
                $cycle['start_day'] = $start_date_obj->format('jS');
                $cycle['start_year'] = $start_date_obj->format('Y');
                $end_date_obj = new DateTime($cycle['end_date']);
                $cycle['end_day'] = $end_date_obj->format('jS');
                $cycle['end_year'] = $end_date_obj->format('Y');
                if(!isset($yearly_cycles[$cycle['start_year']])){
                    $yearly_cycles[$cycle['start_year']]=[];
                }
                if(!isset($yearly_cycles[$cycle['end_year']])){
                    $yearly_cycles[$cycle['end_year']]=[];
                }

                if(count($kit_data) > 0){
                    $ending_cycle_id = $kit_date_ranges[$kit_data['id']]['cycle_id'];
                    $ending_cycle_num = $kit_date_ranges[$kit_data['id']]['cycle_num'];
                    $end_date = isset($kit_date_ranges[$kit_data['id']]['end_date'])? $kit_date_ranges[$kit_data['id']]['end_date'] : $cycle['end_date'];
                    $interventions = array(
                        array('title'=>'testkit','kitid'=>$kit_data['id'],'barcode'=>$kit_data['barcode'],
                            'start_date'=>$cycle['start_date'],'end_date'=>$end_date,'ending_cycle_id'=>$ending_cycle_id,'ending_cycle_num'=>$ending_cycle_num),
                    );

                } else {
                    $interventions = array(
                        array('title'=>'testkit','kitid'=>'','barcode'=>'','start_date'=>'','end_date'=>'','ending_cycle_id'=>'','ending_cycle_num'=>''),
                    );
                }

                $user_rec = $userRecsObj->GetCustomRec($cycle['id']);
                $recs = isset($user_rec['recommendations_json'])?json_decode($user_rec['recommendations_json'],true):[];

                if( (isset($recs[1]) || isset($recs[2]) || isset($recs[3])) ){
                    $interventions[]=array('title'=>'lifestyle','start_date'=>$cycle['start_date'],'end_date'=>$cycle['end_date'],'defaulted'=>$initial_start_defaulted);

                    if(!in_array('lifestyle',$intervention_list)){$intervention_list[] = 'lifestyle';}
                }

                if( isset($recs[4]) ){
                    $interventions[]=array('title'=>'therapy','start_date'=>$cycle['start_date'],'end_date'=>$cycle['end_date'],'defaulted'=>$initial_start_defaulted);
                    if(!in_array('therapy',$intervention_list)){$intervention_list[] = 'therapy';}
                }

                if( isset($recs[5]) ){
                    $interventions[]=array('title'=>'medication','start_date'=>$cycle['start_date'],'end_date'=>$cycle['end_date'],'defaulted'=>$initial_start_defaulted);
                    if(!in_array('medication',$intervention_list)){$intervention_list[] = 'medication';}

                }
                //tbd: allow for other interventions
                $cycle['interventions'] = $interventions;
                if(count($interventions) > $row_count){$row_count = count($interventions);}


                //Break out the yearly data

                if($cycle['start_year'] != $cycle['end_year']){
                    $cycle_y1 = $cycle;
                    $cycle_y2 = $cycle;

                    $cycle_y1['end_day'] = '31';
                    $cycle_y1['end_year'] = $cycle['start_year'];
                    $cycle_y1['end_date'] = $cycle['start_year']."-12-31";
                    $yearly_cycles[$cycle['start_year']]['rows'] = $row_count;
                    $yearly_cycles[$cycle['start_year']]['interventions'] = $intervention_list;
                    $yearly_cycles[$cycle['start_year']]['cycles'][] = $cycle_y1;

                    $cycle_y2['start_day'] = '01';
                    $cycle_y2['start_year'] = $cycle['end_year'];
                    $cycle_y2['start_date'] = $cycle['end_year']."-01-01";
                    $yearly_cycles[$cycle['end_year']]['rows'] = $row_count;
                    $yearly_cycles[$cycle['end_year']]['interventions'] = $intervention_list;
                    $yearly_cycles[$cycle['end_year']]['cycles'][] = $cycle_y2;
                } else {
                    $yearly_cycles[$cycle['start_year']]['rows'] = $row_count;
                    $yearly_cycles[$cycle['start_year']]['interventions'] = $intervention_list;
                    $yearly_cycles[$cycle['start_year']]['cycles'][] = $cycle;
                }
                unset($cycle['id']);
            }

            echo json_encode(array("data"=>$yearly_cycles));


        } else {
            http_response_code(400);
            echo json_encode(array("error"=>"Missing user id"));
        }
        break;


    default:
        http_response_code(400);
        echo json_encode(array("error"=>"Method not found"));

}
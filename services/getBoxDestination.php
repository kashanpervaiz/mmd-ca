<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"])."/includes/phpHeader.php";
$result = array();
$kitDataObj = new Box();
$destinationObj = new Destination();


if(isset($_POST['box_id']) and isset($_POST['justName']))
{
	$kitData = $kitDataObj->GetData($_POST['box_id']);
	$destinationData = $destinationObj->GetData($kitData[0]['destination_id']);
	echo json_encode($destinationData[0]['name']);
}
elseif(isset($_POST['box_id']))
{
	$result = $kitDataObj->GetData($_POST['box_id']);
	echo json_encode($result[0]['destination_id']);
}

?>
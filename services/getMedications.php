<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"])."/includes/phpHeader.php";
$result = array();

if(isset($_POST['userid']))
{
	$userMedicationObj = new UserMedications();
	$result = $userMedicationObj->GetUserMedications($_SESSION['userid']);	
}

echo json_encode($result);
?>
<?php

return [
    'paths' => [
        'migrations' => '%%PHINX_CONFIG_DIR%%/migrations',
        'seeds' => '%%PHINX_CONFIG_DIR%%/seeds'
    ],
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_database' => 'local',
        'local' => [
            'adapter' => 'mysql',
            'host' => 'mysql',
            'name' => 'moodmddb_loc',
            'user' => 'root',
            'pass' => 'root',
            'port' => 3306,
            'charset' => 'utf8',
        ],
        # TODO: add more environments here
    ],
    'version_order' => 'creation'
];

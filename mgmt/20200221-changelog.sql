INSERT INTO `pagetext` (`pagename`, `elementname`, `value`, `language`, `enabled`, `lastmod_utc`) VALUES
('emails', 'assessment_complete_admin_subject', 'Lifestyle assessment completed', 'en', 1, NOW());

INSERT INTO `pagetext` (`pagename`, `elementname`, `value`, `language`, `enabled`, `lastmod_utc`) VALUES
('emails', 'assessment_complete_admin_body', '%%firstname%% %%lastname%%<br><br>stress_score: %%stress_score%%<br>anxiety_score: %%anxiety_score%%<br>depression_score: %%depression_score%%', 'en', 1, NOW());

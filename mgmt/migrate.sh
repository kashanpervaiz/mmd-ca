#!/bin/bash

# Ensure working directory is app root
cd "$(dirname "$0")/"

# Run db migration
PHP_COMMAND="/usr/local/bin/php";

echo "Running database migrations..."

MIGRATION_OUTPUT=$(exec ${PHP_COMMAND} ../vendor/bin/phinx migrate -c ./phinx.php)

echo "$MIGRATION_OUTPUT"

if [ $? = "0" ] ; then
    echo " - Done!"
else
    echo "ERROR! Something failed during db migration, please check for errors"
    exit 1
fi

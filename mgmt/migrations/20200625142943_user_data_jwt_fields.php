<?php

use Phinx\Migration\AbstractMigration;

class UserDataJwtFields extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('user_data')->addColumn('token', 'string', [
            'limit' => 255,
            'null' => true,
            'after' => 'user_id',
            'default' => null
        ])->addColumn('token_expiration', 'datetime', [
            'null' => true,
            'after' => 'token',
            'default' => null
        ])->update();
    }
}

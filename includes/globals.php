<?php

$GLOBALS['corePath'] = realpath($_SERVER["DOCUMENT_ROOT"]);
$GLOBALS['assetsURL']   =  "https://assets.moodmd.com";

if(is_ssl())
{
 $GLOBALS['coreURL']   =  "https://".$_SERVER["SERVER_NAME"];
}
else
{
 $GLOBALS['coreURL']   =  "http://".$_SERVER["SERVER_NAME"];
}

$serverName = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : "www.moodmd.com";
switch(true){
    case strpos($serverName, 'loc.') !== false:
        $GLOBALS['env'] = "loc";
        break;
    case strpos($serverName, 'dev.') !== false:
        $GLOBALS['env'] = "dev";
        break;
    case strpos($serverName, 'dev1.') !== false:
        $GLOBALS['env'] = "dev1";
        break;
    case strpos($serverName, 'demo.') !== false:
        $GLOBALS['env'] = "demo";
        break;
    case strpos($serverName, 'stage.') !== false:
        $GLOBALS['env'] = "stage";
        break;
    default:
        $GLOBALS['env'] = "www";
}


//Useage: $GLOBALS[$appUrlKey]or $GLOBALS['coreURL'] for getting to core.

$_days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday'); // 0 (for Sunday) through 6 (for Monday) 
$GLOBALS['days']  = $_days;

///////////////////////////Global Funtions/////////////////////////
function is_ssl()
{
    if(isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && strtolower($_SERVER['HTTP_X_FORWARDED_PROTO']) == 'https'){
        return true;
    }elseif ( isset($_SERVER['HTTPS']) ){
        if ( 'on' == strtolower($_SERVER['HTTPS']) )
            return true;
        if ( '1' == $_SERVER['HTTPS'] )
            return true;
    } elseif ( isset($_SERVER['SERVER_PORT']) && ( '443' == $_SERVER['SERVER_PORT'] ) ){
        return true;
    }
    return false;
}	
	
function timezoneExhibitsDST($tzId) {
    $tzId = trim($tzId);
    $tz = new DateTimeZone($tzId);
    $date = new DateTime("now",$tz);  
    $trans = $tz->getTransitions();
    $result = false;
    foreach ($trans as $k => $t) 
      if ($t["ts"] > $date->format('U')) {
          $result = $trans[$k-1]['isdst'];    
    }
    return $result;
}

function array2table($arr,$width)
   {
   $count = count($arr); 
   if($count > 0){
       reset($arr);
       $num = count(current($arr));
       echo ("<table align=\"center\" border=\"1\"cellpadding=\"5\" cellspacing=\"0\" width=\"$width\">\n");
       echo ("<tr>\n");
       foreach(current($arr) as $key => $value){
           echo "<th>";
           echo $key."&nbsp;";
           echo "</th>\n";  
           }  
       echo "</tr>\n";
       while ($curr_row = current($arr)) {
           echo "<tr>\n";
           $col = 1;
           while (false !== ($curr_field = current($curr_row))) {
               echo "<td>";
               echo $curr_field."&nbsp;";
               echo "</td>\n";
               next($curr_row);
               $col++;
               }
           while($col <= $num){
               echo "<td>&nbsp;</td>\n";
               $col++;      
           }
           echo "</tr>\n";
           next($arr);
           }
       echo "</table>\n";
       }
   }
   

	
	function makeUnixFriendlyName($rawName)
	{
			$betterName = iconv('UTF-8', 'ASCII//IGNORE', $rawName);
			$badChars = array(' ','~','`','!','@','#','%','&',';',':','=','.',',','<','>','/','(',')','$','^','*','-','+','{','}','[',']','?','\\','\'');
			$betterName = str_replace($badChars,"_",$betterName);
			$betterName = strtolower  ($betterName);
			return $betterName;
	}

	function stripNewLines($text)
	{
		return str_replace(array("\r\n", "\r", "\n", "\t"), ' ', $text);
	}
	
	function getLocalDateTimeFromDBTimestamp($timestamp)
	{
		$timezoneName = isset($_SESSION['timezoneName']) && strlen($_SESSION['timezoneName'])>0 ? $_SESSION['timezoneName'] : "Etc/GMT";
		$date = new DateTime($timestamp, new DateTimeZone("Etc/GMT"));
		$date->setTimezone(new DateTimeZone($timezoneName));
		$utc =  $date->format('Y-m-d H:i:sP') ;
		$ts  =  $date->format('Y-m-d H:i:s') ;
		$ymd =  $date->format('Y-m-d') ;
		$hms =  $date->format('H:i:s') ;
		$commonDate =  $date->format('m/d/Y') ; //March 10, 2001
		$prettyDay =  $date->format('l, F j') ;  //Saturday, Mar 10
		$prettyTime =  $date->format('g:i a') ;  //5:16 pm
		$dayOfWeek =  $date->format('w') ;
		
		$result = array();
		$result['utc'] = $utc;
		$result['ts'] = $ts;
		$result['ymd'] = $ymd;
		$result['hms'] = $hms;
		$result['commonDate'] = $commonDate;
		$result['prettyDay'] = $prettyDay;
		$result['prettyTime'] = $prettyTime;
		$result['dayOfWeek'] = $dayOfWeek;
		return $result;
	}	
	
	function getGMTFromLocalTimeStamp($timestamp)
	{
		$timezoneName = isset($_SESSION['timezoneName']) && strlen($_SESSION['timezoneName'])>0 ? $_SESSION['timezoneName'] : "Etc/GMT";
		$date = new DateTime($timestamp, new DateTimeZone($timezoneName));
		$date->setTimezone(new DateTimeZone("Etc/GMT"));
		$utc =  $date->format('Y-m-d H:i:sP') ;
		$ts  =  $date->format('Y-m-d H:i:s') ;
		$ymd =  $date->format('Y-m-d') ;
		$hms =  $date->format('H:i:s') ;
		$commonDate =  $date->format('m/d/Y') ; //March 10, 2001
		$prettyDay =  $date->format('l, F j') ;
		$prettyTime =  $date->format('g:i a') ;	
		$dayOfWeek =  $date->format('w') ;
		
		
		$result = array();
		$result['utc'] = $utc;
		$result['ts'] = $ts;
		$result['ymd'] = $ymd;
		$result['hms'] = $hms;
		$result['commonDate'] = $commonDate;
		$result['prettyDay'] = $prettyDay;
		$result['prettyTime'] = $prettyTime;
		$result['dayOfWeek'] = $dayOfWeek;
		return $result;
	}	
?>
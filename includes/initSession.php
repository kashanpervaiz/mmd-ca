<?php
//if (! isset($_SERVER['HTTPS']) or $_SERVER['HTTPS'] == 'off' ) {
//    $redirect_url = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
//    header("Location: $redirect_url");
//    exit();
//}
use Aws\Exception\AwsException;
use Aws\SecretsManager\SecretsManagerClient;

header ("Expires: ".gmdate("D, d M Y H:i:s", time())." GMT");
header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");  
header ("Cache-Control: no-cache, must-revalidate");  
header ("Pragma: no-cache"); 
session_start();
 
date_default_timezone_set('Etc/GMT');

$scriptName = $_SERVER['SCRIPT_NAME'];
if($scriptName[0] == "/"){$scriptName = ltrim($scriptName, '/');}
$urlParts = explode("/",$scriptName);
$appName =  count($urlParts) > 1 ? $urlParts[0] : "core";
$appName = trim($appName);

//$_SESSION['loggedIn'] = true;
//$_SESSION['userid'] = "1";
//$_SESSION['screenname'] = "dMan";
//$_SESSION['firstname'] = "Brian";
//$_SESSION['lastname'] = "Vanyo";
//$_SESSION['birthday'] = "1965-09-15";
//$_SESSION['userrole'] = "100";
//$_SESSION['gender'] = "M";	 		
//$_SESSION['language'] = "en";
//$_SESSION['preferredUnits'] = "us";
//$_SESSION['currDate'] = "";

if(!isset($_SESSION['language'])){$_SESSION['language'] = 'en';}
//////////////////////////////////////////////////////
error_reporting(E_ERROR | E_PARSE);
error_reporting(E_ALL); //debugging only
ini_set('display_errors', True);
//////////////////////////////////////////////////////

function getSecrets(){
    $env = "www";
    $allowedSubdomains = ['www', 'stage', 'loc', 'dev', 'dev1', 'dev2'];
    if(isset($_SERVER['SERVER_NAME'])){
        $serverName = $_SERVER['SERVER_NAME'];
        $server_parts = explode(".", $serverName);
        if(count($server_parts) > 2 && in_array($server_parts[0], $allowedSubdomains)){ //allowed subdomains
            $env = $server_parts[0];
        }
    }else{
        $dirname = dirname(__DIR__);
        foreach($allowedSubdomains as $subdomain){
            $testDir = "/".$subdomain;
            if(strripos($dirname,$testDir)!== false){
                $env = $subdomain;
                break;
            }
        }
    }

    //AWS Secrets
    if($env == 'www'){ $env = 'prod'; }
    if(empty($_SESSION['recaptcha_v3'])){ $_SESSION['recaptcha_v3'] = getAWSSecrets(getARN('recaptchaV3ca'));}
    if(empty($_SESSION['mandrill'])){ $_SESSION['mandrill'] = getAWSSecrets(getARN('mandrill'));}
    if(empty($_SESSION['mailchimp'])){ $_SESSION['mailchimp'] = getAWSSecrets(getARN('mailchimp'));}
    if(empty($_SESSION['database'])){ $_SESSION['database'] = getAWSSecrets(getARN($env,$env));}
    $brian = "true";
}


getSecrets();

function getAWSSecrets($secretName){

    $aws_option = [
        'version' => 'latest',
        'region'  => 'ca-central-1'
    ];

    $client = new SecretsManagerClient($aws_option);

    try {
        $result = $client->getSecretValue([
            'SecretId' => $secretName,
        ]);

    } catch (AwsException $e) {
        $error = $e->getAwsErrorCode();
        if ($error == 'DecryptionFailureException') {
            // Secrets Manager can't decrypt the protected secret text using the provided AWS KMS key.
            // Handle the exception here, and/or rethrow as needed.
            throw $e;
        }
        if ($error == 'InternalServiceErrorException') {
            // An error occurred on the server side.
            // Handle the exception here, and/or rethrow as needed.
            throw $e;
        }
        if ($error == 'InvalidParameterException') {
            // You provided an invalid value for a parameter.
            // Handle the exception here, and/or rethrow as needed.
            throw $e;
        }
        if ($error == 'InvalidRequestException') {
            // You provided a parameter value that is not valid for the current state of the resource.
            // Handle the exception here, and/or rethrow as needed.
            throw $e;
        }
        if ($error == 'ResourceNotFoundException') {
            // We can't find the resource that you asked for.
            // Handle the exception here, and/or rethrow as needed.
            throw $e;
        }
    }
    // Decrypts secret using the associated KMS CMK.
    // Depending on whether the secret is a string or binary, one of these fields will be populated.
    if (isset($result['SecretString'])) {
        $secret = json_decode($result['SecretString'],true);
    } elseif(isset($result['SecretBinary'])) {
        $secret = base64_decode($result['SecretBinary']);
    } else {
        $secret = [];
    }
    return $secret;

}

function getARN($from,$to=0){
    $arns = [
        "recaptchaV2"=>["arn:aws:secretsmanager:ca-central-1:345339713320:secret:/application/misc/google-recaptcha-v2/credentials-uEr1Sq"],
        "recaptchaV3"=>["arn:aws:secretsmanager:ca-central-1:345339713320:secret:/application/misc/google-recaptcha-v3/credentials-VErKaG"],
        "recaptchaV3ca"=>["arn:aws:secretsmanager:ca-central-1:345339713320:secret:/application/misc/google-recaptcha-v3-ca/credentials-WMuJ9Y"],
        "mandrill"=>["arn:aws:secretsmanager:ca-central-1:345339713320:secret:/application/misc/mandrill/credentials-G8DxRC"],
        "mailchimp"=>["arn:aws:secretsmanager:ca-central-1:345339713320:secret:/application/misc/mailchimp/credentials-mswqzp"],
        "loc"=>["loc"=>"arn:aws:secretsmanager:ca-central-1:345339713320:secret:/application/local/local/main/db/credentials-CCBDk0"], //locallocal
        "dev"=>["dev"=>"arn:aws:secretsmanager:ca-central-1:345339713320:secret:/application/dev/dev/main/db/credentials-OelQxY"],
        "prod"=>["prod"=>"arn:aws:secretsmanager:ca-central-1:345339713320:secret:/application/prod/prod/main/db/credentials-2m3vqe",
            "datascience"=>"arn:aws:secretsmanager:ca-central-1:345339713320:secret:/application/prod/datascience/main/db/credentials-rpvhEX"],
        "stage"=>["stage"=>"arn:aws:secretsmanager:ca-central-1:345339713320:secret:/application/stage/stage/main/db/credentials-wmQb0y",
            "dev"=>"arn:aws:secretsmanager:ca-central-1:345339713320:secret:/application/stage/dev/main/db/credentials-VErKaG",
            "prod"=>"arn:aws:secretsmanager:ca-central-1:345339713320:secret:/application/stage/prod/main/db/credentials-Vyzlf9"]
    ];

    return $arns[$from][$to] ?? "Not_Listed";
}


<?php

require_once realpath($_SERVER["DOCUMENT_ROOT"])."/vendor/autoload.php";

use Moodmd\Session\Token;

include_once realpath($_SERVER["DOCUMENT_ROOT"])."/includes/initSession.php";
include_once realpath($_SERVER["DOCUMENT_ROOT"])."/includes/globals.php";
include_once $GLOBALS['corePath']."/includes/data.php";

$maintenance_mode = (new SiteMaintenanceMode())->InMaintenanceMode('ca');
if($maintenance_mode && !(isset($_SESSION['userrole_level']) && $_SESSION['userrole_level'] >= 80 ) && basename($_SERVER["SCRIPT_FILENAME"]) != 'signin.php'){ //80 == QA
    closeSession();
    echo "<script>location.href='" . $GLOBALS['coreURL'] . "/index.html'</script>";
}

/* Test if user in open region */
$location_info = \Moodmd\Utilities\Location::GetLocationData(getRealUserIp());
$rulesObj = new AccessRules();
$isInOpenRegion = $rulesObj->isInOpenRegion($location_info['country_code'],$location_info['postal_code']);
if(!isset($_SESSION['countrycode']) || $_SESSION['countrycode'] == '') { $_SESSION['countrycode'] = strtoupper($location_info['country_code']); };
switch(strtoupper($location_info['country_code'])){
    case 'CA':
        if(!isset($_SESSION['postalcode']) || $_SESSION['postalcode'] == '') { $_SESSION['postalcode'] = strtoupper($location_info['postal_code'])." ???"; };
        if(!isset($_SESSION['preferredUnits']) || $_SESSION['preferredUnits'] == '') { $_SESSION['preferredUnits'] = "si";}
        break;
    case 'US':
        if(!isset($_SESSION['postalcode']) || $_SESSION['postalcode'] == '') { $_SESSION['postalcode'] = strtoupper($location_info['postal_code']); };
        if(!isset($_SESSION['preferredUnits']) || $_SESSION['preferredUnits'] == '') { $_SESSION['preferredUnits'] = "us";}
        break;
}

/*
if(!(($GLOBALS['env'] == 'loc') || isset($_GET['demo']) || isset($_SESSION['is_demo']))) {
//entry form
    if (isset($_POST['ef_submit'])) {
        $akOBJ = new AccessKeys();
        $unArray = $akOBJ->GetKeys();
        $keys = array_keys($unArray);
        if (in_array($_POST['ef_username'], $keys) && ($_POST['ef_password'] == $unArray[$_POST['ef_username']])) {
            $_SESSION['ef_authenticated'] = true;
        } else {
            $_SESSION['ef_authenticated'] = null;
            unset($_SESSION['ef_authenticated']);
        }
    }
    $entryform = '<!doctype html><html lang="en"><head></head><body>
    <style>
        .frmCont {margin:20px auto;text-align:center;font-family: sans-serif;color:#4d4d4d;}
        h1 {font-size:24px;text-transform:uppercase;color:#2ab5c7;margin:0;}
        p {font-size:16px;margin:10px 0;}
        p span {font-style: italic;font-size:14px;}
        form {margin:20px 0;}
        input {width:150px;height:20px;border: 1px solid #a0a09f;padding: 2px 2px 2px 5px;background-color: #fff;border-radius:5px;margin:0 3px;}
        .btn {width:auto;background-color:#2ab5c7;color:#fff;height:auto;text-transform:uppercase;padding:6px 20px;border-radius:30px;border:0;outline:none;}
    </style>
    <div class="frmCont">
        <h1>welcome participants</h1>
        <p>Please enter the website username and password that was provided to you.<br/><span class="note">(This is different from your personal account username/password)</span></p>
        <form name="entryform" id="entryform" method="post" action="' . $_SERVER['PHP_SELF'] . '" autocomplete="off">
        <input type="text" name="ef_username" value="" placeholder="username" />
        <input type="password" name="ef_password" value="" placeholder="password" />
        <input type="submit" name="ef_submit" class="btn" value="Submit"/>
        </form>
    </div></body></html>';
    if (empty($_SESSION['ef_authenticated']) && (strstr($_SERVER['REQUEST_URI'], '/apis/') === false)) {
        echo $entryform;
        die();
    }
//end entry form
} else {
    $_SESSION['ef_authenticated'] = "locuser";
    if(isset($_GET['demo'])){
        $_SESSION['is_demo'] = true;
    }
} */
$_SESSION['ef_authenticated'] = "locuser";
//echo "<pre>";print_r([$_SERVER['REQUEST_URI'],$_SERVER['SCRIPT_NAME'],$_SERVER['PHP_SELF'],]);echo "</pre>";

$safeArray = array('index.php','signin.php');
$loginRequiredArray =  array('quickstart_results.php');
//if( (!isset($_SESSION['userid']) or $_SESSION['userid'] == '') and in_array(basename($_SERVER['PHP_SELF']),$safeArray) )
if( (!isset($_SESSION['userid']) or $_SESSION['userid'] == '') and in_array(basename($_SERVER['PHP_SELF']),$loginRequiredArray) ) {
	echo \Moodmd\Session\Base::forceLoginRedirect();

} elseif( $GLOBALS['env'] == 'demo' and empty($_SESSION['demo']) and in_array(basename($_SERVER['PHP_SELF']),$loginRequiredArray) ){

    echo "<script>location.href='" . $GLOBALS['coreURL'] . "/signin.php'</script>";
    $_SESSION['demo'] = "moving_on";
}

//Record the page they were on durring a kit registration.
if(isset($_SESSION['userid']) && in_array($_SERVER['REQUEST_URI'],['/registerkit.php','/profile.php?btn=reg','/assessment.php','/lifestyleAssessment.php','/thankyou.php'])){
    $userDataObj = new UserData();
    $user_data = $userDataObj->GetUserData($_SESSION['userid']);
    $userDataObj->Update(['id'=>$user_data['id'],'request_uri'=>$_SERVER['REQUEST_URI']]);
}

$languageObj = new Languages();
$activeLanguageList = $languageObj->GetActiveLanguages();
$GLOBALS['show_language_selector'] = $languageObj->ShowLanguageSelector();

$pagetextObject = new PageText();
$headertextarray = $pagetextObject->GetPageText('header',$_SESSION['language']);
$footertextarray = $pagetextObject->GetPageText('footer',$_SESSION['language']);
$buttontextarray = $pagetextObject->GetPageText('common',$_SESSION['language']);

$HeaderKitDataObj = new KitData();
$headerReadyKitCount = (isset($_SESSION['userid']) && isValidUserId($_SESSION['userid'])) ? $HeaderKitDataObj->getReadyCountForUserId($_SESSION['userid']) : 0;
$headerUserKitCount = (isset($_SESSION['userid']) && isValidUserId($_SESSION['userid'])) ? $HeaderKitDataObj->getKitCountForUserId($_SESSION['userid']) : 0;

/*============== CAPTCHA CODE =============*/
require_once $GLOBALS['corePath']."/lib/recaptcha_1.1.2/src/autoload.php";

$formfieldsarray = $pagetextObject->GetPageText('formfields',$_SESSION['language']);

$validCaptcha = true;
$recaptchaError = "";
//$validCaptcha = false;
//$recaptchaError = "";

//if (isset($_POST['g-recaptcha-response']))
//{
//    $recaptcha = new \ReCaptcha\ReCaptcha($_SESSION['recaptcha_v3']['secret_key']);
//    $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
//    if ($resp->isSuccess())
//    {
//        $validCaptcha = true;
//    }
//    else
//    {
//        $recaptchaError = 	$formfieldsarray['indicateNotARobot'];
//    }
//}

//// Functions ////
//sendEMail("1","bavanyo@wellbeingz.com","kit_ready","");
/**
 * @todo allow multiple recipients
 *
 * @param int|string $userId
 * Integer for user id or a string for a simple email
 * @param $from
 * @param string $messageKey
 * Template prefix
 * @param array $messageParams
 */
function sendEMail($userId, $from, $messageKey, $messageParams = array())
{
    /*
    welcome
    kit_registered
    kit_ready
    */
    //$userObj = new User($userId);
    //$to = $userObj->email;
    if (isValidUserId($userId)) {
        $userObj = new User();
        $to = $userObj->GetEmail($userId);
    } else {
        $to = $userId;
    }

    // Email template should be edited through Mailchimp (and Mandrill for subject)
    // TODO: allow passing language flag
    /*$pagetextObject = new PageText();
    $subject = $pagetextObject->GetSpecificPageText('emails', $messageKey . "_subject", $_SESSION['language']);
    $message = $pagetextObject->GetSpecificPageText('emails', $messageKey . "_body", $_SESSION['language']);*/

    $merge_vars = [];
    foreach ($messageParams as $key => $val) {
        $merge_vars[] = [
            'name' => strtolower($key),
            'content' => $val
        ];
    }

    preg_match("/<(.*?)>/", $from, $matches);
    $reply_to = isset($matches[1]) ? $matches[1] : $from;

    try {
        $mandrill = new Mandrill($_SESSION['mandrill']['api_key']);
        $template = str_replace('_', '-', $messageKey);
        $mandrill_message = [
            'from_email' => $matches[1] ?? 'info@moodmd.com',
            'reply_to' => $reply_to,
            'global_merge_vars' => $merge_vars,
            'to' => [
                [
                    'email' => $to,
                    'name' => $to,
                    'type' => 'to'
                ]
            ]
        ];

        return $mandrill->messages->sendTemplate($template, [], $mandrill_message);
    } catch (Mandrill_Error $e) {
        // Mandrill errors are thrown as exceptions
        // echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
        // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
        throw $e;
    }
}

function formatToUSDate($sourceDate)
{
    $time = strtotime($sourceDate);
    //$newformat = date('F j, Y',$time);
    $newformat = date('m/d/Y',$time);
    return $newformat;  
}

function formatToDBDate($sourceDate)
{
    $time = strtotime($sourceDate);
    $newformat = date('Y-m-d',$time);
    return $newformat;
}

function formatToDBTime($sourceDate)
{
    $time = strtotime($sourceDate);
    $newformat = date('HH:MM:II',$time);
    return $newformat;
}

function setRadioChecked($a,$b){
    return $a == $b ? " checked='checked'" : "";
}

function getPost($key,$default = null)
{
	
	$return = isset($_POST[$key]) ? $_POST[$key] : $default;
	return $return;

}

function getGet($key,$default = null)
{
    $return = isset($_GET[$key]) ? $_GET[$key] : $default;
    return $return;
}

function array_to_csv_download($array, $filename = "export.csv", $delimiter=",") {
    header('Content-Type: application/csv');
    header('Content-Disposition: attachment; filename="'.$filename.'";');

    // open the "output" stream
    // see http://www.php.net/manual/en/wrappers.php.php#refsect2-wrappers.php-unknown-unknown-unknown-descriptioq
    $f = fopen('php://output', 'w');

    foreach ($array as $line) {
        fputcsv($f, $line, $delimiter);
    }
}


function camelCase($str, array $noStrip = [])
{
    // non-alpha and non-numeric characters become spaces
    $str = preg_replace('/[^a-z0-9' . implode("", $noStrip) . ']+/i', ' ', $str);
    $str = trim($str);
    // uppercase the first character of each word
    $str = ucwords($str);
    $str = str_replace(" ", "", $str);
    $str = lcfirst($str);

    return $str;
}

function getRealUserIp(){
    switch(true){
        case (!empty($_SERVER['HTTP_X_REAL_IP'])) : return $_SERVER['HTTP_X_REAL_IP'];
        case (!empty($_SERVER['HTTP_CLIENT_IP'])) : return $_SERVER['HTTP_CLIENT_IP'];
        case (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) : return $_SERVER['HTTP_X_FORWARDED_FOR'];
        default : return $_SERVER['REMOTE_ADDR'];
    }
}

function resetJaneDemo(){
    $kitDataObj  = new KitData();
    $demo_kits = array(
        'jane_demo' => array(2072,2071,2070,2069)
    );

    $jane_demo_kits = [
        '38' => '{"error":"","result":{"ReferenceCode":"1620085000083","ProviderReferenceCode":"1620085000083","RAW":{"PatientName":"Jane Demo","PatientDOB":"1975-01-08","Tests":[{"SampleCollection":"2019-03-01 08:00:00","Name":"5-hiaa","Code":"5-hiaa","Result":"2290","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-03-01 08:00:00","Name":"cortisol","Code":"cortisol","Result":"6.9","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-03-01 08:00:00","Name":"cortisol-diurnal-1","Code":"cortisol-diurnal-1","Result":"50","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-03-01 12:00:00","Name":"cortisol-diurnal-2","Code":"cortisol-diurnal-2","Result":"85","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-03-01 16:00:00","Name":"cortisol-diurnal-3","Code":"cortisol-diurnal-3","Result":"34","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-03-01 20:00:00","Name":"cortisol-diurnal-4","Code":"cortisol-diurnal-4","Result":"13.1","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-03-01 08:00:00","Name":"cortisone-diurnal-1","Code":"cortisone-diurnal-1","Result":"100.1","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-03-01 12:00:00","Name":"cortisone-diurnal-2","Code":"cortisone-diurnal-2","Result":"180.1","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-03-01 16:00:00","Name":"cortisone-diurnal-3","Code":"cortisone-diurnal-3","Result":"97","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-03-01 20:00:00","Name":"cortisone-diurnal-4","Code":"cortisone-diurnal-4","Result":"45","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-03-01 08:00:00","Name":"dheas","Code":"dheas","Result":"11.8","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-03-01 08:00:00","Name":"dopac","Code":"dopac","Result":"370","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-03-01 08:00:00","Name":"dopamine","Code":"dopamine","Result":"60","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-03-01 08:00:00","Name":"epinephrine-pooled","Code":"epinephrine-pooled","Result":"2.4","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-03-01 08:00:00","Name":"estradiol","Code":"estradiol","Result":"37.7","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-03-01 08:00:00","Name":"gaba","Code":"gaba","Result":"142","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-03-01 08:00:00","Name":"glutamate","Code":"glutamate","Result":"4310","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-03-01 08:00:00","Name":"glycine","Code":"glycine","Result":"112","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-03-01 08:00:00","Name":"histamine","Code":"histamine","Result":"17.5","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-03-01 08:00:00","Name":"homovanillic-acid","Code":"homovanillic-acid","Result":"3010","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-03-01 08:00:00","Name":"norepinephrine-pooled","Code":"norepinephrine-pooled","Result":"21","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-03-01 08:00:00","Name":"normetanephrine","Code":"normetanephrine","Result":"32","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-03-01 08:00:00","Name":"phenethylamine","Code":"phenethylamine","Result":"12.1","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-03-01 08:00:00","Name":"progesterone","Code":"progesterone","Result":"33","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-03-01 08:00:00","Name":"serotonin","Code":"serotonin","Result":"32","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-03-01 08:00:00","Name":"testosterone","Code":"testosterone","Result":"40","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-03-01 08:00:00","Name":"vanillylmandelic-acid","Code":"vanillylmandelic-acid","Result":"4815","UnitOfMeasure":"","Range":"","ResultLevel":""}]}}}',
        '39' => '{"error":"","result":{"ReferenceCode":"1620085000162","ProviderReferenceCode":"1620085000162","RAW":{"PatientName":"Jane Demo","PatientDOB":"1975-01-08","Tests":[{"SampleCollection":"2019-04-01 08:00:00","Name":"5-hiaa","Code":"5-hiaa","Result":"2790","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-04-01 08:00:00","Name":"cortisol","Code":"cortisol","Result":"6.6","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-04-01 08:00:00","Name":"cortisol-diurnal-1","Code":"cortisol-diurnal-1","Result":"35","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-04-01 12:00:00","Name":"cortisol-diurnal-2","Code":"cortisol-diurnal-2","Result":"70","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-04-01 16:00:00","Name":"cortisol-diurnal-3","Code":"cortisol-diurnal-3","Result":"21.2","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-04-01 20:00:00","Name":"cortisol-diurnal-4","Code":"cortisol-diurnal-4","Result":"9.5","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-04-01 08:00:00","Name":"cortisone-diurnal-1","Code":"cortisone-diurnal-1","Result":"88.2","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-04-01 12:00:00","Name":"cortisone-diurnal-2","Code":"cortisone-diurnal-2","Result":"165","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-04-01 16:00:00","Name":"cortisone-diurnal-3","Code":"cortisone-diurnal-3","Result":"78","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-04-01 20:00:00","Name":"cortisone-diurnal-4","Code":"cortisone-diurnal-4","Result":"36.7","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-04-01 08:00:00","Name":"dheas","Code":"dheas","Result":"11.8","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-04-01 08:00:00","Name":"dopac","Code":"dopac","Result":"610","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-04-01 08:00:00","Name":"dopamine","Code":"dopamine","Result":"121","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-04-01 08:00:00","Name":"epinephrine-pooled","Code":"epinephrine-pooled","Result":"2.4","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-04-01 08:00:00","Name":"estradiol","Code":"estradiol","Result":"32","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-04-01 08:00:00","Name":"gaba","Code":"gaba","Result":"186","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-04-01 08:00:00","Name":"glutamate","Code":"glutamate","Result":"2811","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-04-01 08:00:00","Name":"glycine","Code":"glycine","Result":"132","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-04-01 08:00:00","Name":"histamine","Code":"histamine","Result":"18","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-04-01 08:00:00","Name":"homovanillic-acid","Code":"homovanillic-acid","Result":"3676","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-04-01 08:00:00","Name":"norepinephrine-pooled","Code":"norepinephrine-pooled","Result":"21","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-04-01 08:00:00","Name":"normetanephrine","Code":"normetanephrine","Result":"29.9","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-04-01 08:00:00","Name":"phenethylamine","Code":"phenethylamine","Result":"11","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-04-01 08:00:00","Name":"progesterone","Code":"progesterone","Result":"40","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-04-01 08:00:00","Name":"serotonin","Code":"serotonin","Result":"58","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-04-01 08:00:00","Name":"testosterone","Code":"testosterone","Result":"38","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-04-01 08:00:00","Name":"vanillylmandelic-acid","Code":"vanillylmandelic-acid","Result":"4521","UnitOfMeasure":"","Range":"","ResultLevel":""}]}}}',
        '40'=>'{"error":"","result":{"ReferenceCode":"1620085000173","ProviderReferenceCode":"1620085000173","RAW":{"PatientName":"Jane Demo","PatientDOB":"1975-01-08","Tests":[{"SampleCollection":"2019-05-01 08:00:00","Name":"5-hiaa","Code":"5-hiaa","Result":"3300","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-05-01 08:00:00","Name":"cortisol","Code":"cortisol","Result":"6.0","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-05-01 08:00:00","Name":"cortisol-diurnal-1","Code":"cortisol-diurnal-1","Result":"28.7","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-05-01 12:00:00","Name":"cortisol-diurnal-2","Code":"cortisol-diurnal-2","Result":"62.8","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-05-01 16:00:00","Name":"cortisol-diurnal-3","Code":"cortisol-diurnal-3","Result":"17.9","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-05-01 20:00:00","Name":"cortisol-diurnal-4","Code":"cortisol-diurnal-4","Result":"8.3","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-05-01 08:00:00","Name":"cortisone-diurnal-1","Code":"cortisone-diurnal-1","Result":"43","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-05-01 12:00:00","Name":"cortisone-diurnal-2","Code":"cortisone-diurnal-2","Result":"89","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-05-01 16:00:00","Name":"cortisone-diurnal-3","Code":"cortisone-diurnal-3","Result":"76","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-05-01 20:00:00","Name":"cortisone-diurnal-4","Code":"cortisone-diurnal-4","Result":"22","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-05-01 08:00:00","Name":"dheas","Code":"dheas","Result":"10.2","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-05-01 08:00:00","Name":"dopac","Code":"dopac","Result":"843","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-05-01 08:00:00","Name":"dopamine","Code":"dopamine","Result":"152","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-05-01 08:00:00","Name":"epinephrine-pooled","Code":"epinephrine-pooled","Result":"2.4","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-05-01 08:00:00","Name":"estradiol","Code":"estradiol","Result":"4.1","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-05-01 08:00:00","Name":"gaba","Code":"gaba","Result":"210","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-05-01 08:00:00","Name":"glutamate","Code":"glutamate","Result":"1623","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-05-01 08:00:00","Name":"glycine","Code":"glycine","Result":"137","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-05-01 08:00:00","Name":"histamine","Code":"histamine","Result":"19.1","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-05-01 08:00:00","Name":"homovanillic-acid","Code":"homovanillic-acid","Result":"4100","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-05-01 08:00:00","Name":"norepinephrine-pooled","Code":"norepinephrine-pooled","Result":"21","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-05-01 08:00:00","Name":"normetanephrine","Code":"normetanephrine","Result":"23.5","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-05-01 08:00:00","Name":"phenethylamine","Code":"phenethylamine","Result":"12.5","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-05-01 08:00:00","Name":"progesterone","Code":"progesterone","Result":"84","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-05-01 08:00:00","Name":"serotonin","Code":"serotonin","Result":"70","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-05-01 08:00:00","Name":"testosterone","Code":"testosterone","Result":"31","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-05-01 08:00:00","Name":"vanillylmandelic-acid","Code":"vanillylmandelic-acid","Result":"4066","UnitOfMeasure":"","Range":"","ResultLevel":""}]}}}',
        '41'=>'{"error":"","result":{"ReferenceCode":"1620085000049","ProviderReferenceCode":"1620085000049","RAW":{"PatientName":"Jane Demo","PatientDOB":"1975-01-08","Tests":[{"SampleCollection":"2019-06-01 08:00:00","Name":"5-hiaa","Code":"5-hiaa","Result":"4876","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-06-01 08:00:00","Name":"cortisol","Code":"cortisol","Result":"6.0","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-06-01 08:00:00","Name":"cortisol-diurnal-1","Code":"cortisol-diurnal-1","Result":"26.5","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-06-01 12:00:00","Name":"cortisol-diurnal-2","Code":"cortisol-diurnal-2","Result":"59.8","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-06-01 16:00:00","Name":"cortisol-diurnal-3","Code":"cortisol-diurnal-3","Result":"16.8","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-06-01 20:00:00","Name":"cortisol-diurnal-4","Code":"cortisol-diurnal-4","Result":"7","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-06-01 08:00:00","Name":"cortisone-diurnal-1","Code":"cortisone-diurnal-1","Result":"39","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-06-01 12:00:00","Name":"cortisone-diurnal-2","Code":"cortisone-diurnal-2","Result":"67","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-06-01 16:00:00","Name":"cortisone-diurnal-3","Code":"cortisone-diurnal-3","Result":"65","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-06-01 20:00:00","Name":"cortisone-diurnal-4","Code":"cortisone-diurnal-4","Result":"20","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-06-01 08:00:00","Name":"dheas","Code":"dheas","Result":"10.3","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-06-01 08:00:00","Name":"dopac","Code":"dopac","Result":"910","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-06-01 08:00:00","Name":"dopamine","Code":"dopamine","Result":"153","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-06-01 08:00:00","Name":"epinephrine-pooled","Code":"epinephrine-pooled","Result":"2.4","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-06-01 08:00:00","Name":"estradiol","Code":"estradiol","Result":"2.2","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-06-01 08:00:00","Name":"gaba","Code":"gaba","Result":"280","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-06-01 08:00:00","Name":"glutamate","Code":"glutamate","Result":"2234","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-06-01 08:00:00","Name":"glycine","Code":"glycine","Result":"140","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-06-01 08:00:00","Name":"histamine","Code":"histamine","Result":"20.4","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-06-01 08:00:00","Name":"homovanillic-acid","Code":"homovanillic-acid","Result":"4230","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-06-01 08:00:00","Name":"norepinephrine-pooled","Code":"norepinephrine-pooled","Result":"21","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-06-01 08:00:00","Name":"normetanephrine","Code":"normetanephrine","Result":"22.9","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-06-01 08:00:00","Name":"phenethylamine","Code":"phenethylamine","Result":"15.3","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-06-01 08:00:00","Name":"progesterone","Code":"progesterone","Result":"92","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-06-01 08:00:00","Name":"serotonin","Code":"serotonin","Result":"92","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-06-01 08:00:00","Name":"testosterone","Code":"testosterone","Result":"39","UnitOfMeasure":"","Range":"","ResultLevel":""},{"SampleCollection":"2019-06-01 08:00:00","Name":"vanillylmandelic-acid","Code":"vanillylmandelic-acid","Result":"4327","UnitOfMeasure":"","Range":"","ResultLevel":""}]}}}'
    ];
    $kitResultObj  = new KitResults();
    foreach($jane_demo_kits as $id=>$kit_data){
        $kitData = array("id" => $id, "vendorData" => $kit_data, "kitData" => $kit_data);
        $kitResultObj->Update($kitData);
    }

    $dates = array();
    for($week = 33; $week > 0; $week-=2){
        $past_date = date('Y-m-d',strtotime("-".$week." week"));
        $dates[] = $past_date;
    }

    $userAssessmentObj = new UserAssessments();
    $userLifestyleObj = new UserLifestyle();

    $kitDataObj->Update(array('id'=>'2069','timestamp_ready'=>$dates[0]));
    $userLifestyleObj->Update(array('id'=>'167','date_submitted'=>$dates[0]));
    $userAssessmentObj->Update(array('id'=>'386','cycle_num'=>'0','stress_score'=>'216','anxiety_score'=>'214','depression_score'=>'122','date_submitted'=>$dates[0],'formjson'=>'{"stress_q1":"4","stress_q2":"3","stress_q3":"3","stress_q4":"3","stress_q5":"2","stress_q6":"3","stress_q7":"3","stress_q8":"2","stress_q9":"3","anxiety_q1":"2","anxiety_q2":"2","anxiety_q3":"2","anxiety_q4":"2","anxiety_q5":"2","anxiety_q6":"2","anxiety_q7":"3","depression_q1":"1","depression_q2":"0","depression_q3":"2","depression_q4":"2","depression_q5":"2","depression_q6":"1","depression_q7":"2","depression_q8":"1","depression_q9":"0","symptoms-details-1":"","symptoms-details-2":"","symptoms-details-3":"","id":"565","update_assessment_form":"update_assessment_form"}'));
    $userAssessmentObj->Update(array('id'=>'387','cycle_num'=>'1','stress_score'=>'216','anxiety_score'=>'214','depression_score'=>'122','date_submitted'=>$dates[1],'formjson'=>'{"stress_q1":"4","stress_q2":"3","stress_q3":"3","stress_q4":"3","stress_q5":"2","stress_q6":"3","stress_q7":"3","stress_q8":"2","stress_q9":"3","anxiety_q1":"2","anxiety_q2":"2","anxiety_q3":"2","anxiety_q4":"2","anxiety_q5":"2","anxiety_q6":"2","anxiety_q7":"3","depression_q1":"1","depression_q2":"0","depression_q3":"2","depression_q4":"2","depression_q5":"2","depression_q6":"1","depression_q7":"2","depression_q8":"1","depression_q9":"0","symptoms-details-1":"","symptoms-details-2":"","symptoms-details-3":"","id":"565","update_assessment_form":"update_assessment_form"}'));
    $userAssessmentObj->Update(array('id'=>'388','cycle_num'=>'2','stress_score'=>'216','anxiety_score'=>'214','depression_score'=>'122','date_submitted'=>$dates[2],'formjson'=>'{"stress_q1":"4","stress_q2":"3","stress_q3":"3","stress_q4":"3","stress_q5":"2","stress_q6":"3","stress_q7":"3","stress_q8":"2","stress_q9":"3","anxiety_q1":"2","anxiety_q2":"2","anxiety_q3":"2","anxiety_q4":"2","anxiety_q5":"2","anxiety_q6":"2","anxiety_q7":"3","depression_q1":"1","depression_q2":"0","depression_q3":"2","depression_q4":"2","depression_q5":"2","depression_q6":"1","depression_q7":"2","depression_q8":"1","depression_q9":"0","symptoms-details-1":"","symptoms-details-2":"","symptoms-details-3":"","id":"565","update_assessment_form":"update_assessment_form"}'));
    $userAssessmentObj->Update(array('id'=>'389','cycle_num'=>'3','stress_score'=>'216','anxiety_score'=>'214','depression_score'=>'122','date_submitted'=>$dates[3],'formjson'=>'{"stress_q1":"4","stress_q2":"3","stress_q3":"3","stress_q4":"3","stress_q5":"2","stress_q6":"3","stress_q7":"3","stress_q8":"2","stress_q9":"3","anxiety_q1":"2","anxiety_q2":"2","anxiety_q3":"2","anxiety_q4":"2","anxiety_q5":"2","anxiety_q6":"2","anxiety_q7":"3","depression_q1":"1","depression_q2":"0","depression_q3":"2","depression_q4":"2","depression_q5":"2","depression_q6":"1","depression_q7":"2","depression_q8":"1","depression_q9":"0","symptoms-details-1":"","symptoms-details-2":"","symptoms-details-3":"","id":"565","update_assessment_form":"update_assessment_form"}'));

    $kitDataObj->Update(array('id'=>'2070','timestamp_ready'=>$dates[4]));
    $userLifestyleObj->Update(array('id'=>'168','date_submitted'=>$dates[4]));
    $userAssessmentObj->Update(array('id'=>'390','cycle_num'=>'4','stress_score'=>'183','anxiety_score'=>'157','depression_score'=>'144','date_submitted'=>$dates[4],'formjson'=>'{"stress_q1":"4","stress_q2":"2","stress_q3":"2","stress_q4":"2","stress_q5":"2","stress_q6":"2","stress_q7":"2","stress_q8":"3","stress_q9":"3","anxiety_q1":"2","anxiety_q2":"1","anxiety_q3":"2","anxiety_q4":"2","anxiety_q5":"1","anxiety_q6":"1","anxiety_q7":"2","depression_q1":"2","depression_q2":"1","depression_q3":"2","depression_q4":"2","depression_q5":"2","depression_q6":"1","depression_q7":"2","depression_q8":"1","depression_q9":"0","symptoms-details-1":"","symptoms-details-2":"","symptoms-details-3":"","id":"567","update_assessment_form":"update_assessment_form"}'));
    $userAssessmentObj->Update(array('id'=>'391','cycle_num'=>'5','stress_score'=>'183','anxiety_score'=>'157','depression_score'=>'144','date_submitted'=>$dates[5],'formjson'=>'{"stress_q1":"4","stress_q2":"2","stress_q3":"2","stress_q4":"2","stress_q5":"2","stress_q6":"2","stress_q7":"2","stress_q8":"3","stress_q9":"3","anxiety_q1":"2","anxiety_q2":"1","anxiety_q3":"2","anxiety_q4":"2","anxiety_q5":"1","anxiety_q6":"1","anxiety_q7":"2","depression_q1":"2","depression_q2":"1","depression_q3":"2","depression_q4":"2","depression_q5":"2","depression_q6":"1","depression_q7":"2","depression_q8":"1","depression_q9":"0","symptoms-details-1":"","symptoms-details-2":"","symptoms-details-3":"","id":"567","update_assessment_form":"update_assessment_form"}'));
    $userAssessmentObj->Update(array('id'=>'392','cycle_num'=>'6','stress_score'=>'183','anxiety_score'=>'157','depression_score'=>'144','date_submitted'=>$dates[6],'formjson'=>'{"stress_q1":"4","stress_q2":"2","stress_q3":"2","stress_q4":"2","stress_q5":"2","stress_q6":"2","stress_q7":"2","stress_q8":"3","stress_q9":"3","anxiety_q1":"2","anxiety_q2":"1","anxiety_q3":"2","anxiety_q4":"2","anxiety_q5":"1","anxiety_q6":"1","anxiety_q7":"2","depression_q1":"2","depression_q2":"1","depression_q3":"2","depression_q4":"2","depression_q5":"2","depression_q6":"1","depression_q7":"2","depression_q8":"1","depression_q9":"0","symptoms-details-1":"","symptoms-details-2":"","symptoms-details-3":"","id":"567","update_assessment_form":"update_assessment_form"}'));
    $userAssessmentObj->Update(array('id'=>'393','cycle_num'=>'7','stress_score'=>'183','anxiety_score'=>'157','depression_score'=>'144','date_submitted'=>$dates[7],'formjson'=>'{"stress_q1":"4","stress_q2":"2","stress_q3":"2","stress_q4":"2","stress_q5":"2","stress_q6":"2","stress_q7":"2","stress_q8":"3","stress_q9":"3","anxiety_q1":"2","anxiety_q2":"1","anxiety_q3":"2","anxiety_q4":"2","anxiety_q5":"1","anxiety_q6":"1","anxiety_q7":"2","depression_q1":"2","depression_q2":"1","depression_q3":"2","depression_q4":"2","depression_q5":"2","depression_q6":"1","depression_q7":"2","depression_q8":"1","depression_q9":"0","symptoms-details-1":"","symptoms-details-2":"","symptoms-details-3":"","id":"567","update_assessment_form":"update_assessment_form"}'));

    $kitDataObj->Update(array('id'=>'2071','timestamp_ready'=>$dates[8]));
    $userLifestyleObj->Update(array('id'=>'169','date_submitted'=>$dates[8]));
    $userAssessmentObj->Update(array('id'=>'394','cycle_num'=>'8','stress_score'=>'150','anxiety_score'=>'128','depression_score'=>'111','date_submitted'=>$dates[8],'formjson'=>'{"stress_q1":"2","stress_q2":"2","stress_q3":"2","stress_q4":"2","stress_q5":"3","stress_q6":"2","stress_q7":"2","stress_q8":"1","stress_q9":"2","anxiety_q1":"1","anxiety_q2":"1","anxiety_q3":"2","anxiety_q4":"2","anxiety_q5":"1","anxiety_q6":"1","anxiety_q7":"1","depression_q1":"1","depression_q2":"1","depression_q3":"1","depression_q4":"2","depression_q5":"2","depression_q6":"1","depression_q7":"1","depression_q8":"1","depression_q9":"0","symptoms-details-1":"","symptoms-details-2":"","symptoms-details-3":"","id":"569","update_assessment_form":"update_assessment_form"}'));
    $userAssessmentObj->Update(array('id'=>'395','cycle_num'=>'9','stress_score'=>'150','anxiety_score'=>'128','depression_score'=>'111','date_submitted'=>$dates[9],'formjson'=>'{"stress_q1":"2","stress_q2":"2","stress_q3":"2","stress_q4":"2","stress_q5":"3","stress_q6":"2","stress_q7":"2","stress_q8":"1","stress_q9":"2","anxiety_q1":"1","anxiety_q2":"1","anxiety_q3":"2","anxiety_q4":"2","anxiety_q5":"1","anxiety_q6":"1","anxiety_q7":"1","depression_q1":"1","depression_q2":"1","depression_q3":"1","depression_q4":"2","depression_q5":"2","depression_q6":"1","depression_q7":"1","depression_q8":"1","depression_q9":"0","symptoms-details-1":"","symptoms-details-2":"","symptoms-details-3":"","id":"569","update_assessment_form":"update_assessment_form"}'));
    $userAssessmentObj->Update(array('id'=>'396','cycle_num'=>'10','stress_score'=>'150','anxiety_score'=>'128','depression_score'=>'111','date_submitted'=>$dates[10],'formjson'=>'{"stress_q1":"2","stress_q2":"2","stress_q3":"2","stress_q4":"2","stress_q5":"3","stress_q6":"2","stress_q7":"2","stress_q8":"1","stress_q9":"2","anxiety_q1":"1","anxiety_q2":"1","anxiety_q3":"2","anxiety_q4":"2","anxiety_q5":"1","anxiety_q6":"1","anxiety_q7":"1","depression_q1":"1","depression_q2":"1","depression_q3":"1","depression_q4":"2","depression_q5":"2","depression_q6":"1","depression_q7":"1","depression_q8":"1","depression_q9":"0","symptoms-details-1":"","symptoms-details-2":"","symptoms-details-3":"","id":"569","update_assessment_form":"update_assessment_form"}'));
    $userAssessmentObj->Update(array('id'=>'397','cycle_num'=>'11','stress_score'=>'150','anxiety_score'=>'128','depression_score'=>'111','date_submitted'=>$dates[11],'formjson'=>'{"stress_q1":"2","stress_q2":"2","stress_q3":"2","stress_q4":"2","stress_q5":"3","stress_q6":"2","stress_q7":"2","stress_q8":"1","stress_q9":"2","anxiety_q1":"1","anxiety_q2":"1","anxiety_q3":"2","anxiety_q4":"2","anxiety_q5":"1","anxiety_q6":"1","anxiety_q7":"1","depression_q1":"1","depression_q2":"1","depression_q3":"1","depression_q4":"2","depression_q5":"2","depression_q6":"1","depression_q7":"1","depression_q8":"1","depression_q9":"0","symptoms-details-1":"","symptoms-details-2":"","symptoms-details-3":"","id":"569","update_assessment_form":"update_assessment_form"}'));

    $kitDataObj->Update(array('id'=>'2072','timestamp_ready'=>$dates[12]));
    $userLifestyleObj->Update(array('id'=>'170','date_submitted'=>$dates[12]));
    $userAssessmentObj->Update(array('id'=>'398','cycle_num'=>'12','stress_score'=>'108','anxiety_score'=>'100','depression_score'=>'77','date_submitted'=>$dates[12],'formjson'=>'{"stress_q1":"2","stress_q2":"1","stress_q3":"2","stress_q4":"1","stress_q5":"2","stress_q6":"2","stress_q7":"1","stress_q8":"1","stress_q9":"1","anxiety_q1":"1","anxiety_q2":"1","anxiety_q3":"2","anxiety_q4":"1","anxiety_q5":"1","anxiety_q6":"1","anxiety_q7":"0","depression_q1":"1","depression_q2":"0","depression_q3":"1","depression_q4":"1","depression_q5":"1","depression_q6":"1","depression_q7":"1","depression_q8":"1","depression_q9":"0","symptoms-details-1":"","symptoms-details-2":"","symptoms-details-3":"","id":"571","update_assessment_form":"update_assessment_form"}'));
    $userAssessmentObj->Update(array('id'=>'399','cycle_num'=>'13','stress_score'=>'108','anxiety_score'=>'100','depression_score'=>'77','date_submitted'=>$dates[13],'formjson'=>'{"stress_q1":"2","stress_q2":"1","stress_q3":"2","stress_q4":"1","stress_q5":"2","stress_q6":"2","stress_q7":"1","stress_q8":"1","stress_q9":"1","anxiety_q1":"1","anxiety_q2":"1","anxiety_q3":"2","anxiety_q4":"1","anxiety_q5":"1","anxiety_q6":"1","anxiety_q7":"0","depression_q1":"1","depression_q2":"0","depression_q3":"1","depression_q4":"1","depression_q5":"1","depression_q6":"1","depression_q7":"1","depression_q8":"1","depression_q9":"0","symptoms-details-1":"","symptoms-details-2":"","symptoms-details-3":"","id":"571","update_assessment_form":"update_assessment_form"}'));
    $userAssessmentObj->Update(array('id'=>'400','cycle_num'=>'14','stress_score'=>'108','anxiety_score'=>'100','depression_score'=>'77','date_submitted'=>$dates[14],'formjson'=>'{"stress_q1":"2","stress_q2":"1","stress_q3":"2","stress_q4":"1","stress_q5":"2","stress_q6":"2","stress_q7":"1","stress_q8":"1","stress_q9":"1","anxiety_q1":"1","anxiety_q2":"1","anxiety_q3":"2","anxiety_q4":"1","anxiety_q5":"1","anxiety_q6":"1","anxiety_q7":"0","depression_q1":"1","depression_q2":"0","depression_q3":"1","depression_q4":"1","depression_q5":"1","depression_q6":"1","depression_q7":"1","depression_q8":"1","depression_q9":"0","symptoms-details-1":"","symptoms-details-2":"","symptoms-details-3":"","id":"571","update_assessment_form":"update_assessment_form"}'));
    $userAssessmentObj->Update(array('id'=>'401','cycle_num'=>'15','stress_score'=>'108','anxiety_score'=>'100','depression_score'=>'77','date_submitted'=>$dates[15],'formjson'=>'{"stress_q1":"2","stress_q2":"1","stress_q3":"2","stress_q4":"1","stress_q5":"2","stress_q6":"2","stress_q7":"1","stress_q8":"1","stress_q9":"1","anxiety_q1":"1","anxiety_q2":"1","anxiety_q3":"2","anxiety_q4":"1","anxiety_q5":"1","anxiety_q6":"1","anxiety_q7":"0","depression_q1":"1","depression_q2":"0","depression_q3":"1","depression_q4":"1","depression_q5":"1","depression_q6":"1","depression_q7":"1","depression_q8":"1","depression_q9":"0","symptoms-details-1":"","symptoms-details-2":"","symptoms-details-3":"","id":"571","update_assessment_form":"update_assessment_form"}'));
    $userAssessmentObj->Update(array('id'=>'402','cycle_num'=>'16','stress_score'=>'108','anxiety_score'=>'100','depression_score'=>'77','date_submitted'=>$dates[16],'formjson'=>'{"stress_q1":"2","stress_q2":"1","stress_q3":"2","stress_q4":"1","stress_q5":"2","stress_q6":"2","stress_q7":"1","stress_q8":"1","stress_q9":"1","anxiety_q1":"1","anxiety_q2":"1","anxiety_q3":"2","anxiety_q4":"1","anxiety_q5":"1","anxiety_q6":"1","anxiety_q7":"0","depression_q1":"1","depression_q2":"0","depression_q3":"1","depression_q4":"1","depression_q5":"1","depression_q6":"1","depression_q7":"1","depression_q8":"1","depression_q9":"0","symptoms-details-1":"","symptoms-details-2":"","symptoms-details-3":"","id":"571","update_assessment_form":"update_assessment_form"}'));

///Smart log
///
    $date_back_32_weeks = date('Y-m-d',strtotime("-32 weeks"));

    if(date('w',strtotime($date_back_32_weeks)) > 0){ //0 == Sunday
        $first_sunday = date('Y-m-d', strtotime('last Sunday', strtotime($date_back_32_weeks)));
    } else {
        $first_sunday = $date_back_32_weeks;
        //create a new week.
    }

    if(date('w',strtotime($date_back_32_weeks)) != 6){ //6 == Saturday
        $next_saturday = date('Y-m-d', strtotime('next Saturday'));
    } else {
        $next_saturday = $date_back_32_weeks;
        //create a new week.
    }

    $earlier = new DateTime($first_sunday);
    $later = new DateTime($next_saturday);
    $today = new DateTime();

    $calendar_length = $later->diff($earlier)->format("%a");
    $todays_daynum = $today->diff($earlier)->format("%a");

    $calendar = array();
    for($day_num=0;$day_num<=$calendar_length;$day_num++){
        $next_data = date('Y-m-d',strtotime($first_sunday."00:00:00 +".$day_num." days"));
        $calendar[] = $next_data;
    }

    $smartLogObj = new UserSmartLog();
    $janeDemoDataRows = $smartLogObj->GetDataByUserId(6);


    foreach($janeDemoDataRows as &$row){
        if($row['activity_id'] == '0'){
            $row['log'] = json_encode($calendar);
        }else{
            $log_data = [];
            for($day_num=0;$day_num<$todays_daynum;$day_num++){
                $next_data = rand(0,100);
                if ($day_num < $todays_daynum) {
                    $next_data = intval(rand(0, 100) / 20) ? '1' : '';
                } else {
                    $next_data = '';
                }
                $log_data[] = $next_data;
            }
            $row['log'] = json_encode($log_data);

        }
        $smartLogObj->Update($row);
    }
}


function mmd_encrypt($plaintext){
    $key = 'the best secret key ever'; //todo move $key to db
    $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
    $iv = openssl_random_pseudo_bytes($ivlen);
    $ciphertext_raw = openssl_encrypt($plaintext, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
    $hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
    $ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );
    $ciphertext = urlencode($ciphertext);
    return $ciphertext;
}
function mmd_decode($ciphertext){
    $key = 'the best secret key ever'; //todo move $key to db
    $c = urldecode($ciphertext);
    $c = base64_decode($c);
    $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
    $iv = substr($c, 0, $ivlen);
    $hmac = substr($c, $ivlen, $sha2len=32);
    $ciphertext_raw = substr($c, $ivlen+$sha2len);
    $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
    $calcmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
    if (hash_equals($hmac, $calcmac))//PHP 5.6+ timing attack safe comparison
    {
        return $original_plaintext;
    }
}

function isValidUserId($user_id){
    return preg_match('/^[0-9A-Z]{1,8}$/',$user_id) !== false;
}

function closeSession(){

    $is_demo = isset($_SESSION['is_demo']);

    if (isset($_SESSION)) {
        if (array_key_exists('userid', $_SESSION)) {
            $userDataObj = new UserData();
            $userDataObj->invalidateToken($_SESSION['userid']);
        }
        $_SESSION['loggedIn'] = false;
        $_SESSION['type'] = "";
        $_SESSION['userid'] = "";
        $_SESSION['token'] = "";
        $_SESSION['firstname'] = "";
        $_SESSION['lastname'] = "";
//        $_SESSION['birthday'] = "";
//        $_SESSION['age'] = "";
//        $_SESSION['userrole'] = "";
//        $_SESSION['gender'] = "";
//        $_SESSION['mensesstatus_id'] = "";
//        $_SESSION['mensesfirstday'] = "";
//        $_SESSION['menses_state'] = "";
//        $_SESSION['currDate'] = "";
//        $_SESSION['height'] = "";
//        $_SESSION['weight'] = "";
//        $_SESSION['waist_size'] = "";
//        $_SESSION['ethnicity_id'] = "";
        $_SESSION['current_assessment_id'] = "";
        $_SESSION['currentKit'] = "";
//        $_SESSION['results_section'] = "";
        $_SESSION['current_cycle_num'] = '';
        $_SESSION['latest_cycle_num'] = '';
        $_SESSION['current_cycle_id'] = '';
        $_SESSION['latest_cycle_id'] = '';
//        $_SESSION['first_log_entry_date'] = '';
        $_SESSION['request_uri'] = '';
        $_SESSION['userrole_level'] = '';
        $_SESSION['userrole_id'] = '';
        unset($_SESSION['loggedIn']);
        unset($_SESSION['type']);
        unset($_SESSION['userid']);
        unset($_SESSION['token']);
        unset($_SESSION['firstname']);
        unset($_SESSION['lastname']);
//        unset($_SESSION['birthday']);
//        unset($_SESSION['age']);
//        unset($_SESSION['userrole']);
//        unset($_SESSION['gender']);
//        unset($_SESSION['language']);
//        unset($_SESSION['preferredUnits']);
//        unset($_SESSION['mensesfirstday_ts']);
//        unset($_SESSION['currDate']);
//        unset($_SESSION['height']);
//        unset($_SESSION['weight']);
//        unset($_SESSION['waist_size']);
//        unset($_SESSION['ethnicity_id']);
//        unset($_SESSION['menses_cycle_type_id']);
//        unset($_SESSION['hysterectomy']);
//        unset($_SESSION['hysterectomy_ts']);
//        unset($_SESSION['ovaries_removed']);
//        unset($_SESSION['ovaries_removed_ts']);
//        unset($_SESSION['currently_pregnant']);
//        unset($_SESSION['currently_pregnant_ts']);
//        unset($_SESSION['menses_state']);
        unset($_SESSION['current_assessment_id']);
        unset($_SESSION['currentKit']);
        unset($_SESSION['results_section']);
        unset($_SESSION['current_cycle_num']);
        unset($_SESSION['latest_cycle_num']);
        unset($_SESSION['current_cycle_id']);
        unset($_SESSION['latest_cycle_id']);
//        unset($_SESSION['first_log_entry_date']);
        unset( $_SESSION['request_uri']);
        unset($_SESSION['userrole_level']);
        unset($_SESSION['userrole_id']);
        //entry form
        $_SESSION['ef_authenticated'] = null;
        unset($_SESSION['ef_authenticated']);
        //end entry form
        unset($_SESSION);
    }
    $_SESSION['language'] = 'en';
    $_SESSION['preferredUnits'] = 'us';

    if($is_demo){
        $_SESSION['is_demo'] = true;
    }
    getSecrets();

}

function openSession($user_id){
    /////////////////////////////////////////////////////////////////////
    $dbBase = new dbBase();
    $sql = "SELECT TIME_FORMAT(TIMEDIFF(NOW(), UTC_TIMESTAMP), '%H:%i') AS offset";
    $data = $dbBase->runQuery($sql);
    $local_offest = $data[0]['offset'];
    if($local_offest[0] != '-'){
        $local_offest = "+".$local_offest;
    }
    $_SESSION['local_timezone_offset'] = $local_offest;
    /////////////////////////////////////////////////////////////////////
    $startDate = date('Y-m-d');
    if($user_id == '6'){
        resetJaneDemo();
    }

    $userDataObj = new UserData();
    $userData = $userDataObj->GetUserData($user_id);
    
    # Generates and save token (and its expiration)
    $token_expiration = date_create('now')->modify('+ 1 day')->format('Y-m-d H:i:s');
    $payload = [
        'user_id' => $user_id,
        'expiration' => $token_expiration
    ];
    $jwt = Token::encode($payload);

    $user = [
        'id' => $userData['id'],
        'token' => $jwt,
        'token_expiration' => $token_expiration
    ];
    $userDataObj->Update($user);

    //$birthday = new DateTime($userData['birthday']);
    //$now = new DateTime();
    //$interval = $now->diff($birthday);
    //$age = $interval->format('%y');

    $_SESSION['loggedIn'] = true;
    $_SESSION['userid'] = $user_id;
    $_SESSION['token'] = $jwt;
    $_SESSION['firstname'] = $userData['firstname'];
    $_SESSION['lastname'] = $userData['lastname'];
    $_SESSION['mobile_number'] = $userData['mobile_number'];
//    $_SESSION['birthday'] = $userData['birthday'];
//    $_SESSION['age'] = $age;
    $_SESSION['userrole'] = $userData['role'];
//    $_SESSION['gender'] = $userData['gender'];
//    $_SESSION['height'] = $userData['height'];
//    $_SESSION['weight'] = $userData['weight'];
//    $_SESSION['waist_size'] = $userData['waist_size'];
//    $_SESSION['ethnicity_id'] = $userData['ethnicity_id'];
//    $_SESSION['language'] = $userData['language'];
//    $_SESSION['preferredUnits'] = $userData['preferredUnits'];
//    $_SESSION['currDate'] = $startDate;
//    $_SESSION['mensesfirstday_ts'] = isset($userData['mensesfirstday_ts'])? $userData['mensesfirstday_ts']:"";
//    $_SESSION['menses_cycle_type_id'] = isset($userData['menses_cycle_type_id'])? $userData['menses_cycle_type_id']:"";
//    $_SESSION['hysterectomy'] = $userData['hysterectomy'];
//    $_SESSION['hysterectomy_ts'] = $userData['hysterectomy_ts'];
//    $_SESSION['ovaries_removed'] = $userData['ovaries_removed'];
//    $_SESSION['ovaries_removed_ts'] = $userData['ovaries_removed_ts'];
//    $_SESSION['currently_pregnant'] = $userData['currently_pregnant'];
//    $_SESSION['currently_pregnant_ts'] = $userData['currently_pregnant_ts'];
    $_SESSION['userrole_level'] = $userData['role'];
    $_SESSION['userrole_id'] = $userData['role_id'];
    $_SESSION['emailVerified'] = $userData['emailVerified'];
    $_SESSION['request_uri'] = $userData['request_uri'];

    //Default this to empty so the code in results.php will fill it with your latest;
    //if(isset($_SESSION['currentKit'])){unset($_SESSION['currentKit']);}

      $_SESSION['type'] = "normal";
//    $_SESSION['results_section'] = "#section-results";

    $userSmartLogObj = new UserSmartLog();
    $log_length =  $userSmartLogObj->GetLogLength($user_id);
    $first_log_entry_date = $userSmartLogObj->GetFirstLogEntryDate($user_id);
    $_SESSION['first_log_entry_date'] = $first_log_entry_date;

    $useCyclesObj = new UserCycles();
    $latestCycleData = $useCyclesObj->GetLatestUserCycle($user_id);
    if($latestCycleData['cycle_num'] < intval($log_length/14)){
        $useCyclesObj->Add($user_id, intval($log_length/14));
        $latestCycleData = $useCyclesObj->GetLatestUserCycle($user_id);
    }
    $_SESSION['current_cycle_num'] = $latestCycleData['cycle_num'];
    $_SESSION['latest_cycle_num'] = $latestCycleData['cycle_num'];
    $_SESSION['current_cycle_id'] = $latestCycleData['id'];
    $_SESSION['latest_cycle_id'] = $latestCycleData['id'];


//    $assessmentDataObj = new  UserAssessments();
//    $latestAssessment =  $assessmentDataObj->GetLatestUserData($_SESSION['latest_cycle_id']);
//    if(count($latestAssessment) == 0){
//        $_SESSION['current_assessment_id'] = $assessmentDataObj->Add(['cycle_id'=>$_SESSION['latest_cycle_id']],[],false);
//    } else {
//        $_SESSION['current_assessment_id'] = $latestAssessment['id'];
//    }

//    $kitDataObj = new KitData();
//    $latestKitId = $kitDataObj->GetUserTestKitsUpToCycleId($_SESSION['latest_cycle_id']);
//    $kitReadyCount = $kitDataObj->getReadyCountForUserId($user_id);

//    if ($kitReadyCount > 0) {
//        $userGynecologyObj = new UserGynecology();
//        $_SESSION['menses_state'] = $userGynecologyObj->getMensesState($user_id, $latestKitId[0]['id']);
//    } else {
//        $_SESSION['menses_state'] = 'x';
//    }
//    return ['kitReadyCount'=>$kitReadyCount,'latestKitId'=>$latestKitId,'request_uri'=>$userData['request_uri']];
}


        <footer>
            <div class="inner-container">
                <div class="flex">
                    <div>&copy; 2020 MoodMD Neurotech Inc.</div>
                    <div>
                        <a href="<?php echo $GLOBALS['coreURL']; ?>/termsofservice.php"><?php echo $footertextarray['footer-terms']; ?></a> |
                        <a href="<?php echo $GLOBALS['coreURL']; ?>/privacypolicy.php"><?php echo $footertextarray['footer-policy']; ?></a> |
                        <a href="mailto:info@moodmd.com">Contact Us</a>
                    </div>
                </div>
                <p class="disclaimer">The materials and information provided in this presentation, document and/or any other communication (“Communication”) from MoodMD Neurotech Inc. or any related entity or person (collectively “MoodMD”) are strictly for informational purposes only and are not intended for use as diagnosis, prevention or treatment of a health problem or as a substitute for consulting a qualified medical professional. Some of the concepts presented herein may be theoretical.</p>
            </div>
        </footer>
        <div id="commonDialog"></div>
    </body>
</html>
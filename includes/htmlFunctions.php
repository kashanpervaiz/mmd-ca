<script>
	function roundToHalf(value)
	{
		return Math.round((value*2)+1)/2;
	}

	function roundToQuater(value)
	{
		return Math.round((value*4)+1)/4;
	}

/*
mmol/L = SI measure
mg/dL = US measure

To convert glucose levels:
  glucose mg/dl from mmol/l: mg/dl = 18 � mmol/l
  glucose mmol/l from mg/dl: mmol/l = mg/dl / 18

To convert cholesterol levels:
  Cholesterol mg/dl = mmol/l x 38.6
  Cholesterol mmol/l = mg/dl � 38.6

To convert triglyceride levels:
  Triglyceride mg/dl = mmol/l x 88.5
  Triglyceride mmol/l = mg/dl � 88.5

A1C From this link ( http://care.diabetesjournals.org/content/31/8/1473.full )
  AGmg/dl = 28.7 � A1C - 46.7
  AGmmol/l = 1.59 � A1C - 2.
*/


	function wbConvert(measure, value)
	{

		convertions['lbToKg'] = parseFloat (1/2.20);
		convertions['KgTolb'] = 2.20;
		convertions['inTocm'] = 2.54;
		convertions['cmToin'] = parseFloat (1/2.54);
		convertions['glucose_SItoUS'] = 18;
		convertions['glucose_UStoSI'] = parseFloat (1/18);
		convertions['cholesterol_SItoUS'] = 38.6;
		convertions['cholesterol_UStoSI'] = parseFloat (1/38.6);
		convertions['triglyceride_SItoUS'] = 	88.5;
		convertions['triglyceride_UStoSI'] = 	parseFloat (1/88.5);
		var returnVal = value;
		
		if(myArray.indexOf(convertions[measure])>= 0)
		{
			var units   = '<?php echo $_SESSION['preferredUnits']   ; ?>';
			if( units   == 'us')
			{ 
				returnVal =  value;
			}
			else
			{
				factor = convertions[measure];
				returnVal =  (Math.round(10*value*factor))/10;
			}
		}
		else
		{
			returnVal =  value;
		}
		
		return returnVal;
	}

</script>
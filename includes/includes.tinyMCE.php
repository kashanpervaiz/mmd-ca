<script type="text/javascript" src="<?php echo $GLOBALS['coreURL']; ?>/lib/tinymce/js/tinymce/tinymce.min.js"></script>

<script type="text/javascript">
    tinymce.init({
        selector: "textarea.mceEditor",
        branding: false,
        height: '400',
        verify_html : false,
        forced_root_block : "",
        entity_encoding : "named",
        allow_script_urls: true,
        menubar: 'file edit insert view format table tools help',
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table directionality",
            "emoticons template paste textpattern"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor emoticons | removeformat",
        image_advtab: true,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ]
    });
</script>

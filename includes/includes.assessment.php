<?php
    $display_score = isset($_SESSION['userrole']) && $_SESSION['userrole'] > 10 ? "inline-block" : "none"
?>

<script>
    /*var sections = new Array();
    sections[0] = ['stress','anxious','depressed','panic','irritated'];
    sections[1] = ['fallingasleep','stayingasleep','hyperactivity','tired','wired','getup'];
    sections[2] = ['diet','activity'];
    sections[3] = ['impulsivity','behavior','addiction','alcohol','coffee'];
    sections[4] = ['concentration','weight','sick','libido'];*/

    var sections = [
        // ['sleep',['sleep_q1','sleep_q2']],
        //['weight',['weight_q1']],
        ['stress', ['stress_q1', 'stress_q2', 'stress_q3', 'stress_q4', 'stress_q5', 'stress_q6', 'stress_q7', 'stress_q8', 'stress_q9']],
        ['anxiety', ['anxiety_q1', 'anxiety_q2', 'anxiety_q3', 'anxiety_q4', 'anxiety_q5', 'anxiety_q6', 'anxiety_q7']],
        ['depression', ['depression_q1', 'depression_q2', 'depression_q3', 'depression_q4', 'depression_q5', 'depression_q6', 'depression_q7', 'depression_q8', 'depression_q9']]
    ]

    var sectionText = {
        'sleep': 'During the past 2 weeks...',
        //'weight':'In the past 2 weeks,do you feel your weight has changed?',
        'stress': 'The questions in this scale ask you about your feelings and thoughts during the last month. In each case, please select how often you felt or thought a certain way.',
        'anxiety': 'Over the last <b>2 weeks</b>, how often have you been bothered by the following problems?',
        'depression': 'Over the last <b>2 weeks</b>, how often have you been bothered by the following problems?'
    }

    var frequency = new Array();
    frequency['-1'] = '';
    frequency['0'] = 'never';
    frequency['1'] = 'almostnever';
    frequency['2'] = 'sometimes';
    frequency['3'] = 'fairlyoften';
    frequency['4'] = 'veryoften';

    var frequency2 = new Array();
    frequency2['-1'] = '';
    frequency2['0'] = 'notatall';
    frequency2['1'] = 'severaldays';
    frequency2['2'] = 'morethanhalfthedays';
    frequency2['3'] = 'nearlyeveryday';

    var questionsText = [];

    $(function(){

        // for jane demo only
        <?php if(in_array($_SESSION['userid'],['6','1CE7WYCU'])){ ?>
        if(/assessment.php/.test(window.location.href)){
            $('button#finish').removeClass('disabled').text('continue demo');
        }
        <?php } ?>

        var count =<?php echo count($symptoms) + 1;?>;
        $('.addSymptom').on('click', function(){
            $(this).before('<fieldset><label>Symptom Detail</label><textarea id="symptoms-details-' + count + '" name="symptoms-details-' + count + '"></textarea></fieldset>');
            count++;
        })


        $(document).on('click', '#frm-assessment #finish', function(){
            <?php if(!in_array($_SESSION['userid'],['6','1CE7WYCU'])){ ?>
                if(window.location.hash == '##section-moodassessment'){
                    $('#dialog-msg').dialog({
                        modal: true,
                        width: Math.min(500, $(window).width() - 20),
                        resizable: false,
                        buttons: {
                            Ok: function(){
                                $('.leftnav-moodassessment').addClass('hidden');
                                window.location.hash = '##section-results';
                                $(this).dialog('close');
                            }
                        }
                    });
                }

            <?php } ?>

            //var assessmentDataForm = $("#frm-assessment").serialize();
            $.when(doSetAssessmentData()).done(function(data){
                setAssessmentsInclude('<?php echo $_SESSION['current_assessment_id']; ?>');
                $('#finish').show();

                if(/assessment.php/.test(window.location.href)){
                    <?php if( isset($newAssessment) and $newAssessment === true ){ ?>
                        window.location.href = '/myprofile.php'
                    <?php } else { ?>
                        <?php if(!in_array($_SESSION['userid'],['6','1CE7WYCU'])){ ?>
                            $.post("<?php echo $GLOBALS['coreURL']; ?>/services/userdata/updatebyuserid", {
                                user_id: "<?php echo $_SESSION['userid'];?>",
                                request_uri: '/lifestyleAssessment.php'
                            });
                        <?php } ?>
                        window.location.href = '/lifestyleAssessment.php'
                    <?php } ?>
                }
            }).fail(function(jqXHR, textStatus, errorThrown){
                //console.log(errorThrown);
                if (errorThrown == 'Unauthorized') {
                    window.location.href = '<?php echo $GLOBALS['coreURL']; ?>/signout.php';
                }
            })
        })

        function doSetAssessmentData(){
            return $.post("<?php echo $GLOBALS['coreURL']; ?>/services/assessments/set", $('#frm-assessment').serialize());
        }

        function doGetQuestions(page){
            var data = {'page': page, 'vlanguage': 'en'};
            return $.post("<?php echo $GLOBALS['coreURL']; ?>/services/pagetext/getpagetext", data);

        }

        function getQuestions(){
            $.when(doGetQuestions('assessment_questions2')).done(function(data){
                //console.log(data)
                questionsText = $.parseJSON(data);
                setAssessmentsInclude('<?php echo $_SESSION['current_assessment_id']; ?>');
                //console.log(questionsText)
            }).fail(function(jqXHR, textStatus, errorThrown){
                //console.log(errorThrown);
                if (errorThrown == 'Unauthorized') {
                    window.location.href = '<?php echo $GLOBALS['coreURL']; ?>/signout.php';
                }
            })
        }

        getQuestions();

        function setAssessmentsInclude(assessment_id){
            $.when(getAssessmentInclude(assessment_id)).done(function(data){
                response = $.parseJSON(data);
                //console.log(response);
                if(response.error == ''){
                    $('#assessment_questions').html('');
                    $('#assessment_symptoms').html('');
                    var assessment_questions_html = '';
                    var assessment_symptoms_html = '';

                    var stressRow = '<tr>';
                    stressRow += '<td style="width:50%;"><hr/></td>';
                    stressRow += '    <td>Never</td>';
                    stressRow += '    <td>Almost Never</td>';
                    stressRow += '    <td width="10%">Sometimes</td>';
                    stressRow += '    <td>Fairly Often</td>';
                    stressRow += '   <td>Very Often</td>';
                    stressRow += '   </tr>';


                    var otherRow = '<tr>';
                    otherRow += '<td style="width:50%;"><hr/></td>';
                    otherRow += '    <td>Not at all</td>';
                    otherRow += '    <td>Several days</td>';
                    otherRow += '    <td width="10%">More than half the days</td>';
                    otherRow += '    <td>Nearly every day</td>';
                    otherRow += '   </tr>';

                    $.each(sections, function(i, section){
                        // console.log('faby',section[0]);

                        assessment_questions_html += '<div class="section">';
                        assessment_questions_html += '<h4 style="display:inline-block;">' + section[0] + ' Inventory</h4>';
                        assessment_questions_html += '<div id="' + section[0] + '_table_score" style="display:<?php echo $display_score;?>;margin-left:20px;"></div>';
                        assessment_questions_html += '<p>' + sectionText[section[0]] + '</p>';
                        assessment_questions_html += '</div>';
                        assessment_questions_html += '<table id="' + section[0] + '_table" class="section_table">';


                        $.each(section[1], function(j, topic){

                            if(section[0] == 'stress' && j % 3 == 0 || section[0] == 'sleep' && j % 2 == 0){
                                assessment_questions_html += stressRow;
                            }

                            if((section[0] == 'anxiety' && j % 4 == 0) || (section[0] == 'depression' && j % 5 == 0)){
                                assessment_questions_html += otherRow;
                            }
                            assessment_questions_html += '<tr>';
                            assessment_questions_html += '<td>' + questionsText[topic] + '</td>';

                            if(section[0] == 'stress' || section[0] == 'sleep'){
                                for(j = 0; j < 5; j++){
                                    assessment_questions_html += '<td><input type="radio" name="' + topic + '" id="' + topic + '-' + frequency[j] + '" value="' + j + '" ' + setRadioChecked(response.results.assessmentForm[topic], j) + '/><label for="' + topic + '-' + frequency[j] + '" class="radio">&nbsp;</label></td>';
                                }
                            } else if(section[0] == 'weight'){
                                assessment_questions_html += '<td><input type="text" name="' + topic + '" id="' + topic + '-' + frequency[j] + '" value="' + j + '" /></td>';
                            } else{
                                for(j = 0; j < 4; j++){
                                    assessment_questions_html += '<td><input type="radio" name="' + topic + '" id="' + topic + '-' + frequency2[j] + '" value="' + j + '" ' + setRadioChecked(response.results.assessmentForm[topic], j) + '/><label for="' + topic + '-' + frequency2[j] + '" class="radio">&nbsp;</label></td>';
                                }
                            }
                            assessment_questions_html += '</tr>';

                        });

                        assessment_questions_html += '</table> ';


                    });


                    $.each(response.results.symptoms, function(i, symptom){
                        assessment_symptoms_html += '<fieldset>';
                        assessment_symptoms_html += '  <label>Symptom Detail</label>';
                        assessment_symptoms_html += '  <textarea name="symptoms-details-' + i + '" id="symptoms-details-' + i + '">' + symptom + '</textarea>';
                        assessment_symptoms_html += '</fieldset> ';

                    });

                    //if(count(symptoms) < 3){
                    for(key = ((response.results.symptoms.length) + 1); key < 4; key++){
                        assessment_symptoms_html += '<fieldset>';
                        assessment_symptoms_html += '  <label>Symptom Detail</label>';
                        assessment_symptoms_html += '  <textarea name="symptoms-details-' + key + '" id="symptoms-details-' + key + '"></textarea>';
                        assessment_symptoms_html += '</fieldset> ';
                    }

                    $('#assessment_questions').html(assessment_questions_html);
                    $('#assessment_symptoms').html(assessment_symptoms_html);
                }

                if($('#frm-assessment input[type=radio]:checked').length){
                    $('#frm-assessment #finish').removeClass('disabled');
                };


            }).fail(function(jqXHR, textStatus, errorThrown){
                //console.log(errorThrown);
                if (errorThrown == 'Unauthorized') {
                    window.location.href = '<?php echo $GLOBALS['coreURL']; ?>/signout.php';
                }
            });
        }


        function getAssessmentInclude(assessment_id){
            return  $.post("<?php echo $GLOBALS['coreURL']; ?>/services/assessments/get", {
                id: assessment_id,
                user_id: <?php echo $_SESSION['userid'];?>,
                lang: '<?php echo $_SESSION['language'];?>'
            });
        };

        // $(document).on('change', '#frm-assessment input[type="radio"]', function(){
        //     doSetAssessmentData();
        // });


        $(document).on('change', '#frm-assessment input[type="radio"]', function(){
            var inputs = $('#frm-assessment input[type="radio"]');
            var groups = [];

            for(var i = 0; i < inputs.length; ++i){
                groups.push(inputs[i].name)
            }
            let allChecked = Array.from(new Set(groups)).length;
           // console.log(allChecked)

            var count = 0;
            $('#frm-assessment input[type=radio]:checked').each(function(){
                count++;
            });
            //console.log(count, allChecked)
            if(count == allChecked){
                $('#frm-assessment #finish, #quickstart-finish').removeClass('disabled');
            } else{
                $('#frm-assessment #finish, #quickstart-finish').addClass('disabled')
            }

        });

    });


    $(window).on('load', function(){

        //for admin
        getTotalScore();

        function getTotalScore(){
            $('.section_table').each(function(){
                var total_score = 0;
                var whichTable = $(this).attr('id');
                $('#' + whichTable).find('input:checked').each(function(){
                    var thisVal = parseInt($(this).val());
                    total_score += parseInt(thisVal);

                })

                $('#' + whichTable + '_score').html(getTotalPercent(whichTable, total_score));
            })
        }


        function getTotalPercent(whichTable, total_score){
            switch(whichTable){
                case 'stress_table':
                    total_score = parseInt(100 * total_score / 36) + '%';
                    $('#stress_table_score').html(total_score);
                    break;
                case 'anxiety_table':
                    total_score = parseInt(100 * total_score / 21) + '%';
                    $('#anxiety_table_score').html(total_score);
                    break;
                case 'depression_table':
                    total_score = parseInt(100 * total_score / 27) + '%';
                    $('#depression_table_score').html(total_score);
                    break;
                default:
                    total_score = total_score;
            }
            return total_score;
        }

        $(document).on('click', 'input[type="radio"]', function(){
            getTotalScore();
        });

    });

    function setRadioChecked(a, b){
        return a == b ? ' checked=\'checked\'' : '';
    }

</script>

<div class="hdr">
    <h4>Mood Assessment</h4>
    <p>Please answer each question below or update any that have changed (if previously answered).</p>
</div>
<form enctype="multipart/form-data" id="frm-assessment" name="frm-assessment" method="post" action="" autocomplete="off">
    <div class="frm">
        <div id="assessment_questions"></div>
    </div>
    <div class="symptoms">
        <h4>MISC. SYMPTOMS</h4>
        <p class="subtitle">Symptom Details <span>(optional)</span></p>
        <p>Please tell us, in your own words, which symptoms or behaviors you are having the most trouble with and
            exactly what you are experiencing. Feel free to add any that are not included above - or expand on any you
            have already marked above.
        </p>
        <div id="assessment_symptoms"></div>
        <div class="addSymptom"><i class="fas fa-plus"></i> ADD Symptom</div>
    </div>
    <fieldset>
        <?php if( isset($assessmentData['id']) && $assessmentData['id'] > 0 ){ ?>
            <input type="hidden" name="id" value="<?php echo $assessmentData['id']; ?>">
        <?php } ?>
        <input type="hidden" name="cycle_id" value="<?php echo $assessmentData['cycle_id']; ?>">
        <input type="hidden" name="update_assessment_form" value="update_assessment_form">

        <button type="button" id="finish" class="disabled">submit answers</button>
    </fieldset>
</form>
<div id="dialog-msg" style="display:none">
    <h3 align="center">Thank you!</h3>
    <p align="center">Your assessment has been submitted.</p>
    <p align="center"><i>You'll be able to update your answers in another 2-weeks. View your completed assessments on your profile page.</i></p>
</div>
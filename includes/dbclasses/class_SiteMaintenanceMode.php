<?php 
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

class SiteMaintenanceMode extends dbBase
{
    public $maintenance_mode = "";
    public $site = "";

   function __construct($id = ""){
        parent::__construct();
        $this->table = "site_maintenance_mode";
        if(strlen($id)>0)
        {
            parent::Load($id);
        }
   }

	function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true){
		$ignore = array('id','add_post');
		$id = parent::Add($postData,$ignore,$bUseOnlyIgnoreList);
		return $id;
	}

	function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true){
		$ignore = array('id','update_post');
		$id = parent::Update($postData,$ignore,$bUseOnlyIgnoreList);
	}

    function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true){
        $ignore = array('id','add_post','update_post');
        //if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        $id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
        return $id;
    }


//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////
    function InMaintenanceMode($site){
        $response = false;
        $link = parent::createLinki();

        $sql = "SELECT maintenance_mode FROM site_maintenance_mode WHERE site = '$site'";
        $row = self::runQuery($sql,true);
        $maintenance_mode = $row['maintenance_mode'] ?? 0;
        $response = $maintenance_mode != 0;

        if(is_resource($link) )
        {
            mysqli_close($link);
        }

        return $response;
    }

}


<?php 
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

class Languages extends dbBase
{

	public $abbreviation2 = "";
	public $language = "";
	public $enabled = "";

	 function __construct() 
   {
		parent::__construct();
		$this->table = "languages";
   }
   

	function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','add_post');		
		$id = parent::Add($postData,$ignore);	
		return $id;
	}
   
	function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','update_languages','lasttab','update_post');
		$id = parent::Update($postData,$ignore);
	}

    function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true)
    {
        $ignore = array('id','add_post','update_post');
        //if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        $id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
        return $id;
    }

	function GetLanguages($id="")
	{
		return parent::GetData($id,$this->table);
	}

///////////////////////////////////////////////////////////////
	function GetLanguage($abbreviation2="")
	{
		$link = parent::createLinki();
		$sql = "SELECT `language` from `".$this->table."` where `abbreviation2` = '".$abbreviation2."'";
		$SQLresult = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
		$result = "";
		
				while($row = mysqli_fetch_assoc($SQLresult))
				{
					$result = $row['language'];
				}

		mysqli_close($link);
		return $result;
	}
 	

	function GetActiveLanguages()
	{
		$link = parent::createLinki();
		$sql = "SELECT `abbreviation2`,`language` from `".$this->table."` where `enabled` = '1'";
		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
		$array = array();
				while($row = mysqli_fetch_assoc($result))
				{
					$array[] = $row;
				}
		//var_dump($array);
		mysqli_close($link);
		return $array;
	}		

	function ShowLanguageSelector()
	{
		$result = false;
		
		$link = parent::createLinki();
		$sql = "SELECT count(*) AS 'count' from `".$this->table."` where `enabled` = '1'";
		$data = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
		$row = mysqli_fetch_assoc($data);
		
		if(intval($row['count'])>1)
		{
			$result = true;
		}

		mysqli_close($link);
		return $result;
		
	}

		
}



?>
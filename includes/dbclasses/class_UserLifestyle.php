<?php 
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

//Generic class template
// Notes:
// xx = Class Name
// xxx  = table name
// $..  = column name(s)

class UserLifestyle extends dbBase
{
    public $user_id = "";
    public $exercise_score = "";
    public $diet_score = "";
    public $sleep_score = "";
    public $form_json = "";
    public $date_submitted = "";

    function __construct($id = "")
   {
    parent::__construct();
	  $this->table = "user_lifestyle";
		if(strlen($id)>0)
		{
			parent::Load($id);
		}
   }

	function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','add_post');
		$date = new DateTime();
		if(isset($postData['date_submitted'])){
		    $date = new DateTime($postData['date_submitted']);
        }
        $postData['date_submitted'] = $date->format("Y-m-d");

		$id = parent::Add($postData,$ignore);
		return $id;
	}

	function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','update_post');
        $date = new DateTime();
        if(isset($postData['date_submitted'])){
            $date = new DateTime($postData['date_submitted']);
        }
        $postData['date_submitted'] = $date->format("Y-m-d");

		$id = parent::Update($postData,$ignore);
	}

    function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true)
    {
        $ignore = array('id','add_post','update_post');
        if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        $id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
        return $id;
    }

//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////
    function getUserLifestyleData($user_id , $kit_id){
        $link = parent::createLinki();
        $sql = "select * from `".$this->table."` where  `user_id` = '".$user_id."' and `kit_id` = '".$kit_id."' order by `date_submitted` DESC LIMIT 1";
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $array = array();
        while($row = mysqli_fetch_assoc($result))
        {
            $array = $row;
        }
        //var_dump($array);die();
        mysqli_close($link);
        return $array;
    }

    function getAllUserData($user_id,$orderby = 'DESC'){
        $link = parent::createLinki();
        $sql = "select * from `".$this->table."` where  `user_id` = '".$user_id."' order by `date_submitted` ". $orderby;
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $array = array();
        while($row = mysqli_fetch_assoc($result))
        {
            $array[] = $row;
        }
        //var_dump($array);die();
        mysqli_close($link);
        return $array;
    }

    function setUserLifestyleData($data){
//        ls_sections['exercise'] = ['exercise_q1'],
//        ls_sections['diet'] = ['diet_q1','diet_q2','diet_q3','diet_q4','diet_q5','diet_q6','diet_q7','diet_q8'];
//        ls_sections['sleep'] = ['sleep_q1', 'sleep_q2', 'sleep_q3', 'sleep_q4', 'sleep_q5', 'sleep_q6', 'sleep_q7', 'sleep_q8'];
        $link = parent::createLinki();
        $user_id = $data['user_id'];
        $kit_id = $data['kit_id'];
        $exercise_score = 0;//$data['exercise_q1']+$data['exercise_q2']/2;
        $diet_score = 0;//$data['diet_q1'];
        $sleep_score = 0;//$data['sleep_q1']+$data['sleep_q2']/2;
        $json_data = json_encode($data);
        $date = new DateTime();
        $date_submitted = $date->format("Y-m-d");

        $sql = "REPLACE INTO ".$this->table." (user_id,kit_id,exercise_score,diet_score,sleep_score,form_json,date_submitted)
                VALUES  ('$user_id','$kit_id','$exercise_score','$diet_score','$sleep_score','$json_data','$date_submitted')";
        $result = mysqli_query($link,$sql) or die( "REPLACE INTO: ".mysqli_error($link)."<br/>".$sql );
        $id = mysqli_insert_id($link);
//        $array = array();
//        while($row = mysqli_fetch_assoc($result))
//        {
//            $array = $row;
//        }
        //var_dump($array);die();
        mysqli_close($link);
        return $id;
    }

    function getLifeStyleRecommendations($user_id,$kit_id,$cycle_id){
        $exercise_queue = array();
        $diet_queue = array();
        $sleep_queue = array();
        $mind_queue = array();
        $lifeStylePageTextObj = new PageText();
        $lifeStyleActivityTitles = $lifeStylePageTextObj->GetPageText("recommendation_activities_progress_titles", $_SESSION['language']);
        $lifeStyleActivityMessages = $lifeStylePageTextObj->GetPageText("recommendation_activities_message_by_topic", $_SESSION['language']);
        $lifeStylePreActivityMessages = $lifeStylePageTextObj->GetPageText("recommendation_activities_preactivity_message_by_topic", $_SESSION['language']);
        $exercise_progress_bar = array(
            'exercise_1'=>0,
            'exercise_1'=>0,
            'exercise_2'=>0,
            'exercise_3'=>0,
            'exercise_4'=>0,
            'exercise_5'=>0,
            'exercise_6'=>0,
            'exercise_7'=>0,
            'exercise_8'=>0
        );
        $diet_progress_bar = array(
            'diet_1'=>0,
            'diet_1'=>0,
            'diet_2'=>0,
            'diet_3'=>0,
            'diet_4'=>0,
            'diet_5'=>0,
            'diet_6'=>0,
            'diet_7'=>0,
            'diet_8'=>0
        );
        $sleep_progress_bar = array(
            'sleep_1'=>0,
            'sleep_1'=>0,
            'sleep_2'=>0,
            'sleep_3'=>0,
            'sleep_4'=>0,
            'sleep_5'=>0,
            'sleep_6'=>0,
            'sleep_7'=>0,
            'sleep_8'=>0
        );
        $mind_progress_bar = array(
            'mind_1'=>0,
            'mind_2'=>0,
            'mind_3'=>0,
            'mind_4'=>0,
            'mind_5'=>0,
            'mind_6'=>0,
            'mind_7'=>0,
            'mind_8'=>0,
            'mind_9'=>0
        );
        $data = self::getUserLifestyleData($user_id,$kit_id);
        if (isset($data) && isset($data['form_json'])) {
            $form_data = json_decode($data['form_json'], true);
            foreach ($form_data as $key => $val) {
                $name = str_ireplace('_q', '_', $key);
                switch ($name) {
                    case 'exercise_1':
                        if ($val == 0) {
                            $exercise_queue[] = 'exercise_1';
                        } else if ($val == 1) {
                            $exercise_queue[] ='exercise_2';
                            $exercise_progress_bar['exercise_1'] = 1;
                        } else if ($val == 2) {
                            $exercise_queue[] ='exercise_3';
                            $exercise_progress_bar['exercise_1'] = 1;
                            $exercise_progress_bar['exercise_2'] = 1;
                        } else if ($val == 3) {
                            $exercise_queue[] ='exercise_4';
                            $exercise_progress_bar['exercise_1'] = 1;
                            $exercise_progress_bar['exercise_2'] = 1;
                            $exercise_progress_bar['exercise_3'] = 1;
                        } else if ($val == 4) {
                            $exercise_queue[] ='exercise_5';
                            $exercise_progress_bar['exercise_1'] = 1;
                            $exercise_progress_bar['exercise_2'] = 1;
                            $exercise_progress_bar['exercise_3'] = 1;
                            $exercise_progress_bar['exercise_4'] = 1;
                        }
                        break;


                    case 'diet_1':
                    case 'diet_5':
                    case 'diet_6':
                    case 'diet_7':
                    case 'diet_8':
                        if ($val == 0) {
                            $diet_queue[] = $name;
                        } else {
                            $diet_progress_bar[$name] = 1;
                        }
                        break;
                    case 'diet_2':
                    case 'diet_3':
                    case 'diet_4':
                        if ($val == 1) {
                            $diet_queue[] = $name;
                        } else {
                            $diet_progress_bar[$name] = 1;
                        }
                        break;

                    case 'sleep_1':
                    case 'sleep_2':
                    case 'sleep_3':
                    case 'sleep_5':
                    case 'sleep_8':
                        if ($val == 1) {
                            $sleep_queue[] = $name;
                        } else {
                            $sleep_progress_bar[$name] = 1;
                        }
                        break;
                    case 'sleep_4':
                    case 'sleep_6':
                    case 'sleep_7':
                        if ($val == 0) {
                            $sleep_queue[] = $name;
                        } else {
                            $sleep_progress_bar[$name] = 1;
                        }
                        break;
                }
            }
        }

        //Add maintenance if everything is already happening
        if(count($exercise_queue) == 0){$exercise_queue[] = 'exercise_9';}
        if(count($diet_queue) == 0){$diet_queue[] = 'diet_9';}
        if(count($sleep_queue) == 0){$sleep_queue[] = 'sleep_9';}

        $userCycleObj = new UserCycles();
        $cycle = $userCycleObj->GetCycleNumForCycleId($cycle_id);

        $mind_queue = $mind_progress_bar;
        for($i = 1;$i<=($cycle+1) && $i<= 9;$i++){  //adding 1 because the images and db text keys start with '_1'
            $mind_progress_bar['mind_'.$i] = 1;
        }


        $response = array(
            'exercise_queue' => $exercise_queue,
            'diet_queue' => $diet_queue,
            'sleep_queue' => $sleep_queue,
            'mind_queue' => $mind_queue,
            'exercise_progress' => $exercise_progress_bar,
            'diet_progress' => $diet_progress_bar,
            'sleep_progress' => $sleep_progress_bar,
            'mind_progress' => $mind_progress_bar,
            'titles'=>$lifeStyleActivityTitles,
            'messages'=>$lifeStyleActivityMessages,
            'pre_activity_messages'=>$lifeStylePreActivityMessages
        );

        uasort($response['exercise_progress'],"cmp");
        uasort($response['diet_progress'],"cmp");
        uasort($response['sleep_progress'],"cmp");
        return $response;
    }

}

function cmp($a, $b)
{
    if ($a == $b) {
        return 0;
    }
    return ($a > $b) ? -1 : 1;
}
  
?>
<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

//Generic class template
// Notes:
// xx = Class Name
// xxx  = table name
// $..  = column name(s)

class UserSmartLog extends dbBase
{
    public $user_id = "";
	public $topic_id = "";
	public $activity_id = "";
	public $log = "";
	public $log_length = "";
	public $score;
	public $sort_order = "";

     function __construct($id = "")
	 {
		 parent::__construct();
		 $this->table = "user_smart_log";
		 if(strlen($id)>0)
		 {
			 parent::Load($id);
		 }
	 }

    function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','add_post');
		$id = parent::Add($postData,$ignore);
		return $id;
	}

    function Update($postData, $ignore = array(), $bUseOnlyIgnoreList = true)
	{
		$ignore_list = array_merge(array('id','update_post','date_range'),$ignore);
	    $id = parent::Update($postData,$ignore_list);
	}

    function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true)
    {
        $ignore = array('id','add_post','update_post');
        //if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        $id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
        return $id;
    }

//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////
    function getSmartLogForUserByIaAndDate($user_id, $date){

        $userCustomRecObj = new UserCustomRecommendations();
        $userCycleObj = new UserCycles();
        $cycleNum = self::getCycleNumForDate($user_id,$date);
        $cycle_id = $userCycleObj->GetCycleIdForUserCycle($user_id,$cycleNum);
        $custom_record = $userCustomRecObj->GetCustomRec($cycle_id);
        $custom_rec = isset($custom_record['recommendations_json'])? json_decode($custom_record['recommendations_json'],true) : [] ;
        $custom_topics = [];
        if(count($custom_rec)>0){
            foreach($custom_rec as $step => $topics){
                foreach($topics as $topic_id => $activities){
                    $custom_topics[$topic_id] = $activities;
                }
            }
        }

        $result = self::getLog($user_id);
        $array = array();
        $row_count = 0;
        $date_index = 0;

        if(date('w',strtotime($date)) > 0){ //0 == Sunday
            $sunday = date('Y-m-d', strtotime('last Sunday', strtotime($date)));
        } else {
            $sunday = $date;
            //create a new week.
        }

        if(!self::testDate($result,$sunday)){
            self::AddDays($result);
            $result = self::getLog($user_id);
        }

        $date_range = self::getStartEndDates($result);
        $array['date_range'] = $date_range;
        $array['log'] = array();

        mysqli_data_seek($result,0);
        $suppress_second_rows = [];
        $topics_to_suppress = [1,2,3,4];
        $log_length = 0;
		while($row = mysqli_fetch_assoc($result))
		{

		    $row['log'] = json_decode($row['log'],true);
            if($row_count == 0){
                $log_length = count($row['log']);
                $date_index = array_search($sunday,$row['log']);
            } else {
                if(count($row['log']) < $log_length){ //we need to add some days!
                    $days_to_catch_up  = $log_length - count($row['log']);
                    for($n=0;$n<$days_to_catch_up;$n++){
                        $row['log'][] = "";
                    }
                    $update_data = ['id'=>$row['id'],'log'=>json_encode($row['log']),'log_length'=>$log_length];
                    self::Update($update_data);

                }
            }
            if(in_array($row['topic_id'],[9,10])){ //Suppress Therapy and Medication
                continue;
            }
            if( $row['topic_id'] > 0 && count($custom_rec)>0 && !in_array($row['activity_id'], $custom_topics[$row['topic_id']] )){ //filter out rows not in current custom rec
                continue;
            }

            if(in_array($row['topic_id'],$suppress_second_rows)){
                continue;
            }
            if(in_array($row['topic_id'],$topics_to_suppress)){
                $suppress_second_rows[] = $row['topic_id']; //add it for next time
            }

            $row['log'] = array_slice($row['log'],$date_index,7,true);
            $array['log'][] = $row;
            $row_count++;
		}
		//var_dump($array);die();
        usort($array['log'], array("UserSmartLog","sm_cmp"));
		return $array;

	}

	function getLog($user_id){
        $link = parent::createLinki();

        $sql = "SELECT t.*,rt.title AS 'topic',pt.value AS 'activity' ";
        $sql .= " from ".$this->table." t ";
        $sql .= " LEFT JOIN recommendation_topics rt ON rt.id = t.topic_id";
        $sql .= " LEFT JOIN recommendation_activities ra ON ra.id = t.activity_id";
        $sql .= " LEFT JOIN pagetext pt ON pt.elementname = ra.text_key and pt.pagename='recommendation_activities_title'";
        $sql .= " where user_id = '".$user_id."' ORDER BY rt.display_order ASC, ra.priority DESC";
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        mysqli_close($link);
        return $result;
    }

    function getLogData($user_id){
        $results = self::getLog($user_id);
        ///
        $dateObj = new DateTime();
        $date = $dateObj->format('Y-m-d');
        if(date('w',strtotime($date)) > 0){ //0 == Sunday
            $sunday = date('Y-m-d', strtotime('last Sunday', strtotime($date)));
        } else {
            $sunday = $date;
            //create a new week.
        }

        if(!self::testDate($results,$sunday)){
            self::AddDays($results);
            $results = self::getLog($user_id);
        }
        ///
        ///
        $data = array();
        while($row = mysqli_fetch_assoc($results))
        {
            $data[] = $row;
        }
        return $data;
    }

    function testDate($result,$date){
        mysqli_data_seek($result,0);
        $row = mysqli_fetch_assoc($result);
        if(!isset($row['log'])){
            return false;
        }
        $row['log'] = json_decode($row['log'],true);
        $date_index = array_search($date,$row['log']);
        return $date_index === false? false : true;
    }

    function getStartEndDates($result){
        $today_dt = new DateTime();
        $today =  $today_dt->format('Y-m-d');
        $today_dt->modify('+1 day');
        $tomorrow = $today_dt->format('Y-m-d');
        $range = array("start"=>$today,"end"=>$tomorrow);

         mysqli_data_seek($result,0);
        $row = mysqli_fetch_assoc($result);
        if(!isset($row['log'])){
            return $range;
        }
        $row['log'] = json_decode($row['log'],true);
        $min = $row['log'][0];
        $max = end($row['log']);
        mysqli_data_seek($result,0);
        return array("start"=>$min,"end"=>$max);
    }


    function CreateLog($user_id,$date,$rows){

		//Each row needs to look like
		//   array("topic_id"=>"activity_id",""=>"","sort_order"=>"")
		//where sort order starts a '1'



		$calendar = array();
		$empty_log = array();


        $sunday = date('Y-m-d', strtotime('last Sunday', strtotime($date)));
        $datetime = new DateTime($sunday);
        $length=7;

        $log_length = self::GetLogLength($user_id);
        if($log_length > 7){
            $sunday = self::GetFirstLogEntryDate($user_id);
            $length = $log_length;
        }


		for ($i = 0; $i<$length; $i++) {
			//echo $datetime->format('Y-m-d');
			$calendar[] = $datetime->format('Y-m-d');
			$empty_log[] = "";
			$datetime->modify('+1 day');
		}

        if (!self::alreadyCreated($user_id,0,0)) { //Add calendar row
            $new_log_row = array(
                "user_id" => $user_id,
                "topic_id" => "0",
                "activity_id" => "0",
                "log" => json_encode($calendar),
                "log_length" => $length,
                "sort_order" => "0");
            self::Add($new_log_row);
        }

        //Add activities
		foreach($rows as $row){

            if (!self::alreadyCreated($user_id,$row['topic_id'],$row['activity_id'])) {
                $new_log_row = array(
                    "user_id" => $user_id,
                    "topic_id" => $row['topic_id'],
                    "activity_id" => $row['activity_id'],
                    "log" => json_encode($empty_log),
                    "log_length" => $length,
                    "sort_order" => $row['sort_order']);
                self::Add($new_log_row);
            }
        }
	}

	function alreadyCreated($user_id,$topic_id,$activity_id){
        $link = parent::createLinki();
        $sql = "SELECT * ";
        $sql .= " from ".$this->table." t ";
        $sql .= " where user_id = '".$user_id."' AND topic_id='".$topic_id."' AND activity_id='".$activity_id."'";

        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $row_count = false;
        while($row = mysqli_fetch_assoc($result))
        {
            $row_count = true;
        }
        //var_dump($array);die();
        mysqli_close($link);
        return $row_count;
    }

    function AddDays($result){
        mysqli_data_seek($result,0);
        $row = mysqli_fetch_assoc($result);
        $calendar = array();
        $days = array();
        $next_sunday_dt = new DateTime('next sunday');
        $next_sunday =  $next_sunday_dt->format('Y-m-d');
        if(!isset($row['log'])){
            return false;
        }
        $row['log'] = json_decode($row['log'],true);

         //get latest day
        $lastday = end($row['log']);
        $rolling_dt = new DateTime($lastday);

        //build and save new header row
        $rolling_dt->modify('+1 day');
        $rolling_date = $rolling_dt->format('Y-m-d');
        while($rolling_date < $next_sunday){
            $calendar[] = $rolling_date;
            $days[] = "";
            $rolling_dt->modify('+1 day');
            $rolling_date = $rolling_dt->format('Y-m-d');
        }
        $row['log_length'] = count($row['log']) + count($calendar);
        $row['log'] = json_encode(array_merge($row['log'],$calendar));
        $ignore_list = array("id","topic","activity");
        self::Update($row,$ignore_list);  //Update header row

        //Add extra days to each line item
        while($row = mysqli_fetch_assoc($result)){
            $row['log'] = json_decode($row['log'],true);
            $row['log_length'] = count($row['log']) + count($days);
            $row['log'] = json_encode(array_merge($row['log'],$days));
            self::Update($row,$ignore_list);  //Update header row
        }

    }

    function setCheck($post_data){
        $grid_val = $_REQUEST['gridval'];
        $coordinates  = explode("-",$post_data['grid']);
        $id = $coordinates[0];
        $row_num = $coordinates[1];

        $row = self::GetData($id)[0];
        $row['log'] = json_decode($row['log'],true);
        $row['log'][$row_num] = $grid_val;
        $row['log'] = json_encode($row['log']);
        self::Update($row);

    }

    function GetFirstLogEntryDate($user_id){
        $link = parent::createLinki();
        $sql = "SELECT * ";
        $sql .= " from ".$this->table." t ";
        $sql .= " where t.user_id = '".$user_id."' ORDER BY sort_order ASC";
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );

        $first_log_entry_date = '0000-00-00';
        $dates = array();
        while ($log_data =  mysqli_fetch_assoc($result)) {
            $log = json_decode($log_data['log'], true);

            //Get log dates as an array
            if($log_data['topic_id']==0 and $log_data['activity_id']==0){
                $log_length = count($log);
                $index = $log_length;
                $dates = $log;
            }

            //go throught each log row and find the position of the earliest checkmark
            foreach ($log as $column_num => $val) {
                if ($column_num >= $index) {
                    continue;
                }
                if ($val == '1') {
                    $index = $column_num;
                    continue;
                }
            }

            //get the data at that position
            if ($index < $log_length) {
                $first_log_entry_date = $dates[$index];
            }
        }

        return $first_log_entry_date; //note this is in UTC
    }

    function GetLogLength($user_id){
        $first_log_entry = self::GetFirstLogEntryDate($user_id);
        if($first_log_entry == '0000-00-00') {
           return 0;
        }

        $now = new DateTime();
        $first_date = new DateTime($first_log_entry);
        $diff = $now->diff($first_date);
        $days = ($diff->days) ;
        return $days;
    }

    function GetCurrentScore($user_id,$current_cycle_id){
        //make sure the log length is right//////////////////////
        $result = self::getLog($user_id);
        $dateObj = new DateTime();
        $date = $dateObj->format('Y-m-d');
        if(date('w',strtotime($date)) > 0){ //0 == Sunday
            $sunday = date('Y-m-d', strtotime('last Sunday', strtotime($date)));
        } else {
            $sunday = $date;
            //create a new week.
        }
        if(!self::testDate($result,$sunday)){
            self::AddDays($result);
            $result = self::getLog($user_id);
        }


        $userCyclesObj = new UserCycles();
        $current_cycle_data = $userCyclesObj->GetDataForUserCycleId($current_cycle_id);
        $current_cycle = $current_cycle_data['cycle_num'];

        $userCustomRecObj = new UserCustomRecommendations();
        $current_activities = $userCustomRecObj->GetActivityList($current_cycle_id );
        /////////////////////////////////////////////////////// tbd: Fix JaneDemo

        //
        // For demo
        if (in_array($user_id,[6])) {

            switch(true){
                case $current_cycle <= 3:
                    $lifestylescores = array('exercise' => '89', 'diet' => '72', 'sleep' => '32', 'supplements' => '71', 'adaptogens' => '73', 'mind' => '88');
                    break;
                case $current_cycle <= 7:
                    $lifestylescores = array('exercise' => '78', 'diet' => '63', 'sleep' => '45', 'supplements' => '78', 'adaptogens' => '71', 'mind' => '89');
                    break;
                case $current_cycle <= 11:
                    $lifestylescores = array('exercise' => '71', 'diet' => '69', 'sleep' => '49', 'supplements' => '71', 'adaptogens' => '88', 'mind' => '92');
                    break;
                default:
                    $lifestylescores = array('exercise' => '75', 'diet' => '67', 'sleep' => '55', 'supplements' => '78', 'adaptogens' => '80', 'mind' => '89');
                    break;

            }
            return $lifestylescores;
        }


        //////////////


        $topicsObj = new RecommendationTopics();
        $topic_data = $topicsObj->GetAllData();
        $topic_map = [];
        $topic_activities = [];  //activities in this topic
        $compliance = [];
        $scores = [];
        foreach($topic_data as $topic){
            if($topic == 'retest'){continue;}
            $topic_name = $topic['text_key'] != "body" ? $topic['text_key'] != 'nutrients'? $topic['text_key']:"supplements" : "exercise";
            $topic_map[$topic['id']] = $topic_name;
        }

        //default the scores and row counts
        foreach($topic_map as $topic){
            $scores[$topic] = 0;
            $topic_activities[$topic] = [];
        }

        $first_log_entry = self::GetFirstLogEntryDate($user_id);
        if ($first_log_entry == '0000-00-00') {
            return $scores;
        }

        $now = new DateTime();
        $first_date = new DateTime($first_log_entry);
        $diff = $now->diff($first_date);
        $log_length = ($diff->days);
        $current_cycle_starting_index = $current_cycle*14;


        /// look in smartlog
        /// for each topic
        /// for each activity filtered by activities in user's current rec
        /// create a compliance array for days from  $current_cycle_starting_index to the end (or for $days_into_cycle)
        /// then sum up the array as per the 14 days in a cycle.

        $user_smart_log = self::getLogData($user_id);
        foreach($user_smart_log as $log_row => $log_data){
            if($log_row == 0){continue;} //skip dates row
            if(count($current_activities)> 0 && !in_array($log_data['activity_id'],$current_activities)){continue;} //only consider activities in the current rec
            $log = json_decode($log_data['log'],true);
            for($day = $current_cycle_starting_index; (($day < $log_length) && ($day < $current_cycle_starting_index+14)); $day++){
                $topic = $topic_map[$log_data['topic_id']];
                if(!isset($compliance[$topic])){$compliance[$topic] = [];}
                if(!isset($compliance[$topic][$day])){$compliance[$topic][$day] = false;}
                if(!in_array($log_data['activity_id'],$topic_activities[$topic])){
                    $topic_activities[$topic][] = $log_data['activity_id'];
                }
                $current = $compliance[$topic][$day];
                $new = $log[$day];
                $compliance[$topic][$day] = (filter_var($compliance[$topic][$day],FILTER_VALIDATE_BOOLEAN) or filter_var($log[$day],FILTER_VALIDATE_BOOLEAN));
            }

        }

        //Set the scores
        foreach($compliance as $topic => $compliance_data){
            $count = 0;
            foreach($compliance_data as $day){
                $count += intval($day);
            }
            if($topic == 'exercise'){
                $scores[$topic] = strval(min(round(100*($count/(10*count($topic_activities[$topic]))),0),100)); //only
            } else {
                $scores[$topic] = strval(min(round(100*($count/(14*count($topic_activities[$topic]))),0),100));
            }


        }
       // echo "<pre>".print_r($scores)."</pre>";
        return $scores;

    }

    function getCycleNumForDate($user_id,$date_to_test){
        $first_log_entry = self::GetFirstLogEntryDate($user_id);
        if($first_log_entry == '0000-00-00'){ return 0;}

        $test_date = new DateTime($date_to_test);
        $first_date = new DateTime($first_log_entry);
        $diff = $test_date->diff($first_date);
        $length = ($diff->days);
        //return intval($length/14)+1;
        return intval($length/14);

    }

    /* Private Functions */

    private static function sm_cmp($a, $b){
        if ($a['sort_order'] == $b['sort_order']) {
            return 0;
        }
        return ($a['sort_order'] < $b['sort_order']) ? -1 : 1;
    }

}


?>
<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

class EmailQueue extends dbBase
{
	public $send_to = "";
	public $send_subject = "";
	public $message_body = "";
	public $send_from = "";
	public $send_cc = "";
	public $send_bcc = "";
	public $other_headers = "";  //an array of key value pairs
	public $created = "";
	public $send_status = "";

   function __construct($id = "")
   {
    parent::__construct();
	  $this->table = "email_queue";
		if(strlen($id)>0)
		{
			parent::Load($id);
		}
   }

	function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','add_post');
		$id = parent::Add($postData,$ignore);
		return $id;
	}

	function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','update_post');
		$id = parent::Update($postData,$ignore);
	}

	function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','add_post','update_post');
		//if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
		$id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
		return $id;
	}

////////////////////////////////////////////////////
////  Class Specific Functions
////////////////////////////////////////////////////
    function QueueMessage($send_to,$send_subject,$message_body,$send_from,$send_cc='',$send_bcc='',$other_headers=[]){
		$hash = hash('sha512',$send_to.":".$send_subject.":".$message_body.":".$send_from);
		$new_email = array(
			'send_to' => addslashes($send_to),
			'send_subject' => addslashes($send_subject),
			'message_body' => addslashes($message_body),
			'send_from' => addslashes($send_from),
			'send_cc' => addslashes($send_cc),
			'send_bcc' => addslashes($send_bcc),
			'other_headers' => json_encode($other_headers),
			'message_hash' => $hash
		);
		$result = self::Add($new_email);
		return $result;
	}

	function GetUnsentMail(){
		$link = parent::createLinki();
		$sql = "SELECT * from `".$this->table."` tbl where tbl.send_status = 'queued'";
		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
		$array = array();
        while($row = mysqli_fetch_assoc($result))
        {
            $array[] = $row;
        }
		mysqli_close($link);
		return $array;

    }

    function SetMailToSent($id){
		$link = parent::createLinki();
		$sql = "UPDATE `".$this->table."` tbl SET tbl.send_status = 'sent' where tbl.id = '".$id."'";
		$result = mysqli_query($link,$sql) or die( "UPDATE: ".mysqli_error($link)."<br/>".$sql );
		mysqli_close($link);
		return $result;
	}
	function SetMailToError($id){
		$link = parent::createLinki();
		$sql = "UPDATE `".$this->table."` tbl SET tbl.send_status = 'error' where tbl.id = '".$id."'";
		$result = mysqli_query($link,$sql) or die( "UPDATE: ".mysqli_error($link)."<br/>".$sql );
		mysqli_close($link);
		return $result;
	}

	function GetDailyNotifications(){


		$link = parent::createLinki();
		$sql = "SELECT 
					u.email,
					ud.firstname,
					ud.lastname,
					sl.id,
					DATEDIFF(NOW(),REPLACE(REPLACE(SUBSTRING(sl.log,3,10),'\"',''),',','')) AS 'days' -- ,
					-- CAST( (DATEDIFF(NOW(),REPLACE(REPLACE(SUBSTRING(sl.log,3,10),'\"',''),',','')) / 7) AS UNSIGNED) as message_number,
				FROM user_smart_log sl
				JOIN users u ON u.id = sl.user_id
				JOIN user_data ud ON ud.user_id = sl.user_id
				WHERE  sl.id IN(
					SELECT MAX(sl.id) FROM user_smart_log sl
					WHERE DATEDIFF(NOW(),REPLACE(REPLACE(SUBSTRING(sl.log,3,10),'\"',''),',','')) IS NOT NULL
					GROUP BY sl.user_id
				)
				-- AND MOD(DATEDIFF(NOW(),REPLACE(REPLACE(SUBSTRING(sl.log,3,10),'\"',''),',','')),7) = 0
				GROUP BY u.id
				ORDER BY u.id DESC;";
		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
		$array = array();
		while($row = mysqli_fetch_assoc($result))
		{
			$array[] = $row;
		}
		mysqli_close($link);
		return $array;
	}

}


?>

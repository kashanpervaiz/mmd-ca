<?php 
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

class UserCycles extends dbBase
{
    public $user_id = "";
    public $cycle_num = "";
    public $start_date = "";
    public $end_date = "";

	 function __construct($id = "")
   {
    parent::__construct();
	  $this->table = "user_cycles";
		if(strlen($id)>0)
		{
			parent::Load($id);
		}
   }

	function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','add_post');
		$id = parent::Add($postData,$ignore,$bUseOnlyIgnoreList);
		return $id;
	}

	function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','update_post');
		$id = parent::Update($postData,$ignore,$bUseOnlyIgnoreList);
	}

    function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true)
    {
        $ignore = array('id','add_post','update_post');
        //if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        $id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
        return $id;
    }


//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////
    function GetUserIdForCycleId($cycle_id){
        $link = parent::createLinki();
        $sql = "SELECT user_id ";
        $sql .= " from ".$this->table." t ";
        $sql .= " where t.id = '".$cycle_id." LIMIT 1'";
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $response = [];
        while($row = mysqli_fetch_assoc($result))
        {
            $response = $row['user_id'];
        }
        mysqli_close($link);
        return $response;
    }

    function GetCycleNumForCycleId($cycle_id){
        $link = parent::createLinki();
        $sql = "SELECT cycle_num ";
        $sql .= " from ".$this->table." t ";
        $sql .= " where t.id = '".$cycle_id." LIMIT 1'";
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $response = [];
        while($row = mysqli_fetch_assoc($result))
        {
            $response = $row['cycle_num'];
        }
        mysqli_close($link);
        return $response;
    }

    function GetUserIdAndCycleNumForCycleId($cycle_id){
        $link = parent::createLinki();
        $sql = "SELECT user_id,cycle_num ";
        $sql .= " from ".$this->table." t ";
        $sql .= " where t.id = '".$cycle_id." LIMIT 1'";
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $response = [];
        while($row = mysqli_fetch_assoc($result))
        {
            $response = $row;
        }
        mysqli_close($link);
        return $response;
    }

    function GetCycleIdsUptoCycleId($cycle_id){

	    $cycle_data = self::GetUserIdAndCycleNumForCycleId($cycle_id) ;

        $link = parent::createLinki();
        $sql = "SELECT id ";
        $sql .= " from ".$this->table." t ";
        $sql .= " where t.cycle_num <='".$cycle_data['cycle_num']."' AND t.user_id = '".$cycle_data['user_id']."'";
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $response = [];
        while($row = mysqli_fetch_assoc($result))
        {
            $response[] = $row['id'];
        }
        mysqli_close($link);
        return $response;
    }


    function GetUserCycles($user_id,  $orderBy='ASC'){
        $link = parent::createLinki();
        $sql = "SELECT * ";
        $sql .= " from ".$this->table." t ";
        $sql .= " where user_id = '".$user_id."' ORDER BY cycle_num ".$orderBy;
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $response = [];
        while($row = mysqli_fetch_assoc($result))
        {
            $response[] = $row;
        }
        mysqli_close($link);
        return $response;
    }

    function GetLatestUserCycle($user_id){
        $link = parent::createLinki();
        $sql = "SELECT * ";
        $sql .= " from ".$this->table." t ";
        $sql .= " where user_id = '".$user_id."' ORDER BY cycle_num DESC limit 1";
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $response = [];
        while($row = mysqli_fetch_assoc($result))
        {
            $response = $row;
        }
        mysqli_close($link);
        return $response;
    }

    function GetUserCycleIds($user_id,  $orderBy='ASC'){
        $link = parent::createLinki();
        $sql = "SELECT id ";
        $sql .= " from ".$this->table." t ";
        $sql .= " where user_id = '".$user_id."' ORDER BY cycle_num ".$orderBy;
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $response = [];
        while($row = mysqli_fetch_assoc($result))
        {
            $response[] = $row;
        }
        mysqli_close($link);
        return $response;
    }

    function GetDataForUserCycleId($cycle_id){
        $link = parent::createLinki();
        $sql = "SELECT * ";
        $sql .= " from ".$this->table." t ";
        $sql .= " where id = '".$cycle_id."'";
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $response = [];
        while($row = mysqli_fetch_assoc($result))
        {
            $response = $row;
        }
        mysqli_close($link);
        return $response;
    }

    function GetCycleIdForUserCycle($user_id,$cycle_num){
        $link = parent::createLinki();
        $sql = "SELECT id ";
        $sql .= " from ".$this->table." t ";
        $sql .= " where user_id = '".$user_id."' AND cycle_num='".$cycle_num."'";
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $response = [];
        while($row = mysqli_fetch_assoc($result))
        {
            $response = $row;
        }
        mysqli_close($link);
        return isset($response['id']) ? $response['id'] : 0; //0 is not a valid value for an index
    }


    function GetCycleIdForUserDate($user_id,$date){
        $link = parent::createLinki();
        $sql = "SELECT id ";
        $sql .= " from ".$this->table." t ";
        $sql .= " where t.user_id = '".$user_id."' AND t.start_date <='".$date."' AND t.end_date >='".$date."'";
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $response = [];
        while($row = mysqli_fetch_assoc($result))
        {
            $response = $row;
        }
        mysqli_close($link);
        return isset($response['id']) ? $response['id'] : 0; //0 is not a valid value for an index
    }
    
    function UpdateUserCycles($user_id){
        $userSmartLogObj = new UserSmartLog();

        $first_date = $userSmartLogObj->GetFirstLogEntryDate($user_id);
        $log_length = $userSmartLogObj->GetLogLength($user_id);
        $max_cycle_num = intval($log_length / 14);

        if($first_date != '0000-00-00'){
            $start_datetime = new DateTime($first_date);
            $current_date = $start_datetime->format('Y-m-d');

            for($cycle = 0; $cycle <= $max_cycle_num; $cycle++){

                $cycle_num = $userSmartLogObj->getCycleNumForDate($user_id, $current_date);
                $next_row = ['user_id' => $user_id,'cycle_num' => $cycle_num, 'start_date' => $current_date, 'end_date'=>date( "Y-m-d", strtotime( $current_date." +13 day" ))];

                if(self::Add($next_row, [], false) < 0){
                    $cycle_id = self::GetCycleIdForUserCycle($user_id,$cycle_num);
                    $next_row['id'] = $cycle_id;
                    self::Update($next_row, [], false);
                }

                $next_datetime = $start_datetime->modify('+14 day');
                $current_date = $next_datetime->format('Y-m-d');

            }
        } else {
            $start_datetime = new DateTime();
            $current_date = $start_datetime->format('Y-m-d');
            //$default = ['user_id' => $user_id,'cycle_num' => '0', 'start_date' => $current_date, 'end_date'=>date( "Y-m-d", strtotime( $current_date." +13 day" ))];
            $default = ['user_id' => $user_id,'cycle_num' => '0', 'start_date' => null, 'end_date'=> null];
            if(self::Add($default, [], false) < 0){
                $cycle_id = self::GetCycleIdForUserCycle($user_id,'0');
                $default['id'] = $cycle_id;
                self::Update($default, [], false);
            }
        }
    }

    function ClearUserCycles($user_id){
        $link = parent::createLinki();

        $update_sql = "UPDATE ".$this->table." t SET t.start_date = null,t.end_date = null WHERE t.user_id = '".$user_id."'";
        mysqli_query($link,$update_sql) or die( "CLEAR: ".mysqli_error($link)."<br/>".$update_sql );

        mysqli_close($link);
    }

}
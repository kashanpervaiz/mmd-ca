<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"]) . "/includes/dbclasses/dbBaseV2.php";

class NotificationTriggers extends dbBase
{
    public $cycle_number = null;
    public $days_in_cycle = null;
    public $template = "";

    function __construct($id = "")
    {
        parent::__construct();
        $this->table = 'notification_triggers';
        if (strlen($id) > 0) {
            parent::Load($id);
        }
    }

    function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true)
    {
        $ignore = array('id', 'add_post');
        $id = parent::Add($postData, $ignore);
        return $id;
    }

    function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true)
    {
        $ignore = array('id', 'update_post');
        $id = parent::Update($postData, $ignore);
    }

    function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true)
    {
        $ignore = array('id', 'add_post', 'update_post');
        //if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        $id = parent::Replace($postData, $ignore, $bUseOnlyIgnoreList);
        return $id;
    }

//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////

    /**
     * Returns a list of notifications based on cycle number and loglength
     * @return array
     */
    public function getNotificationsByLogLength()
    {
        $link = parent::createLinki();
        $result = mysqli_query($link, "SELECT * FROM {$this->table}") or die(mysqli_error());
        $notifications = [];
        while ($row = mysqli_fetch_assoc($result)) {
            $notifications[$row['cycle_number'] . '-' . $row['days_in_cycle']] = $row['template'];
        }
        mysqli_close($link);
        return $notifications;
    }
}

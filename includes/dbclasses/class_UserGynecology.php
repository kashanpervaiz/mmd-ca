<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

//Generic class template
// Notes:
// xx = Class Name
// xxx  = table name
// $..  = column name(s)

class UserGynecology extends dbBase
{
    public $user_id = "";
    public $kit_id = "";
    public $menses_cycle_type_id = "";
    public $mensesfirstday_ts = "";
    public $hysterectomy = "";
    public $hysterectomy_ts = "";
    public $ovaries_removed = "";
    public $ovaries_removed_ts = "";
    public $currently_pregnant = "";
    public $currently_pregnant_ts = "";
    public $date_submitted = "";

	 function __construct($id = "")
   {
    parent::__construct();
	  $this->table = "user_gynecology";
		if(strlen($id)>0)
		{
			parent::Load($id);
		}
   }

	function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','add_post');
        if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
		$id = parent::Add($postData,$ignore,$bUseOnlyIgnoreList);
		return $id;
	}

	function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','update_post');
		$id = parent::Update($postData,$ignore,$bUseOnlyIgnoreList);
	}

    function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true)
    {
        $ignore = array('id','add_post','update_post');
        if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        $id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
        return $id;
    }


//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////
function getMensesState($user_id, $kit_id){
    $userDataObj = new UserData();
    $userData = $userDataObj->GetUserData($user_id);
    if($userData['gender'] == 'M'){return "x";}

    $kitDataObj = new KitData();
    $kitData = $kitDataObj->GetData($kit_id);
    $collectionDate = $kitData[0]['collectionDate'];

    $link = parent::createLinki();
    $sql = "SELECT DATE(`mensesfirstday_ts`) AS 'mensesfirstday' FROM `".$this->table."` WHERE `user_id` = '".$user_id."' and DATE(`mensesfirstday_ts`) <= '".$collectionDate."' ORDER BY `id` DESC LIMIT 1";
    $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
    $row = mysqli_fetch_assoc($result);
    mysqli_close($link);

    $collection_date = new DateTime($collectionDate);
    $mensesfirstday_date = new DateTime($row['mensesfirstday']);

    $diff = $mensesfirstday_date->diff($collection_date);
    //$diff = $collection_date->diff($mensesfirstday_date);

    $days = ($diff->days) ;
    $status = 'l';
    switch(true){
        case $days <= 7:
            $status = 'f';
            break;
        case $days <= 14:
            $status = 'o';
            break;
        default:
            $status = 'l';
    }
    return $status;

}

}


?>
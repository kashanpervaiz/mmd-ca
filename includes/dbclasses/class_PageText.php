<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

class PageText extends dbBase
{
	public $pagename = "";
	public $elementname = "";
	public $value = "";
	public $language = "";
	public $enable = "";

	 function __construct(){
		parent::__construct();
		$this->table = "pagetext";
   }
   
	function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true){
        $link = parent::createLinki();
	    $ignore = array('id','update_pagetext','pageSelector','languageSelector','elementSelector','update_post');
        $postData['value'] = mysqli_real_escape_string($link,$postData['value']);
		$id = parent::Update($postData,$ignore);
	}

    function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true){
        $ignore = array('id','add_post','update_post');
        //if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        $id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
        return $id;
    }
	
	function UpdateSpecific($pagename ="", $elementname="", $language ="", $newValue){
		$link = parent::createLinki();
		$newValue = mysqli_real_escape_string($link,$newValue);		
		$sql = "UPDATE `pagetext` SET `value` = '".$newValue."' WHERE `enabled` = '1' AND `pagename` = '".$pagename."' AND `elementname` = '".$elementname."' AND `language` = '".$language."'";
		$result = mysqli_query($link,$sql) or die( "UPDATE: ".mysqli_error($link)."<br/>".$sql );
		mysqli_close($link);
	}
	
	function UpdateAllLanguages($pagename ="", $elementname="", $newValue){

			$Languages = new Languages();
			$activeLanguages = $Languages->GetActiveLanguages();
		
		$link = parent::createLinki();		
		$newValue = mysqli_real_escape_string($link,$newValue);
		
		foreach($activeLanguages as $lang)
		{
					
			$sql = "UPDATE `pagetext` SET `value` = '".$newValue."' WHERE `enabled` = '1' AND BINARY `pagename` = '".$pagename."' AND BINARY `elementname` = '".$elementname."' AND `language` = '".$lang['abbreviation2']."'";
			$result = mysqli_query($link,$sql) or die( "UPDATE: ".mysqli_error($link)."<br/>".$sql );
		}
					
		mysqli_close($link);					
	}
	
	function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true){
			$Languages = new Languages();
			$activeLanguages = $Languages->GetActiveLanguages();

			$ignore = array('id','add_pagetext','pageSelector','languageSelector','elementSelector','add_post');
		
			foreach($activeLanguages as $lang)
			{
				$postData['language'] = $lang['abbreviation2'];
				$id = parent::Add($postData,$ignore);
			}
					
	}
	
	function AddRow($postData){
			$ignore = array('id','add_pagetext','add_post');		
			$id = parent::Add($postData,$ignore);
			return $id;			
	}
	
	function GetData($pagename ="", $language ="", $elementname = ""){


				$link = parent::createLinki();
				
				$sql = "SELECT * FROM `pagetext` WHERE `enabled` = '1'";
				
				if(strlen($pagename) > 0)
				{
                   // $sql .= "  AND BINARY `pagename` = '".$pagename."'";
                    $sql .= "  AND `pagename` = '".$pagename."'";
				}
				
				if(strlen($language) > 0)
				{
                    //$sql .= " AND BINARY `language` = '".$language."'";
                    $sql .= " AND `language` = '".$language."'";
				}
				
				if(strlen($elementname) > 0)
				{
                    //$sql .= " AND BINARY `elementname` = '".$elementname."'";
                    $sql .= " AND `elementname` = '".$elementname."'";
				}

				$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
				while($row = mysqli_fetch_assoc($result)){
					$array[] = $row;}
				
                mysqli_close($link);
				if(isset($array))
				{
				 reset($array);
				 return $array;
				} else {
                    return array(
                        'id'=>'0',
                        'pagename'=>$pagename,
                        'elementname'=>$elementname,
                        'value'=>'Noting found',
                        'language'=>'en',
                        'enabled'=>'1',
                        'sql'=>$sql
                    );
                }

	}	

	function GetDataDESC($pagename ="", $language =""){


				$link = parent::createLinki();
				
				$sql = "SELECT * FROM `pagetext` WHERE `enabled` = '1'";
				
				if(strlen($pagename) > 0)
				{
					$sql .= "  AND `pagename` = '".$pagename."'";
				}
				
				if(strlen($language) > 0)
				{
					$sql .= " AND `language` = '".$language."'";
				}
				$sql .= " ORDER BY `id` DESC";
				
				$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
				while($row = mysqli_fetch_assoc($result)){
					$array[] = $row;}

                mysqli_close($link);

				if(isset($array))
				{
				 reset($array);
				 return $array;
				}

	}	
	
	function GetPageText($pagename ="", $language =""){
			if(strlen($pagename) > 0 )
			{
				$link = parent::createLinki();
				
				$sql = "SELECT * FROM `pagetext` where `enabled` = '1' AND `pagename` = '".$pagename."'";
				
				if(strlen($language) > 0)
				{
					$sql .= " AND `language` = '".$language."'";
				}
				
				$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
				$array = array();
				while($row = mysqli_fetch_assoc($result))
				{
					$array[$row['elementname']] = $row['value'];
				}
				mysqli_close($link);
				return $array;
			}
	}
  
    function GetSpecificPageTextValue($pagename ="", $elementname="", $language ="",$replace=array()){  //i.e. ['bryan':'brian']
			if(strlen($pagename) > 0 )
			{
				$link = parent::createLinki();
				
				$sql = "SELECT * FROM `pagetext` where `enabled` = '1' AND `pagename` = '".$pagename."'";
				
				if(strlen($elementname) > 0)
				{
					$sql .= " AND `elementname` = '".$elementname."'";
				}
				
				if(strlen($language) > 0)
				{
					$sql .= " AND `language` = '".$language."'";
				}
				
				$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
				$value = "";
				while($row = mysqli_fetch_assoc($result))
				{
                    $value = $row['value'];
				    if(count($replace)){
                        $key = key($replace);
                        $val= $replace[$key];
                        $find = "%%".$key."%%";
                        $value = str_replace($find,$val,$value);
                    }
				}
				mysqli_close($link);
				return $value;
			}
	}
	
	function GetSpecificPageText($pagename ="", $elementname="", $language =""){
			if(strlen($pagename) > 0 )
			{
				$link = parent::createLinki();
				
				$sql = "SELECT * FROM `pagetext` where `enabled` = '1' AND `pagename` = '".$pagename."'";
				
				if(strlen($elementname) > 0)
				{
					$sql .= " AND `elementname` = '".$elementname."'";
				}
				
				if(strlen($language) > 0)
				{
					$sql .= " AND `language` = '".$language."'";
				}
				
				$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
				$array = array();
				while($row = mysqli_fetch_assoc($result))
				{
					$array[$row['elementname']] = $row['value'];
				}
				mysqli_close($link);
				return $array;
			}
	}	
	
	function GetActivePages(){
		$link = parent::createLinki();
		$sql = "SELECT `pagename` from `pagetext` where `enabled` = '1' GROUP BY `pagename`";
		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
		$array = array();
				while($row = mysqli_fetch_array($result))
				{
					$array[] = $row[0];
				}
		//var_dump($array);
		mysqli_close($link);
		return $array;
	}		
	
	function GetActiveLanguags(){
		$link = parent::createLinki();
		$sql = "SELECT `language` from `pagetext` where `enabled` = '1' GROUP BY `language`";
		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
		$array = array();
				while($row = mysqli_fetch_array($result))
				{
					$array[] = $row[0];
				}
		//var_dump($array);
		mysqli_close($link);
		return $array;
	}	
	
	function GetElementNames($pagename){
		$link = parent::createLinki();
		$sql = "SELECT `elementname` from `pagetext` where `pagename`='".$pagename."' AND `enabled` = '1' GROUP BY `elementname`  COLLATE 'utf8_bin'";
		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
		$array = array();
				while($row = mysqli_fetch_array($result))
				{
					$array[] = $row[0];
				}
		//var_dump($array);
		mysqli_close($link);
		return $array;
	}	
	
	function GetElementNamesEndingWith($pagename, $elementEnding ){
		$link = parent::createLinki();
		$sql = "SELECT `elementname` from `pagetext` where `pagename` = '".$pagename."' AND `elementname` LIKE '%".$elementEnding."' AND `enabled` = '1' GROUP BY `elementname`  COLLATE 'utf8_bin'";
		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
		$array = array();
				while($row = mysqli_fetch_array($result))
				{
					$array[] = $row[0];
				}
		//var_dump($array);
		mysqli_close($link);
		return $array;
	}	
	
		
	function GetSpecificPageTextWhereElementStartsWith($pagename ="", $elementname="", $language =""){
			if(strlen($pagename) > 0 )
			{
				$link = parent::createLinki();
				
				$sql = "SELECT * FROM `pagetext` where `pagename` = '".$pagename."'";
				
				if(strlen($elementname) > 0)
				{
					$sql .= " AND `elementname` LIKE '".$elementname."%'";
				}
				
				if(strlen($language) > 0)
				{
					$sql .= " AND `language` = '".$language."'";
				}
				
				$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
				$array = array();
				while($row = mysqli_fetch_assoc($result))
				{
					$array[$row['elementname']] = $row['value'];
				}
				mysqli_close($link);
				return $array;
			}
	}	

    function GetDataForLanguage($language = "en"){

            $link = parent::createLinki();

            $sql = "SELECT * FROM `".$this->table."` WHERE `language` = '".$language."'";

            $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
            $array = array();
            while($row = mysqli_fetch_assoc($result))
            {
                $array[] = $row;
            }
            mysqli_close($link);
            return $array;

    }

    function GetRecommendationText($language = "en"){

        $link = parent::createLinki();

        $sql = "SELECT * FROM pagetext pt WHERE 
                pt.language = '$language' AND                 
                pt.pagename IN (
                'recommendation_steps',
                'recommendation_topics',
                'recommendation_topics_education',
                'recommendation_activities_title',
                'recommendation_activities_subtitle',
                'recommendation_activities_explanation',
                'recommendation_activities_explanation_popup',
                'recommendation_activities_explanation_foodsource',
                'VendorNameMap'
                )";

        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $array = array();
        while($row = mysqli_fetch_assoc($result))
        {
            if(!isset($array[$row['pagename']])){
                $array[$row['pagename']] = [];
            }
            if(!isset($array[$row['pagename']][$row['elementname']])){
                $array[$row['pagename']][$row['elementname']] = '';
            }
            $array[$row['pagename']][$row['elementname']] = $row['value'];
        }
        mysqli_close($link);
        return $array;

    }

		
}

?>
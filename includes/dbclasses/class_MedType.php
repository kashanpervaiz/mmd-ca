<?php 
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

class MedType extends dbBase
{
	public $name = "";
	public $value = "";

	 function __construct($id = "") 
   {
    parent::__construct();
	  $this->table = "med_type";
		if(strlen($id)>0)
		{
			parent::Load($id,$this->table);
		}
   }
	
	function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','add_post');		
		$id = parent::Add($postData,$ignore);	
		return $id;
	}
		
	function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','update_post');
		$id = parent::Update($postData,$ignore);
	}

    function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true)
    {
        $ignore = array('id','add_post','update_post');
        //if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        $id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
        return $id;
    }
	
//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////
	
	
}

  
?>
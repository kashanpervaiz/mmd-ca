<?php
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

////Generic class template
//// Notes:
//// xx = Class Name
//// xxx  = table name
//// $..  = column name(s)

class UserData extends dbBase
{

	public $user_id = ""; 						//bigint(20) unsigned NOT NULL,
	public $token = null;                       // varchar(255) NULL,
	public $token_expiration = null;
	public $uuid = ""; 								// varchar(32) NOT NULL,
	public $firstname = ""; 					//varchar(32) DEFAULT NULL,
	public $lastname = ""; 						//varchar(32) NOT NULL,
    public $mobile_number = "";
	public $screenname = ""; 					//varchar(32) NOT NULL,
	public $birthday = ""; 						//date DEFAULT NULL,
	public $gender = ""; 							//varchar(2) DEFAULT NULL,
    public $height = "";                       //Measured in total inches
    public $weight = "";                       //Measured in LBS
    public $waist_size = "";                   //Measured in total inches
    public $ethnicity_id = "";
	public $role_id = ""; 						//bigint(20) NOT NULL DEFAULT '1' COMMENT 'default to participant',
	public $preferedlanguage_id = ""; //varchar(2) NOT NULL DEFAULT 'en',
	public $preferredUnits = ""; 			//varchar(2) NOT NULL DEFAULT 'us' COMMENT 'us = US, si = metric',
	public $mensesfirstday_ts = "";
	public $emailVerified = "";
	public $accountCreation = "";
    public $menses_cycle_type_id = "";
	public $hysterectomy = "";
	public $hysterectomy_ts = "";
	public $ovaries_removed = "";
	public $ovaries_removed_ts = "";
	public $currently_pregnant = "";
	public $currently_pregnant_ts = "";
	public $validate_token = "";
	public $validate_token_expiration = "";
	public $affiliation = "";

		//menses_state
		//pre-menopausal
		//pre-menopausal irregular
		//postmenopausal
		//hysterectomy, ovaries not removed
		//hysterectomy, ovaries removed
		//oopherectomy without hysterectomy
		//menses_first_day



	 function __construct($id = "")
   {
		parent::__construct();
		$this->table = "user_data";

		if(strlen($id)>0)
		{
			$this->Load($id);
		}
   }

	function Add($postData, $ignore = array(), $bUseOnlyIgnoreList = false)
	{
		if (isset($postData['user_id']) && isset($postData['mensesfirstday_ts']) && isset($postData['hysterectomy_ts']) && isset($postData['ovaries_removed_ts']) && isset($postData['currently_pregnant_ts'])) { //add new user gyn row for each update, even for males for now
			$gyn_data = array(
				'user_id'=>$postData['user_id'],
				'mensesfirstday_ts'=>$postData['mensesfirstday_ts'],
				'menses_cycle_type_id'=>$postData['menses_cycle_type_id'],
				'hysterectomy'=>$postData['hysterectomy'],
				'hysterectomy_ts'=>$postData['hysterectomy_ts'],
				'ovaries_removed'=>$postData['ovaries_removed'],
				'ovaries_removed_ts'=>$postData['ovaries_removed_ts'],
				'currently_pregnant'=>$postData['currently_pregnant'],
				'currently_pregnant_ts'=>$postData['currently_pregnant_ts'],
			);
			$user_gyn_obj = new UserGynecology();
			$user_gyn_obj->Replace($gyn_data, [], false);
		}
		if(isset($postData['user_id']) && isset($postData['cycle_id'])){
			$start_datetime = new DateTime();
			$current_date = $start_datetime->format('Y-m-d');
			$weight_data = [
				'user_id'=>$postData['user_id'],
				'cycle_id'=>$postData['cycle_id'],
				'date_submitted' =>$current_date
			];
			if(isset($postData['weight'])){$weight_data['weight'] =$postData['weight']; }
			if(isset($postData['waist_size'])){$weight_data['waist_size'] =$postData['waist_size']; }
			$userWeightObj = new UserWeight();
			$userWeightObj->Replace($weight_data, [], false);

		}

		$ignore = array_merge(array('id','add_post'),$ignore);
		$postData['uuid'] = $this->GetUUID();
		$id = parent::Add($postData,$ignore,$bUseOnlyIgnoreList);
		return $id;
	}

	function Update($postData, $ignore = array(), $bUseOnlyIgnoreList = false)
	{
		if (isset($postData['user_id']) && isset($postData['kit_id']) ) { //add new user gyn row for each update, even for males for now

			//Note: kit_id is the primary key in user_gyn
			$gyn_data = array(
				'user_id'=>$postData['user_id'],
				'kit_id'=>$postData['kit_id'],
				'mensesfirstday_ts'=>    isset($postData['mensesfirstday_ts'])?$postData['mensesfirstday_ts']:NULL,
				'menses_cycle_type_id'=> isset($postData['menses_cycle_type_id'])?$postData['menses_cycle_type_id']:'1',
				'hysterectomy'=>          isset($postData['hysterectomy'])?$postData['hysterectomy']:'0',
				'hysterectomy_ts'=>      isset($postData['hysterectomy_ts'])?$postData['hysterectomy_ts']:NULL,
				'ovaries_removed'=>      isset($postData['ovaries_removed'])?$postData['ovaries_removed']:'0',
				'ovaries_removed_ts'=>   isset($postData['ovaries_removed_ts'])?$postData['ovaries_removed_ts']:NULL,
				'currently_pregnant'=>   isset($postData['currently_pregnant'])?$postData['currently_pregnant']:'0',
				'currently_pregnant_ts'=>isset($postData['currently_pregnant_ts'])?$postData['currently_pregnant_ts']:NULL
			);
			$user_gyn_obj = new UserGynecology();
			$user_gyn_obj->Replace($gyn_data, [], false);
		}
		if(isset($postData['user_id']) && isset($postData['cycle_id'])){
			$start_datetime = new DateTime();
			$current_date = $start_datetime->format('Y-m-d');
			$weight_data = [
				'user_id'=>$postData['user_id'],
				'cycle_id'=>$postData['cycle_id'],
				'date_submitted' =>$current_date
			];
			if(isset($postData['weight'])){$weight_data['weight'] =$postData['weight']; }
			if(isset($postData['waist_size'])){$weight_data['waist_size'] =$postData['waist_size']; }
			$userWeightObj = new UserWeight();
			$userWeightObj->Replace($weight_data, [], false);

		}

		$ignore = array_merge(array('id','update_post'),$ignore);
		$id = parent::Update($postData,$ignore,$bUseOnlyIgnoreList);
	    return $id;
	}

	function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','add_post','update_post');
		//if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
		$id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
		return $id;
	}

//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////

	function GetUserData($userId)
	{
		$link = parent::createLinki();

		$sql = "SELECT  
                    `user_data`.`id`, 
                    `user_data`.`token_expiration`,
                    `user_data`.`uuid`, 
                    `user_data`.`firstname`, 
                    `user_data`.`lastname`, 
                    `user_data`.`mobile_number`, 
                    `user_data`.`screenname`, 
                    `user_data`.`birthday`, 
                    `user_data`.`gender`, 
                    `user_data`.`height`, 
                    `user_weight`.`weight`, 
                    `user_weight`.`waist_size`, 
                    `user_data`.`ethnicity_id`, 
                    `user_data`.`role_id`, 
                    `role`.`value` AS 'role', 
                    `user_data`.`preferedlanguage_id` AS 'language', 
                    `user_data`.`preferredUnits`, 
                    `user_gynecology`.`mensesfirstday_ts`, 
                    `user_gynecology`.`menses_cycle_type_id`, 
                    `user_gynecology`.`hysterectomy`, 
                    `user_gynecology`.`hysterectomy_ts`, 
                    `user_gynecology`.`ovaries_removed`, 
                    `user_gynecology`.`ovaries_removed_ts`, 
                    `user_gynecology`.`currently_pregnant`, 
                    `user_gynecology`.`currently_pregnant_ts`, 
                    `user_data`.`validate_token`, 
                    `user_data`.`validate_token_expiration`, 
                    `user_data`.`emailVerified`, 
                    `user_data`.`request_uri`,
					`user_data`.`affiliation` 
                FROM `".$this->table."`
                JOIN `role` ON  `user_data`.`role_id`=`role`.`id`
                LEFT JOIN 
                (
                  SELECT * FROM `user_gynecology` WHERE `user_gynecology`.`user_id` = '".$userId."' ORDER BY `user_gynecology`.`date_submitted` DESC LIMIT 1
                ) AS user_gynecology ON `user_gynecology`.`user_id` = `user_data`.`user_id`
                LEFT JOIN 
                (
                  SELECT * FROM `user_weight` WHERE `user_weight`.`user_id` = '".$userId."' ORDER BY `user_weight`.`cycle_id` DESC LIMIT 1
                ) AS user_weight ON `user_weight`.`user_id` = `user_data`.`user_id`
                WHERE `user_data`.`user_id` = '".$userId."'";

		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
		$array = array();
				while($row = mysqli_fetch_assoc($result))
				{
					$array = $row;
				}
		//var_dump($array);die();
		mysqli_close($link);

		self::addBlanks($array);
		return $array;
	}

    public function GetUserDataByToken($token)
    {
        $token = filter_var($token, FILTER_SANITIZE_STRING);
        $link = parent::createLinki();

        $sql = "SELECT  
                    `user_data`.`id`, 
                    `user_data`.`uuid`, 
                    `user_data`.`firstname`, 
                    `user_data`.`lastname`, 
                    `user_data`.`mobile_number`, 
                    `user_data`.`screenname`, 
                    `user_data`.`birthday`, 
                    `user_data`.`gender`, 
                    `user_data`.`height`, 
                    `user_weight`.`weight`, 
                    `user_weight`.`waist_size`, 
                    `user_data`.`ethnicity_id`, 
                    `user_data`.`role_id`, 
                    `role`.`value` AS 'role', 
                    `user_data`.`preferedlanguage_id` AS 'language', 
                    `user_data`.`preferredUnits`, 
                    `user_gynecology`.`mensesfirstday_ts`, 
                    `user_gynecology`.`menses_cycle_type_id`, 
                    `user_gynecology`.`hysterectomy`, 
                    `user_gynecology`.`hysterectomy_ts`, 
                    `user_gynecology`.`ovaries_removed`, 
                    `user_gynecology`.`ovaries_removed_ts`, 
                    `user_gynecology`.`currently_pregnant`, 
                    `user_gynecology`.`currently_pregnant_ts`, 
                    `user_data`.`validate_token`, 
                    `user_data`.`validate_token_expiration`, 
                    `user_data`.`emailVerified`, 
                    `user_data`.`request_uri`,
					`user_data`.`affiliation` 
                FROM `" . $this->table . "`
                JOIN `role` ON  `user_data`.`role_id`=`role`.`id`
                LEFT JOIN 
                (
                  SELECT 
                      `user_gynecology`.`user_id`,
                      `user_gynecology`.`menses_cycle_type_id`,
                      `user_gynecology`.`mensesfirstday_ts`,
                      `user_gynecology`.`hysterectomy`,
                      `user_gynecology`.`hysterectomy_ts`,
                      `user_gynecology`.`ovaries_removed`,
                      `user_gynecology`.`ovaries_removed_ts`,
                      `user_gynecology`.`currently_pregnant`,
                      `user_gynecology`.`currently_pregnant_ts`
                  FROM `user_gynecology`
                  JOIN `user_data` ON `user_gynecology`.`user_id` = `user_data`.`user_id` 
                  WHERE `user_data`.`validate_token` = '" . mysqli_real_escape_string($link, $token) . "' ORDER BY `user_gynecology`.`date_submitted` DESC LIMIT 1
                ) AS user_gynecology ON `user_gynecology`.`user_id` = `user_data`.`user_id`
                LEFT JOIN 
                (
                  SELECT * FROM `user_weight` WHERE `user_weight`.`user_id` = '".$userId."' ORDER BY `user_weight`.`cycle_id` DESC LIMIT 1
                ) AS user_weight ON `user_weight`.`user_id` = `user_data`.`user_id`
                WHERE  `validate_token` = '" . mysqli_real_escape_string($link, $token) . "'";

        $result = mysqli_query($link, $sql) or die("SELECT: " . mysqli_error($link) . "<br/>" . $sql);
        $array = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $array = $row;
        }
        //var_dump($array);die();
        mysqli_close($link);
		self::addBlanks($array);
        return $array;
    }

    function GetEmailInfo($userId)
    {
        $link = parent::createLinki();
        $sql = "SELECT `firstname`,`lastname`";
        $sql .= " from `".$this->table."` where `user_id` = '".$userId."'";
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $data = array();
        while($row = mysqli_fetch_assoc($result))
        {
            $data = $row;
        }
        //var_dump($array);die();
        mysqli_close($link);
        return $data;
    }

    function addBlanks(&$row){
	 	foreach($row as $key=>&$col){
	 		if(!isset($col)){$col = "";}
		}

	}

	/**
	 * @param $userId
	 *
	 * @return bool|mysqli_result
	 */
	public function invalidateToken($userId)
	{
		$link = $this->createLinki();
		$sql = "UPDATE `" . $this->table . "` SET `token`=NULL, `token_expiration`=NULL WHERE `user_id` = '" . $userId . "'";
		$result = mysqli_query($link, $sql) or die("SELECT: " . mysqli_error($link) . "<br/>" . $sql);
		if (is_resource($link)) {
			mysqli_close($link);
		}
		return $result;
	}

	//getting affiliations that exist to build auto suggestion
	function getAllAffiliations()
    {
        $link = parent::createLinki();
        $sql = "SELECT affiliation";
        $sql .= " from `".$this->table."` WHERE affiliation IS NOT NULL GROUP BY affiliation";
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $data = array();
        while($row = mysqli_fetch_assoc($result))
        {
            $data[] = $row['affiliation'];
			//print_r($row);
        }
        //var_dump($array);die();
        mysqli_close($link);
		//print_r($data);
        return $data;
    }

}


?>

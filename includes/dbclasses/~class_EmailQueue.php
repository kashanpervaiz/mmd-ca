<?php 
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

////Generic class template
//// Notes:
//// xx = Class Name
//// xxx  = table name
//// $..  = column name(s)


class EmailQueue_old extends dbBase
{
	public $fkIdType = "";
	public $fkIdValue = "";
	public $from = "";
	public $messageKey = "";
	public $messageParams = "";
	public $status = "";
	
	 function __construct($id = "") 
   {
    parent::__construct();
	  $this->table = "emailqueue";
		if(strlen($id)>0)
		{
			parent::Load($id);
		}
   }
	
	function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','add_post');		
		$id = parent::Add($postData,$ignore);	
		return $id;
	}
		
	function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','update_post');
		$id = parent::Update($postData,$ignore);
	}

	
//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////
	function QueueMessage($fkIdType, $fkIdValue, $from, $messageKey, $messageParams = "")
	{
		if(is_array ( $messageParams ))
		{$messageParams = serialize($messageParams);}
		else
		{$messageParams = "";}
		$newMessage = array("fkIdType"=>$fkIdType, "fkIdValue"=>$fkIdValue, "from"=>$from, "messageKey"=>$messageKey, "messageParams"=>$messageParams);
		$this->Add($newMessage);
	}
	
}

  
?>
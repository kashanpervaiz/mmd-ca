<?php 
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

////Generic class template
//// Notes:
//// xx = Class Name
//// xxx  = table name
//// $..  = column name(s)

class UserMedications extends dbBase
{
	public $user_id = ""; 
	public $medication = ""; //name
	public $medication_type=""; //otc, prescrition
	public $delivery = ""; //oral, injection, 
	public $dose_strength = ""; 
	public $dose_frequency = "";	
	public $date_last_used = "";
	public $time_last_used = "";
	public $lastmod_utc = "";

	 function __construct($id = "") 
   {
	  parent::__construct();
	  $this->table = "user_medications";
		if(strlen($id)>0)
		{
			parent::Load($id,$this->table);
		}
   }
	
	function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','add_post');		
		$id = parent::Add($postData,$ignore);	
		return $id;
	}
		
	function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','update_post');
		$id = parent::Update($postData,$ignore);
	}

    function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true)
    {
        $ignore = array('id','add_post','update_post');
        //if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        $id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
        return $id;
    }
	
//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////
	function GetUserMedications($userId,$sort ="DESC")
	{
		$link = parent::createLinki();
		$sql = "SELECT * ";
		$sql .= " from `".$this->table."` where `user_id` = '".$userId."' ORDER BY `lastmod_utc` ".$sort;
		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
		$array = array();
				while($row = mysqli_fetch_assoc($result))
				{
			        //$row['date_last_used'] = formatToUSDate($row['date_last_used']);
					$array[] = $row;
				}
		//var_dump($array);die();
		mysqli_close($link);
		return $array;
	}
	
	function formatToUSDate($sourceDate)
    {
        if( isset($sourceDate) and strlen($sourceDate) > 0)
        {
        $time = strtotime($sourceDate);
        $newformat = date('F j, Y',$time);
        return $newformat;
        }
        else
        {return $sourceDate;}  
    }		
	
}

  
?>
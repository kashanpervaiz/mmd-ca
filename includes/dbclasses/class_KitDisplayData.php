<?php 
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

////Generic class template
//// Notes:
//// xx = Class Name
//// xxx  = table name
//// $..  = column name(s)

class KitDisplayData extends dbBase
{
	public $wbDataTypeKey = "";
	public $sortOrder = "";
	public $groupKey = "";
	public $moreInfoKey = "";
	public $min = "";
	public $max = "";

	 function __construct($id = "") 
   {
    parent::__construct();
	  $this->table = "kit_displaydata";
		if(strlen($id)>0)
		{
			parent::Load($id);
		}
   }
	
	function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','add_post');		
		$id = parent::Add($postData,$ignore);	
		return $id;
	}
		
	function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','update_post');
		$id = parent::Update($postData,$ignore);
	}

    function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true)
    {
        $ignore = array('id','add_post','update_post');
        //if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        $id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
        return $id;
    }
	
//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////

	function GetDataForKey($wbKey)
	{
		$link = parent::createLinki();
		$sql = "SELECT `wbDataTypeKey`,`sortOrder`,`groupKey`,`groupSortOrder`,`moreInfoKey`";
		$sql .= " from `".$this->table."` where `wbDataTypeKey` = '".$wbKey."'";
		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
		$response = array("wbDataTypeKey"=>"unknown","sortOrder"=>"99","groupKey"=>"ungrouped","moreInfoKey"=>"","min"=>"0","max"=>"0");
		while($row = mysqli_fetch_assoc($result))
		{
			$response = $row;
		}
		//var_dump($array);die();
		mysqli_close($link);
		return $response;	
	}	
	
}

  
?>
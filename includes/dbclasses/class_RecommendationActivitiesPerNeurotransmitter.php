<?php 
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

////Generic class template
//// Notes:
//// xx = Class Name
//// xxx  = table name
//// $..  = column name(s)

class RecommendationActivitiesPerNeurotransmitter extends dbBase
{
public $neurotransmitter = "";
public $imbalance = "";
public $activity_list = "";

	 function __construct($id = "")
   {
    parent::__construct();
	  $this->table = "recommendation_activities_per_neurotransmitter";
		if(strlen($id)>0)
		{
			parent::Load($id);
		}
   }

	function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','add_post');
		$id = parent::Add($postData,$ignore);
		return $id;
	}

	function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','update_post');
		$id = parent::Update($postData,$ignore);
	}

    function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true)
    {
        $ignore = array('id','add_post','update_post');
        //if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        $id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
        return $id;
    }

//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////
 function GetActivitiesForNt($id)
 {
     $link = parent::createLinki();

     $sql = "SELECT `activity_list` FROM `".$this->table."` WHERE `id` = '".$id."'";

     $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
     $array = array();
     $row = mysqli_fetch_assoc($result);

     if(isset($row['activity_list'])){
         $array = json_decode($row['activity_list']);
     }


     //var_dump($array);die();
     mysqli_close($link);
     return $array;
 }

 function UpdateActivities($post_data){

     if(!isset($post_data['neurotransmitters_id'])){return;}

     $link = parent::createLinki();
     $activities_list = array();
     foreach ($post_data as $key => $value ) {

         if(strpos($key, 'activity_id_') !== false){
             $activities_list[] = $value;
         }

     }
     $sql = "UPDATE ".$this->table." SET `activity_list` = '".json_encode($activities_list)."' WHERE `id` = ".$post_data['neurotransmitters_id'];
     $result = mysqli_query($link, $sql) or die("UPDATE: " . mysqli_error($link) . "<br/>" . $sql);
     mysqli_close($link);
     return $result;
 }

}

  
?>
<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

class User extends dbBase
{
    public $email = "";
    public $password = "";
    public $user_id = "";

    function __construct($id = ""){
        parent::__construct();
        $this->table = "users";
        if(strlen($id)>0)
        {
            $this->Load($id);
        }
    }

    function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true){
        $ignore = array('id','add_post');
        $id = parent::Add($postData,$ignore);
        if($id > 0){
            $new_user = new User($id);
            $id = $new_user->user_id;
        }
        return $id;
    }

    function Update($postData, $ignore = array(), $bUseOnlyIgnoreList = true){
        $ignore = array_merge($ignore,array('id','update_post'));
        if(!isset($postData['id'])){
            $id = self::GetIDdForUserId($postData['user_id']);
            $postData['id'] = $id;
        }
        $id = parent::Update($postData,$ignore);
        if($id > 0){
            $new_user = new User($id);
            $id = $new_user->user_id;
        }
        return $id;
    }

    function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true){
        $ignore = array('id','add_post','update_post');
        //if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        if(!isset($postData['id'])){
            $id = self::GetIDdForUserId($postData['user_id']);
            $postData['id'] = $id;
        }
        $id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
        if($id > 0){
            $new_user = new User($id);
            $id = $new_user->user_id;
        }
        return $id;
    }

//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////
    function Validate($user,$pass)
    {
        $response = array();
        $link = parent::createLinki();

        $sql = "Select `user_id`,`password` FROM `users` WHERE `users`.`email` = '".$user."'";
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );

        if(mysqli_num_rows($result) > 0)
        {
            $row = mysqli_fetch_assoc($result);
            if(password_verify($pass, $row['password']) || md5($pass) == $row['password']){
                $response['user_id'] = $row['user_id'];
            }
        }

        if(is_resource($link) )
        {
            mysqli_close($link);
        }

        return $response;

    }

    /* This one is truly 'id' not 'user_id'*/
    function GetIDdForUserId($user_id){
        $sql = "SELECT id FROM $this->table t WHERE t.user_id = '$user_id'";
        $result = self::runQuery($sql,true);
        $id = 0;
        if(isset($result['id']) && $result['id'] > 0){
            $id =  $result['id'];
        }
        return $id;
    }

    function GetEmail($userId)
    {
        $link = parent::createLinki();
        $sql = "SELECT `email`";
        $sql .= " from `".$this->table."` where `user_id` = '".$userId."'";
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $email = "";
        while($row = mysqli_fetch_assoc($result))
        {
            $email = $row['email'];
        }
        //var_dump($array);die();
        mysqli_close($link);
        return $email;
    }

    function isUniqueEmail($email)
    {
        $bUnique = false;
        $link = parent::createLinki();
        $sql = "SELECT count(*)";
        $sql .= " from `".$this->table."` where `email` = '".$email."'";
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );

        $row = mysqli_fetch_row($result);
        $bUnique =  (intval($row[0])== '0') ? true : false;
        //$bUnique = mysqli_fetch_row($result);

        mysqli_close($link);
        return $bUnique;

    }

    public function getNewPassword($email)
    {
        $link = parent::createLinki();
        $random_password=md5(uniqid(rand()));
        $new_password=substr($random_password, 0, 8);
        $sql  = "UPDATE `".$this->table."` SET `password` = '".md5($new_password)."' WHERE `email` = '".$email."'";
        $result = mysqli_query($link,$sql) or die( "UPDATE: ".mysqli_error($link)."<br/>".$sql );

        //A bug in php 7.3.12 does not return rows_affected so I made this work around
        // $link->info returns the string 'Rows matched: 0 Changed: 0 Warnings: 0'
        // so i'm examining that via regex.
        //$matches[0] contains the array of values...
        $pattern = "/\d/";
        preg_match_all($pattern,$link->info,$matches);

        if(isset($matches[0][1]) && $matches[0][1] > 0)
        {
            mysqli_close($link);
            return $new_password;
        }
        else
        {
            mysqli_close($link);
            return "";
        }


    }

    public function GetUserIdFromEmail($email)
    {
        $link = parent::createLinki();
        $sql = "SELECT `user_id` ";
        $sql .= " from `".$this->table."` where `email` = '".$email."'";
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $id = "0";
        while($row = mysqli_fetch_assoc($result))
        {
            $id = $row['user_id'];
        }
        //var_dump($array);die();
        mysqli_close($link);
        return $id;
    }

    function GetAllUsers($order='ASC'){
        $array = array();
        $sql = "SELECT user_id,email FROM `".$this->table."` ORDER BY `id` ".$order;

        $link = $this->createLinki();
        $result = mysqli_query($link,$sql) or die( "GetAllUsers: ".mysqli_error($link)."\r\n". $sql);
        while($row = mysqli_fetch_assoc($result)){
            $array[] = $row;}

        if(is_resource($link) )
        {
            mysqli_close($link);
        }

        return $array;
    }

    function GetAllParticipants($order='ASC'){
        $array = array();
        $sql = "SELECT t.user_id,t.email FROM ".$this->table." t
        JOIN user_data ud on ud.user_id  = t.user_id
        WHERE ud.role_id = 1 
        ORDER BY t.id ".$order;

        $link = $this->createLinki();
        $result = mysqli_query($link,$sql) or die( "GetAllUsers: ".mysqli_error($link)."\r\n". $sql);
        while($row = mysqli_fetch_assoc($result)){
            $array[] = $row;}

        if(is_resource($link) )
        {
            mysqli_close($link);
        }

        return $array;
    }

    function GetUsersWithKitData($order='DESC', $ignore_future_cycles = true){

        $future = $ignore_future_cycles ? " AND (uc.start_date IS NULL OR uc.start_date  <= NOW())":"";
        $array = array();

        $sql = "SELECT DISTINCT u.user_id FROM users u
                JOIN user_cycles uc ON uc.user_id = u.user_id
                JOIN kit_data kd ON kd.cycle_id = uc.id
                JOIN kit_results kr ON kr.kit_id = kd.id
                WHERE kr.vendorData IS NOT NULL AND kd.active_kit = 1 $future 
                ORDER BY u.id ".$order;

        $link = $this->createLinki();
        $result = mysqli_query($link,$sql) or die( "GetAllUsers: ".mysqli_error($link)."\r\n". $sql);
        while($row = mysqli_fetch_assoc($result)){
            $array[] = $row;}

        if(is_resource($link) )
        {
            mysqli_close($link);
        }

        return $array;
    }
}


?>
<?php 
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

//Generic class template
// Notes:
// xx = Class Name
// xxx  = table name
// $..  = column name(s)

class DataRanges extends dbBase
{
	public $wbDataTypeKey = "";
	public $gender = "";
	public $agemin = "";
	public $agemax = "";
	public $period = "";
	public $min = "";
	public $max = "";
	public $desireable = "";
	public $descriptionKey = "";
	public $indicatorKey = "";
	public $range_min = "";
	public $range_max = "";
	public $slider_min = "";
	public $slider_max = "";
	public $slider_class = "";

	 function __construct($id = "") 
   {
    parent::__construct();
	  $this->table = "data_ranges";
		if(strlen($id)>0)
		{
			parent::Load($id);
		}
   }
	
	function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','add_post');		
		$id = parent::Add($postData,$ignore);	
		return $id;
	}
		
	function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','update_post');
		$id = parent::Update($postData,$ignore);
	}

    function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true)
    {
        $ignore = array('id','add_post','update_post');
        //if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        $id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
        return $id;
    }
	
//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////

function GetRangeData($wbDataTypeKey,$period,$value,$age = 35,$gender="M",$menses_state="x")
{
		$value = str_replace('<','',$value);
		$value = str_replace('>','',$value);
		$value = str_replace('=','',$value);
		
		$link = parent::createLinki();
		$sql = "SELECT `min`,`max`,`descriptionKey`,`indicatorKey`,`desireable`,`range_min`,`range_max`,`slider_min`,`slider_max`,`slider_class` from `".$this->table."`";
		$sql .= " WHERE `wbDataTypeKey` = '".$wbDataTypeKey."' ";
		$sql .= " AND `period` = '".$period."'";
		$sql .= " AND '".$age."' BETWEEN `agemin` AND `agemax`";
        $sql .= " AND (`gender` = 'x' OR `gender` = '".$gender."')";
        $sql .= " AND (`menses_state` = 'x' OR `menses_state` = '".$menses_state."')";
		$sql .= " AND ( (`min`<='".$value."' AND `max`>'".$value."' AND `desireable` = 'down')";
		$sql .= "  OR  (`min`<'".$value."' AND `max`>='".$value."' AND `desireable` = 'up') )";
		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
		$data = mysqli_fetch_assoc($result);
//				while($row = mysqli_fetch_assoc($result))
//				{
//					$data[] = $row;
//				}
		//var_dump($data);return;
		if(!isset($data)){$data = array('min'=>-99999999,'max'=>999999999,'descriptionKey'=>"healtyBetween",'indicatorKey'=>'0','desireable'=>'down','range_min'=>'-99999999','range_max'=>'99999999','slider_min'=>'-200','slider_max'=>'300','slider_class'=>'rygyr');}
		mysqli_close($link);
		return $data;	
}

function GetWbKeys()
{
		$link = parent::createLinki();
		$sql = "SELECT `wbDataTypeKey` from `".$this->table."` GROUP BY `wbDataTypeKey`";
		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
		$data = array();
		while($row = mysqli_fetch_assoc($result))
		{
			$data[] = $row['wbDataTypeKey'];
		}
//		//var_dump($array);
		mysqli_close($link);
		return $data;	
}

function GetDataForWBKey($wbKeyName)
{
		$link = parent::createLinki();
		$sql = "SELECT * from `".$this->table."` WHERE `wbDataTypeKey`='".$wbKeyName."' ORDER BY `gender` ASC,`menses_state` ASC,`agemax` ASC, `indicatorKey` ASC, `period` ASC";
		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
		$data = array();
		while($row = mysqli_fetch_assoc($result))
		{
			$data[] = $row;
		}
//		//var_dump($array);
		mysqli_close($link);
		return $data;		
}
function GetDescriptionKeys()
{
		$link = parent::createLinki();
		$sql = "SELECT `descriptionKey` from `".$this->table."` GROUP BY `descriptionKey`";
		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
		$data = array();
		while($row = mysqli_fetch_assoc($result))
		{
			$data[] = $row['descriptionKey'];
		}
//		//var_dump($array);
		mysqli_close($link);
		return $data;			
}

	
	
}

  
?>
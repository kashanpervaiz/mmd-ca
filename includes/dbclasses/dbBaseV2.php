<?php

class dbBase{

	protected $dbhost = '';
	protected $dbport = '';	
	protected $dbase = '';
	protected $dbuser = '';
	protected $dbpass = '';

	public $id = "";
	protected $table = "" ;
	
	 function __construct(){
       $this->dbhost = $_SESSION['database']['dbhost'];//mysql:3306';
       $this->dbase = $_SESSION['database']['dbase'];//'moodmddb_loc';
       $this->dbuser = $_SESSION['database']['dbuser'];//'moodmd_user_loc';
       $this->dbpass = $_SESSION['database']['dbpass'];//'moodMdPass01!!';
       $this->dbport = $_SESSION['database']['dbport'];//'3306';
   }
	
	public function createLinki()
	{
	//echo $this->dbport." ".$this->dbuser." ".$this->dbpass."<br/>".PHP_EOL;

		$host = str_replace(":".$this->dbport,"",$this->dbhost);
		$port = intval($this->dbport);

	  $link = mysqli_connect($host, $this->dbuser, $this->dbpass, $this->dbase, $port);
		if (!$link) {
		    die('Connect Error (' . mysqli_connect_errno() . ') '
		            . mysqli_connect_error());
		}
	
		mysqli_query($link,"set character_set_server='utf8'");
		mysqli_query($link,"set names 'utf8'");
		
		return $link;
	}	

	 public function Load($id = "")
   {
   		$link = $this->createLinki();
			$sql = "Select * FROM `".$this->table."` WHERE `id` = '".$id."'";
			$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
			$array = array();
			while($row = mysqli_fetch_assoc($result)){
				$array[] = $row;}

			if(sizeof($array)>0)
			{
				reset($array[0]);

//				while(list($k,$v) = each($array))
//				{
//        $$k = $v;
//        echo($$k." ".$v."<br>");die();
//      	}

				while (!is_null($key = key($array[0]) ) )
				{
					//echo $key." ".current($array[0])."<br>";
					$this->{$key} = current($array[0]);
					if((current($array[0])== null) || (current($array[0])== '-1')){$this->complete = false; }
					next($array[0]);
				}
			}
			if(is_resource($link) )
			{
				mysqli_close($link);
			}

	 }

	 public function LoadbyUserID($userid = "",$date="")
   {

   		$link = $this->createLinki();
   		$sql = "";
   		if(strlen($date)== 0 || $date == Date('Y-m-d'))
   		{
				$sql = "Select * FROM `".$this->table."` WHERE `userid` = '".$userid."' ORDER BY `timestamp` DESC LIMIT 1"; //get the latest
   		}
   		else
   		{
 				$sql = "Select * FROM `".$this->table."` WHERE `userid` = '".$userid."' AND date(`timestamp`) <= date('".$date."') ORDER BY `timestamp` DESC LIMIT 1";  //get the last good date
   		}

			$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
			while($row = mysqli_fetch_assoc($result)){
				$array[] = $row;}

			if(sizeof($array)>0)
			{
				reset($array[0]);

//				while(list($k,$v) = each($array))
//				{
//        $$k = $v;
//        echo($$k." ".$v."<br>");die();
//      	}

				while (!is_null($key = key($array[0]) ) )
				{
					//echo $key." ".current($array[0])."<br>";
					$this->{$key} = current($array[0]);
					if((current($array[0])== null) || (current($array[0])== '-1')){$this->complete = false; }
					next($array[0]);
				}
			}
		mysqli_close($link);
	 }

	 public function GetData($id = "")
   {
			$array = array();
   		$link = $this->createLinki();
			$sql = "Select * FROM `".$this->table."` WHERE `id` = '".$id."'";
			$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
			while($row = mysqli_fetch_assoc($result)){
				$array[] = $row;}

			if(count($array) > 0 && count($array[0])>0)
			{
				reset($array[0]);
			}
			
			if(is_resource($link) )
			{
				mysqli_close($link);
			}
			
			return $array;
		
	 }

	public function GetAllData($order = 'DESC')
	{
			$array = array();
			$sql = "SELECT * FROM `".$this->table."` ORDER BY `id` ".$order;

	   	$link = $this->createLinki();
			$result = mysqli_query($link,$sql) or die( "GetAllData: ".mysqli_error($link)."\r\n". $sql);
			while($row = mysqli_fetch_assoc($result)){
				$array[] = $row;}

			if(is_resource($link) )
			{
				mysqli_close($link);
			}

			return $array;
	}

	public function GetDataByUserId($userid = "")
   {
        $array = array();
        $link = $this->createLinki();

        $sql = "Select * FROM `".$this->table."` WHERE `user_id` = '".$userid."'";
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );

        while($row = mysqli_fetch_assoc($result)){
            $array[] = $row;
        }
        mysqli_close($link);
        return $array;
	 }

    public function GetDataByUserIdAndTimestamp($userid = "",$date="")
    {
        $array = array();
        $link = $this->createLinki();
        $sql = "";
        if(strlen($date)== 0 || $date == Date('Y-m-d'))
        {
            $sql = "Select * FROM `".$this->table."` WHERE `user_id` = '".$userid."' ORDER BY `timestamp` DESC LIMIT 1"; //get the latest
        }
        else
        {
            $sql = "Select * FROM `".$this->table."` WHERE `user_id` = '".$userid."' AND date(`timestamp`) <= date('".$date."') ORDER BY `timestamp` DESC LIMIT 1";  //get the last good date
        }
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        while($row = mysqli_fetch_assoc($result)){
            $array[] = $row;}

        reset($array[0]);

        mysqli_close($link);

        return $array;

    }

	public function GetRowsAbove($beginningId, $order = 'DESC', $limit='100')
	{
			$array = array();
			$sql = "SELECT * FROM `".$this->table."` WHERE `id` >= ".$beginningId." ORDER BY `id` ".$order." LIMIT ".$limit;

	   	$link = $this->createLinki();
			$result = mysqli_query($link,$sql) or die( "GetAllData: ".mysqli_error($link)."\r\n". $sql);
			while($row = mysqli_fetch_assoc($result)){
				$array[] = $row;}

			if(is_resource($link) )
			{
				mysqli_close($link);
			}

			return $array;
	}
	
	public function GetRowsBelow($beginningId, $order = 'DESC', $limit='100')
	{
			$array = array();
			$sql = "SELECT * FROM `".$this->table."` WHERE `id` <= ".$beginningId." ORDER BY `id` ".$order." LIMIT ".$limit;

	   	$link = $this->createLinki();
			$result = mysqli_query($link,$sql) or die( "GetAllData: ".mysqli_error($link)."\r\n". $sql);
			while($row = mysqli_fetch_assoc($result)){
				$array[] = $row;}

			if(is_resource($link) )
			{
				mysqli_close($link);
			}

			return $array;
	}

	public function GetFirstId()
	{
		
			$id = "1";
			$sql = "SELECT `id` FROM `".$this->table."` ORDER BY id ASC LIMIT 1";
	   	$link = $this->createLinki();
			$result = mysqli_query($link,$sql) or die( "GetAllData: ".mysqli_error($link)."\r\n". $sql);
			while($row = mysqli_fetch_assoc($result)){
				$id = $row['id'];}

			if(is_resource($link) )
			{
				mysqli_close($link);
			}

			return $id;		
	}
	
	public function GetLastId()
	{
		
			$id = "1";
			$sql = "SELECT `id` FROM `".$this->table."` ORDER BY id DESC LIMIT 1";
	   	$link = $this->createLinki();
			$result = mysqli_query($link,$sql) or die( "GetAllData: ".mysqli_error($link)."\r\n". $sql);
			while($row = mysqli_fetch_assoc($result)){
				$id = $row['id'];}

			if(is_resource($link) )
			{
				mysqli_close($link);
			}

			return $id;		
	}
	 public function CloneData($userid = "")
   {
			$finalResult = false;
   		$link = $this->createLinki();
			$sql = "Select * FROM `".$this->table."` WHERE `userid` = '".$userid."' ORDER BY `timestamp` DESC LIMIT 1";
			$result = mysqli_query($link,$sql) or die( "SELECT 1: ".mysqli_error($link)."<br/>".$sql );
			while($row = mysqli_fetch_assoc($result)){
				$array[] = $row;}

			$selectStr = "";
			reset($array[0]);

				while (!is_null($key = key($array[0]) ) )
				{
					if($key != "id" and $key != "timestamp")
					{
						//echo $key." ".current($array[0])."<br>";
						//$this->{$key} = current($array[0]);
						$selectStr .= "`".$key."`,";
					}
					next($array[0]);
				}

				if(strlen($selectStr) > 1)
				{
					$selectStr = substr($selectStr,0, strlen($selectStr)-1);
				}

				if(strlen($selectStr) > 0)
				{
					$cloneSql = "INSERT INTO ".$this->table."(".$selectStr.") Select ".$selectStr." FROM `".$this->table."` WHERE `userid` = '".$userid."' ORDER BY `timestamp` DESC LIMIT 1";
					$finalResult = mysqli_query($link,$cloneSql) or die( "CloneData Insert 2: ".mysqli_error($link)."<br/>".$cloneSql );
				}
				
			mysqli_close($link);
			return $finalResult;
	 }

	public function Checked($testName)
  {
  	if(isset($this->{$testName})){echo($this->{$testName} == "1") ? "checked=\"checked\"" : "";}
  }

  public function Set($testName,$value)
  {
  	$this->{$testName} = $value;
  }

  public function Get($testName)
  {
  	if(strlen($testName)>0)
  	{
  		return $this->{$testName};
  	}
  	else
  		return "";
  }

  public function SetDate($testName,$value)
  {
  	//echo("SetDate<br>");
  	//$format = '%d/%m/%Y %H:%M:%S';
		//$strf = strftime($format);

		if(strlen($value) > 0  && strlen($testName) > 0)
  	{
  		list($month,$day,$year)=explode('-',$value);
			$db_date = $year."-".$month."-".$day;
			$this->{$testName} = $db_date;
		}
  }


  public function WriteValue($testName)
  {
  	echo($this->{$testName});
  }

  public function WriteTime($testName)
  {
		if(strlen($this->{$testName}) > 0 )
  	{
  		list($hour,$min,$sec)=explode(':',$this->{$testName});
			$display_time = $hour.":".$min;
			echo($display_time);
		}
		else
			echo("");
  }

  public function WriteDate($testName)
  {
  	//$format = '%d/%m/%Y %H:%M:%S';
		//$strf = strftime($format);
		//echo("WriteDate<br>");

		if(strlen($this->{$testName}) > 0 && $this->{$testName}!="0000-00-00")
  	{
  		list($date,$time) = explode(' ',$this->{$testName});
  		list($year,$month,$day)=explode('-',$date);
			$display_date = $month."-".$day."-".$year;
			echo($display_date);
		}
		else
			echo("");
  }

  public function WriteDateString($testName)
  {
  	//echo("WriteDateString<br>");
  	//$format = '%d/%m/%Y %H:%M:%S';
		//$strf = strftime($format);
		$result = "0/0/0000";

		if(strlen($testName) > 0 )
  	{
  		list($date,$time) = explode(' ',$testName);
  		list($year,$month,$day)=explode('-',$date);
			$display_date = $month."-".$day."-".$year;
			return $display_date;
		}
		else
			return $result;
  }

  public function setData($arr)
   {
   $count = count($arr);
   if($count > 0)
   {
       echo ('<table>\n');
       reset($arr);
       $num = count(current($arr));
       foreach(current($arr) as $key => $value)
       {
           $key."&nbsp;";
       }
       while ($curr_row = current($arr)) {
           echo "<tr>\n";
           $col = 1;
           while (false !== ($curr_field = current($curr_row))) {
               echo "<td>";
               echo $curr_field."&nbsp;";
               echo "</td>\n";
               next($curr_row);
               $col++;
               }
           while($col <= $num){
               echo "<td>&nbsp;</td>\n";
               $col++;
           }
           echo "</tr>\n";
           next($arr);
           }
       echo "</table>\n";
       }
   }

//parent::p();

	public function Add($postData,$ignore = array(), $bUseOnlyIgnoreList = true)
	{
			if(isset($postData['id'])){
			    unset($postData['id']);
            }

	        $link = $this->createLinki();
			$dbColumns = array();
			if(!$bUseOnlyIgnoreList)
			{
				$sql = "DESCRIBE `".$this->table."`";
				$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
				while($row = mysqli_fetch_assoc($result))
				{
					$dbColumns[] = $row['Field'];
				}			
			}

		  $regex_date = "/^([0-9]{1,2})(\-|\/|\.)([0-9]{1,2})(\-|\/|\.)([0-9]{4})$/";
	   	$regex_time = "/^([0-9]{1,2}):([0-9]{1,2})$/";
	   	$regex_bool = "/^b_/";
	   	$regex_search = "/^search_/";
	   	$insert1 = "";
	   	$insert2 = "";
			while($key = key($postData))
			{

				if(preg_match($regex_search,$key))
				{
					next($postData);
					continue;
				}

				if( (in_array($key,$ignore)=== false) && ($bUseOnlyIgnoreList || in_array($key,$dbColumns)) )
				{

						if($key != "password")
						{
							if(strlen($insert1)>0){ $insert1.=",";}
							if(strlen($insert2)>0){ $insert2.=",";}
							$insert1 .= "`";
	   					$insert2 .= "'";
						}
						if(preg_match($regex_date,$postData[$key]))
						{
							//echo("preg_matchDateAdd<br>");
							list($month,$day,$year)=explode('-',$postData[$key]);
							$result = $year."-".$month."-".$day;
						}
						else if(preg_match($regex_time,$postData[$key]))
						{
							$result = $postData[$key].":00";
						}
						else if(preg_match($regex_bool,$key))
						{
							$result = "1";
						}
						else if($key == "password")
						{
							next($postData);
							continue;
						}
						else
						{
							$result = mysqli_escape_string($link,$postData[$key]);
						}

						if($key != "password")
						{
	   					$insert1 .= $key."`";
	   					$insert2 .= $result."'";
	   				}

				}
				next($postData);
			}

			if(isset($postData['password']) && strlen($postData['password'])>0)
			{
				$insert1 .= ",`password`";
                $password = $postData['password'];
                $insert2 .= ",'".password_hash($password, PASSWORD_BCRYPT, ['cost' => '10'])."'";
			}

	    $insert = "INSERT INTO `".$this->table."` (".$insert1.") VALUES (".$insert2.")";

	   	$link = $this->createLinki();
			$result = mysqli_query($link,$insert);// or die( "INSERT: ".mysqli_error($link)."\r\n". $insert);
			if($result)
			{
				$id=mysqli_insert_id($link);
			}
			else
			{
				$id= -1 * mysqli_errno($link);
			}

			if(is_resource($link) )
			{
				mysqli_close($link);
			}
			return $id;
	}

	public function Update($postData,$ignore = array(), $bUseOnlyIgnoreList = true)
	{
		$link = $this->createLinki();
		$dbColumns = array();
		if(!$bUseOnlyIgnoreList)
		{
			$sql = "DESCRIBE `".$this->table."`";
			$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
			while($row = mysqli_fetch_assoc($result))
			{
				$dbColumns[] = $row['Field'];
			}			
		}
		
        $regex_date = "/^([0-9]{1,2})(\-|\/|\.)([0-9]{1,2})(\-|\/|\.)([0-9]{4})$/";
        $regex_time = "/^([0-9]{1,2}):([0-9]{1,2})$/";
        $regex_bool = "/^b_/";
        $regex_search = "/^search_/";
        $update1 = "";
		while($key = key($postData))
		{
			if(preg_match($regex_search,$key))
			{
				next($postData);
				continue;
			}
			if((in_array($key,$ignore)=== false) && ($bUseOnlyIgnoreList || in_array($key,$dbColumns) ))
			{
					//echo($key."<br/>");
					if($key == "password")
					{
						next($postData);
						continue;
					}

					if(strlen($update1)>0 && $key != "password" ){ $update1.=",";}

					if(preg_match($regex_date,$postData[$key]))
					{
						//echo("preg_matchDateUpdate<br>");
						list($month,$day,$year)=explode('-',$postData[$key]);
						$result = $year."-".$month."-".$day;
					}
					else if(preg_match($regex_time,$postData[$key]))
					{
						$result = $postData[$key].":00";
					}
					else if(preg_match($regex_bool,$key))
					{
						$result = "1";
					}
					else
					{
						$result = $postData[$key];
						//$result = mysqli_escape_string($link,$postData[$key]);
						//echo( $key."=".$postData[$key]."<br>");
					}
   				$update1 .= "`".$key."`=".(is_null($result) ? 'NULL' : "'".$result."'");

			}

			next($postData);
		}
		if(isset($postData['password']) && strlen($postData['password'])>0)
		{
			if(strlen($update1) > 0){$update1 .= ", ";}
            $password = $postData['password'];
            $update1 .= "`password`= '".password_hash($password, PASSWORD_BCRYPT, ['cost' => '10'])."'";
		}		
		
		$updateTime = "";
		if(isset($postData['updatetime']) && strlen($postData['updatetime'])>0)
		{
			$updateTime = " AND `updatetime` <  '".$postData['updatetime']."'";
		}
		
		$id = $postData['id'];

    $update = "UPDATE `".$this->table."` SET ".$update1." WHERE `id`= '".$id."'".$updateTime;
   	
		$result = mysqli_query($link,$update); // or die( "UPDATE: ".mysqli_error($link)."<br/>".$update );
		
			if(!$result)
			{
				$id= -1 * mysqli_errno($link);
			}

			if(is_resource($link) )
			{
				mysqli_close($link);
			}
		return $id;
	}

    public function Replace($postData,$ignore = array(), $bUseOnlyIgnoreList = true)
    {

        $link = $this->createLinki();
        $dbColumns = array();
        if(!$bUseOnlyIgnoreList)
        {
            $sql = "DESCRIBE `".$this->table."`";
            $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
            while($row = mysqli_fetch_assoc($result))
            {
                $dbColumns[] = $row['Field'];
            }
        }

        $regex_date = "/^([0-9]{1,2})(\-|\/|\.)([0-9]{1,2})(\-|\/|\.)([0-9]{4})$/";
        $regex_time = "/^([0-9]{1,2}):([0-9]{1,2})$/";
        $regex_bool = "/^b_/";
        $regex_search = "/^search_/";
        $insert1 = "";
        $insert2 = "";
        while($key = key($postData))
        {

            if(preg_match($regex_search,$key))
            {
                next($postData);
                continue;
            }

            if( (in_array($key,$ignore)=== false) && ($bUseOnlyIgnoreList || in_array($key,$dbColumns)) )
            {

                if($key != "password")
                {
                    if(strlen($insert1)>0){ $insert1.=",";}
                    if(strlen($insert2)>0){ $insert2.=",";}
                    $insert1 .= "`";
                    $insert2 .= "'";
                }
                if(preg_match($regex_date,$postData[$key]))
                {
                    //echo("preg_matchDateAdd<br>");
                    list($month,$day,$year)=explode('-',$postData[$key]);
                    $result = $year."-".$month."-".$day;
                }
                else if(preg_match($regex_time,$postData[$key]))
                {
                    $result = $postData[$key].":00";
                }
                else if(preg_match($regex_bool,$key))
                {
                    $result = "1";
                }
                else if($key == "password")
                {
                    next($postData);
                    continue;
                }
                else
                {
                    $result = mysqli_escape_string($link,$postData[$key]);
                }

                if($key != "password")
                {
                    $insert1 .= $key."`";
                    $insert2 .= $result."'";
                }

            }
            next($postData);
        }

        if(isset($postData['password']) && strlen($postData['password'])>0)
        {
            $insert1 .= ",`password`";
            $password = $postData['password'];
            $insert2 .= ",'".password_hash($password, PASSWORD_BCRYPT, ['cost' => '10'])."'";
        }

        $insert = "REPLACE INTO `".$this->table."` (".$insert1.") VALUES (".$insert2.")";

        $link = $this->createLinki();
        $result = mysqli_query($link,$insert);// or die( "INSERT: ".mysqli_error($link)."\r\n". $insert);
        if($result)
        {
            $id=mysqli_insert_id($link);
        }
        else
        {
            $id= -1 * mysqli_errno($link);
        }

        if(is_resource($link) )
        {
            mysqli_close($link);
        }
        return $id; //Note: If Update it won't be a new id.
    }


    public function Remove($postData,$ignore="")
	{
    $id = $postData['id'];
    $remove = "DELETE FROM `".$this->table."` WHERE `id`= '".$id."'";
   	$link = $this->createLinki();
		$result = mysqli_query($link,$remove) or die( "DELETE: ".mysqli_error($link) );
			if(is_resource($link) )
			{
				mysqli_close($link);
			}
	}
	public function RemoveFile($postData,$ignore)
	{
    $link = $this->createLinki();
    $id = $postData['id'];

    $sql = "SELECT * FROM `".$this->table."` WHERE `id`= '".$id."'";
 		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link) );
 		$num = mysqli_num_rows($result);
 		while($row = mysqli_fetch_assoc($result))
 		{
 			$location = $row['location'];
 			$basepath = dirname($_SERVER['SCRIPT_FILENAME']);
			$fileurl = "http://".$_SERVER['SERVER_NAME'];
			$location = str_replace($fileurl,$basepath,$location);

			if(is_file($location))
			{
				unlink($location);
			}

 		}

    $remove = "DELETE FROM `".$this->table."` WHERE `id`= '".$id."'";
		$result = mysqli_query($link,$remove) or die( "DELETE: ".mysqli_error($link) );
			if(is_resource($link) )
			{
				mysqli_close($link);
			}
	}


	public function WriteListEdit($type)
	{
		$num = 0;
 		$link = $this->createLinki();
 		$sql = "SELECT";
 		$sql .= " * ";
 		$sql .= "FROM `".$this->table."` ";
        $color = "";

 		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link) ."\r\n".$sql);
 		$num = mysqli_num_rows($result);

 		echo ("<table border='0' cellpadding='0' cellspacing='0' class='editTable'>");
 		echo ("<tr><th>Descritption</th><th>&nbsp;</th></tr>");

			while($row = mysqli_fetch_assoc($result))
			{
				if ($color != "#E3E4E7")
				{
					$color = "#E3E4E7";
				}
				else
				{
					$color = "#ffffff";
				}

				$id = $row['id'];
				$description = $row['description'];

				echo('<tr><form enctype="multipart/form-data" name="list_admin" action="'.$_SERVER['PHP_SELF'].'?type='.$type.'" method="post"><tr>'."\r\n");
				echo('<td><input type="text" name="description" value="'.$description.'"></td>'."\r\n");
				echo('<td><input type="submit" class="button" name="update" value="Update"></td>'."\r\n");
				echo('<input type="hidden" name="id" value="'.$id.'">'."\r\n");
				echo('</form></tr>'."\r\n");
			}
			echo('<form enctype="multipart/form-data" name="list_admin" action="'.$_SERVER['PHP_SELF'].'?type='.$type.'" method="post">'."\r\n");
			echo('<tr><td><input type="text" name="description" value=""></td>'."\r\n");
			echo('<td><input type="submit" class="button" name="add" value="Add"></td></tr>'."\r\n");
			echo ('<tr><td colspan="2" align="center"><input type="submit" class="button" name="done" value="Done"></td></tr>');
			echo('</form>'."\r\n");
			echo("</table>"."\r\n");

			if(is_resource($link) )
			{
				mysqli_close($link);
			}
	}

	public function makeUnixFriendlyName($rawName)
	{
			$pieces = explode(".", $rawName);
			$contentName = $pieces[0];
			$contentExt = $pieces[1];

			$betterName = iconv('UTF-8', 'ASCII//IGNORE', $contentName);
			$badChars = array(' ','~','`','!','@','#','%','&',';',':','=','.',',','<','>','/','(',')','$','^','*','-','+','{','}','[',']','?','\\','\'');
			$betterName = str_replace($badChars,"_",$betterName);
			$betterName = strtolower  ($betterName);
			return $betterName.".".$contentExt;
	}
	//ISO-8859-1

	public function GetColumns()
	{
			$link = $this->createLinki();
			$sql = "DESCRIBE `".$this->table."`";
			$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
			while($row = mysqli_fetch_assoc($result))
			{
				$array[] = $row['Field'];
			}
			if(is_resource($link) )
			{
				mysqli_close($link);
			}
			return $array;
	}
	
	public function GetUUID()
	{
			$link = $this->createLinki();
			$sql = "SELECT UUID() AS 'uuid'";
			$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
			$response = "";
			while($row = mysqli_fetch_assoc($result))
			{
				$response = $row['uuid'];
			}
			if(is_resource($link) )
			{
				mysqli_close($link);
			}
			return $response;		
	}

    function runQuery($sql,$single_row = false){
        $response = [];
        if(strlen($sql) > 0){
            $link = $this->createLinki();
            $result = mysqli_query($link, $sql) or die("runQuery: " . mysqli_error($link) . "<br/>" . $sql);
            if(!is_bool($result)){
                while($row = mysqli_fetch_assoc($result)){
                    if($single_row){
                        $response = $row;
                    }else{
                        $response[] = $row;
                    }
                }
            }else{
                $response = $result;
            }
            if(is_resource($link)){
                mysqli_close($link);
            }
        }
        return $response;
    }
	
	public function GetConnectionInfo()
	{
		return "Database: ".$this->dbase."  User: ".$this->dbuser;
	}


    function doQuery($connection_data,$sql){

        $data = array();
        $link = mysqli_connect($connection_data['dbhost'], $connection_data['dbuser'], $connection_data['dbpass'], $connection_data['dbase'], $connection_data['dbport']);
        if (!$link) {
            die('Connect Error (' . mysqli_connect_errno() . ') '
                . mysqli_connect_error());
        }

        if($result = mysqli_query($link,$sql)){
            if( !is_bool($result) ){
                while ($row = mysqli_fetch_assoc($result)) {
                    $data[] = $row;
                }
                mysqli_free_result($result);
            }
        }


        if(is_resource($link) )
        {
            mysqli_close($link);
        }
        return $data;

    }

}
?>
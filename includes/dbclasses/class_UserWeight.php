<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

class UserWeight extends dbBase
{
	public $user_id = "";
	public $weight = "";
	public $waist_size = "";
	public $date_submitted = "";

	 function __construct($id = "")
   {
    parent::__construct();
	  $this->table = "user_weight";
		if(strlen($id)>0)
		{
			parent::Load($id);
		}
   }

	function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','add_post');
		$id = parent::Add($postData,$ignore);
		return $id;
	}

	function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','update_post');
		$id = parent::Update($postData,$ignore);
	}

	function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','add_post','update_post');

		$id = self::Add($postData,$ignore,$bUseOnlyIgnoreList);
		if($id < 0){
			$id = self::GetIdForCycleId($postData['cycle_id']);
			$postData['id'] = $id;
			self::Update($postData,$ignore,$bUseOnlyIgnoreList);
        }
		return $id;
	}

//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////
	function GetIdForCycleId($cycle_id){
		$link = parent::createLinki();
		$sql = "SELECT id";
		$sql .= " from `".$this->table."` where `cycle_id` = '".$cycle_id."' ORDER BY `id` DESC LIMIT 1";
		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
		$id = -1;
		while($row = mysqli_fetch_assoc($result))
		{
			$id = $row['id'];
		}
		//var_dump($array);die();
		mysqli_close($link);
		return $id;

	}

	function GetLatestWeight($user_id){
		$link = parent::createLinki();
		$sql = "SELECT weight,waist_size
		         from ".$this->table." 
		         where user_id = '".$user_id."'
		         ORDER BY `id` DESC limit 1";
		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
		$response = array();
		while($row = mysqli_fetch_assoc($result))
		{
			$response = $row;
		}
		//var_dump($array);die();
		mysqli_close($link);
		return $response;
	}

}


?>

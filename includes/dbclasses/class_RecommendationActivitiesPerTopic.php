<?php 
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

////Generic class template
//// Notes:
//// xx = ActivitiesPerRecommendation
//// xxx  = activities_per_recommendation
//// $..  = column name(s)

class RecommendationActivitiesPerTopic extends dbBase
{
	public $topic_id = "";
	public $activity_id = "";

	 function __construct($id = "") 
   {
    parent::__construct();
	  $this->table = "recommendation_activities_per_topic";
		if(strlen($id)>0)
		{
			parent::Load($id);
		}
   }
	
	function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','add_post');		
		$id = parent::Add($postData,$ignore);	
		return $id;
	}
		
	function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','update_post');
		$id = parent::Update($postData,$ignore);
	}

    function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true)
    {
        $ignore = array('id','add_post','update_post');
        //if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        $id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
        return $id;
    }
	
//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////
    function GetActivitiesForTopic($topic_id){

        $link = parent::createLinki();

        $sql = "SELECT `activity_id` FROM `".$this->table."` WHERE `topic_id` = '".$topic_id."' AND `hidden`=0 ORDER BY topic_id ASC,sort_order ASC";

        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $array = array();
        while($row = mysqli_fetch_assoc($result))
        {
            $array[] = $row['activity_id'];
        }
        //var_dump($array);die();
        mysqli_close($link);
        return $array;
    }

    function UpdateActivitiesForTopic($post_data){

        if(!isset($post_data['topic_id'])){return;}

	     $link = parent::createLinki();

        $sql = "DELETE  FROM `".$this->table."` WHERE `topic_id` = '".$post_data['topic_id']."'";
        $result = mysqli_query($link,$sql) or die( "DELETE: ".mysqli_error($link)."<br/>".$sql );
        $count = 1;
        foreach ($post_data as $key => $value ) {

            if(strpos($key, 'activity_id_') !== false){
                $sql = "INSERT INTO ".$this->table." (`topic_id`,`activity_id`,`sort_order`) VALUES ('".$post_data['topic_id']."','".$value."','".$count."')";
                $result = mysqli_query($link, $sql) or die("SELECT: " . mysqli_error($link) . "<br/>" . $sql);
            }
            $count++;
        }

        return true;
    }

    function GetTopics(){

        $link = parent::createLinki();

        $sql = "SELECT * FROM `".$this->table."` order by id,sort_order asc";

        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $array = array();
        while($row = mysqli_fetch_assoc($result))
        {
            $array[$row['topic_id']][$row['activity_id']] = array();
        }
        //var_dump($array);die();
        mysqli_close($link);
        return $array;
    }
	
}

  
?>
<?php 
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

////Generic class template
//// Notes:
//// xx = Class Name
//// xxx  = table name
//// $..  = column name(s)

class KitVendorValueMap extends dbBase
{

	public $vendor = "";
	public $vendorKey = "";
	public $wbDataTypeKey = "";
	
	public $valueMap = array();
	

	 function __construct($id = "") 
   {
		parent::__construct();
		$this->table = "kit_vendorvaluemap";   	
		if(strlen($id)>0)
		{
			parent::Load($id);
		}
   }

	function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','add_HBKVendorValueMap','add_post');		
		$id = parent::Add($postData,$ignore);	
		return $id;
	}
		
	function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','update_HBKVendorValueMap','update_post');
		$id = parent::Update($postData,$ignore);
	}

    function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true)
    {
        $ignore = array('id','add_post','update_post');
        //if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        $id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
        return $id;
    }
//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////
	function GetMap($vendor)
	{

			$array = array();
   		$link = $this->createLinki();
			$sql = "Select `vendorKey`,`wbDataTypeKey` FROM `".$this->table."` WHERE `vendor` = '".$vendor."'";
			$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
			while($row = mysqli_fetch_assoc($result)){
				$array[$row['vendorKey']] = $row['wbDataTypeKey'];}

			if(count($array) > 0 && isset($array[0]) && count($array[0])>0)
			{
				reset($array[0]);
			}
			
		  mysqli_close($link);
		  $this->valueMap = $array;
			return $array;
	}	
	
	function LookupVendorKey($vendorKey)
	{
		$result = "not found";
		if(count($this->valueMap) > 0)
		{
			if(isset($this->valueMap[$vendorKey]))
			{
				$result = $this->valueMap[$vendorKey];
			}
		}
		return $result;
	}	
	
}

?>
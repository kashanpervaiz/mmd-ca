<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

class KitData extends dbBase
{

	public $cycle_id = "";
	public $barcode = "";
	public $labId = "";
	public $productRef = "";
	public $timestamp_shipped = ""; //timestamp_created
	public $timestamp_registration = "";
    public $timestamp_ready = "";
    public $timestamp_first_viewed = "";
	public $box_id = "";
	public $collectionDate = "";
	public $collectionTime = "";
	public $association_id = "";
	public $areas_to_address = "";


	 function __construct($id = "")
   {
		parent::__construct();
   	    $this->table = "kit_data";
		if(strlen($id)>0)
		{
			$this->Load($id);
		}
   }

	function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','add_post');
		$id = parent::Add($postData,$ignore);
		return $id;
	}

	function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','update_post');
		$id = parent::Update($postData,$ignore);
	}

    function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true)
    {
        $ignore = array('id','add_post','update_post');
        //if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        $id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
        return $id;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
    function GetUserForKitId($kit_id)
    {
        $link = parent::createLinki();

        $sql = "SELECT uc.user_id from ".$this->table." tt
                JOIN user_cycles uc on uc.id = tt.cycle_id
                WHERE tt.id = '".$kit_id."'";


        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $array = array();
        while($row = mysqli_fetch_assoc($result))
        {
            $array = $row['user_id'];
        }
//		//var_dump($array);
        mysqli_close($link);
        return $array;
    }


    function GetUserTestKitsForUser($user_id, $sortOrder='DESC')
    {
        $link = parent::createLinki();
        $sql = "SELECT tt.*,uc.cycle_num from ".$this->table." tt
                JOIN user_cycles uc on uc.id = tt.cycle_id
                WHERE uc.user_id = '$user_id'
                ORDER BY uc.cycle_num ".$sortOrder;
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $array = array();
        while($row = mysqli_fetch_assoc($result))
        {
            $array[] = $row;
        }
//		//var_dump($array);
        mysqli_close($link);
        return $array;
    }

    function GetUserTestKitsUpToDate($user_id, $date, $display_tab = "")
    {
        $link = parent::createLinki();
        $sql = "SELECT * from ".$this->table." tt
                WHERE tt.cycle_id IN (SELECT id FROM user_cycles uc WHERE uc.user_id = '$user_id') 
                AND tt.timestamp_ready <= '".$date."' AND tt.timestamp_ready != '0000-00-00' ORDER BY tt.timestamp_ready DESC LIMIT 1";
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $array = array();
        while($row = mysqli_fetch_assoc($result))
        {
            $array[] = $row;
        }
//		//var_dump($array);
        mysqli_close($link);
        return $array;
    }

	function GetUserTestKitsUpToCycleId($cycle_id, $sortOrder='DESC')
	{
		$userCycleObj = new UserCycles();
        $cycle_ids = $userCycleObj->GetCycleIdsUptoCycleId($cycle_id);
        $cycle_ids_string = "'".implode("','",$cycle_ids)."'";

	    $link = parent::createLinki();

        $sql = "SELECT tt.*,uc.cycle_num from ".$this->table." tt
                JOIN user_cycles uc on uc.id = tt.cycle_id
                WHERE tt.cycle_id IN (".$cycle_ids_string.") 
                ORDER BY tt.timestamp_ready ".$sortOrder;

		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
		$array = array();
				while($row = mysqli_fetch_assoc($result))
				{
					$array[] = $row;
				}
//		//var_dump($array);
		mysqli_close($link);
		return $array;
	}

    function GetUserTestKitForCycleId($cycle_id)
    {
        $link = parent::createLinki();

        $sql = "SELECT * from ".$this->table." tt
                WHERE tt.cycle_id = '".$cycle_id."'";

        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $array = array();
        while($row = mysqli_fetch_assoc($result))
        {
            $array = $row;
        }
        mysqli_close($link);
        return $array;
    }

    function getKitDateRanges($user_id){

        $link = parent::createLinki();

        $sql = "SELECT kd.id,uc.start_date,uc.end_date,uc.id as 'cycle_id',uc.cycle_num FROM user_cycles uc
                LEFT JOIN ".$this->table." kd ON uc.id = kd.cycle_id
                WHERE uc.user_id = '".$user_id."'
                ORDER BY uc.cycle_num ASC";

        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $array = array();
        $kit_data = [];
        $last_kit_id = '';
        while($row = mysqli_fetch_assoc($result))
        {
            if(isset($row['id']) && $last_kit_id != $row['id']){
                $kit_data[$row['id']]['start_date']  = $row['start_date'];
                //default these for cases of only one cycle///////////
                $new_end_date_obj = new DateTime($row['start_date']. "+27days");
                $new_end_date = $new_end_date_obj->format("Y-m-d");
                $kit_data[$row['id']]['end_date']  = $new_end_date;
                $kit_data[$row['id']]['cycle_id']  = $row['cycle_id'];
                $kit_data[$row['id']]['cycle_num']  = $row['cycle_num'];
                //////////////////////////////////////////////////////
                $last_kit_id = $row['id'];
            } else {
                $kit_data[$last_kit_id]['end_date']  = $row['end_date'];
                $kit_data[$last_kit_id]['cycle_id']  = $row['cycle_id'];
                $kit_data[$last_kit_id]['cycle_num']  = $row['cycle_num'];
            }
        }
        mysqli_close($link);
        return $kit_data;
    }

	function GetBarcodeForId($id)
	{

		$link = parent::createLinki();
		$sql = "SELECT `barcode` from `".$this->table."` where `id` = '".$id."'";
		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
		$data = "";
				while($row = mysqli_fetch_assoc($result))
				{
					$data = $row['barcode'];
				}
//		//var_dump($array);
		mysqli_close($link);
		return $data;
	}

	function GetAssociationForId($id)
	{

		$link = parent::createLinki();
		$sql = "SELECT `association_id` from `".$this->table."` where `id` = '".$id."'";
		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
		$data = "";
				while($row = mysqli_fetch_assoc($result))
				{
					$data = $row['association_id'];
				}
//		//var_dump($array);
		mysqli_close($link);
		return $data;
	}

	function GetIdForBarcode($barcode)
	{

		$link = parent::createLinki();
		$sql = "SELECT `id` from `".$this->table."` where `barcode` = '".$barcode."'";
		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
		$data = "";
				while($row = mysqli_fetch_assoc($result))
				{
					$data = $row['id'];
				}
//		//var_dump($array);
		mysqli_close($link);
		return $data;
	}
	
	function findBox($barcode)
	{

		$link = parent::createLinki();
		$sql = "SELECT `box_id` from `".$this->table."` where `barcode` = '".$barcode."'";
		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
		$data = "0";
				while($row = mysqli_fetch_assoc($result))
				{
					$data = $row['box_id'];
				}
//		//var_dump($array);
		mysqli_close($link);
		return $data;
	}	

	function validateBarcode($barcode)
	{

		$link = parent::createLinki();
		$sql = "SELECT `id` from `".$this->table."` where `barcode` = '".$barcode."' AND `cycle_id` = 0";
		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
		$row = mysqli_fetch_assoc($result);
		$data = $row['id'];

		//Check if in use
		if(!isset($data))
		{
			$sql = "SELECT `id` from `".$this->table."` where `barcode` = '".$barcode."'";
			$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
			$row = mysqli_fetch_assoc($result);
			$data = $row['id'];
			if(isset($data) and intval($data)>0)
			{
				$data = -1;
			}

		}

		mysqli_close($link);
		return $data;
	}


	function registerBarcode($barcode,$cycle_id,$collectionDate,$collectionTime1,$collectionTime2,$collectionTime3,$collectionTime4)
	{
		$link = parent::createLinki();
		$sql = "UPDATE `".$this->table."` SET 
		`cycle_id` = '".$cycle_id."',
		`timestamp_registration`= NOW(),
		`collectionDate`='".$collectionDate."',
		`collectionTime1`='".$collectionTime1."',
		`collectionTime2`='".$collectionTime2."',
		`collectionTime3`='".$collectionTime3."',
		`collectionTime4`='".$collectionTime4."'
		 WHERE `barcode` = '".$barcode."'";
		$result = mysqli_query($link,$sql) or die( "UPDATE: ".mysqli_error($link)."<br/>".$sql );
		$data = mysqli_affected_rows($link);
		mysqli_close($link);
		return $data;
	}

	function updateBarcode($barcode,$cycle_id)
	{
		$link = parent::createLinki();
		$sql = "UPDATE `".$this->table."` SET `cycle_id` = '".$cycle_id."',`timestamp_registration`= NOW() WHERE `barcode` = '".$barcode."'";
		$result = mysqli_query($link,$sql) or die( "UPDATE: ".mysqli_error($link)."<br/>".$sql );
		$data = mysqli_affected_rows($link);
		mysqli_close($link);
		return $data;
	}

	function addNewKit($post)
	{
	    $insert1 = "`productRef`,`labId`,`barcode`,`timestamp_shipped`";
	    $insert2 = "'".$post['productRef']."','".$post['labId']."','".$post['barcode']."','".$post['timestamp_shipped']."'";

	    $insert = "INSERT INTO `".$this->table."` (".$insert1.") VALUES (".$insert2.")";

	   	$link = $this->createLinki();
			$result = mysqli_query($link,$insert);// or die( "INSERT: ".mysqli_error($link)."\r\n". $insert);
			if($result)
			{
				$id=mysqli_insert_id($link);
			}
			else
			{
				$id= -1 * mysqli_errno($link);
			}

			if(is_resource($link) )
			{
				mysqli_close($link);
			}
			return $id;
	}

	function GetBoxContent($boxId)
	{
		$link = parent::createLinki();
		$sql = "SELECT `id`,`cycle_id`,`barcode`,`collectionDate`,`collectionTime`, `productRef` from `".$this->table."` where `box_id` = '".$boxId."'";
		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
		$data = array();
				while($row = mysqli_fetch_assoc($result))
				{
					$data[] = $row;
				}
//		//var_dump($array);
		mysqli_close($link);
		return $data;
	}

	function AddKitToBox($barcode,$box_id)
	{
		$link = parent::createLinki();
		$sql = "SELECT `id`,`cycle_id`,`box_id` from `".$this->table."` where `barcode` = '".$barcode."'";
		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
		$row = mysqli_fetch_assoc($result);
		if(!isset($row))
		{
			$result = -4; //barcode doesnt exist
		}
		else if(strlen($row['box_id']) > 0 and intval($row['box_id']) > 0)
		{
			$result = -3; //barcode already in a box
		}
		else if(strlen($row['cycle_id']) == 0  or intval($row['cycle_id']) == 0)
		{
			$result = -2; //barcode not registered
		}
		else
		{
				$sql = "UPDATE `".$this->table."` SET `box_id` = '".$box_id."' WHERE `barcode` = '".$barcode."'";
				$result = mysqli_query($link,$sql) or die( "UPDATE: ".mysqli_error($link)."<br/>".$sql );
				$data = mysqli_affected_rows($link);
				if($data > 0)
				{
					$result = $box_id;
				}
				else
				{
					$result = -1; //unknown issue
				}
		}


		mysqli_close($link);
		return $result;
	}

	function RemoveKitFromBox($barcode,$box_id)
	{
		$link = parent::createLinki();

		$sql = "UPDATE `".$this->table."` SET `box_id` = '".$box_id."' WHERE `barcode` = '".$barcode."'";
		$result = mysqli_query($link,$sql) or die( "UPDATE: ".mysqli_error($link)."<br/>".$sql );
		$data = mysqli_affected_rows($link);
		if($data > 0)
		{
			$result = $box_id;
		}
		else
		{
			$result = -1; //unknown issue
		}

		mysqli_close($link);
		return $result;
	}

    function getKitCountForCycleId($cycle_id){ //used to see if they have already registered any with this cycle
        $link = parent::createLinki();
        $sql = "SELECT COUNT(*) as 'count' FROM `".$this->table."` WHERE `cycle_id` = '".$cycle_id."'";
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $row = mysqli_fetch_assoc($result);
        mysqli_close($link);
        return isset($row['count'])?$row['count']:'0';
    }


    function getKitCountForUserId($user_id){
        $link = parent::createLinki();

        $sql = "SELECT COUNT(*) as 'count' FROM `".$this->table."`
        JOIN `user_cycles` ON `user_cycles`.`id` = `".$this->table."`.`cycle_id` 
        WHERE  `user_cycles`.`user_id` = '".$user_id."'";

        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $row = mysqli_fetch_assoc($result);
        mysqli_close($link);
        return isset($row['count'])?$row['count']:'0';
    }

    function getReadyCountForUserId($user_id){
        $link = parent::createLinki();

        $sql = "SELECT COUNT(*) as 'count' FROM `".$this->table."`
        JOIN `user_cycles` ON `user_cycles`.`id` = `".$this->table."`.`cycle_id`  
        WHERE `".$this->table."`.`timestamp_ready` IS NOT NULL AND  `user_cycles`.`user_id` = '".$user_id."'";

        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $row = mysqli_fetch_assoc($result);
        mysqli_close($link);
        return isset($row['count'])?$row['count']:'0';
    }

    function GetFirstViewedDate($kit_id){
        $kit_data = self::GetData($kit_id);
        $last_viewed_date = $kit_data[0]['timestamp_first_viewed'];

        if(!isset($last_viewed_date)){
            $objDateTime = new DateTime();
            $today = $objDateTime->format('Y-m-d H:i:s');
            $data = ['id'=>$kit_id,'timestamp_first_viewed'=>$today];
            self::Update($data);
            $last_viewed_date = $today;
        }

        return $last_viewed_date;
    }

}

?>
<?php 
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

//Generic class template
// Notes:
// xx = StepsPerTestKit
// xxx  = steps_per_test_kit
// $..  = column name(s)

class RecommendationStepsPerTestKit extends dbBase
{
	public $step_id = "";
    public $testkit_id = "";

   function __construct($id = "") 
   {
    parent::__construct();
	  $this->table = "recommendation_steps_per_testkit";
		if(strlen($id)>0)
		{
			parent::Load($id);
		}
   }
	
	function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','add_post');		
		$id = parent::Add($postData,$ignore);	
		return $id;
	}
		
	function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','update_post');
		$id = parent::Update($postData,$ignore);
	}

    function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true)
    {
        $ignore = array('id','add_post','update_post');
        //if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        $id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
        return $id;
    }
	
//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////
	
	function GetStepsForKit($kit_id){

        $link = parent::createLinki();

        $sql = "SELECT `step_id` FROM `".$this->table."` WHERE `testkit_id` = '".$kit_id."'";

        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $array = array();
        while($row = mysqli_fetch_assoc($result))
        {
            $array[] = $row['step_id'];
        }
        //var_dump($array);die();
        mysqli_close($link);
        return $array;
    }
}

  
?>
<?php 
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

////Generic class template
//// Notes:
//// xx = RecommendationsPerStep
//// xxx  = recommendations_per_step
//// $..  = column name(s)

class RecommendationTopicsPerStep extends dbBase
{
	public $step_id = "";
	public $topic_id = "";

	 function __construct($id = "") 
   {
    parent::__construct();
	  $this->table = "recommendation_topics_per_step";
		if(strlen($id)>0)
		{
			parent::Load($id);
		}
   }
	
	function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','add_post');		
		$id = parent::Add($postData,$ignore);	
		return $id;
	}
		
	function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','update_post');
		$id = parent::Update($postData,$ignore);
	}

    function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true)
    {
        $ignore = array('id','add_post','update_post');
        //if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        $id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
        return $id;
    }
	
//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////
	function GetTopicsForStep($step_id){
        $link = parent::createLinki();

        $sql = "SELECT `topic_id` FROM `".$this->table."` WHERE `step_id` = '".$step_id."' AND `hidden`=0 ORDER BY step_id ASC,sort_order ASC";

        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $array = array();
        while($row = mysqli_fetch_assoc($result))
        {
            $array[] = $row['topic_id'];
        }
        //var_dump($array);die();
        mysqli_close($link);
        return $array;

    }
	
}

  
?>
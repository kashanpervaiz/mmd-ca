<?php 
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

//Generic class template
// Notes:
// xx = Class Name
// xxx  = table name
// $..  = column name(s)

class UserSummary extends dbBase
{
    public $user_id = "";
    public $cycle = "";
    public $cycle_id = "";
    public $scores = "";

    function __construct($id = "")
    {
        parent::__construct();
        $this->table = "user_summary";
        if(strlen($id)>0)
        {
            parent::Load($id);
        }
    }

	function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','add_post');
		$id = parent::Add($postData,$ignore);
		return $id;
	}

	function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','update_post');
		$id = parent::Update($postData,$ignore);
	}

    function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true)
    {
        $ignore = array('id','add_post','update_post');
        //if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        $id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
        return $id;
    }

//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////
    function GetUserSummaryData($userId, $sortOrder = 'ASC')
    {
        $link = parent::createLinki();
        $sql = "SELECT * FROM ".$this->table." t
                JOIN user_cycles uc ON uc.id = t.cycle_id
                WHERE uc.user_id = '".$userId."'
                ORDER BY uc.cycle_num ".$sortOrder;
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $array = array();
        while($row = mysqli_fetch_assoc($result))
        {
            $array[$row['cycle_num']] = $row;
        }
        //var_dump($array);die();
        mysqli_close($link);
        return $array;
    }

    function ReplaceWithCurrentData($cycle_id,$json_data){
        $link = parent::createLinki();
        $sql = "REPLACE INTO ".$this->table." (`cycle_id`,`scores`) VALUES ('".$cycle_id."','".$json_data."')";
        $result = mysqli_query($link,$sql) or die( "REPLACE: ".mysqli_error($link)."<br/>".$sql );
        mysqli_close($link);
        return $result;
    }

    function GetLatestSummary($user_id){
        $rows = self::GetUserSummaryData($user_id, 'DESC');
        $row = isset($rows[0])?$rows[0]:[];
        return $row;
    }

    function getColor($score){
        $result = "red";
        if($score >= 66){
            $result = "red";
        }else if($score >= 33){
            $result = "yellow";
        } else {
            $result = "green";
        }
        return $result;
    }

}

  
?>
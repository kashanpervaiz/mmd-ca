<?php 
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

////Generic class template
//// Notes:
//// xx = Class Name
//// xxx  = table name
//// $..  = column name(s)



class KitResults extends dbBase
{
	public $kit_id = "";
	public $vendor = "";
	public $vendorData = "";
	public $kitData = "";	//KitData "wbDataTypeKey"=>"","value"=>"","units"=>"","range"=>""


	 function __construct($id = "") 
   {
		parent::__construct();
		$this->table = "kit_results";		
		if(strlen($id)>0)
		{
			$this->Load($id);
		}
   }
   
	function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','add_post');		
		$id = parent::Add($postData,$ignore);	
		return $id;
	}
		
	function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','update_post');
		$id = parent::Update($postData,$ignore);
	}

    function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true)
    {
        $ignore = array('id','add_post','update_post');
        //if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        $id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
        return $id;
    }
	
//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////
	
	function GetTestKitResult($kit_id)
	{	
		$link = parent::createLinki();
		$sql = "SELECT `vendor`,`vendorData` from `".$this->table."` where `kit_id` = '".$kit_id."'";
		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
//		$array = array();
//				while($row = mysqli_fetch_assoc($result))
//				{
//					$array[] = $row['vendorData'];
//				}
//		$data  = json_decode($array[0]['kitData'],true);
		$row = mysqli_fetch_assoc($result);
		$data = $row['vendorData'];
		$vendor = $row['vendor'];
		mysqli_close($link);
		return array('data'=>$data,'vendor'=>$vendor);
	}	
	
	function UpdateVendorResults($testKitId, $rawData,$cleanedData= "")
	{
		if($cleanedData== ""){$cleanedData=$rawData;}
	    $link = parent::createLinki();
		$sql = "UPDATE `".$this->table."` SET `vendorData` = '".json_encode($rawData)."',`kitData` = '".json_encode($cleanedData)."' WHERE `kit_id` = '".$testKitId."'";
		mysqli_query($link,$sql) or die( "UPDATE: ".mysqli_error($link)."<br/>".$sql );

 		$data = mysqli_affected_rows ($link );
 		
		mysqli_close($link);
		return $data;		
	}	
	
	function GetKitIds()
	{
		$link = parent::createLinki();
		$sql = "SELECT `kit_id` from `".$this->table."` where `vendorData` IS NOT NULL";
		$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
		$array = array();
		while($row = mysqli_fetch_assoc($result))
		{
			$array[] = $row['kit_id'];
		}
		mysqli_close($link);
		return $array;		
	}


}

  
?>
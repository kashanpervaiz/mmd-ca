<?php 
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";
/*
 * Create rules for postal codes and affiliations
 */

class AccessRules extends dbBase
{
    public $rule_name = "";
    public $rule_body = "";

   function __construct($id = ""){
        parent::__construct();
        $this->table = "access_rules";
        if(strlen($id)>0)
        {
            parent::Load($id);
        }
   }

	function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true){
		$ignore = array('id','add_post');
		$id = parent::Add($postData,$ignore,$bUseOnlyIgnoreList);
		return $id;
	}

	function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true){
		$ignore = array('id','update_post');
		$id = parent::Update($postData,$ignore,$bUseOnlyIgnoreList);
	}

    function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true){
        $ignore = array('id','add_post','update_post');
        //if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        $id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
        return $id;
    }


//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////
    function isInOpenRegion($country_code,$postal_code): bool
    {
        $sql = "SELECT * FROM $this->table ar WHERE ar.rule_name='postal_code' AND ar.country_code = '$country_code'";
        $rules = self::runQuery($sql);
        $postal_code = strtolower($postal_code);
        $result = false;
        foreach($rules as $rule){
            if(strlen($rule['rule_body']) > 0){
                $body_parts = explode(",",strtolower($rule['rule_body']));
                /*
                 * Use $result = $result || XXX so that is it's already set, it stays that way.
                 */
                switch(strtolower($rule['rule_type'])){
                    case 'in':
                        $result = ($result || in_array($postal_code,$body_parts)) ;
                        break;
                    case 'like':
                        foreach($body_parts as $part){
                            $result = ($result || strripos($postal_code,$part) !== false) ;
                        }
                        break;
                }
            }

        }
        return $result;
    }

}


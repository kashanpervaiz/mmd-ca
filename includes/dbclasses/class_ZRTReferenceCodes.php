<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
 
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

////Generic class template
//// Notes:
//// xx = Class Name
//// xxx  = table name
//// $..  = column name(s)

class ZRTReferenceCodes extends dbBase
{

	public $referencecode = "";
	public $timestamp_received = "";
	public $timestamp_retrieved = "";
	public $bSuccess = "";
	public $error = "";


	 function __construct($id = "") 
   {
		parent::__construct();
		$this->table = "zrt_reference_codes";
		if(strlen($id)>0)
		{
			$this->Load($id);
		}
   }
   	
	function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','add_ZRTReferenceCodes','add_post');		
		$id = parent::Add($postData,$ignore);	
		return $id;
	}
		
	function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','update_ZRTReferenceCodes','update_post');
		$id = parent::Update($postData,$ignore);
	}

    function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true)
    {
        $ignore = array('id','add_post','update_post');
        //if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        $id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
        return $id;
    }

//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////
	
	
}

  
?>
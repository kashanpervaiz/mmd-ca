<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

//Generic class template
// Notes:
// xx = Class Name
// xxx  = table name
// $..  = column name(s)

class UserCustomRecommendations extends dbBase
{
    public $cycle_id = "";
	public $recommendations_json = "";
    public $comments = "";
	public $admin_id;
	public $date_created = "";


	 function __construct($id = "")
   {
    parent::__construct();
	  $this->table = "user_custom_recommendations";
		if(strlen($id)>0)
		{
			parent::Load($id);
		}
   }

	function Add($postData, $ignore = [], $bUseOnlyIgnoreList = false)
	{
		$ignore = array('id','add_post');
		$id = parent::Add($postData,$ignore,$bUseOnlyIgnoreList);
		return $id;
	}

	function Update($postData, $ignore = [], $bUseOnlyIgnoreList = false)
	{
		$ignore = array('id','update_post');
		$id = parent::Update($postData,$ignore,$bUseOnlyIgnoreList);
	}

    function Replace($postData, $ignore = [], $bUseOnlyIgnoreList = false)
    {
        $ignore = array('id','add_post','update_post');
        //if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        $id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
        return $id;
    }


//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////
    function GetCustomRec($cycle_id){
        $link = parent::createLinki();
        $sql = "SELECT * FROM `".$this->table."` WHERE `cycle_id` = '".$cycle_id."'";
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $data = array();
        while($row = mysqli_fetch_assoc($result))
        {
            $data = $row;
        }
        //var_dump($array);die();
        mysqli_close($link);
        return $data;

    }

    function GetActivityList($cycle_id){
        $link = parent::createLinki();
        $sql = "SELECT * FROM `".$this->table."` WHERE `cycle_id` = '".$cycle_id."'";
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $data = array();
        while($row = mysqli_fetch_assoc($result))
        {
            $data = $row;
        }

        $json_data = isset($data['recommendations_json'])? $data['recommendations_json'] : "";

        $rec_data = json_decode($json_data,true);
        $activities = [];
        if(isset($rec_data) && count($rec_data)>0){
            foreach($rec_data as $step){
                foreach($step as $topic){
                    $activities = array_merge($activities,$topic);
                }
            }
        }
        mysqli_close($link);
        return $activities;
    }

}



<?php 
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

////Generic class template
//// Notes:
//// xx = Class Name
//// xxx  = table name
//// $..  = column name(s)

class Box extends dbBase
{
	public $destination_id = "";
	public $timestamp_completed = "";

	 function __construct($id = "") 
   {
    parent::__construct();
	  $this->table = "box";
		if(strlen($id)>0)
		{
			$this->Load($id);
		}
   }
	
	function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','add_post');		
		$id = parent::Add($postData,$ignore, $bUseOnlyIgnoreList);
		return $id;
	}
		
	function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','update_post');
		$id = parent::Update($postData,$ignore, $bUseOnlyIgnoreList);
	}

    function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true)
    {
        $ignore = array('id','add_post','update_post');
        //if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        $id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
        return $id;
    }
//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////
	function GetOpenBoxes()
	{
			$link = parent::createLinki();
			$sql = "SELECT * from `".$this->table."` where `timestamp_completed` IS NULL";
			$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
			$data = array();
					while($row = mysqli_fetch_assoc($result))
					{
						$data[] = $row;
					}
	//		//var_dump($array);
			mysqli_close($link);
			return $data;		
	}	
	
	function GetClosedBoxes()
	{
			$link = parent::createLinki();
			$sql = "SELECT * from `".$this->table."` where `timestamp_completed` IS NOT NULL";
			$result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
			$data = array();
					while($row = mysqli_fetch_assoc($result))
					{
						$data[] = $row;
					}
	//		//var_dump($array);
			mysqli_close($link);
			return $data;		
	}	
	
}

  
?>
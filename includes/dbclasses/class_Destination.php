<?php 
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

////Generic class template
//// Notes:
//// xx = Class Name
//// xxx  = table name
//// $..  = column name(s)

class Destination extends dbBase
{
	public $name = "";
	public $address1 = "";
	public $address2 = "";
	public $city = "";
	public $state = "";
	public $zipcode = "";

	 function __construct($id = "") 
   {
    parent::__construct();
	  $this->table = "destination";
		if(strlen($id)>0)
		{
			$this->Load($id);
		}
   }
	
	function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','add_post');		
		$id = parent::Add($postData,$ignore);	
		return $id;
	}
		
	function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','update_post');
		$id = parent::Update($postData,$ignore);
	}

    function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true)
    {
        $ignore = array('id','add_post','update_post');
        //if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        $id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
        return $id;
    }

//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////
	
	
}

  
?>
<?php 
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

//Generic class template
// Notes:
// xx = Class Name
// xxx  = table name
// $..  = column name(s)

class AccessKeys extends dbBase
{
    public $user_key = "";
    public $user_value = "";

   function __construct($id = ""){
        parent::__construct();
        $this->table = "access_keys";
        if(strlen($id)>0)
        {
            parent::Load($id);
        }
   }

	function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true){
		$ignore = array('id','add_post');
		$id = parent::Add($postData,$ignore,$bUseOnlyIgnoreList);
		return $id;
	}

	function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true){
		$ignore = array('id','update_post');
		$id = parent::Update($postData,$ignore,$bUseOnlyIgnoreList);
	}

    function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true){
        $ignore = array('id','add_post','update_post');
        //if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        $id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
        return $id;
    }


//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////
    function GetKeys(){
       $keys = self::GetAllData();
       $result = [];
       foreach($keys as $key){
           $result[$key['user_key']] = $key['user_value'];
       }
       return $result;
    }

}


<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"]) ."/includes/dbclasses/dbBaseV2.php";

////Generic class template
//// Notes:
//// xx = Class Name
//// xxx  = table name
//// $..  = column name(s)

class UserAssessments extends dbBase
{
    public $cycle_id = "";
    public $stress_score="";
    public $anxiety_score="";
    public $depression_score="";
    public $formjson= "";
    public $symptomjson= "";
    public $date_submitted = "";

	 function __construct($id = "")
   {
    parent::__construct();
	  $this->table = "user_assessments";
		if(strlen($id)>0)
		{
			parent::Load($id);
		}
   }

	function Add($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','add_post');
		$id = parent::Add($postData,$ignore,$bUseOnlyIgnoreList);
		return $id;
	}

	function Update($postData, $ignore = "", $bUseOnlyIgnoreList = true)
	{
		$ignore = array('id','update_post','update_assessment_form');
		$id = parent::Update($postData,$ignore,$bUseOnlyIgnoreList);
		return $id;
	}

    function Replace($postData, $ignore = "", $bUseOnlyIgnoreList = true)
    {
        //Note: Primary key is cycle_id

        $ignore = array('id','add_post','update_post');
        //if(!isset($postData['date_submitted'])){$postData['date_submitted'] = date("Y-m-d");}
        $id = parent::Replace($postData,$ignore,$bUseOnlyIgnoreList);
        return $id;
    }

//////////////////////////////////////////////////
//  Class Specific Functions
//////////////////////////////////////////////////
    function GetLatestUserData($cycle_id)
    {
        $link = parent::createLinki();
        $sql = "SELECT *";
        $sql .= " from `".$this->table."` where `cycle_id` = '".$cycle_id."' ORDER BY `id` DESC LIMIT 1";
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $array = array();
        while($row = mysqli_fetch_assoc($result))
        {
            $array = $row;
        }
        //var_dump($array);die();
        mysqli_close($link);
        return $array;
    }

    function GetUserData($user_id, $orderBy = 'DESC')
    {
        $link = parent::createLinki();
        $sql = "SELECT t.* 
                FROM ".$this->table." t
                JOIN user_cycles uc ON uc.id = t.cycle_id
                WHERE uc.user_id = '".$user_id."' ORDER BY uc.cycle_num ".$orderBy;
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $array = array();
        while($row = mysqli_fetch_assoc($result))
        {
            $array[$row['id']] = $row; //add according to assessment_id
        }

        //var_dump($array);die();
        mysqli_close($link);
        return $array;
    }


    function getCount($user_id){
        $link = parent::createLinki();

        $sql = "SELECT count(t.id) as 'count' 
                FROM ".$this->table." t
                JOIN user_cycles uc ON uc.id = t.cycle_id
                WHERE uc.user_id = '".$user_id."'";
        $result = mysqli_query($link,$sql) or die( "SELECT: ".mysqli_error($link)."<br/>".$sql );
        $row = mysqli_fetch_assoc($result);
        mysqli_close($link);

        return isset($row['count'])?$row['count']:'0';
    }

    function getColor($score){
        $result = "red";
        if($score > 200){
            $result = "red";
        }else if($score >= 100){
            $result = "yellow";
        } else {
            $result = "green";
        }
	    return $result;
    }

    function getLevel($score){
        $result = "High";
        if($score > 200){
            $result = "High";
        }else if($score >= 100){
            $result = "Moderate";
        } else {
            $result = "Low";
        }
        return $result;
    }

    function undoAssessment($id){
        $link = parent::createLinki();
        $sql = "DELETE FROM `".$this->table."` WHERE `id` = '".$id."'";
        $result = mysqli_query($link,$sql) or die( "DELETE: ".mysqli_error($link)."<br/>".$sql );
        mysqli_close($link);
    }

    /**
     * @param $user_id
     * @return array|string[]|null
     */
    public function GetUserMoodScores($cycle_id)
    {
        $link = parent::createLinki();
        $sql = "SELECT 
                    ROUND(ua.stress_score/3,0) as 'stress',
                    ROUND(ua.anxiety_score/3,0) as 'anxiety',
                    ROUND(ua.depression_score/3,0) as 'depression'
                FROM user_assessments ua  
                WHERE ua.cycle_id = '" . $cycle_id . "'  
                LIMIT 1";

        $result = mysqli_query($link, $sql) or die("SELECT: " . mysqli_error($link) . "<br/>" . $sql);

        $array = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $array = $row;
        }

        mysqli_close($link);
        return $array;
    }

    function isAssessmentComplete($cycle_id){
        $current_number_of_questions = 25;
        $response = self::GetLatestUserData($cycle_id);
        if(!isset($response) || !isset($response['formjson']) ){
            return false;
        }
        $formjson = isset($response['formjson']) ? $response['formjson'] : "{}";
        $formData = json_decode($formjson,true);
        $count = 0;
        if($formData !== null ){
            foreach($formData as $formelement => $formvalue){
                if(strpos($formelement, '_q') !== false){
                    $count++;
                }
            }
        }
        return $count == $current_number_of_questions ? true : false;
    }

    /**
     *
     */
    function GetEmptyRow() {
        $link = $this->createLinki();
        $dbColumns = array();

        $sql = "DESCRIBE `" . $this->table . "`";
        $result = mysqli_query($link, $sql) or die("SELECT: " . mysqli_error($link) . "<br/>" . $sql);
        while ($row = mysqli_fetch_assoc($result)) {
            if(!in_array($row['Field'], ['date_submitted'])){
                if(strpos($row['Field'], "json") !== false){
                    $dbColumns[$row['Field']] = "";
                }
                else{
                    $dbColumns[$row['Field']] = "0";
                }
            }
        }
        mysqli_close($link);
        return $dbColumns;
    }

}


?>

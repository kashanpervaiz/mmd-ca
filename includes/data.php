<?php
$commonPath = $GLOBALS['corePath'];

include_once $commonPath ."/includes/dbclasses/class_AccessKeys.php";
include_once $commonPath ."/includes/dbclasses/class_AccessRules.php";
include_once $commonPath ."/includes/dbclasses/class_Box.php";
include_once $commonPath ."/includes/dbclasses/class_DataRanges.php";
include_once $commonPath ."/includes/dbclasses/class_Destination.php";
include_once $commonPath ."/includes/dbclasses/class_EmailQueue.php";
include_once $commonPath ."/includes/dbclasses/class_KitDisplayData.php";
include_once $commonPath ."/includes/dbclasses/class_KitData.php";
include_once $commonPath ."/includes/dbclasses/class_KitResults.php";
include_once $commonPath ."/includes/dbclasses/class_KitVendorValueMap.php";
include_once $commonPath ."/includes/dbclasses/class_Languages.php";
include_once $commonPath ."/includes/dbclasses/class_UserLifestyle.php";
include_once $commonPath ."/includes/dbclasses/class_MedDelivery.php";
include_once $commonPath ."/includes/dbclasses/class_MedFrequency.php";
include_once $commonPath ."/includes/dbclasses/class_MedType.php";
include_once $commonPath ."/includes/dbclasses/class_MensesCycleType.php";
include_once $commonPath ."/includes/dbclasses/class_PageText.php";
include_once $commonPath ."/includes/dbclasses/class_NotificationTriggers.php";
include_once $commonPath ."/includes/dbclasses/class_Role.php";
include_once $commonPath ."/includes/dbclasses/class_User.php";
include_once $commonPath ."/includes/dbclasses/class_UserCycles.php";
include_once $commonPath ."/includes/dbclasses/class_UserSymptoms.php";
include_once $commonPath ."/includes/dbclasses/class_UserAssessments.php";
include_once $commonPath ."/includes/dbclasses/class_UserCustomRecommendations.php";
include_once $commonPath ."/includes/dbclasses/class_UserData.php";
include_once $commonPath ."/includes/dbclasses/class_UserMedications.php";
include_once $commonPath ."/includes/dbclasses/class_UserSmartLog.php";
include_once $commonPath ."/includes/dbclasses/class_UserSummary.php";
include_once $commonPath ."/includes/dbclasses/class_UserGynecology.php";
include_once $commonPath ."/includes/dbclasses/class_UserWeight.php";
include_once $commonPath ."/includes/dbclasses/class_ZRTReferenceCodes.php";
include_once $commonPath ."/includes/dbclasses/class_ChemRaw.php";
include_once $commonPath ."/includes/dbclasses/class_Ethnicities.php";
include_once $commonPath ."/includes/dbclasses/class_SiteMaintenanceMode.php";

include_once $commonPath ."/includes/dbclasses/class_RecommendationStepsPerTestKit.php";
include_once $commonPath ."/includes/dbclasses/class_RecommendationSteps.php";
include_once $commonPath ."/includes/dbclasses/class_RecommendationTopicsPerStep.php";
include_once $commonPath ."/includes/dbclasses/class_RecommendationTopics.php";
include_once $commonPath ."/includes/dbclasses/class_RecommendationActivitiesPerTopic.php";
include_once $commonPath ."/includes/dbclasses/class_RecommendationActivities.php";
include_once $commonPath ."/includes/dbclasses/class_RecommendationActivitiesPerNeurotransmitter.php";

include_once $commonPath ."/includes/dbclasses/class_KevinRanges.php";

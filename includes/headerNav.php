<?php

$capagetextarray = $pagetextObject->GetPageText('ca_index', $_SESSION['language']);

if (!isset($HeaderKitDataObj)) {
	$HeaderKitDataObj = new KitData();
	$headerReadyKitCount = (isset($_SESSION['userid']) && isValidUserId($_SESSION['userid']) ) ? $HeaderKitDataObj->getReadyCountForUserId($_SESSION['userid']) : 0;
}

if (!isset($HeaderUserCycles)) {
	$HeaderUserCycles = new UserCycles();
}

?>

<header>
	<div class="hdrCont flex">
		<div class="logo">
			<a href="<?php echo $GLOBALS['coreURL'] ?>">
				<img src="<?php echo $GLOBALS['assetsURL']; ?>/images/logos/logo.png" alt="MoodMD" title="MoodMD" />
			</a>
			<div class="txt">
				<span>|</span><?php echo $headertextarray['hdr-txt']; ?>
			</div>
		</div>
		<div class="nav">
			<?php if (isset($_SESSION['userid']) && isValidUserId($_SESSION['userid']) ) { ?>
				<a href="<?php echo $GLOBALS['coreURL']; ?>/signout.php"><?php echo ($headertextarray['signout']); ?></a></i>
				<button class="btn" onclick="window.location.href='<?php echo $GLOBALS['coreURL']; ?>/quickstart_results.php?cycle_id=<?php echo $_SESSION['current_cycle_id']; ?>'"><?php echo $capagetextarray['view-results-btn']; ?></button>
			<?php } else { ?>
				<a href="<?php echo $GLOBALS['coreURL']; ?>/signin.php?btn=signin" id="nav-btn-signin"><?php echo ($headertextarray['signin']); ?></a>
				<button class="btn" onclick="window.location.href='<?php echo $GLOBALS['coreURL']; ?>/quickstart.php'"><?php echo $capagetextarray['take-assessment-btn']; ?></button>
			<?php } ?>
		</div>
	</div>
</header>
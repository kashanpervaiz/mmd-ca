<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"])."/includes/phpHeader.php";
include_once $GLOBALS['corePath']."/lib/php-smtp-email-validation/smtp_validateEmail.class.php";

function verifyEmailMethodA($email)
{
		// an optional sender
		$sender = 'info@moodmd.com';
		// instantiate the class
		$SMTP_Validator = new SMTP_validateEmail();
		// turn on debugging if you want to view the SMTP transaction
		$SMTP_Validator->debug = false;
		// do the validation
		$results = $SMTP_Validator->validate(array($email), $sender);
		$validEmail	= $results[$email];
		return $validEmail ? true : false ;
}

function verifyEmailMethodB($email)
{

		$apiKey = "live_600b29cb4d2556e02c71e2872ba34d4d4f334bc8d1b9185677d3bf54bed8fa78";
		$emailAPIUrl = "https://api.kickbox.io/v2/verify?email=".$email."&apikey=".$apiKey;

		// Get cURL resource
		$curl = curl_init();
		// Set some options - we are passing in a useragent too here
		curl_setopt_array($curl, array(
		    CURLOPT_RETURNTRANSFER => 1,
		    CURLOPT_URL => $emailAPIUrl
		));
		// Send the request & save response to $resp
		$resp = curl_exec($curl);
		$data = json_decode($resp,true);
		// Close request to clear up some resources
		curl_close($curl);

		return $data['result'] == 'deliverable' ? true : false;
}



?>
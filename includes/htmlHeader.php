<!doctype html>
<html lang="en">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5G6Z3M2');</script>
    <!-- End Google Tag Manager -->

    <title>MoodMD</title>
	<!-- ISO-8859-1 	Latin alphabet part 1 	North America, Western Europe, Latin America, the Caribbean, Canada, Africa -->
	<!-- ISO-8859-2 	Latin alphabet part 2 	Eastern Europe  -->
	<!-- ISO-8859-3 	Latin alphabet part 3 	SE Europe, Esperanto, miscellaneous others -->
	<!-- ISO-8859-4 	Latin alphabet part 4 	Scandinavia/Baltics (and others not in ISO-8859-1) -->
	<!-- ISO-8859-5 	Latin/Cyrillic part 5 	The languages that are using a Cyrillic alphabet such as Bulgarian, Belarusian, Russian and Macedonian  -->
<?php
$characterSet = "ISO-8859-1";//default
$ISO_8859_5Array = array('ru','bg','cu','be','mk');
if(!isset($_SESSION['language'])){
    $_SESSION['language'] = 'en';
}
if(in_array($_SESSION['language'],$ISO_8859_5Array))
{
	$characterSet = "ISO-8859-5";
}
?>
	<!-- 	<meta charset="<?php echo $characterSet; ?>"> -->
    <meta name="robots" content="noindex, nofollow">
	<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $characterSet; ?>" />
	<meta http-equiv="cache-control" content="max-age=0" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Expires" content="-1">

	<?php if($_SESSION['language'] == "ru") {
	echo "	<meta http-equiv='Content-language' content='ru' />\r\n".PHP_EOL;
	echo "	<meta name='language' content='ru' />\r\n".PHP_EOL;
	}?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta http-equiv="Expires" content="-1"/>

    <meta name="description" content="Dedicated to improving mood, increasing energy & motivation, and optimizing physical & mental performance" />
	<meta name="keywords" content="wellbeing,wellness,physical wellness, mental wellness, spiritual wellness, environmental wellness, social wellness, financial wellness, occupational wellness" />

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo $GLOBALS['assetsURL']; ?>/images/favicons/favicon-57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo $GLOBALS['assetsURL']; ?>/images/favicons/favicon-60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $GLOBALS['assetsURL']; ?>/images/favicons/favicon-72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $GLOBALS['assetsURL']; ?>/images/favicons/favicon-76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $GLOBALS['assetsURL']; ?>/images/favicons/favicon-114.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $GLOBALS['assetsURL']; ?>/images/favicons/favicon-152.png">
    <link rel="icon" type="image/png" href="<?php echo $GLOBALS['assetsURL']; ?>/images/favicons/favicon-16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="<?php echo $GLOBALS['assetsURL']; ?>/images/favicons/favicon-32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?php echo $GLOBALS['assetsURL']; ?>/images/favicons/favicon-96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="<?php echo $GLOBALS['assetsURL']; ?>/images/favicons/favicon-160.png" sizes="160x160">
    <link rel="icon" type="image/png" href="<?php echo $GLOBALS['assetsURL']; ?>/images/favicons/favicon-196.png" sizes="196x196">
    <link rel="shortcut icon" type="image/png" href="<?php echo $GLOBALS['assetsURL']; ?>/images/favicons/favicon.ico">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600;700&family=Work+Sans:wght@200;300;600&display=swap" rel="stylesheet">


    <script src="<?php echo $GLOBALS['coreURL']; ?>/lib/jquery-3.3.1.min.js"></script>
    <script src="<?php echo $GLOBALS['coreURL']; ?>/lib/modernizr-custom.js"></script>

	<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['coreURL']; ?>/lib/jquery-ui-1.12.1.custom/jquery-ui.css" />
	<script type="text/javascript" src="<?php echo $GLOBALS['coreURL']; ?>/lib/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>

	<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['coreURL']; ?>/lib/timepicker/jquery-ui-timepicker-addon.css" />
     <script type="text/javascript" src="<?php echo $GLOBALS['coreURL']; ?>/lib/timepicker/jquery-ui-timepicker-addon.js"></script>

	<script type="text/javascript" src="<?php echo $GLOBALS['coreURL']; ?>/lib/jquery.validate.min.js"></script>
    <script type="text/javascript" src="<?php echo $GLOBALS['coreURL']; ?>/lib/jstz-1.0.4.min.js"></script>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    
	<script type="text/javascript" src="<?php echo $GLOBALS['coreURL']; ?>/lib/moment.min.js"></script>

	<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['coreURL']; ?>/lib/slick-1.8.1/slick/slick.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['coreURL']; ?>/lib/slick-1.8.1/slick/slick-theme.css"/>
    <script type="text/javascript" src="<?php echo $GLOBALS['coreURL']; ?>/lib/slick-1.8.1/slick/slick.min.js"></script>

    <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['coreURL']; ?>/css/global.min.css" />
    <?php include_once $GLOBALS['corePath']."/js/global-js.php" ?>

	<script src='https://www.google.com/recaptcha/api.js'></script>

    <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['coreURL']; ?>/lib/timeline/jquery.timeline.min.css"/>
    <script type="text/javascript" src="<?php echo $GLOBALS['coreURL']; ?>/lib/timeline/jquery.timeline.min.js"></script>

</head>

<!--[if IE 7 ]>    <body class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <body class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <body class="ie9"> <![endif<]-->
<!--[if (gt IE 9)|!(IE)]><!--><body><!--<![endif]-->
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5G6Z3M2"
            height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

 <?php include_once $GLOBALS['corePath']."/includes/headerNav.php" ?>




#Docker

Download Docker from [https://www.docker.com/products/docker-desktop][here]

In the project folder, type: `docker-compose build` to build the initial image.
 
We will need 2 additional steps before launching our containers.

1. Create a persistent volume needed to store the database content with `docker volume create --name mmd-db` (this will prevent losing our precious /var/mysql/data when rebuilding containers)
2. Create a shared network between the containers with `docker network create web`
 
We can now finally launch the container by typing `docker-compose up -d`.

To stop the container, type `docker-compose down`

[here]: https://www.docker.com/products/docker-desktop

# /etc/hosts
Add the following entry to your `/etc/hosts` file:

```
127.0.0.1 loc.moodmd.com
```

#Sequel Pro (or similar MySQL client)

Use the following settings to connect to the database on your host:

```
host: 127.0.0.1
port: 8090
user: root
pass: root
```

The next step would be to get a SQL dump of stage/prod DB. After this step has been completed, we need to apply some changes to the downloaded file:

```
 user@local > cat stage_dump.sql | grep moodmd_user
/*!50003 CREATE */ /*!50017 DEFINER=`moodmd_user_stg`@`%` */ /*!50003 TRIGGER `users_before_insert` BEFORE INSERT ON `users` FOR EACH ROW BEGIN
/*!50003 CREATE */ /*!50017 DEFINER=`moodmd_user_stg`@`%` */ /*!50003 TRIGGER `users_before_update` BEFORE UPDATE ON `users` FOR EACH ROW BEGIN
/*!50003 CREATE*/ /*!50020 DEFINER=`moodmd_user_stg`@`%`*/ /*!50003 FUNCTION `GenRandString`(LENGTH SMALLINT(3)) RETURNS varchar(100) CHARSET utf8
```

As we can see above, there are some user defined triggers that runs with a different user. We need to replace `moodmd_user_stg` with `moodmd_user_loc` (since that's the username present in our MySQL container). We can do that by typing:

`sed -i -e 's/moodmd_user_stg/moodmd_user_loc/g' stage_dump.sql`

Now, you can proceed to import a SQL dump from stage server into the guest MySQL container

<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"])."/includes/phpHeader.php";

$pagetextarray = $pagetextObject->GetPageText('privacypolicy.php',$_SESSION['language']);

?>
<?php include_once $GLOBALS['corePath']."/includes/htmlHeader.php"; ?>

<section id="privacy-policy">
    <div class="inner-container">
        <h1><?php echo $pagetextarray['title']; ?></h1>
        <hr/>
        <div><?php echo $pagetextarray['ppText']; ?></div>
    </div>
</section>


<?php include_once $GLOBALS['corePath']."/includes/htmlFooter.php"; ?>
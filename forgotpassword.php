<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"])."/includes/phpHeader.php";
$pagetextarray = $pagetextObject->GetPageText('forgotpassword.php',$_SESSION['language']);
 
$msg = "";
if(isset($_POST['submit']))
{
		$user = new User();
		$newpass = $user->getNewPassword($_POST['email']);
		$userId = $user->GetUserIdFromEmail($_POST['email']);
		if(strlen($newpass) > 0)
		{
			$_SESSION['loggedIn'] = false;
			$_SESSION['userid'] = "";
			$_SESSION['userrole'] = "";
			$_SESSION['participantid'] = "";

            $to = $userId;
			
			$params = array("newpassword"=>$newpass,"returnurl"=>$GLOBALS['coreURL']."/signin.php");
	 		sendEMail($to,"MoodMD <info@moodmd.com>","forgot_password_ca",$params);
			echo("<script>alert('".$pagetextarray['newpasswordsent']."');</script>");
			echo("<script>location.href='signin.php'</script>");	//back to login					

		}
		else
		{
			$msg = $pagetextarray['emailnotfound'];
		}

}


?>
<?php include_once $GLOBALS['corePath']."/includes/htmlHeader.php"; ?>

<script>
$(document).ready(function(){
		$("input[name='email']").focus();

});
</script>

<section id="forgotPassword">
     <form action="" method="post" autocomplete="off">
          <table class="loginBox">
               <?php if(strlen($msg)>0){
                    echo "<tr><td align='center'>".$msg."</td></tr>";
               } ?>
               <tr><td align="center"><?php echo $pagetextarray['title'];  ?></td></tr>
               <tr><td align="center"><input type="text" name="email"/></td></tr>
               <tr><td align="center"><button type="submit" value="Submit" name="submit"><?php echo $buttontextarray['SUBMIT']; ?></button></td></tr>
          </table>
     </form>
</section>


<?php include_once $GLOBALS['corePath']."/includes/htmlFooter.php"; ?>

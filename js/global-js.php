<script>

    $(function () {

        $('#header .hdrCont li a').on('click', function (e) {
            e.preventDefault();
            $('#headerNav .hdrCont li a.selected').removeClass('selected');
            $(this).addClass('selected');

        });

        $('#commonDialog').dialog({
            autoOpen: false,
            modal: true,
            width: Math.min(600, $(window).width() - 20),
            maxHeight: $(window).height() - 20,
            height: 'auto',
            resizable: false,
            dialogClass: 'dialog-common',
            open: function () {
                $(".ui-dialog-content").scrollTop(0);
            },
            close: function() {
                $('#commonDialog').empty();
                $(this).html('');
                $(this).dialog('close');
            },
            buttons: {}
        });

        $(document).on('click', '.video-container:not(.novideo)', function () {
            var thisvideo = $(this).find('.video-player').html();
            $('.video-modal').dialog({
            	autoOpen: true,
                modal: true,
                width: Math.min(900, $(window).width() - 20),
                height: 'auto',
                resizable: false,   
				my: "center",
				at: "center",
				of: window,        
                open: function( event, ui ) {  
                    $(this).html('');  
                    $(this).append(thisvideo);              
                    $(this).find('video').attr('controls', '');
                    $(this).find('video').trigger($(this).find('video').prop('paused') ? 'play' : 'pause'); 					
                },
                close:function(event, ui){                    
                    $(this).find('video')[0].pause();
                }
            });           
        });
        $(document).on('click', '.video-container.novideo .video-player a', function (e) {
            e.preventDefault();
            var thislink = $(this).attr('href');

            $('.video-modal').dialog({
                modal: true,
                width: Math.min(900, $(window).width() - 20),
                height: $(window).height() - 20,
                resizable: false,
                open: function( event, ui ) {
                    $(this).html('');
                    $(this).html('<iframe src="'+thislink+'" width="100%" height="100%"></iframe>');
                }
            });
        });

        $(document).on('click', '.btnOrderKit', function(e){
            e.preventDefault();
            window.location.href= 'https://www.moodmd.com/shop/test-kits/moodmd-neuro-hormonal-kit/';
        })

		$(document).on('click','.audioclip',function(e){
			var url = $(this).attr('href');	
			e.preventDefault();
			//e.stopPropagation();
			$('.video-modal').dialog({
            	autoOpen: true,
                modal: true,
                width: Math.min(300, $(window).width() - 20),
                height: 200,
                resizable: false,   
				my: "center",
				at: "center",
				of: window,        
                open: function( event, ui ) {  
					$(this).html('');  
                    $(this).append('<video controls="" autoplay="" name="media"><source src="'+url+'" type="audio/mpeg"></video>');                               					
				},
				close:function(event, ui){                    
                    $(this).find('video')[0].pause();
                }
            });         
			
		})
        $(document).on('click', '.btn-demo a', function (e) {
            e.preventDefault();
            var thisvideo = '<video width="600" height="auto" preload="preload"><source src="'+$(this).attr('href')+'" type="video/mp4" /></video>';
            $('.video-modal').dialog({
                autoOpen: true,
                modal: true,
                width: Math.min(900, $(window).width() - 20),
                maxHeight: $(window).height() - 20,
                height: 'auto',
                resizable: false,
                my: "center",
                at: "center",
                of: window,
                open: function( event, ui ) {
                    $(this).html('');
                    $(this).append(thisvideo);
                    $(this).find('video').attr('controls', '');
                    $(this).find('video').trigger($(this).find('video').prop('paused') ? 'play' : 'pause');
                },
                close:function(event, ui){
                    $(this).find('video')[0].pause();
                }
            });
        });
		
        // ------ language dropdown -----

      <?php /*
             if( $('.langSelected').length){
                  var langSel = $('.langSelected').closest('a').html();

                  $('#lang.lang-dropdown .selected a').html(langSel);
             }

             $("#lang.lang-dropdown .selected").on('click',function(e) {
                  e.preventDefault();
                 $("#lang.lang-dropdown .options ul").toggle();
             });

             $("#lang.lang-dropdown .options ul li a").on('click',function(e) {
                  e.preventDefault();
                 var text = $(this).html();
                 var langSelected = $(this).find('img').attr('lang');
                 $("#lang.lang-dropdown .selected a").html(text);
                 $("#lang.lang-dropdown .options ul").hide();
                 $.post("<?php echo $GLOBALS['coreURL']; ?>/services/setSessionVar.php", { sessionVarName: "language", sessionVarValue: langSelected }, function(data) {
					//alert("Data Loaded: " + data);
					//location.reload();
					window.location.reload(true);
			   	})
            });



     $(document).on('click', function(e) {
         var $clicked = $(e.target);
         if (! $clicked.parents().hasClass("lang-dropdown"))
             $("#lang.lang-dropdown .options ul").hide();
     });
    */ ?>
        //-------- login in ---------------

       $('.btnSignin').on('click', function () {

            $('#frmSigninSignupUpdate').attr('action', '<?php //echo $GLOBALS['coreURL']; ?>/signin.php').submit();

        });

        $('.btnSignout').on('click', function () {
            window.localStorage.setItem('wb_logged_in', 'false');
            $('#frmSigninSignupUpdate').attr('action', '<?php //echo $GLOBALS['coreURL']; ?>/signout.php').submit();

        })

        $('.btnSignup').on('click', function () {

          $('#frmSigninSignupUpdate').attr('action', '<?php echo $GLOBALS['coreURL']; ?>/registration.php').submit();

        });

        $('#mobile-menu .fa-bars').on('click', function () {
            $('#mobile-menu .menu').show('fast', function () {
                $('.menu-close').on('click', function () {
                    $(this).closest('.menu').hide();
                })
            });


        })


        //-------------show and hide passwords-------------

        $('[type="password"]').each(function () {
            var $this = $(this);
            // $this.after('<span class="showhidePW" title="Show Password"><i class="fas fa-eye"></i></span>');

        });
        $('.showhidePW').on('click', function () {

            var $showHide = $(this);

            if ($showHide.attr('title') == "Show Password") {
                $showHide.prev('[type="password"]').attr('type', 'text');
                $showHide.attr('title', 'Hide Password');
            }
            else {
                $showHide.prev('[type="text"]').attr('type', 'password');
                $showHide.attr('title', 'Show Password');
            }

        });


        //-------------datepicker ----------------


        $(document).on('click focus', '.datepicker, .datepicker-img, .datepicker-past, .datepicker-y', function (e) {
            var thisDate;
            var dateFormat;
            var changeMonth;
            var minDate = "";
            var maxDate = "";

            if ($(this).hasClass('datepicker-past')) {
                thisDate = '1975-01-01';
            }
            else {
                thisDate = '';
            }
            if($(this).hasClass('datepicker-y')){
                dateFormat = 'yy';
                changeMonth = false;
                $('.ui-datepicker-calendar, .ui-datepicker-month, .ui-datepicker-prev, .ui-datepicker-next').hide();
            }
            else {
                dateFormat = 'yy-mm-dd';
                changeMonth = true;
            }
            if($(this).hasClass('datepicker-img')){
                $(this).prev('input').trigger('focus');
            }

            if($(this).hasClass('daterange')) {
                minDate = $(this).data('mindate');
                maxDate = $(this).data('maxdate');
            }

            $(this).datepicker({
                minDate: minDate,
                maxDate: maxDate,
                dateFormat: dateFormat,
                changeYear: true,
                changeMonth: changeMonth,
                showMonthAfterYear: true,
                defaultDate: thisDate,
                yearRange: '1920:<?php echo intval(date('Y')); ?>',
                // onSelect: function () {
                //     $(this).datepicker('option', 'dateFormat', 'MM d, yy');
                // }

            });

        });


        //update user profile


        $('button[name="update_user"]').on('click', function () {
            var thisDatepicker = $(this).closest('form').find('.datepicker');

            thisDatepicker.datepicker("option", "dateFormat", "yy-mm-dd");

            $(this).closest('form').submit();

        });


        //-------------timepicker -------------

        $(document).on('focus', '.timepicker', function () {
            $(this).timepicker();
        });

        //====================================
        //for post and get requests
        $.ajaxPrefilter(function( options, originalOptions, jqXHR ) {
            if ( !options.beforeSend) {
                options.beforeSend = function (xhr) {
                    xhr.setRequestHeader('Authorization', "Bearer <?php echo $_SESSION['token'] ?? ''; ?>");
                }
            }
        });

    });

    $(window).on('load', function () {
        $('select').selectmenu({
               icons: { button: "arrow arrow-down" }
        });

        $("#accordion").accordion({
            heightStyle: "content",
            collapsible: true,
            active: false
        });
        $(".sub-accordion").accordion({
            heightStyle: "content",
            collapsible: true,
            active: false,
            icons: { "header": "ui-icon-plus", "activeHeader": "ui-icon-minus" }
        });
		
	
    })


    function get_time_zone_offset() {
        var current_date = new Date();
        var tzoHours = -current_date.getTimezoneOffset() / 60;
        var tzoMinutes = (current_date.getTimezoneOffset() % 60) * 60;
        var tzoMinutes = parseInt(tzoMinutes) > 0 ? tzoMinutes : "00";
        var tzo = tzoHours + ":" + tzoMinutes;
        return tzoHours >= 0 ? "+" + tzo : tzo;

    }

    function in_array(needle, haystack) {
        for(var i in haystack) {
            if(haystack[i] == needle) return true;
        }
        return false;
    }

    //-----------------------------


</script>

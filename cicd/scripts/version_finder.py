import boto3
import sys
import os

def find_latest_image_version(ecr_client, account_id, repo_name, environment):
  latest_version = 0
  all_images = []
  response = ecr_client.list_images(
      registryId=account_id,
      repositoryName=repo_name,
      filter={
          'tagStatus': 'TAGGED'
      }
  )

  all_images = all_images + response['imageIds']

  while 'nextToken' in response:
    response = ecr_client.list_images(
        registryId=account_id,
        repositoryName=repo_name,
        nextToken=response['nextToken'],
        filter={
            'tagStatus': 'TAGGED'
        }
    )
    all_images = all_images + response['imageIds']

  for image in all_images:
    if environment in image['imageTag']:
      tag = (image['imageTag']).split('.')
      version = int(tag[-1])
      if version > latest_version:
        latest_version = version

  return latest_version

region = os.environ['AWS_REGION']
account_id = os.environ['ACCOUNT_ID']
repo_name = os.environ['REPO_NAME']

which_result = sys.argv[1]
which_type = sys.argv[2]
env = sys.argv[3]

if which_type == "deployment":

  SSM = boto3.client('ssm', region_name=region)

  ssm_parameter_name = '/application/ecs/' + repo_name + '/' + env + '/version'

  last_deployed_version_result = SSM.get_parameter(
      Name=ssm_parameter_name
  )

  last_deployed_version = int(last_deployed_version_result['Parameter']['Value'])

  if which_result == 'next':

    print(last_deployed_version + 1)

  else:

    print(last_deployed_version)

elif which_type == "image":

  ECR = boto3.client('ecr', region_name=region)

  latest_image_version = find_latest_image_version(
    ecr_client=ECR,
    account_id=account_id,
    repo_name=repo_name,
    environment=env
  )

  if which_result == 'next':

    print(latest_image_version + 1)

  else:

    print(latest_image_version)

else:
  sys.exit(100)

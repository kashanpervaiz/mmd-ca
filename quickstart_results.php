<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"]) . "/includes/phpHeader.php";
$qspagetextarray = $pagetextObject->GetPageText('ca_quickstart_results', $_SESSION['language']);
$indexpagetextarray = $pagetextObject->GetPageText('ca_index', $_SESSION['language']);

if (isset($_REQUEST['cycle_id'])) {
	$assessmentsObj = new UserAssessments();

	$assessmentData = $assessmentsObj->GetLatestUserData($_REQUEST['cycle_id']);

	if (!isset($assessmentData) || count($assessmentData) == 0) {
		echo "<script>location.href='" . $GLOBALS['coreURL'] . "/'</script>";
	}
	$assessmentData = $assessmentData;
	$stress_level = $assessmentsObj->getLevel($assessmentData['stress_score']);
	$anxiety_level = $assessmentsObj->getLevel($assessmentData['anxiety_score']);
	$depression_level = $assessmentsObj->getLevel($assessmentData['depression_score']);

	$stress_color = $assessmentsObj->getColor($assessmentData['stress_score']);
	$anxiety_color = $assessmentsObj->getColor($assessmentData['anxiety_score']);
	$depression_color = $assessmentsObj->getColor($assessmentData['depression_score']);
	$average_color = $assessmentsObj->getColor(($assessmentData['stress_score'] + $assessmentData['anxiety_score'] + $assessmentData['depression_score']) / 3);

	$frequency = array('never', 'rarely', 'sometimes', 'often', 'always');
	//$symptoms = !empty($assessmentData['symptomjson']) ? json_decode($assessmentData['symptomjson'],true): array("1"=>"","2"=>"","3"=>"");
} else {
	echo "<script>location.href='" . $GLOBALS['coreURL'] . "/'</script>";
}
?>

<?php include_once $GLOBALS['corePath'] . "/includes/htmlHeader.php" ?>
<?php include_once $GLOBALS['corePath'] . "/includes/includes.moreInfoPopUp.php"; ?>

<script>
	$(function() {

		$(document).on('click', '.testResults .testname', function(e) {
			e.preventDefault();
			var $this = $(this);

			var moreinfo = $this.data('more');
			var displayName = $this.data('displayname');
			var totalLevel = $this.data('totallevel').trim();
			if (totalLevel.indexOf(' ') != -1) {
				totalLevel = totalLevel.replace(' ', '-');
			}
			var totalMeasure = $this.data('totalmeasure');
			var levelColor = $this.closest('.testResults').find('.ui-slider').data('color').toLowerCase();

			var content = getSpecificPageText('moreinfo', moreinfo);

			$('#commonDialog').html(content);
			$('#commonDialog .your-results').addClass(levelColor);
			$('#commonDialog .total-level').html(totalLevel).addClass(levelColor);
			$('#commonDialog .total-measure').html(totalMeasure);

			//--------------------

			$('#commonDialog .' + totalLevel).show();
			$('#commonDialog').dialog('option', 'title', displayName.toUpperCase());
			$('#commonDialog').dialog('open');
		});


		let mood_scores = '<div class="testResults">';
		mood_scores += '<a href="#" class="testname" data-more="stress" data-displayname="STRESS" data-totallevel="<?php echo $stress_level; ?>" data-totalmeasure="<?php echo intval($assessmentData['stress_score'] / 3); ?>">STRESS</a>';
		mood_scores += '<div class="testresult"><div class="horizontalSlider gyr" data-testResult="<?php echo $assessmentData['stress_score']; ?>" data-sliderValue="<?php echo $assessmentData['stress_score']; ?>" data-color="<?php echo $stress_color; ?>" data-totallevel="<?php echo $stress_level; ?>"></div></div>';
		mood_scores += '</div>';

		mood_scores += '<div class="testResults">';
		mood_scores += '<a href="#" class="testname" data-more="anxiety" data-displayname="ANXIETY" data-totallevel="<?php echo $anxiety_level; ?>" data-totalmeasure="<?php echo intval($assessmentData['anxiety_score'] / 3); ?>">ANXIETY</a>';
		mood_scores += '<div class="testresult"><div class="horizontalSlider gyr" data-testResult="<?php echo $assessmentData['anxiety_score']; ?>" data-sliderValue="<?php echo $assessmentData['anxiety_score']; ?>" data-color="<?php echo $anxiety_color; ?>" data-totallevel="<?php echo $anxiety_level; ?>"></div></div>';
		mood_scores += '</div>';

		mood_scores += '<div class="testResults">';
		mood_scores += '<a href="#" class="testname" data-more="depression" data-displayname="DEPRESSION" data-totallevel="<?php echo $depression_level; ?>"data-totalmeasure="<?php echo intval($assessmentData['depression_score'] / 3); ?>" >DEPRESSION</a>';
		mood_scores += '<div class="testresult"><div class="horizontalSlider gyr" data-testResult="<?php echo $assessmentData['depression_score']; ?>" data-sliderValue="<?php echo $assessmentData['depression_score']; ?>" data-color="<?php echo $depression_color; ?>" data-totallevel="<?php echo $depression_level; ?>"></div></div>';
		mood_scores += '</div>';

		$('.mood-scores').html(mood_scores);

		var slider = 'horizontalSlider';
		$(".horizontalSlider.gyr").slider({
			orientation: 'horizontal',
			min: 0,
			max: 300,
			step: 1
		});

		$(".horizontalSlider").each(function() {
			var sliderVal = $(this).attr('data-sliderValue');
			var sliderLevel = $(this).attr('data-totallevel');
			$(this).slider("option", "value", sliderVal);

			var sliderRes = $(this).attr("data-testResult");

			var handle = $(this).find(".ui-slider-handle").html('<span>' + sliderRes + '</span>');

			if ($(this).closest('.mood-scores').length) {
				handle = $(this).find(".ui-slider-handle").html('<span>' + sliderLevel + '</span>');
			}

		});


	});
</script>
<div id="quickstart-results">
	<div class="qr">
		<div class="inner-container">
			<div class="flex">
				<div><img src="<?php echo $GLOBALS['assetsURL']; ?>/images/results/quickstart_results.png" /></div>
				<div class="txt">
					<h2>YOUR RESULTS</h2>
					<div class="mood-scores flex"></div>

					<h4><?php echo $qspagetextarray['results-say']; ?></h4>
					<p><?php echo $qspagetextarray['results-indicate']; ?><br />
						<span class="avg">
							<?php if ($average_color == 'yellow' || $average_color == 'green') {
								echo $qspagetextarray['avg-results-yg'];
							} else {
								echo $qspagetextarray['avg-results-r'];
							} ?>
						</span>
					</p>
					<?php echo $qspagetextarray['results-explanation']; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="whatsNext">
		<div class="inner-container">
			<h4>What's Next?</h4>
			<div class="flex">
				<div>
					<?php echo $qspagetextarray['whats-next']; ?>

				</div>
				<div><img src="<?php echo $GLOBALS['assetsURL']; ?>/images/test-kit4.png" /></div>
			</div>
			<hr />
			<p><?php echo $qspagetextarray['contact-us']; ?></p>
			<p><?php echo $qspagetextarray['learn-more']; ?><?php echo $indexpagetextarray['demo-btn']; ?></p>
		</div>
	</div>

</div>
<div class="video-modal"></div>

<?php include_once $GLOBALS['corePath'] . "/includes/htmlFooter.php"; ?>
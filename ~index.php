<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"]) . "/includes/phpHeader.php";

////////////////////////////////////
//$_SESSION['loggedIn'] = false;
//$_SESSION['userid'] = "";
//$_SESSION['screenname'] = "";
//$_SESSION['firstname'] = "";
//$_SESSION['lastname'] = "";
//$_SESSION['birthday'] = "";
//$_SESSION['age'] = "";
//$_SESSION['userrole'] = "";
//$_SESSION['gender'] = "";
//$_SESSION['preferredUnits'] = "us";
//$_SESSION['mensesstatus_id'] = "";
//$_SESSION['mensesfirstday'] = "";
//$_SESSION['currDate'] = "";
//
//unset($_SESSION['loggedIn']);
//unset($_SESSION['userid']);
//unset($_SESSION['screenname']);
//unset($_SESSION['firstname']);
//unset($_SESSION['lastname']);
//unset($_SESSION['birthday']);
//unset($_SESSION['age']);
//unset($_SESSION['userrole']);
//unset($_SESSION['gender']);
//unset($_SESSION['preferredUnits']);
//unset($_SESSION['mensesstatus_id']);
//unset($_SESSION['mensesfirstday']);
//unset($_SESSION['currDate']);
///////////////////////////////////

$capagetextarray = $pagetextObject->GetPageText('ca_index', $_SESSION['language']);

?>
<?php include_once $GLOBALS['corePath'] . "/includes/htmlHeader.php"; ?>
<script>
	$(function() {

		$('.slideshow-container').slick({
			infinite: true,
			slidesToShow: 3,
			slidesToScroll: 1,
			dots: false,
			autoplay: true,
			autoplaySpeed: -1,
			arrows: false,
			easing: 'linear',
			speed: 10000,
			pauseOnHover: true,
			responsive: [{
					breakpoint: 1200,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 720,
					settings: {
						slidesToShow: 1,
					}
				}
			]
		});

	});
</script>
<section id="index">
	<div class="billboard">
		<img src="<?php echo $GLOBALS['assetsURL']; ?>/images/hp/billboard.jpg" />
		<div class="txt">
			<h2>Do You Experience...</h2>

			<div class="set set1">
				<span>Frequent illnesses</span>
				<span>Irritability</span>
				<span>Fatigue</span>
				<span>Difficulty concentrating</span>
				<span>Sleep problems</span>
				<span>Digestive problems</span>
				<span>Diminished libido</span>
				<span>Nervousness</span>
				<span>Addictive behaviors</span>
			</div>
			<div class="set set2">
				<span>Fear of future events</span>
				<span>Panic attacks</span>
				<span>Shortness of breath</span>
				<span>Chest tightness</span>
				<span>Nausea</span>
				<span>Unrealistic fears</span>
				<span>Profuse sweating</span>
				<span>Trembling-shaking</span>
			</div>
			<div class="set set3">
				<span>Hopelessness</span>
				<span>Loss of interest</span>
				<span>Absent libido</span>
				<span>Insomnia</span>
				<span>Emptiness</span>
				<span>Suicidal thoughts</span>
				<span>Lethargy</span>
				<span>Overeating</span>
			</div>

		</div>
	</div>
	<div class="take-assessment">
		<div class="inner-container">
			<div class="flex">
				<div class="txt">
					<p><?php echo $capagetextarray['take-assessment-txt']; ?></p>
				</div>
				<div>
					<?php if (!isset($_SESSION['loggedIn'])) { ?>
						<button class="btn" onclick="window.location.href='<?php echo $GLOBALS['coreURL']; ?>/quickstart.php'"><?php echo $capagetextarray['take-assessment-btn']; ?></button>
					<?php } else { ?>
						<button class="btn" onclick="window.location.href='<?php echo $GLOBALS['coreURL']; ?>/quickstart_results.php?cycle_id=<?php echo $_SESSION['current_cycle_id']; ?>'"><?php echo $capagetextarray['view-results-btn']; ?></button>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
	<div class="website-demo">
		<div class="inner-container">
			<div class="flex">
				<div class="txt">
					<p><?php echo $capagetextarray['demo-txt']; ?></p>
				</div>
				<div class="demo-btn"><?php echo $capagetextarray['demo-btn']; ?></div>
			</div>
		</div>
	</div>
	<div class="sad">
		<div class="inner-container">
			<?php echo $capagetextarray['sad-hdr']; ?>
			<?php echo $capagetextarray['sad-txt']; ?>
		</div>
		<div class="last"><img src="<?php echo $GLOBALS['assetsURL']; ?>/images/hp/sad.png" /></div>
	</div>
	<div class="slideshow-container">
		<div class="slide slide-mental-health">
			<img src="<?php echo $GLOBALS['assetsURL']; ?>/images/hp/slide-mental-health.jpg" />
			<div class="text">
				<?php echo $capagetextarray['slide-mental-health']; ?>
			</div>
		</div>
		<div class="slide slide-suicide">
			<img src="<?php echo $GLOBALS['assetsURL']; ?>/images/hp/slide-suicide.jpg" />
			<div class="text">
				<?php echo $capagetextarray['slide-suicide']; ?>
			</div>
		</div>
		<div class="slide slide-stress">
			<img src="<?php echo $GLOBALS['assetsURL']; ?>/images/hp/slide-stress.jpg" />
			<div class="text">
				<?php echo $capagetextarray['slide-stress']; ?>
			</div>
		</div>
		<div class="slide slide-depression">
			<img src="<?php echo $GLOBALS['assetsURL']; ?>/images/hp/slide-depression.jpg" />
			<div class="text">
				<?php echo $capagetextarray['slide-depression']; ?>
			</div>
		</div>
	</div>
	<div class="main-forces">
		<div class="inner-container">
			<?php echo $capagetextarray['main-forces-hdr']; ?>
			<div class="flex">
				<div class="flex">
					<?php echo $capagetextarray['main-forces-brain']; ?>
				</div>

				<div class="flex">
					<?php echo $capagetextarray['main-forces-mind']; ?>
				</div>
			</div>
		</div>
	</div>

	<div class="cycle">
		<div class="inner-container">
			<div class="flex">
				<div>
					<?php echo $capagetextarray['cycle-hdr']; ?>
					<?php echo $capagetextarray['cycle-txt']; ?>
				</div>
				<div><img src="<?php echo $GLOBALS['assetsURL']; ?>/images/hp/cycle.png"" /></div>
            </div>
        </div>
    </div>

    <div class=" current-mood">
					<div class="inner-container">
						<div class="flex">
							<div><img src="<?php echo $GLOBALS['assetsURL']; ?>/images/hp/face.png"" /></div>
                <div class=" txt">
								<?php echo $capagetextarray['current-mood-hdr']; ?>
								<?php echo $capagetextarray['current-mood-txt']; ?>
							</div>

						</div>
					</div>
				</div>
				<div class="divider"></div>
				<div class="intro">
					<div class="inner-container">
						<img src="<?php echo $GLOBALS['assetsURL']; ?>/images/logos/logo.png" />
						<hr />
						<?php echo $capagetextarray['intro-hdr']; ?>
						<hr />
						<?php echo $capagetextarray['intro-txt']; ?>
					</div>
				</div>

				<div class="mood-profile">
					<div class="inner-container">
						<?php echo $capagetextarray['mood-profile-hdr']; ?>
						<?php echo $capagetextarray['mood-profile-subhdr']; ?>
						<div class="flex">
							<div class="txt">
								<?php echo $capagetextarray['mood-profile-txt1']; ?>
							</div>
							<div><img src="<?php echo $GLOBALS['assetsURL']; ?>/images/hp/mood-profile.png" /></div>
							<div class="txt">
								<?php echo $capagetextarray['mood-profile-txt2']; ?>
							</div>
						</div>
					</div>
				</div>
				<div class="personalized-plan">
					<div class="inner-container">
						<?php echo $capagetextarray['personalized-plan-hdr']; ?>
						<div class="flex">
							<div><img src="<?php echo $GLOBALS['assetsURL']; ?>/images/hp/experts.png" /></div>
							<div class="txt">
								<?php echo $capagetextarray['personalized-plan-txt']; ?>
							</div>
							<div class="last"><img src="<?php echo $GLOBALS['assetsURL']; ?>/images/hp/ai.png" /></div>
						</div>

					</div>
				</div>

				<div class="small-moves">
					<div class="inner-container">
						<?php echo $capagetextarray['small-moves-hdr']; ?>
						<?php echo $capagetextarray['small-moves-subhdr']; ?>
						<div class="flex">
							<div>
								<div><img src="<?php echo $GLOBALS['assetsURL']; ?>/images/hp/smallmove-lifestyle.png" /></div>
								<?php echo $capagetextarray['small-moves-lifestyle-txt']; ?>
							</div>
							<div>
								<div><img src="<?php echo $GLOBALS['assetsURL']; ?>/images/hp/smallmove-supps.png" /></div>
								<?php echo $capagetextarray['small-moves-supps-txt']; ?>
							</div>
							<div>
								<div><img src="<?php echo $GLOBALS['assetsURL']; ?>/images/hp/smallmove-mind.png" /></div>
								<?php echo $capagetextarray['small-moves-mind-txt']; ?>
							</div>
						</div>
						<h2>with access to<br /><span>advanced support</span></h2>
						<div class="flex">
							<div>
								<div><img src="<?php echo $GLOBALS['assetsURL']; ?>/images/hp/smallmove-support.png" /></div>
								<?php echo $capagetextarray['small-moves-support-txt']; ?>
							</div>
							<div>
								<div><img src="<?php echo $GLOBALS['assetsURL']; ?>/images/hp/smallmove-meds.png" /></div>
								<?php echo $capagetextarray['small-moves-meds-txt']; ?>
							</div>
						</div>
					</div>
				</div>
				<div class="program">
					<div class="inner-container">
						<?php echo $capagetextarray['program-hdr']; ?>
						<?php echo $capagetextarray['program-txt']; ?>
					</div>
				</div>

				<div class="mini-assessment">
					<div class="inner-container">
						<?php echo $capagetextarray['mini-assessment-hdr']; ?>
						<div class="flex">
							<div><img src="<?php echo $GLOBALS['assetsURL']; ?>/images/hp/mini-assessment.png" /></div>
							<div class="txt">
								<?php echo $capagetextarray['mini-assessment-txt']; ?>
								<div>
									<?php if (!isset($_SESSION['loggedIn'])) { ?>
										<button class="btn" onclick="window.location.href='<?php echo $GLOBALS['coreURL']; ?>/quickstart.php'">Take the Assessment</button>
									<?php } else { ?>
										<button class="btn" onclick="window.location.href='<?php echo $GLOBALS['coreURL']; ?>/quickstart_results.php?cycle_id=<?php echo $_SESSION['current_cycle_id']; ?>'">View my Results</button>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>

</section>
<div class="video-modal"></div>
<?php include_once $GLOBALS['corePath'] . "/includes/htmlFooter.php"; ?>
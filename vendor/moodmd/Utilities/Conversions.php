<?php
namespace Moodmd\Utilities;


class Conversions
{
    public static function convert($measure, $value,$preferredUnits,$precision = 0)
	{

        $convertions['lbToKg'] = floatval(1/2.20462);
        $convertions['KgTolb'] = 2.20462;
        $convertions['inTocm'] = 2.54;
        $convertions['cmToin'] = floatval(1/2.54);
        $convertions['glucose_SItoUS'] = 18;
        $convertions['glucose_UStoSI'] = floatval(1/18);
        $convertions['cholesterol_SItoUS'] = 38.6;
        $convertions['cholesterol_UStoSI'] = floatval(1/38.6);
        $convertions['triglyceride_SItoUS'] = 	88.5;
        $convertions['triglyceride_UStoSI'] = 	floatval(1/88.5);

		if( $preferredUnits   == 'si') //we store metrics in the db
        { return $value;}
        else
        {
            $factor = $convertions[$measure];
            //return  (round(10*$value*$factor))/10;
            return  round($value*$factor,$precision,PHP_ROUND_HALF_UP); //in standard measures we don't have tenths
        }
	}    

}
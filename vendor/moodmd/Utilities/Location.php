<?php
namespace Moodmd\Utilities;

class Location
{
    public static function GetLocationData($remote_ip,$force = false){
        $dir = $_SERVER["DOCUMENT_ROOT"].'maxmind';
        $file_name = $dir.'/GeoLite2-City/GeoLite2-City.mmdb';

        if(!is_dir($dir)){
            shell_exec("mkdir ".$dir);
            shell_exec("chmod -R 777 " . $dir);
        }

        $filetime = $oneweekago = strtotime("now");
        if (file_exists($file_name)) {
            $filetime = filemtime($file_name);
            $oneweekago = strtotime("-1 week");
        }
        if(!file_exists($file_name) || $oneweekago >= $filetime || $force !== false){

            // configuration
            $client = new \tronovav\GeoIP2Update\Client(array(
                'license_key' => '7tJkLL3ASm8UnokX',
                'dir' => $dir,
                'editions' => array('GeoLite2-ASN', 'GeoLite2-City', 'GeoLite2-Country'),
            ));// run update
            $client->run();// After updating, you can get information about the result:
            //print_r($client->updated());// update result
            //print_r($client->errors());// update errors
        }
        shell_exec("chmod -R 777 ".$dir);

        if(preg_match("/^172.*/i",$remote_ip)){
            $remote_ip = "66.183.240.59"; //76.90.170.43 190.225.139.184 216.71.216.65
        }
        $location_data = [
            "country_code"=>"",
            "state_code"=>"",
            "postal_code"=>"",
            "country_name"=>"",
            "state_name"=>"",
            "city_name"=>"",
            "lattitude"=>"",
            "longitude"=>"",
            "ipaddress"=>""
        ];
        if(!preg_match("/^172.*/i",$remote_ip)){// This creates the Reader object, which should be reused across
            // lookups.
            $reader = new \GeoIp2\Database\Reader($file_name);// Replace "city" with the appropriate method for your database, e.g.,
            // "city".
            $record = $reader->city($remote_ip);
            $location_data = [
                "country_code"=>$record->country->isoCode,
                "state_code"=>$record->mostSpecificSubdivision->isoCode,
                "postal_code"=>$record->postal->code,
                "country_name"=>$record->country->name,
                "state_name"=>$record->mostSpecificSubdivision->name,
                "city_name"=>$record->city->name,
                "lattitude"=>$record->location->latitude,
                "longitude"=>$record->location->longitude,
                "ipaddress"=>$record->traits->ipAddress
            ];
//            print($record->country->isoCode . "</br>");
//            print($record->country->name . "</br>");
//            print($record->mostSpecificSubdivision->name . "</br>");
//            print($record->mostSpecificSubdivision->isoCode . "</br>");
//            print($record->city->name . "</br>");
//            print($record->postal->code . "</br>");
//            print($record->location->latitude . "</br>");
//            print($record->location->longitude . "</br>");
//            print($record->traits->ipAddress . "</br>" );
        }else{
            $location_data['ipaddress'] = $remote_ip;
//            echo "Internal ip: ".$remote_ip."</br>";
        }
        return $location_data;
    }
}
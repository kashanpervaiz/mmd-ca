<?php

namespace Moodmd\Utilities;
/**
 * Class Encryption
 * @package Moodmd\Utilities
 *
 * Example Useage
 * $fixed_key = "fe1896ebd6b2c4ca6353f87c4d712ba47537fb4248b3a1e12f54b8c57180823b";
 * $cipher = Moodmd\Utilities\Encryption::encrypt(json_encode(["Brian"=>"Vanyo"]),$fixed_key);
 * echo "<pre>";print_r($cipher);echo "</pre>";
 * $decipher = Moodmd\Utilities\Encryption::dencrypt($cipher,$fixed_key);
 * echo "<pre>";print_r($decipher);echo "</pre>";
 *
 */

class Encryption
{
    public static function encrypt($data,$key){
        //$cipher="AES-128-CBC"; //aes128 AES-128-CBC
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext_raw = openssl_encrypt($data, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
        $ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );
        return $ciphertext;
    }

    public static function decrypt($data, $key){
        $c = base64_decode($data);
        //$cipher="AES-128-CBC";
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = substr($c, 0, $ivlen);
        $hmac = substr($c, $ivlen, $sha2len=32);
        $ciphertext_raw = substr($c, $ivlen+$sha2len);
        $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
        $calcmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
        $deciphertext = "";
        if (@hash_equals($hmac, $calcmac))
        {
            $deciphertext =  $original_plaintext;
        } else {
            $deciphertext =  "Unable to decypher the message";
        }
        return $deciphertext;
    }

    public static function generateKey($length){
        $strength_is_good = false;
        $key = "";
        while(!$strength_is_good){
            $key = openssl_random_pseudo_bytes($length,$strength_is_good);
        }
        return bin2hex($key);
    }
}

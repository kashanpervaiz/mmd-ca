<?php

namespace Moodmd\Session;

use \Firebase\JWT\JWT;

class Token
{

    const KEY = '7f4df41a1c7826aa2d27b83b20888883ea1b5777bee76d6d512b9821b8e251d8';

    /**
     * Get header Authorization
     * */
    public static function getAuthorizationHeader()
    {
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        } else {
            if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
                $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
            } elseif (function_exists('apache_request_headers')) {
                $requestHeaders = apache_request_headers();
                // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
                $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)),
                    array_values($requestHeaders));
                //print_r($requestHeaders);
                if (isset($requestHeaders['Authorization'])) {
                    $headers = trim($requestHeaders['Authorization']);
                }
            }
        }
        return $headers;
    }

    /**
     * get access token from header
     * */
    public static function getBearerToken()
    {
        $headers = self::getAuthorizationHeader();
        // HEADER: Get the access token from the header
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }

    public static function encode($opts = [])
    {
        JWT::$leeway = 60; // $leeway in seconds
        return JWT::encode(array_merge(self::_payload(), $opts), self::KEY);
    }

    public static function decode($jwt)
    {
        JWT::$leeway = 60; // $leeway in seconds
        return JWT::decode($jwt, self::KEY, array('HS256'));
    }

    private static function _payload()
    {
        return [
            // "iss" => "http://loc.moodmd.com", # issuer
            // "aud" => "http://loc.moodmd.com", # audience
            // "iat" => time(), # issued at
            // "nbf" => 1357000000, # not before
        ];
    }

}
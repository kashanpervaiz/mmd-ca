<?php

namespace Moodmd\Session;

class Base
{

    public static function isLoggedIn()
    {
        if (!array_key_exists('loggedIn', $_SESSION)) {
            return false;
        }
        if ($_SESSION['loggedIn'] !== true) {
            return false;
        }
        if (!array_key_exists('userid', $_SESSION)) {
            return false;
        }
        
        # Get token from request headers, fallback to session
        $token = Token::getBearerToken() ?? $_SESSION['token'];
        if (empty($token)) {
            return false;
        }

        $userDataObj = new \UserData();
        $userTmp = $userDataObj->GetUserData($_SESSION['userid']);
        if (empty($userTmp)) {
            return false;
        }

        if ($token != Token::encode([
                'user_id' => $_SESSION['userid'],
                'expiration' => $userTmp['token_expiration']
            ])) {
            closeSession();
            return false;
        }

        return true;
    }

    public static function forceLoginRedirect()
    {
        return "<script>location.href='" . $GLOBALS['coreURL'] . "/signin.php'</script>";
    }
}
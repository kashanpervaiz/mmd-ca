<?php


namespace Moodmd\Routing;


class Base
{

    protected static $_ignoreList = [
        'user/confirm',
        'messaging/send_dashboard_sms',
        'pagetext/getpagetext',
        'assessments/get',
        'healthcheck',
        'userdata/getallaffiliations',
        'crons'
    ];

    /**
     * @return string[]
     */
    public static function getIgnoreList(): array
    {
        return self::$_ignoreList;
    }

    /**
     * @param $path
     *
     * @return bool
     */
    public static function requiresAuth(string $path): bool
    {
        return !in_array($path, self::getIgnoreList());
    }
}
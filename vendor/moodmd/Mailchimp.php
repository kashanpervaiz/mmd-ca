<?php

namespace Moodmd;

/**
 * Class Crm
 *
 * Wrapper class around DrewM's Mailchimp library to submit CRM data
 *
 * @package Moodmd
 */
class Mailchimp
{

    protected $_mailchimp = null;

    public function __construct($key = null)
    {
        $this->_mailchimp = new \DrewM\MailChimp\MailChimp($key);
    }

    /**
     * Check the status of a batch
     *
     * @param $batch_id
     * @return false|string
     */
    public function getBatchStatus($batch_id)
    {
        $batch = $this->_mailchimp->new_batch($batch_id);
        $result = $batch->check_status();
        $output = json_encode($result);
        if (array_key_exists('response_body_url', $result)) {
            $output .= "\nDownload results: " . $result['response_body_url'];
        }
        return $output;
    }

    /**
     * @return \DrewM\MailChimp\Batch
     */
    public function newBatch()
    {
        return $this->_mailchimp->new_batch();
    }

    /**
     * Builds a basic data structure to push into Mailchimp CRM
     *
     * @param $data
     * @return array
     */
    public function getMergeFields($data)
    {
        return [
            'email_address' => $data['email'],
            'status_if_new' => 'subscribed',
            'merge_fields' => [
                'FNAME' => $data['firstname'],
                'LNAME' => $data['lastname'],
                'PHONE' => $data['mobile_number']
            ]
        ];
    }

    /**
     * Get hashed contact id (used to uniquely identify contacts)
     *
     * @param $email
     * @return string
     */
    public function getContactHash($email)
    {
        return $this->_mailchimp->subscriberHash($email);
    }

    /**
     * @return array|false
     */
    public function getLastError()
    {
        return $this->_mailchimp->getLastError();
    }
}

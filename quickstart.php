<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"])."/includes/phpHeader.php";

$assessmentQuestions = $pagetextObject->GetPageText('assessment_questions2',$_SESSION['language']);
$formfieldsarray = $pagetextObject->GetPageText('formfields',$_SESSION['language']);

$assessmentsObj = new UserAssessments();
$assessmentData = $assessmentsObj->GetEmptyRow();


$frequency = array('never','rarely','sometimes','often','always');
$symptoms = !empty($assessmentData['symptomjson'])  ? json_decode($assessmentData['symptomjson'],true): array("1"=>"","2"=>"","3"=>"") ;

$countrycode =  $_SESSION['countrycode'] ?? '';
$postalcode =  $_SESSION['postalcode'] ?? '';
$preferredUnits =  $_SESSION['preferredUnits'] ?? '';

?>

<?php include_once $GLOBALS['corePath']."/includes/htmlHeader.php" ?>
<script src="https://www.google.com/recaptcha/api.js?render=<?php echo $_SESSION['recaptcha_v3']['site_key']; ?>"></script>
<script>
    grecaptcha.ready(function () {
        setCaptchaToken();
    });

    function setCaptchaToken(){
        //console.log(new Date());
        grecaptcha.execute('<?php echo $_SESSION['recaptcha_v3']['site_key']; ?>', { action: 'contact' }).then(function (token) {
            var recaptchaResponse = document.getElementById('recaptchaResponse');
            recaptchaResponse.value = token;
        });
    }

	$(function(){

        setInterval(function () { setCaptchaToken(); }, 2 * 60 * 1000);

		$('#quickstart #frm-assessment #finish').attr('id','quickstart-finish');


        $(document).keyup(function(e) {
            var code = (e.keyCode ? e.keyCode : e.which);

            if(e.shiftKey && e.altKey && code == '70') {
                $(document).keyup(function(e) {
                    var code = (e.keyCode ? e.keyCode : e.which);
                    if(e.shiftKey && e.altKey && code == '70') {
                        var groups = {};
                        $('#frm-assessment #assessment_questions input:radio').each(function() {
                            groups[this.name] = true;
                        });
                        $.each(groups, function(name, bool) {
                            var radios = $('#frm-assessment #assessment_questions input:radio[name=' + name + ']');
                            if(radios.length > 0) {
                                var rd = Math.floor(Math.random() * radios.length);

                                // Remove first and 2nd option
                                rd = ((rd + 2) < radios.length) ? rd + 2 : rd;
                                radios[rd].checked = true;
                            }
                        });

                        $('#frm-assessment #quickstart-finish').removeClass('disabled');

                    }
                })
            }

        })
		
		$(document).on('click','#quickstart-finish',function(e){
            e.preventDefault;
			$('#quickstart-createAcct').dialog({
				modal: true,				
				width: Math.min(800, $(window).width() - 20),
				height: 'auto',
				resizable: false,
				open:function(){
                    $(document).keyup(function(e) {
                        var code = (e.keyCode ? e.keyCode : e.which);

                        if(e.shiftKey && e.altKey && code == '70') {
                            $('#frm-createAcct input#firstname').val('User');
                            $('#frm-createAcct input#lastname').val('Demo');
                            $('#frm-createAcct input#email').val('testuser'+Math.floor(Math.random() * 1000)+'@moodmd.com');
                            $('#frm-createAcct input#postalcode').val('V6K 1W2');
                            $('#frm-createAcct input#password, input#confpassword').val('pswd');
                        }
                    })
					$(this).find('.btnCreatAcct').on('click',function(){

						if ($('#frm-createAcct').valid()) {						
							var thisform = $('#frm-createAcct').serialize();
							
							$.when(confirmUser(thisform)).done(function(data){
								//console.log(data)
								var response = JSON.parse(data)
								//console.log('response ',response.data);
								//console.log('response error ',response.error)
								if(!response.data){
                                    setCaptchaToken();
									alert(response.error);
									return false;
								} else {
									//console.log(response.data);
									var parentform = $('#quickstart #frm-assessment').serialize() + '&cycle_id=' + response.data;
									//console.log(parentform)
									$.when(addAssessment(parentform)).done(function(assData){
										//console.log(assData)
										var res = JSON.parse(assData);
										if(!res.id){
											alert('There has been an error. Please contact MoodMD.');
											return false;
										} else {
											window.location.href = '<?php echo $GLOBALS['coreURL']; ?>/quickstart_results.php?cycle_id=' + response.data;
										}
									})
								}
							})
							
						}

						return false;
					})
				}
			})
			return false;
		});



		//validate popup form
		$('#frm-createAcct').validate({
			rules: {
                firstname: "required",
                lastname: "required",
                email: {
                    required: true,
                    email: true
                },
                postalcode: "required",
                password: "required",
                confpassword: {
                    equalTo: "#password"
                },
                affiliation: {required: true, minlength: 2},
            },
			messages: { //these are overrides
				confpassword: "<?php echo $formfieldsarray['passwordsdontmatch']; ?>",
               // invitation_code: "Invalid Invitation Code"
			},
			errorPlacement: function(error, element) {
				error.appendTo(element.closest('fieldset'));
			}
		});

		//==========================
		function confirmUser(newUserData) {			
			return $.post("<?php echo $GLOBALS['coreURL']; ?>/services/user/confirm", newUserData);
		}
		function addAssessment(frm) {			
			return $.post("<?php echo $GLOBALS['coreURL']; ?>/services/assessments/set", frm);
		}

        function getAllUserAffiliations(){
            
            return $.post("<?php echo $GLOBALS['coreURL']; ?>/services/userdata/getallaffiliations");

        }

		//==========================

        $.when(getAllUserAffiliations()).done(function(data){
            var res = JSON.parse(data);
            var availableTags = res.data;

            $('#affiliation').autocomplete({
                appendTo: '.suggestList',
                source: availableTags
            });
        });
	});

</script>
<section id="quickstart">
    <div class="inner-container">
        <h4>Complete this <strong>simple questionnaire</strong> to understand your current levels of chronic stress, anxiety, and depression.</h4>

        <?php
        $_SESSION['current_assessment_id'] = 0;
        $_SESSION['userid'] = 0;
        include_once $GLOBALS['corePath']."/includes/includes.assessment.php";
        ?>
	</div>	
</section>
<div id="quickstart-createAcct">
	<h4>PLEASE CREATE AN ACCOUNT</h4>
	<p>Complete the form below to review your results. Any information you provide will be kept confidential.</p>
	<form name="frm-createAcct" id="frm-createAcct" method="post" action="">
        <div>*Required information</div>
		<div class="flex">
			<fieldset>
				<input type="text" name="firstname" id="firstname" placeholder="First Name*"/>
			</fieldset>
            <fieldset>
                <input type="text" name="lastname" id="lastname" placeholder="Last Name*"/>
            </fieldset>
		</div>
		<div class="flex">
            <fieldset>
                <input type="text" name="email" id="email" placeholder="Email*"/>
            </fieldset>
            <fieldset>
                <input type="text" name="postalcode" id="postalcode" placeholder="postal code*" value="<?php echo $postalcode; ?>"/>
            </fieldset>
		</div>
        <div class="flex">
            <fieldset>
                <input type="text" name="affiliation" id="affiliation" placeholder="Company*"/>
                <div class="suggestList"></div>
                <div class="note">Enter 'none' if this does not apply to you.</div>
            </fieldset>
        </div>
        <div class="flex">
            <fieldset>
                <input type="password" name="password" id="password" placeholder="Password*"/>
            </fieldset>
            <fieldset>
                <input type="password" name="confpassword" id="confpassword" placeholder="Re-Enter Password*"/>
            </fieldset>
        </div>
		<div>
			<fieldset>
                <button type="button" class="btnCreatAcct" name="add">Create Account</button>
                <input type="hidden" name="emailVerified" value="1" />
                <input type="hidden" name="countrycode" value="<?php echo $countrycode;?>" />
                <input type="hidden" name="lead_source" value="3" />
            </fieldset>
            <input type="hidden" name="recaptcha_response" id="recaptchaResponse">
		</div>
        <p class="note">Your contact information will not be sold or distributed to any third party at any time. For more details, view our <a href="<?php echo $GLOBALS['coreURL']; ?>/privacypolicy.php" target="_blank">Privacy Policy</a>.</p>
	</form>
</div>

<?php include_once $GLOBALS['corePath']."/includes/htmlFooter.php"; ?>

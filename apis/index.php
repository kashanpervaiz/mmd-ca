<?php
include_once realpath($_SERVER["DOCUMENT_ROOT"]) . "/includes/phpHeader.php";
$base_route = "apis";
$uri = $_SERVER['REQUEST_URI'];

$route = explode("/",$uri);
//print_r($route);  [0] => [1] => services [2] => brian [3] => test [4] => 1

if(isset($route[1]) && $route[1] != $base_route){
    http_response_code(404);
    echo json_encode(array("error"=>"Endpoint not found"));
    die();
}else{
    $uri = str_ireplace("/services","",$uri);
}

//case 200: $text = 'OK'; break;
//case 400: $text = 'Bad Request'; break;
//case 401: $text = 'Unauthorized'; break;
//case 403: $text = 'Forbidden'; break;
//case 404: $text = 'Not Found'; break;
//case 405: $text = 'Method Not Allowed'; break;

/*
//Route exmaple(s):
// url:  /services/routes/user/confirm  => /$route[0]/$route[1]/$route[2]/$route[3]

$ignore_list  = [
 'user/confirm',
 'messaging/send_dashboard_sms'
];*/
if (\Moodmd\Routing\Base::requiresAuth(implode('/', array_slice($route, 2, 2)))
    && !\Moodmd\Session\Base::isLabAuthorized()) {
    http_response_code(401);
    closeSession();
    echo json_encode(array("error" => "Unauthorized"));
    die();
}

/*
test : in_array ($route[2].'/'.$route[3] ,  $ignore_list)

*/

if(isset($route[2]) && file_exists(realpath($_SERVER["DOCUMENT_ROOT"]) . "/".$base_route."/routes/".$route[2].".php")){
    require __DIR__ . '/routes/'.$route[2].'.php';
}else{
    http_response_code(404);
    echo json_encode(array("error"=>"Endpoint not found"));
    die();
}

